<?php


$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/console/config/params.php')
);

return [
    'id' => 'app-console',
    'basePath' => ROOT_PATH . '/console',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'Console\Controllers',
    'controllerMap' => [
        'fixture'  => [
            'class'     => \yii\console\controllers\FixtureController::class,
            'namespace' => 'Phycom\Base\Fixtures',
        ],
        'migrate'  => [
            'class'               => \yii\console\controllers\MigrateController::class,
            'migrationPath'       => null,
            'templateFile'        => '@root/vendor/phycom/base/src-console/Migrations/template.php',
            'migrationNamespaces' => [
                'Phycom\Console\Migrations'
            ],
        ],
        'help'     => \Phycom\Console\Controllers\HelpController::class,
        'email'    => \Phycom\Base\Modules\Email\Controllers\ConsoleController::class,
        'sms'      => \Phycom\Base\Modules\Sms\Controllers\ConsoleController::class,
        'delivery' => \Phycom\Base\Modules\Delivery\Controllers\ConsoleController::class
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'fileStorage' => [
            'storages' => [
                'local' => [
                    'baseUrl' => '@phycom/base/file/download',
                ]
            ]
        ],
    ],
    'params' => $params,
];
