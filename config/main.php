<?php

use Phycom\Base\Components\Commerce\Order;
use Phycom\Base\Components\Commerce\Invoice;
use Phycom\Base\Components\Commerce\Shipment;
use Phycom\Base\Components\Commerce\ClientCard;
use Phycom\Base\Components\Commerce\Param;
use Phycom\Base\Components\Commerce\PromoCode;
use Phycom\Base\Components\Commerce\Variant;
use Phycom\Base\Components\Commerce\Shop;
use Phycom\Base\Components\Commerce\ShopScheduleOpen;
use Phycom\Base\Components\Commerce\ShopScheduleClosed;
use Phycom\Base\Components\Commerce\ShopSupply;
use Phycom\Base\Components\Commerce\ShopContent;
use Phycom\Base\Components\Commerce\ProductImporter;

use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Country;

return [
    'vendorPath' => ROOT_PATH . '/vendor',
    'aliases' => [
        '@bower'          => '@vendor/bower-asset',
        '@npm'            => '@vendor/npm-asset'
    ],
	'modules' => [
		'email' => [
			'class' => \Phycom\Base\Modules\Email\Module::class,
			'modules' => [
                \Phycom\Base\Modules\Email\Module::PROVIDER_MAILER => [
                    'mailer' => [
                        'class' => \yii\swiftmailer\Mailer::class,
                        'useFileTransport' => false,
                        'transport' => [
                            'class'      => Swift_SmtpTransport::class,
                            'encryption' => 'tls',
                            'host'       => getenv(ENV . '_SMTP_HOST'),
                            'port'       => getenv(ENV . '_SMTP_PORT'),
                            'username'   => getenv(ENV . '_SMTP_USER'),
                            'password'   => getenv(ENV . '_SMTP_PASS'),
                        ],
                    ]
                ]
			],
			'defaultProvider' => \Phycom\Base\Modules\Email\Module::PROVIDER_MAILER,
			'testMode' => false,
		],
        'sms' => [
            'class' => \Phycom\Base\Modules\Sms\Module::class,
            'modules' => [],
            'testMode' => false,
        ],
		'delivery' => [
			'class' => \Phycom\Base\Modules\Delivery\Module::class,
			'modules' => []
		],
		'payment' => [
			'class' => \Phycom\Base\Modules\Payment\Module::class,
            'modules' => [
                \Phycom\Base\Modules\Payment\Methods\Cash\Module::ID    => [
                    'class' => \Phycom\Base\Modules\Payment\Methods\Cash\Module::class,
                ],
                \Phycom\Base\Modules\Payment\Methods\Invoice\Module::ID => \Phycom\Base\Modules\Payment\Methods\Invoice\Module::class,
            ],
			'defaultMethod' => \Phycom\Base\Modules\Payment\Methods\Invoice\Module::ID
		]
	],
    'components' => [
	    'redis' => [
		    'class' => \yii\redis\Connection::class,
		    'hostname' => getenv(ENV . '_REDIS_HOST'),
		    'port' => getenv(ENV . '_REDIS_PORT'),
		    'database' => 0,
	    ],
	    'session' => [
		    'class' => \yii\redis\Session::class,
		    'redis' => [
			    'hostname' => getenv(ENV . '_REDIS_HOST'),
			    'port' => getenv(ENV . '_REDIS_PORT'),
			    'database' => 1,
		    ],
	    ],
	    'country' => [
	    	'class' => \Phycom\Base\Components\Country::class,
		    'countries' => [],
		    'preferredCountries' => []
	    ],
        'locale' => \yii\i18n\Locale::class,
        'cache' => [
	        'class' => yii\redis\Cache::class,
	        'keyPrefix' => APP . '_ecommerce',
        ],
	    'modelFactory' => [
	        'class' => \Phycom\Base\Components\ModelFactory::class
        ],
	    'db' => [
		    'class' => \yii\db\Connection::class,
		    'dsn' => 'pgsql:host=' . getenv(ENV . '_DB_HOST') . ';port=' . getenv(ENV . '_DB_PORT') . ';dbname=' . getenv(ENV . '_DB_NAME'), // PostgreSQL
		    'username' => getenv(ENV . '_DB_USER'),
		    'password' => getenv(ENV . '_DB_PASS'),
		    'charset' => 'utf8',
		    'enableQueryCache' => true,
		    'enableSchemaCache' => true,
		    'on afterOpen' => function ($e) {
			    $e->sender->createCommand('SET TIMEZONE TO \'' . Yii::$app->timeZone . '\'')->execute();
		    }
	    ],
        'authManager' => [
            'class'           => \Phycom\Base\Components\AuthManager::class,
            'defaultRoles'    => ['user', 'guest'],
            'rootRoles'       => ['system'],
            'sessionCache'    => true,
            'itemTable'       => 'auth_item',
            'ruleTable'       => 'auth_rule',
            'itemChildTable'  => 'auth_item_hierarchy',
            'assignmentTable' => 'user_auth_item'
        ],
        'formatter' => [
            'class'          => \Phycom\Base\Components\Formatter::class,
            'locale'         => 'et-EE',
            'currencyCode'   => 'EUR',
            'timeFormat'     => 'short',
            'dateFormat'     => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'jsDateFormat'   => 'yyyy-mm-dd',
            'addressFields'  => [
                'street' => function(AddressField $f) {
                    $street = $f->street;
                    if ($street && $f->house) {
                        $street .= ' ' . $f->house;
                        if ($f->room) {
                            $street .= '-' . $f->room;
                        }
                    }
                    return $street;
                },
                'province' => fn(AddressField $f) => !$f->city ? $f->province : null,
                'country' => function (AddressField $f) {
                    if ($f->country) {
                        $country = Country::findOne(['code' => strtoupper($f->country)]);
                        $f->country !== (string)Yii::$app->country
                            ? $country->info->getNativeName()
                            : null;
                    }
                },
            ]
        ],
        'site' => [
            'class'    => \Phycom\Base\Components\Site::class,
            'enabled'  => true,
            'logoPath' => '@root/public/img/brand/logo.png',
            'logoUrl'  => '/img/brand/logo.png'
        ],
        'admin' => [
            'class'   => \Phycom\Base\Components\Admin::class,
            'enabled' => true,
        ],
        'commerce' => [
            'class'      => \Phycom\Base\Components\Commerce\Commerce::class,
            'enabled'    => true,
            'components' => [
                'order' => [
                    'class'   => Order::class,
                    'enabled' => true
                ],
                'invoice' => [
                    'class'   => Invoice::class,
                    'enabled' => true
                ],
                'shipment' => [
                    'class'   => Shipment::class,
                    'enabled' => true
                ],
                'clientCards' => [
                    'class'   => ClientCard::class,
                    'enabled' => true
                ],
                'promoCodes'  => [
                    'class'   => PromoCode::class,
                    'enabled' => true
                ],
                'variants'    => [
                    'class'   => Variant::class,
                    'enabled' => true
                ],
                'params'      => [
                    'class'   => Param::class,
                    'enabled' => true
                ],
                'shop' => [
                    'class'   => Shop::class,
                    'enabled' => true
                ],
                'shopOpen'    => [
                    'class'   => ShopScheduleOpen::class,
                    'enabled' => true
                ],
                'shopClosed'  => [
                    'class'   => ShopScheduleClosed::class,
                    'enabled' => true
                ],
                'shopSupply'  => [
                    'class'   => ShopSupply::class,
                    'enabled' => true
                ],
                'shopContent' => [
                    'class'   => ShopContent::class,
                    'enabled' => true
                ],
                'productImporter' => [
                    'class'   => ProductImporter::class,
                    'enabled' => true
                ]
            ]
        ],
        'messages' => [
            'class'   => \Phycom\Base\Components\Message::class,
            'enabled' => false
        ],
        'blog' => [
            'class'   => \Phycom\Base\Components\Blog::class,
            'enabled' => false,
        ],
        'reviews' => [
            'class'   => \Phycom\Base\Components\Review::class,
            'enabled' => false,
        ],
        'comments' => [
            'class'   => \Phycom\Base\Components\Comment::class,
            'enabled' => false
        ],
        'subscription' => [
            'class'   => \Phycom\Base\Components\Subscription::class,
            'enabled' => false,
        ],
        'partnerContracts' => [
            'class'   => \Phycom\Base\Components\PartnerContract::class,
            'enabled' => false
        ],
        'geocoder' => [
            'class' => \Phycom\Base\Components\Geocoder::class,
            'enabled' => false
        ],
	    'urlManagerBackend' => [
            'class' => \yii\web\UrlManager::class,
        ],
        'urlManagerFrontend' => [
            'class' => \yii\web\UrlManager::class,
        ],
	    'view' => [
		    'class' => \yii\web\View::class,
		    'renderers' => [
			    'twig' => [
				    'class' => \yii\twig\ViewRenderer::class,
				    'cachePath' => false,//'@runtime/Twig/cache',
				    'options' => ['autoescape' => false],
                    'globals' => [
                        'app'        => 'Yii::$app',
                        'Html'       => '\yii\helpers\Html',
                        'ActiveForm' => '\yii\widgets\ActiveForm',
                    ],
                    'functions' => [
                        't'            => '\Phycom\Base\Models\MessageTemplate::translateMessage',
                        'conf'         => '\Phycom\Base\Helpers\c::param',
                        'url'          => '\Phycom\Base\Helpers\Url::feFull',
                        'route'        => '\Phycom\Base\Helpers\Url::toFeRoute',
                        'backendUrl'   => '\Phycom\Base\Helpers\Url::beFull',
                        'backendRoute' => '\Phycom\Base\Helpers\Url::toBeRoute',
                    ],
                    'filters'   => [
                        'a'            => '\yii\helpers\Html::a',
                        'url'          => '\Phycom\Base\Helpers\f::url',
                        'currency'     => '\Phycom\Base\Helpers\f::currency',
                        'address'      => '\Phycom\Base\Helpers\f::address',
                        'plainAddress' => '\Phycom\Base\Helpers\f::plainAddress',
                        'phone'        => '\Phycom\Base\Helpers\f::phone',
                        'date'         => '\Phycom\Base\Helpers\f::date',
                        'datetime'     => '\Phycom\Base\Helpers\f::datetime',
                        'units'        => '\Phycom\Base\Helpers\f::units'
                    ]
			    ]
		    ],
	    ],
	    'lang' => [
	        'class' => \Phycom\Base\Components\LanguageManager::class
        ],
	    'log' => [
		    'traceLevel' => YII_DEBUG ? 3 : 0,
		    'targets' => require(__DIR__ . '/logtarget.php')
	    ],
	    'i18n' => [
		    'class' => \Phycom\Base\Components\i18N::class,
		    'termBegin' => '',
		    'termClose' => '',
		    'translations' => [
			    '*' => [
				    'class' => \yii\i18n\GettextMessageSource::class,
				    'basePath' => ['@translations', '@phycom/base/translations'],
				    'sourceLanguage' => 'en',
				    'useMoFile' => false
			    ],
                'common*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@translations',
                    'catalog'  => 'common'
                ],
                'modules*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@translations',
                    'catalog'  => 'modules'
                ],
                'phycom/base*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@phycom/base/translations',
                    'catalog'  => 'main'
                ],
                'phycom/modules*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@phycom/base/translations',
                    'catalog'  => 'modules'
                ],
			    'email*' => [
				    'class' => \Phycom\Base\Components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/base/translations'],
				    'catalog' => 'templates'
			    ],
                'sms*' => [
                    'class' => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => ['@translations', '@phycom/base/translations'],
                    'catalog' => 'templates'
                ],
			    'message*' => [
				    'class' => \Phycom\Base\Components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/base/translations'],
				    'catalog' => 'templates'
			    ],
                'file*' => [
                    'class' => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => ['@translations', '@phycom/base/translations'],
                    'catalog' => 'templates'
                ],
			    'template*' => [
				    'class' => \Phycom\Base\Components\MessageSource::class,
				    'basePath' => ['@translations', '@phycom/base/translations'],
				    'catalog' => 'templates'
			    ]
		    ],
	    ],
	    'fileStorage' => [
		    'class' => \Phycom\Base\Components\FileStorage::class,
		    'storages' => [
			    'local' => [
				    'class' => \yii2tech\filestorage\local\Storage::class,
				    'basePath' => '@files',
				    'baseUrl' => '@web/file/download',
				    'filePermission' => 0775,
				    'buckets' => [
					    \Phycom\Base\Components\FileStorage::BUCKET_TEMP,
					    \Phycom\Base\Components\FileStorage::BUCKET_AVATARS,
					    \Phycom\Base\Components\FileStorage::BUCKET_PRODUCT_FILES,
                        \Phycom\Base\Components\FileStorage::BUCKET_CONTENT_FILES,
					    \Phycom\Base\Components\FileStorage::BUCKET_INVOICES,
					    \Phycom\Base\Components\FileStorage::BUCKET_ORDER,
                        \Phycom\Base\Components\FileStorage::BUCKET_POSTAGE_LABELS
				    ]
			    ]
		    ]
	    ],
	    // this is the high priority queue
	    'queue1' => [
		    'class' => \Phycom\Base\Components\Queue\Beanstalk::class,
            'host' => getenv(ENV . '_BEANSTALK_HOST'),
            'port' => getenv(ENV . '_BEANSTALK_PORT'),
		    'tube' => APP . '-high-priority',
            'ttr' => 5 * 60, // Max time for anything job handling
            'attempts' => 3, // Max number of attempts
	    ],
	    // this is for low priority jobs
	    'queue2' => [
		    'class' => \Phycom\Base\Components\Queue\Beanstalk::class,
            'host' => getenv(ENV . '_BEANSTALK_HOST'),
            'port' => getenv(ENV . '_BEANSTALK_PORT'),
		    'tube' => APP . '-default',
            'ttr' => 5 * 60, // Max time for anything job handling
            'attempts' => 3, // Max number of attempts
	    ],
        'landingPages' => [
            'class'    => \Phycom\Base\Components\PageSpaceCollection::class,
            'postType' => PostType::LAND,
            'items'    => []
        ],
        'pages' => [
            'class'    => \Phycom\Base\Components\PageSpaceCollection::class,
            'postType' => PostType::PAGE,
            'items'    => []
        ],
	    'bootstrap' => \Phycom\Base\Components\Bootstrap::class
    ],
	'bootstrap' => [
		'bootstrap',
		'queue1', // high priority queue registers own console commands
		'queue2', // low priority queue registers own console commands
	],
];
