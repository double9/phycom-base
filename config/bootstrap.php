<?php

Yii::setAlias('@root', ROOT_PATH);

Yii::setAlias('@logs', ROOT_PATH . '/logs');
Yii::setAlias('@files', ROOT_PATH . '/files');

//Yii::setAlias('@common', ROOT_PATH . '/common/src');
//Yii::setAlias('@Common', ROOT_PATH . '@common');

//Yii::setAlias('@frontend', ROOT_PATH . '/frontend/src');
//Yii::setAlias('@Frontend', '@frontend');

//Yii::setAlias('@backend', ROOT_PATH . '/backend/src');
//Yii::setAlias('@Backend', '@backend');

Yii::setAlias('@console', ROOT_PATH . '/console/src');
Yii::setAlias('@Console', '@console');

Yii::setAlias('@translations', ROOT_PATH . '/translations');
Yii::setAlias('@templates', ROOT_PATH . '/templates');

Yii::setAlias('@phycom', PHYCOM_PATH);
Yii::setAlias('@Phycom', '@phycom');
Yii::setAlias('@Phycom/Base', '@phycom/base/src-base');
Yii::setAlias('@Phycom/Console', '@phycom/base/src-console');

Yii::setAlias('@bower', ROOT_PATH . '/vendor/bower');
Yii::setAlias('@npm', ROOT_PATH . '/vendor/npm');

