<?php
return [
    'error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'except' => ['yii\web\HttpException:404', 'yii\web\HttpException:400', 'invalid-request'],
        'logFile' => '@logs/error.log'
    ],
    '400' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['invalid-request', 'yii\web\HttpException:400'],
        'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE'],
        'logFile' => '@logs/400.log'
    ],
    '404' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['yii\web\HttpException:404'],
        'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
        'logFile' => '@logs/404.log'
    ],
    '405' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['yii\web\HttpException:405'],
        'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
        'logFile' => '@logs/405.log'
    ],
    'main' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'except' => [
            'yii\*',
            'email', 'Phycom\Base\Modules\Email\*',
            'payment', 'Phycom\Base\Modules\Payment\*',
            'delivery', 'Phycom\Base\Modules\Delivery\*',
            'transaction'
        ],
        'logFile' => '@logs/main.log',
        'maxLogFiles' => 30,
        'maxFileSize' => 20480
    ],
    'db' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'categories' => ['yii\db\*'],
        'logFile' => '@logs/db.log',
        'maxLogFiles' => 3,
        'maxFileSize' => 20480
    ],
    'yii' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info'],
        'logVars' => [],
        'categories' => ['yii\*'],
        'except' => ['yii\db\*'],
        'logFile' => '@logs/yii.log',
        'maxLogFiles' => 2,
    ],
    'trace' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['trace'],
        'logVars' => [],
        'logFile' => '@logs/trace.log',
        'maxLogFiles' => 2,
        'maxFileSize' => 20480
    ],
    'redis' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['yii\redis\*'],
        'logFile' => '@logs/redis/error.log'
    ],
    'order' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning', 'info', 'trace'],
        'categories' => [
            'order',
            'Phycom\Base\Models\Order*',
            'Phycom\Frontend\Models\Order*',
            'Common\Models\Order*',
            'invoice',
            'yii2tech\filestorage\local\Bucket(invoice)',
            'Phycom\Base\Models\Invoice*',
            'Common\Models\Invoice*',
        ],
        'logFile' => '@logs/order/order.log'
    ],
    'order-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => [
            'order',
            'Phycom\Base\Models\Order*',
            'Phycom\Frontend\Models\Order*',
            'Common\Models\Order*',
            'invoice',
            'yii2tech\filestorage\local\Bucket(invoice)',
            'Phycom\Base\Models\Invoice*',
            'Common\Models\Invoice*',
        ],
        'logFile' => '@logs/order/error.log'
    ],
    'queue' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error', 'warning', 'info', 'trace'],
        'exportInterval' => 1,
        'categories' => ['yii\queue\Queue*', 'Phycom\Base\Jobs\*'],
        'logFile' => '@logs/queue.log',
        'logVars' => [],
    ],
    'invoice-job' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error', 'warning', 'info', 'trace'],
        'exportInterval' => 1,
        'categories' => ['invoice-job', 'Phycom\Base\Jobs\Invoice*'],
        'logFile' => '@logs/order/invoice-job.log'
    ],
    'product-import' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'categories' => [
            'product-import',
            'Phycom\Base\Components\Commerce\ProductImporter',
            'Phycom\Base\Models\Product\Import*',
            'Common\Models\Product\Import*',
        ],
        'logFile' => '@logs/product-import.log',
        'logVars' => [],
    ],
    'email' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['email', 'Phycom\Base\Modules\Email\*'],
        'logFile' => '@logs/message/email.log',
        'logVars' => [],
    ],
    'sms' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['sms', 'Phycom\Base\Modules\Sms\*'],
        'logFile' => '@logs/message/sms.log',
        'logVars' => [],
    ],
    'alert' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['alert'],
        'logFile' => '@logs/message/alert.log',
        'logVars' => [],
    ],
    'message' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['message', 'Phycom\Base\Models\Message*'],
        'logFile' => '@logs/message/message.log',
        'logVars' => [],
    ],
    'message-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['message', 'Phycom\Base\Models\Message*', 'email', 'Phycom\Base\Modules\Email\*', 'alert', 'sms', 'Phycom\Base\Modules\Sms\*'],
        'logFile' => '@logs/message/error.log',
        'logVars' => []
    ],
    'payment' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['payment', 'Phycom\Base\Modules\Payment\*'],
        'logFile' => '@logs/payment/payment.log',
        'logVars' => [],
    ],
    'payment-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['payment', 'Phycom\Base\Modules\Payment\*'],
        'logFile' => '@logs/payment/error.log',
        'logVars' => []
    ],
    'delivery' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['warning','info','trace'],
        'categories' => ['delivery', 'Phycom\Base\Modules\Delivery\*', 'delivery-area'],
        'logFile' => '@logs/delivery/delivery.log',
        'logVars' => ['_REQUEST'],
    ],
    'delivery-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['delivery', 'Phycom\Base\Modules\Delivery\*', 'delivery-area'],
        'logFile' => '@logs/delivery/error.log',
        'logVars' => []
    ],
    'auth' => [
        'class' => 'yii\log\FileTarget',
        'exportInterval' => 1,
        'levels' => ['warning', 'info', 'trace'],
        'categories' => ['auth', 'Phycom\Auth\*'],
        'logFile' => '@logs/auth/auth.log',
        'logVars' => [],
    ],
    'auth-error' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error'],
        'categories' => ['auth', 'Phycom\Auth\*'],
        'logFile' => '@logs/auth/error.log',
        'logVars' => []
    ],
    'file-storage' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error', 'warning', 'info', 'trace'],
        'categories' => ['yii2tech\filestorage\*'],
        'logFile' => '@logs/filestorage.log',
        'logVars' => []
    ],
    'transaction' => [
        'class' => 'yii\log\FileTarget',
        'levels' => ['error', 'warning', 'info', 'trace'],
        'categories' => ['transaction'],
        'logFile' => '@logs/transaction/transaction.log',
        'logVars' => []
    ]
];
