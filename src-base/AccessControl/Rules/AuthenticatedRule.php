<?php

namespace Phycom\Base\AccessControl\Rules;

use yii\rbac\Rule;

/**
 * Class AuthenticatedRule
 * @package Phycom\Base\AccessControl\Rules
 */
class AuthenticatedRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return !\Yii::$app->user->isGuest;
    }
}
