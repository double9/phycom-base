<?php

namespace Phycom\Base\AccessControl\Rules;


use yii\rbac\Rule;
use Yii;

/**
 * Class IsSelfRule
 * @package Phycom\Base\AccessControl\Rules
 */
class IsSelfRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (!$user || Yii::$app->user->isGuest) {
            return false;
        }
        return $user === (int) $params;
    }
}
