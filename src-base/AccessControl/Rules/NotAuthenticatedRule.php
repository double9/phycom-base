<?php

namespace Phycom\Base\AccessControl\Rules;

use yii\rbac\Rule;

/**
 * Class NotAuthenticatedRule
 * @package Phycom\Base\AccessControl\Rules
 */
class NotAuthenticatedRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        return \Yii::$app->user->isGuest;
    }
}
