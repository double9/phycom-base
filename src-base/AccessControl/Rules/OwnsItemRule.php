<?php

namespace Phycom\Base\AccessControl\Rules;

use Phycom\Base\Interfaces\ModelOwnerInterface;

use yii\base\InvalidArgumentException;
use yii\rbac\Rule;
use Yii;

/**
 * Class OwnsItemRule
 * @package Phycom\Base\AccessControl\Rules
 */
class OwnsItemRule extends Rule
{
    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
	    if (!isset($params['id'])) {
		    throw new InvalidArgumentException('Parameter "id" is required');
	    }
	    if (!isset($params['model'])) {
		    throw new InvalidArgumentException('Parameter "model" is required');
	    }
        $id = $params['id'];
        $model = $params['model'];

        if (!$user || Yii::$app->user->isGuest) {
            return false;
        }
	    /**
	     * @var \Phycom\Base\Models\ActiveRecord $model
	     * @var $record ModelOwnerInterface
	     */
        $record = $model::findOne($id);

        if (!$record instanceof ModelOwnerInterface) {
        	throw new InvalidArgumentException('Model must implement ' . ModelOwnerInterface::class);
        }
        return $record->isOwner($user);
    }
}
