<?php

namespace Phycom\Base\Interfaces;


/**
 * Interface StorageInterface
 * @package Phycom\Base\Interfaces
 */
interface CartStorageInterface
{
    /**
     * @param CartInterface $cart
     *
     * @return array|mixed
     */
    public function load(CartInterface $cart);

    /**
     * @param CartInterface $cart
     */
    public function save(CartInterface $cart);
}
