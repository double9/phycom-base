<?php

namespace Phycom\Base\Interfaces;

/**
 * Generic interface for any Phycom application component
 *
 * Interface PhycomComponentInterface
 *
 * @package Phycom\Base\Interfaces
 */
interface PhycomComponentInterface
{
    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string|null
     */
    public function getLabel(): string;

    /**
     * @return array
     */
    public function getJsonSchema(): array;
}
