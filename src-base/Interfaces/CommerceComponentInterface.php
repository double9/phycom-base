<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface CommerceComponentInterface
 *
 * @package Phycom\Base\Interfaces
 *
 * Interface for any Phycom application component that is a child of Commerce component
 */
interface CommerceComponentInterface extends PhycomComponentInterface
{

}
