<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface CartItemDeliveryInterface
 * @package Phycom\Base\Interfaces
 */
interface CartItemDeliveryInterface extends CartItemInterface
{
    /**
     * @return string - delivery item code
     */
    public function getCode();

    /**
     * @return string - delivery method name
     */
    public function getMethod();

    /**
     * @return string - carrier id
     */
    public function getCarrier();

    /**
     * @return string - carrier service token
     */
    public function getService();

    /**
     * @return int - Estimated transit time (duration) in days of the Parcel given by carrier
     */
    public function getDeliveryDays();

    /**
     * @return string - (HH:mm) time in local timezone when shipment arrives to the destination
     */
    public function getArrivalTime();

    /**
     * @return string - (HH:mm) time in local timezone when shipment is actually delivered over to client
     */
    public function getDeliveryTime();

    /**
     * @return \DateTime - a date when the parcel is agreed to be delivered to client
     */
    public function getDeliveryDate();

    /**
     * @return string - Further clarification of the transit times. Often, this includes notes that the transit time as given in "days" is only an average, not a guaranteed time.
     */
    public function getDurationTerms();

    /**
     * @return \DateTime|null
     */
    public function getDispatchAt();

    /**
     * @return string - destination area code
     */
    public function getAreaCode();

    /**
     * @return string - shipment sender address in json format
     */
    public function getFromAddress();

    /**
     * @return string - recipient address in json format
     */
    public function getToAddress();

    /**
     * @return int - recipient user address id in address db table
     */
    public function getToAddressId();

    /**
     * @return string - shipment return address in json format
     */
    public function getReturnAddress();

    /**
     * @return string - unique shipment id in shipment provider system
     */
    public function getShipmentId();

    /**
     * @return string - unique rate id in shipment provider system
     */
    public function getRateId();
}
