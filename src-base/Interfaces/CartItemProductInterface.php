<?php

namespace Phycom\Base\Interfaces;

/**
 * Adds the quantity feature to base cart interface
 *
 * Interface CartItemProductInterface
 * @package Phycom\Base\Interfaces
 */
interface CartItemProductInterface extends CartItemInterface
{
    public function getCode();

    public function getQuantity();

    public function setQuantity(int $value);

    public function getNumUnits();

    public function getTotalPrice();

    public function getOptionValue($attribute);

    public function getOptions();

    /**
     * @return array
     */
    public function getPriceData(): array;

    /**
     * @return float
     */
    public function getWeight();

    /**
     * @return array [length, width, height]
     */
    public function getDimensions();
}
