<?php

namespace Phycom\Base\Interfaces;

/**
 * All objects that can be added to the cart must implement this interface
 *
 * @package Phycom\Base\Interfaces
 */
interface CartItemInterface
{
    /**
     * Returns the price for the cart item
     *
     * @return int
     */
    public function getPrice();

    /**
     * Returns the label for the cart item (displayed in cart etc)
     *
     * @return string
     */
    public function getLabel();

    /**
     * Returns unique id to associate cart item with product
     *
     * @return string
     */
    public function getUniqueId();
}
