<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface CartItemDiscountInterface
 * @package Phycom\Base\Interfaces
 */
interface CartItemDiscountInterface extends CartItemInterface
{
    /**
     * @return string
     */
    public function getCode() : string;

    /**
     * @return int|float
     */
    public function calculateDiscount();
}
