<?php

namespace Phycom\Base\Interfaces;

/**
 * Represents an area in map (area contains multiple street addresses)
 * In commerce application mostly used in conjunction with delivery subsystems
 *
 * Interface AreaInterface
 *
 * @package Phycom\Base\Interfaces
 */
interface AreaInterface
{
    /**
     * @return string - A name that identifies this area
     */
    public function getName() : string;

    /**
     * @return string - ISO country code (2 chars all uppercase)
     */
    public function getCountry() : string;

    /**
     * @return string|null - Human readable name of country
     */
    public function getCountryName() : ?string;

    /**
     * @return string|null - State name if exists
     */
    public function getState() : ?string;

    /**
     * @return string|null
     */
    public function getProvince() : ?string;

    /**
     * @return string|null
     */
    public function getCity() : ?string;

    /**
     * @return string|null
     */
    public function getLocality() : ?string;

    /**
     * @return string|null
     */
    public function getDistrict() : ?string;

    /**
     * @return string|null
     */
    public function getStreet() : ?string;
}
