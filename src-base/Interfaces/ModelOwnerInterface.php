<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface ModelOwnerInterface
 * @package Phycom\Base\Interfaces
 */
interface ModelOwnerInterface
{
	/**
	 * @param int $userId
	 * @returns bool
	 */
	public function isOwner($userId);
}
