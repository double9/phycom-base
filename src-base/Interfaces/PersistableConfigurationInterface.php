<?php

namespace Phycom\Base\Interfaces;

interface PersistableConfigurationInterface
{
    public function serialize(): string;

    public function unserialize(string $data): void;

    public function persistConfig(): bool;

    public function loadConfig(): bool;
}