<?php

namespace Phycom\Base\Interfaces;


use Phycom\Base\Models\ModuleSettingsForm;

/**
 * Interface ModuleSettingsInterface
 * @package Phycom\Base\Interfaces
 */
interface ModuleSettingsInterface
{
    /**
     * @return string|array|callable
     */
    public function getSettingsForm();

    /**
     * @return ModuleSettingsForm
     */
    public function getSettings();
}
