<?php

namespace Phycom\Base\Interfaces;

use yii\data\ActiveDataProvider;

interface SearchModelInterface
{
	/**
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search(array $params = []);
}
