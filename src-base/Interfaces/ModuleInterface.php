<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface ModuleInterface
 *
 * @package Phycom\Base\Interfaces
 */
interface ModuleInterface
{
    /**
     * @return string
     */
    public function getLabel() : string;

    /**
     * @return string
     */
    public function getLogo() : ?string;

    /**
     * @return ModuleSettingsInterface[]
     */
    public function getAllSettings() : array;

    /**
     * @return string|null
     */
    public function getBackendSettingsView() : ?string;

    /**
     * @return PhycomComponentInterface|null
     */
    public function getComponent() : ?PhycomComponentInterface;
}
