<?php

namespace Phycom\Base\Interfaces;

/**
 * Interface CartInterface
 *
 * @package Phycom\Base\Interfaces
 */
interface CartInterface
{
    /**
     * @param CartStorageInterface $storage
     * @return mixed
     */
    public function setStorage(CartStorageInterface $storage);

    /**
     * @return CartStorageInterface
     */
    public function getStorage() : CartStorageInterface;

    /**
     * Returns all items or items of a given type from the cart
     *
     * @param null $itemType
     * @return CartItemInterface[]|CartItemProductInterface[]|CartItemDiscountInterface[]|CartItemDeliveryInterface[]
     */
    public function getItems($itemType = null) : array;

    /**
     * Calculates count of all items or items of a given type from the cart
     * @param null $itemType
     * @return int
     */
    public function getCount($itemType = null) : int;

    /**
     * @param CartItemInterface $element
     * @param bool $save
     * @return CartInterface
     */
    public function add(CartItemInterface $element, bool $save = true) : CartInterface;

    /**
     * Removes an item from the cart
     *
     * @param $uniqueId - item id
     * @param bool $save
     * @return CartInterface
     */
    public function remove($uniqueId, bool $save = true) : CartInterface;

    /**
     * Deletes all items from the cart
     *
     * @param bool $save
     * @return CartInterface
     */
    public function clear(bool $save = true) : CartInterface;

    /**
     * @return mixed
     */
    public function save();

    /**
     * Checks if item with specified id exists in cart
     * @param $uniqueId
     * @return bool
     */
    public function hasItem($uniqueId) : bool;

    /**
     * @param mixed $uniqueId
     * @return CartItemInterface|CartItemProductInterface|CartItemDiscountInterface|CartItemDeliveryInterface|null
     */
    public function getItem($uniqueId) : ?CartItemInterface;
}
