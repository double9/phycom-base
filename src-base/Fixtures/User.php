<?php
namespace Phycom\Base\Fixtures;

use yii\test\ActiveFixture;

class User extends ActiveFixture
{
    public $modelClass = 'Phycom\Base\Models\User';
}
