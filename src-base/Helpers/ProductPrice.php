<?php

namespace Phycom\Base\Helpers;


use Phycom\Base\Models\Attributes\PriceUnitMode;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\Variant;

use yii\base\ModelEvent;
use yii\base\BaseObject;
use yii\base\InvalidValueException;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Class ProductPrice
 *
 * @package Phycom\Base\Helpers
 */
class ProductPrice extends BaseObject
{
    /**
     * @var \Phycom\Base\Models\Product\ProductPrice[]
     */
    public array $prices;
    /**
     * @var Product
     */
    protected Product $product;
    /**
     * @var number
     */
    protected null|float|int $units = null;
    /**
     * @var \Phycom\Base\Models\Product\ProductPrice|null
     */
    protected ?\Phycom\Base\Models\Product\ProductPrice $price = null;

    /**
     * ProductPrice constructor.
     *
     * @param Product $product
     * @param number|null $units
     * @param array $config
     */
    public function __construct(Product $product, $units = null, $config = [])
    {
        $this->product = $product;
        $this->units = $units;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        if (!isset($this->prices)) {
            $this->prices = [];
            foreach ($this->product->prices as $price) {
                if ($price->can_purchase) {
                    $this->prices[] = $price;
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        $priceModel = $this->getProductPrice();
        if ($this->product->discount && $priceModel->discount_amount) {
            return $priceModel->price - $priceModel->discount_amount;
        }
        return $priceModel->price;
    }

    /**
     * @return int|null
     */
    public function getPrevPrice()
    {
        $priceModel = $this->getProductPrice();
        if ($this->product->discount && $priceModel->discount_amount) {
            return $priceModel->price;
        }
        return null;
    }

    /**
     * Returns prices as array of current price and previous price (if set)
     * This is useful in cases when we need to print out current price and previous price to be able to see the discount
     *
     * @return array
     */
    public function getPrices()
    {
        $priceModel = $this->getProductPrice();
        $prices = [];
        if ($this->product->discount && $priceModel->discount_amount) {
            array_push($prices, $priceModel->price - $priceModel->discount_amount);
        }
        array_push($prices, $priceModel->price);
        return $prices;
    }

    /**
     * @return int
     */
    public function getNonDiscountPrice()
    {
        return $this->getProductPrice()->price;
    }

    /**
     * @return int|null
     */
    public function getDiscountPrice()
    {
        $priceModel = $this->getProductPrice();
        if ($this->product->discount && $priceModel->discount_amount) {
            return $priceModel->price - $priceModel->discount_amount;
        }
        return null;
    }

    /**
     * @param int $precision
     * @return false|float|null
     */
    public function getDiscountPercentage($precision = 4)
    {
        $priceModel = $this->getProductPrice();
        if ($this->product->discount && $priceModel->discount_amount) {
            return round($priceModel->discount_amount / $priceModel->price, $precision);
        }
        return null;
    }

    /**
     * @param Variant|string $variant
     * @param string $optionKey
     * @param mixed $value
     * @return int|null
     * @throws InvalidConfigException
     */
    public function getOptionPrice($variant, string $optionKey, $value = null)
    {
        $priceModel = $this->getProductPrice();
        $variantName = $variant instanceof Variant ? $variant->name : $variant;
        $variantInstance = $variant instanceof Variant
            ? $variant
            : Yii::$app->modelFactory->getVariant()::findByName($variant);

        if (!$variantInstance) {
            return 0;
        }

        $qty = 1;

        if ($variantInstance->unitPrice) {

            if (is_numeric($value)) {
                $qty = (float) $value;
            } else if (is_numeric($optionKey)) {
                $qty = (float) $optionKey;
            }
            if (Variant::TYPE_OPTION === $variantInstance->type) {
                $optionKey = '1';
            }
        }


        $priceVariant = Yii::$app->modelFactory->getProductPriceVariation()::findOne([
            'variant_name'     => $variantName,
            'option_key'       => $optionKey,
            'product_price_id' => $priceModel->id
        ]);


        return $priceVariant ? ($qty * $priceVariant->price) : 0;
    }

    /**
     * @param array $options
     * @return array
     */
    public function getTotalPrice(array $options)
    {
        $prices = $this->getPrices();
        foreach ($options as $productVariant => $optionKeys) {
            foreach ($optionKeys as $optionKey) {

                if (is_array($optionKey)) {
                    [$optionKey, $value] = $optionKey;
                } else {
                    $value = null;
                }

                foreach ($prices as $key => $price) {
                    $price += $this->getOptionPrice($productVariant, $optionKey, $value);
                    $prices[$key] = $price;
                }
            }
        }
        return $prices;
    }

    /**
     * @return \Phycom\Base\Models\Product\ProductPrice
     */
    public function getProductPrice()
    {
        if ($this->price) {
            return $this->price;
        }
        if (!$priceModel = $this->findUnitPrice($this->units)) {

            $mode = (string) $this->product->price_unit_mode;
            if (
                $this->units
                && $mode === PriceUnitMode::ANY
                || $mode === PriceUnitMode::LIMIT
                && $this->units > $this->product->prices[0]->num_units
                && $this->units < $this->product->prices[count($this->product->prices) - 1]->num_units
            ) {
                $priceModel = $this->findClosestPrice($this->units);
                $priceModel = $this->convertProductPriceUnits($this->units, $priceModel);
            }

            if (!$priceModel) {
                throw new InvalidValueException('Price is missing for product ' . $this->product->id);
            }
        }
        return $this->price = $priceModel;
    }

    /**
     * @param number|null $units
     * @return \Phycom\Base\Models\Product\ProductPrice|null
     */
    protected function findUnitPrice($units = null)
    {
        if (empty($this->prices)) {
            return null;
        }
        if (null === $units) {
            return $this->prices[0];
        }
        foreach ($this->prices as $price) {
            if ((float) $price->num_units === (float) $units) {
                return $price;
            }
        }
        return null;
    }

    /**
     * Finds closest ProductPrice model for selected units
     * @param number $units
     * @return \Phycom\Base\Models\Product\ProductPrice|null
     */
    protected function findClosestPrice($units)
    {
        $closestPrice = null;
        foreach ($this->prices as $productPrice) {
            if (null === $closestPrice) {
                $closestPrice = $productPrice;
                continue;
            }
            if (abs($units - $productPrice->num_units) < abs($units - $closestPrice->num_units)) {
                $closestPrice = $productPrice;
            } else {
                break;
            }
        }
        return $closestPrice;
    }

    /**
     * @param $units
     * @param \Phycom\Base\Models\Product\ProductPrice $productPrice
     * @return \Phycom\Base\Models\Product\ProductPrice
     */
    protected function convertProductPriceUnits($units, \Phycom\Base\Models\Product\ProductPrice $productPrice)
    {
        if ((float)$productPrice->num_units === (float)$units) {
            return $productPrice;
        }
        // make the model immutable
        $productPrice = clone $productPrice;

        // prevent accidental save of the immutable model
        $productPrice->on($productPrice::EVENT_BEFORE_UPDATE, function ($event) {
            /**
             * @var ModelEvent $event
             */
            $event->isValid = false;
        });
        $productPrice->on($productPrice::EVENT_BEFORE_INSERT, function ($event) {
            /**
             * @var ModelEvent $event
             */
            $event->isValid = false;
        });

        $productPrice->price = $this->convertPriceUnits($productPrice->price, $productPrice->num_units, $units);

        return $productPrice;
    }

    /**
     * @param $amount
     * @param $units
     * @param $targetUnits
     * @return int
     */
    protected function convertPriceUnits($amount, $units, $targetUnits)
    {
        if ($units < 1) {
            $unitPrice = $amount * $units;
        } else {
            $unitPrice = $amount / $units;
        }

        return (int)($targetUnits * $unitPrice);
    }
}
