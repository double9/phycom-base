<?php

namespace Phycom\Base\Helpers;

use yii\base\Baseobject;

/**
 * Taken from: @ https://codereview.stackexchange.com/questions/51895/calculate-future-date-based-on-business-days
 *
 * Class BusinessDaysCalculator
 * @package common\helpers
 *
 * @property bool $isBusinessDay
 */
class BusinessDaysCalculator extends BaseObject
{

	const MONDAY    = 1;
	const TUESDAY   = 2;
	const WEDNESDAY = 3;
	const THURSDAY  = 4;
	const FRIDAY    = 5;
	const SATURDAY  = 6;
	const SUNDAY    = 7;

	protected $date;
	protected $holidays;
	protected $nonBusinessDays;

	/**
	 * @param \DateTimeInterface $startDate - Date to start calculations from
	 * @param \DateTimeInterface[] $holidays - Array of holidays, holidays are not considered business days.
	 * @param \int[] $nonBusinessDays - Array of days of the week which are not business days.
     * @param array $config
	 */
	public function __construct(\DateTimeInterface $startDate, array $holidays = [], array $nonBusinessDays = [self::SATURDAY, self::SUNDAY], array $config = [])
    {
		$this->date = clone $startDate;
		$this->holidays = $holidays;
		$this->nonBusinessDays = $nonBusinessDays;

		parent::__construct($config);
	}

    /**
     * @param $howManyDays
     */
	public function addBusinessDays($howManyDays)
    {
		$i = 0;
		while ($i < $howManyDays) {
			$this->date->modify('+1 day');
			if ($this->isBusinessDay($this->date)) {
				$i++;
			}
		}
	}

    /**
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return int
     */
	public function numBusinessDaysBetween(\DateTimeInterface $start, \DateTimeInterface $end)
    {
        $date = clone $start;
        $diff = (int) $start->diff($end)->format('%d');
        $i = 0;
        for ($n = 0; $n < $diff; $n++) {
            if ($this->isBusinessDay($date)) {
                $i++;
            }
            $date->modify('+1 day');
        }
        return $i;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getNextBusinessDay()
    {
        $maxIterations = 100;
        $i = 0;
        while ($i++ < $maxIterations) {
            $this->date->modify('+1 day');
            if ($this->isBusinessDay($this->date)) {
                return $this->date;
            }
        }
        return null;
    }

    /**
     * @return \DateTimeInterface
     */
	public function getDate()
    {
		return $this->date;
	}

    /**
     * @return bool
     */
    public function getIsBusinessDay()
    {
        return $this->isBusinessDay($this->date);
    }

    /**
     * @param \DateTimeInterface $date
     * @return bool
     */
    private function isBusinessDay(\DateTimeInterface $date)
    {
		if (in_array((int)$date->format('N'), $this->nonBusinessDays)) {
			return false; //Date is a nonBusinessDay.
		}
		foreach ($this->holidays as $day) {
			if ($date->format('Y-m-d') == $day->format('Y-m-d')) {
				return false; //Date is a holiday.
			}
		}
		return true; //Date is a business day.
	}
}
