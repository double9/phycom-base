<?php

namespace Phycom\Base\Helpers;

/**
 * Class Constant
 * @package Phycom\Base\Helpers
 */
class Constant
{
    /**
     * Return array of constants for a class
     *
     * @param string $class Class name
     * @param null|string $prefix Prefix like e.g. "KEY_"
     * @param array $exclude Array of excluding values
     * @param boolean $assoc Return associative array with constant name as key
     *
     * @return array Assoc array of constants
     */
    public static function getClassConstants($class, $prefix = null, $exclude = [], $assoc = false)
    {
        $reflector = new \ReflectionClass($class);
        $constants = $reflector->getConstants();
        $values = [];

        foreach ($constants as $constant => $value) {
            if (($prefix && strpos($constant, $prefix) !== false) || $prefix === null) {
                if (in_array($value, $exclude)) {
                    continue;
                }
                if ($assoc) {
                    $values[$constant] = $value;
                } else {
                    $values[] = $value;
                }
            }
        }
        return $values;
    }
}
