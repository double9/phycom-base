<?php

namespace Phycom\Base\Helpers;


use yii\base\InvalidArgumentException;

/**
 * Class Di
 *
 * @package Phycom\Base\Helpers
 */
class Di
{
    /**
     * @param array|string|callable $definition
     * @param string $interface
     * @return array|callable|string
     * @throws \ReflectionException
     */
    public static function ensureDefinition($definition, string $interface)
    {
        $class = null;

        if (is_string($definition)) {
            $class = $definition;
        } elseif (is_array($definition) && isset($definition['class'])) {
            $class = $definition['class'];
        } elseif (is_callable($definition)) {
            return $definition;
        } else {
            throw new InvalidArgumentException('Definition must contain a class name');
        }

        if (!(new \ReflectionClass($class))->implementsInterface($interface)) {
            throw new InvalidArgumentException('Definition must implement ' . $interface);
        }

        return $definition;
    }
}
