<?php

namespace Phycom\Base\Helpers;

use libphonenumber\PhoneNumberFormat;

use yii;


/**
 * Shorthand for the Yii::$app->formatter
 *
 * Class f
 * @package Phycom\Base\Helpers
 *
 * @method static string currency(mixed $value, string $currency = null, array $options = [], array $textOptions = [])
 * @method static string currencyDisplay(mixed $value, string $currency = null, array $options = [], array $textOptions = [])
 * @method static string shortCurrency(mixed $value, string $currency = null)
 * @method static string messageSubject(string $value)
 * @method static string json(string $value, $cut = null)
 * @method static string titleCase(string $value)
 * @method static string days(int $value)
 * @method static string unaccented(string $value)
 * @method static string raw(mixed $value)
 * @method static string text(string $value)
 * @method static string nText(string $value)
 * @method static string paragraphs(string $value)
 * @method static string html(string $value)
 * @method static string email(string $value, $options = [])
 * @method static string image(mixed $value, $options = [])
 * @method static string integer(mixed $value, array $options = [], array $textOptions = [])
 * @method static string decimal(mixed $value, $decimals = null, $options = [], $textOptions = [])
 * @method static string url(mixed $value, $options = [])
 * @method static string boolean(mixed $value)
 * @method static string units(mixed $value, $unitType)
 * @method static string yesNo(bool $value)
 * @method static string strToBool(string $value)
 * @method static string date(mixed $value, string $format = null)
 * @method static string time(mixed $value, string $format = null)
 * @method static string datetime(mixed $value, string $format = null)
 * @method static string ordinalSuffix(int $number, string $locale = null)
 * @method static string weekday($value, $format = 'D')
 * @method static string percent($value, $decimals = null, $options = [], $textOptions = [])
 * @method static string humanInterval(\DateInterval $value, $lang = null)
 * @method static string humanDate(\DateTime $value, $lang = null)
 * @method static string humanDateTime(\DateTime $value, $lang = null)
 * @method static string phone(mixed $value, $link = false, $format = PhoneNumberFormat::INTERNATIONAL)
 * @method static string address($value = null, bool $link = false)
 * @method static string plainAddress($value = null, bool $link = false)
 * @method static string addressLink($value = null)
 * @method static string mapUrl($value = null)
 * @method static string trimContent(string $value = null)
 * @method static string score($value, $decimals = 1, $decimalSeparator = null, $thousandSeparator = null)
 */
class f
{
    public static function __callStatic($name, $arguments)
    {
        $formatter = Yii::$app->formatter;
        $methodName = 'as' . ucwords($name);

        if (method_exists($formatter, $methodName)) {

            return call_user_func_array(array($formatter, $methodName), $arguments);

        } else if ($arguments && isset($arguments[0])) {

            return $arguments[0];

        } else {
            throw new \Exception('Method not found');
        }
    }
}
