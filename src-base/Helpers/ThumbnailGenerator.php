<?php

namespace Phycom\Base\Helpers;


use Phycom\Base\Models\File;

use yii\base\BaseObject;
use yii\imagine\Image;
use Yii;

/**
 * Class ThumbnailGenerator
 *
 * @package Phycom\Base\Helpers
 */
class ThumbnailGenerator extends BaseObject
{
    /**
     * @var int
     */
    public int $thumbQuality = File::DEFAULT_THUMB_QUALITY;

    /**
     * @var string - file storage bucket name
     */
    protected string $bucket;

    /**
     * @var array
     */
    protected array $thumbSizes = [];

    /**
     * ThumbnailGenerator constructor.
     *
     * @param string $bucket
     * @param array $config
     */
    public function __construct(string $bucket, $config = [])
    {
        $this->bucket = $bucket;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        if (empty($this->thumbSizes)) {
            $this->thumbSizes = (array) c::param('thumbSizes', [
                File::THUMB_SIZE_SMALL  => [120, null],
                File::THUMB_SIZE_MEDIUM => [400, null],
                File::THUMB_SIZE_LARGE  => [640, null]
            ]);
        }
    }

    /**
     * @param string $filename
     */
    public function generate(string $filename)
    {
        $bucket = Yii::$app->fileStorage->getBucket($this->bucket);
        $file = Yii::getAlias('@files/' . $this->bucket . '/' . $filename);

        foreach ($this->thumbSizes as $size => [$width, $height]) {

            $thumbFilename = $this->getThumbFilename($filename, $size);

            if (Yii::$app->fileStorage->getBucket($this->bucket)->fileExists($thumbFilename)) {
                Yii::$app->fileStorage->getBucket($this->bucket)->deleteFile($thumbFilename);
            }

            $tmpFilename = tempnam('/tmp', '_thumb_' . $size);

            Image::thumbnail($file, $width, $height)
                ->save($tmpFilename, ['quality' => $this->thumbQuality]);

            $bucket->copyFileIn($tmpFilename, $thumbFilename);
        }
    }

    /**
     * @param string $filename
     * @param string $size
     * @return string
     */
    public function getThumbFilename(string $filename, $size = File::THUMB_SIZE_DEFAULT): string
    {
        return pathinfo($filename, PATHINFO_FILENAME) . '_' . $size . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    }
}
