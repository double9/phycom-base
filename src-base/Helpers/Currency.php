<?php

namespace Phycom\Base\Helpers;

class Currency
{
    const MULTIPLIER = 100;

    /**
     * Converts monetary decimal amount to integer
     *
     * @param float $value
     * @param null $precision
     * @return float|int
     */
    public static function toInteger($value, $precision = null)
    {
        $value = bcmul((string) self::MULTIPLIER, (string) $value, $precision);
        return $precision ? (double) $value : (int) $value;
    }

    /**
     * converts monetary value to decimal
     *
     * @param int $value
     * @param int $precision
     * @return float
     */
    public static function toDecimal($value, $precision = 4)
    {
        return (double) bcdiv((string) $value, (string) self::MULTIPLIER, $precision);
    }

    /**
     * Reduces the funds by the amount provided
     *
     * @param number $funds
     * @param number $amount
     * @return mixed
     */
    public static function reduce(&$funds, $amount)
    {
        if ($funds <= $amount) {
            $originalFunds = $funds;
            $funds = 0;
            return $originalFunds;
        } else {
            $funds = $funds - $amount;
            return $amount;
        }
    }
}
