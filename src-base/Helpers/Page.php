<?php

namespace Phycom\Base\Helpers;


use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Post;

/**
 * Class Page
 * @package Phycom\Base\Helpers
 */
class Page
{
    public static function findByKey($key)
    {
        return Post::findOne(['key' => $key, 'type' => PostType::PAGE]);
    }
}
