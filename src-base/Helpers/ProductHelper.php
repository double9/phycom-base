<?php

namespace Phycom\Base\Helpers;

use Phycom\Base\Models\Product\ProductCategory;

use yii\base\BaseObject;

/**
 * Class ProductHelper
 * @package Phycom\Base\Helpers
 */
class ProductHelper extends BaseObject
{
    /**
     * Search all categories ans subcategories product is associated with.
     * If product belongs to a category X and Y all subcategories for X and Y are also included
     *
     * @param int $productId
     * @return ProductCategory[]
     */
    public static function getAllCategoriesRecursive(int $productId)
    {
        $query = ProductCategory::find();
        $query->sql = 'WITH RECURSIVE categories AS (
            SELECT c1.* FROM product_category c1
                INNER JOIN product_in_product_category pipc on (c1.id = pipc.category_id AND pipc.product_id = :pid)
            UNION
            SELECT c2.* FROM product_category c2
                INNER JOIN categories t0 ON t0.id = c2.parent_id
            )
            SELECT c.* FROM categories c';
        $query->addParams([
            'pid' => $productId
        ]);
        return $query->all();
    }
}
