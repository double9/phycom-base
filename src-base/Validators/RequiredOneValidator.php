<?php

namespace Phycom\Base\Validators;

use yii\validators\Validator;
use yii;

/**
 * Class RequiredOneValidator
 * @package Phycom\Base\Validators
 */
class RequiredOneValidator extends Validator
{
    public function validateAttributes($model, $attributes = null)
    {
        $attributes = $this->getValidationAttributes($attributes);

        foreach ($attributes as $attribute) {
            $skip = $this->skipOnError && $model->hasErrors($attribute);
            if (!$skip) {
                if ($this->when === null || call_user_func($this->when, $model, $attribute)) {
                    $this->validateAttribute($model, $attribute);
                }
            }
        }
    }

    public function validateAttribute($model, $attribute)
    {
        if (!empty($model->$attribute)) {
            return;
        }
        foreach ($this->attributes as $attr) {
            if ($attribute !== $attr && !empty($model->$attr)) {
                return;
            }
        }

        $fields = [];
        foreach ($this->attributes as $attr) {
            $fields[] = $model->getAttributeLabel($attr);
        }
        $this->addError($model, $attribute, Yii::t('phycom/base/error', 'At least one of the fields {fields} is required', ['fields' => '"' .implode('" ' . Yii::t('phycom/base/main', 'or') . ' "', $fields) . '"' ]));
    }
}
