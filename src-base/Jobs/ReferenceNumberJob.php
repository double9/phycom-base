<?php

namespace Phycom\Base\Jobs;

use Phycom\Base\Components\Queue\Beanstalk;
use yii\base\Exception;
use yii\base\BaseObject;
use yii;

/**
 * Class ReferenceNumberJob
 * @package Phycom\Base\Jobs
 */
class ReferenceNumberJob extends BaseObject implements yii\queue\JobInterface
{
	public $id;

	/**
	 * @param Beanstalk $queue which pushed and is handling the job
	 * @throws Exception
	 */
	public function execute($queue)
	{
		if (!$user = Yii::$app->modelFactory->getUser()::findOne($this->id)) {
			throw new Exception('Error creating reference number: user ' . $this->id . ' not found');
		}
		$user->generateReferenceNumber();
		if (!$user->save(true, ['reference_number'])) {
			throw new Exception('Error saving user reference number: ' . json_encode($user->errors));
		}
	}
}
