<?php

namespace Phycom\Base\Jobs;

use Phycom\Base\Components\FileStorage;
use Phycom\Base\Components\Queue\Beanstalk;
use Phycom\Base\Events\EventWithPayload;

use Mpdf\Mpdf;

use yii\base\Exception;
use yii\base\BaseObject;
use yii;

/**
 * Class InvoicePdfJob
 * @package Phycom\Base\Jobs
 */
class InvoicePdfJob extends BaseObject implements yii\queue\RetryableJobInterface
{
    public int $maxAttempts = 3;

    public string $template = 'invoice';
    /**
     * @var int - invoice number
     */
    public $invoiceNumber;

    public function getTtr()
    {
        return 10;
    }

    public function canRetry($attempt, $error)
    {
        return $attempt < $this->maxAttempts;
    }

    /**
     * @param Beanstalk $queue
     * @throws Exception
     * @throws \Mpdf\MpdfException
     */
    public function execute($queue)
    {
        Yii::info('Creating pdf invoice nr ' . $this->invoiceNumber, 'invoice');
        if (!$invoice = Yii::$app->modelFactory->getInvoice()::findOne(['number' => $this->invoiceNumber])) {
            throw new Exception('Error creating pdf. Invoice ' . $this->invoiceNumber . ' not found');
        }
        $filename = md5($invoice->number) . '.pdf';

        $template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($this->template);

        $html = $template->render([
            'invoice'  => $invoice,
            'logo'     => $this->createBase64Logo(),
            'vatRate'  => Yii::$app->commerce->getVatRate()
        ], $invoice->order->language_code);

        $pdf = $this->createPdf($html);

        Yii::debug('Saving pdf to file ' . $filename, 'invoice');
        $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_INVOICES);
        $bucket->saveFileContent($filename, $pdf);

        if (!$bucket->fileExists($filename)) {
            throw new Exception('Error saving invoice ' . $filename);
        }
        $invoice->file = $filename;
        $invoice->save(false, ['file']);
        Yii::info('Pdf invoice nr ' . $this->invoiceNumber . ' was successfully created. Invoice file: ' . $filename, 'invoice');

        Yii::debug('Trigger ' . $invoice::EVENT_AFTER_INVOICE_FILE_CREATED . ' event', 'invoice');
        /**
         * @var EventWithPayload|object $event
         */
        $event = Yii::createObject([
            'class'   => EventWithPayload::class,
            'payload' => $invoice->getFullFilePath()
        ]);
        $invoice->trigger($invoice::EVENT_AFTER_INVOICE_FILE_CREATED, $event);
        Yii::debug('Event ' . $invoice::EVENT_AFTER_INVOICE_FILE_CREATED . ' was successfully triggered', 'invoice');
    }

    /**
     * @param string $html
     * @return string
     * @throws \Mpdf\MpdfException|yii\base\InvalidConfigException
     */
    protected function createPdf(string $html): string
    {
        Yii::debug('Converting html content to pdf', 'invoice');
        $tempDir = Yii::getAlias('@files/mpdf');
        if (!is_dir($tempDir)) {
            mkdir($tempDir);
        }
        /**
         * @var Mpdf|object $mPdf
         */
        $mPdf = Yii::createObject([
            'class'   => Mpdf::class,
            'tempDir' => $tempDir
        ]);

        $mPdf->WriteHTML($html);

        return $mPdf->Output('', 'S');
    }

    /**
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    protected function createBase64Logo(): string
    {
        $data = file_get_contents(Yii::$app->site->getFullLogoPath());
        $type = Yii::$app->site->getLogoMimeType();

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }
}
