<?php

namespace Phycom\Base\Actions;

use Yii;

/**
 * Class FileDownloadAction
 *
 * @package Phycom\Base\Actions
 */
class FileDownloadAction extends \yii2tech\filestorage\DownloadAction
{
    /**
     * @var bool
     */
    public bool $cacheStaticFiles = true;

    /**
     * @var string
     */
    public string $cacheControlHeader = 'max-age=2592000, public';

    /**
     * @var array|string[]
     */
    public array $cachedFileTypes = ['ico','pdf','jpg','jpeg','png','gif','js','css','swf'];

    /**
     * @param string $bucket
     * @param string $filename
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($bucket, $filename)
    {
        if ($this->cacheStaticFiles && !empty($this->cachedFileTypes)) {
            $this->checkCacheHeaders($filename);
        }
        return parent::run($bucket, $filename);
    }

    /**
     * @param $filename
     */
    protected function checkCacheHeaders($filename): void
    {
        if (preg_match('/\.(' . implode('|', $this->cachedFileTypes) . ')$/', $filename)) {
            $response = Yii::$app->response;
            $headers = $response->getHeaders();
            $headers->set('Cache-Control', $this->cacheControlHeader);
        }
    }
}
