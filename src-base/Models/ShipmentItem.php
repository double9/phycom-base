<?php

namespace Phycom\Base\Models;

use yii;

/**
 * This is the model class for table "shipment_item".
 *
 * @property integer $shipment_id
 * @property integer $order_item_id
 *
 * @property OrderItem $orderItem
 * @property Shipment $shipment
 */
class ShipmentItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment_id', 'order_item_id'], 'required'],
            [['shipment_id', 'order_item_id'], 'integer'],
            [['order_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrderItem()), 'targetAttribute' => ['order_item_id' => 'id']],
            [['shipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShipment()), 'targetAttribute' => ['shipment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'shipment_id' => Yii::t('phycom/base/main', 'Shipment ID'),
            'order_item_id' => Yii::t('phycom/base/main', 'Order Item ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipment()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShipment()), ['id' => 'shipment_id']);
    }
}
