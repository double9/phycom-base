<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\ProductStatus;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Attributes\VendorOption;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Attributes\VendorType;
use Phycom\Base\Models\Behaviors\JsonAttributeBehavior;
use Phycom\Base\Models\Product\Product;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "vendor".
 *
 * @property integer            $id
 * @property VendorType         $type
 * @property string             $name
 * @property string             $legal_name
 * @property string             $reg_number
 * @property VendorStatus       $status
 * @property VendorOption       $options
 * @property bool               $main
 * @property \DateTime          $created_at
 * @property \DateTime          $updated_at
 *
 * @property-read Product[]     $products
 * @property-read Address[]     $addresses
 * @property-read Address       $address
 * @property-read VendorUser[]  $vendorUsers
 * @property-read User[]        $users
 * @property-read Email[]       $emails
 * @property-read Email         $email
 * @property-read Phone[]       $phones
 * @property-read Phone         $phone
 * @property-read Post[]        $posts
 * @property-read Shop          $shop
 * @property-read Shop[]        $shops
 */
class Vendor extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
            'dynamic-attribute' => [
                'attributes' => ['status' => VendorStatus::class, 'type' => VendorType::class]
            ],
            'json-attribute'    => [
                'class'      => JsonAttributeBehavior::class,
                'passOwner'  => true,
                'attributes' => ['options' => VendorOption::class]
            ]
		]);
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'type'], 'required'],
            [['options'], 'safe'],
            [['name', 'legal_name', 'reg_number'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['main'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('phycom/base/main', 'ID'),
            'type'       => Yii::t('phycom/base/main', 'Type'),
            'name'       => Yii::t('phycom/base/main', 'Name'),
            'legal_name' => Yii::t('phycom/base/main', 'Legal Name'),
            'reg_number' => Yii::t('phycom/base/main', 'Reg Number'),
            'status'     => Yii::t('phycom/base/main', 'Status'),
            'options'    => Yii::t('phycom/base/main', 'Options'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function addUser(User $user)
    {
		if (!$this->hasUser($user->id)) {
			$vendorUser = new VendorUser();
			$vendorUser->user_id = $user->id;
			$vendorUser->vendor_id = $this->id;
			$vendorUser->save();
		}
    }

    public function hasUser($userId)
    {
	    return VendorUser::findOne(['user_id' => $userId, 'vendor_id' => $this->id]);
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $this->name = uniqid($this->name . '_');
        $this->status = VendorStatus::DELETED;
        return $this->save(true, ['name', 'status']);
    }

    public function getShop()
    {
        foreach ($this->shops as $shop) {
            if (!$shop->status->in([ShopStatus::INACTIVE, ShopStatus::DELETED])) {
                return $shop;
            }
        }
        return null;
    }

	public function getAddress()
	{
		return !empty($this->addresses) ? $this->addresses[0] : null;
	}

	public function getEmail()
	{
		return !empty($this->emails) ? $this->emails[0] : null;
	}

	public function getPhone()
	{
		return !empty($this->phones) ? $this->phones[0] : null;
	}


	/**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['vendor_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['address.status' => ContactAttributeStatus::DELETED]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorUsers()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getVendorUser()), ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getUsers()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id'])->viaTable('vendor_user', ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmails()
    {
        return $this->hasMany(Email::class, ['vendor_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['email.status' => ContactAttributeStatus::DELETED]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhones()
    {
        return $this->hasMany(Phone::class, ['vendor_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['phone.status' => ContactAttributeStatus::DELETED]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getPost()), ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShops()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShop()), ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getProducts()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id'])
            ->viaTable('product_in_vendor', ['vendor_id' => 'id'])
            ->andWhere(['not', ['product.status' => ProductStatus::DELETED]]);
    }
}
