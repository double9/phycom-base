<?php

namespace Phycom\Base\Models;

use Phycom\Base\Helpers\Date;
use Phycom\Base\Interfaces\ModuleInterface;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Components\Formatter;

use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\base\Module;
use yii\base\Model;
use yii;

/**
 * Class Settings
 * @package Phycom\Base\Models
 *
 * @property-read Module|ModuleInterface $module
 */
abstract class ModuleSettingsForm extends Model
{
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_OPTION = 'option';
    const TYPE_INTEGER = 'integer';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_DATETIME = 'datetime';
    const TYPE_TEXT = 'text';
    const TYPE_ARRAY = 'array';

    use ModelTrait;

    protected $module;
    /**
     * @var Formatter
     */
    protected $formatter;

    protected array $attributeTypes = [];

    public function __construct(Module $module, array $config = [])
    {
        $this->module = $module;
        parent::__construct($config);
    }

    public function formLabel()
    {
        return Inflector::titleize($this->module->id) . ' ' . Yii::t('phycom/backend/main', 'Settings');
    }

    public function formName()
    {
        $n = [$this->module->id];
        if ($this->module->module) {
            $n[] = ucfirst($this->module->module->id);
        }
        $n[] = 'Settings';
        return implode('', $n);
    }

    public function getModule()
    {
        return $this->module;
    }

    public function init()
    {
        parent::init();
        $this->formatter = Yii::createObject([
            'class' => Formatter::class,
            'nullDisplay' => null,
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
            'currencyCode' => null,
            'booleanFormat' => ['0', '1'],
            'numberFormatterOptions' => [
                \NumberFormatter::MAX_FRACTION_DIGITS => 3,
                \NumberFormatter::MIN_FRACTION_DIGITS => 2,
                \NumberFormatter::GROUPING_USED => 0,
            ],
            'numberFormatterTextOptions' => [
                \NumberFormatter::CURRENCY_CODE => '',
                \NumberFormatter::ZERO_DIGIT_SYMBOL => '.'
            ],
            'numberFormatterSymbols' => [
                \NumberFormatter::CURRENCY_SYMBOL => '',
                \NumberFormatter::PERCENT_SYMBOL => '',
                \NumberFormatter::DECIMAL_SEPARATOR_SYMBOL => '.'
            ]
        ]);
    }

    public function populateModule()
    {
        foreach ($this->attributes as $key => $value) {
            if (property_exists($this->module, $key)) {
                $format = $this->getAttributeType($key);
                $this->module->$key = $this->parse($format, $value);
            }
        }
    }

    public function populate()
    {
        foreach ($this->attributes as $name => $value) {

            $module = $this->isParentModule() ? $this->module->id : $this->module->module->id;
            $submodule = $this->isParentModule() ? null : $this->module->id;

            if ($setting = ModuleSetting::findByKey($name, $module, $submodule)) {
                $this->$name = $setting->value;
            } else {
                $type = $this->getAttributeType($name);
                $this->$name = $this->convertString($type, $this->module->$name);
            }
        };
        return $this;
    }

    /**
     * @return bool
     */
    public function save()
    {
        foreach ($this->attributes as $name => $value) {

            $module = $this->isParentModule() ? $this->module->id : $this->module->module->id;
            $submodule = $this->isParentModule() ? null : $this->module->id;
            $formattedValue = $this->format($this->getAttributeType($name), $value);

            $setting = ModuleSetting::set($name, $formattedValue, $module, $submodule);

            if ($setting->hasErrors()) {
                $this->addErrors($setting->errors);
                return false;
            }
        };
        return true;
    }

    public function getAttributeRules($attribute)
    {
        $rules = [];
        foreach ($this->rules() as $rule) {
            $attributes = is_string($rule[0]) ? [$rule[0]] : $rule[0];
            if (in_array($attribute, $attributes)) {
                array_shift($rule);
                $rules[] = $rule;
            }
        }
        return $rules;
    }

    public function getAttributeType($attribute)
    {
        if (isset($this->attributeTypes[$attribute])) {
            return $this->attributeTypes[$attribute];
        }
        $type = null;
        foreach ($this->rules() as $rule) {
            $attributes = is_string($rule[0]) ? [$rule[0]] : $rule[0];
            if (in_array($attribute, $attributes)) {
                switch ($rule[1]) {
                    case 'boolean':
                        return self::TYPE_BOOLEAN;
                    case 'in':
                        return self::TYPE_OPTION;
                    case 'date':
                    case 'time':
                        return self::TYPE_DATETIME;
                    case 'double':
                    case 'number':
                        return self::TYPE_DECIMAL;
                    case 'integer':
                        return self::TYPE_INTEGER;
                }
            }
        }
        return $type ?: self::TYPE_TEXT;
    }


    public function parse($type, $value)
    {
        switch ($type) {
            case self::TYPE_ARRAY:
                return Json::decode($value);
            case self::TYPE_BOOLEAN:
                return (bool) $value;
            case self::TYPE_DATETIME:
                return Date::create($value, $this->formatter->dateFormat);
            case self::TYPE_DECIMAL:
                return floatval($value);
            case self::TYPE_INTEGER:
                return intval($value);
            default:
                return $value;
        }
    }

    public function convertString($type, $value)
    {
        switch ($type) {
            case self::TYPE_ARRAY:
                return Json::encode($value);
            case self::TYPE_BOOLEAN:
                $value = (bool) $value;
                return $value ? '1' : '0';
            case self::TYPE_DATETIME:
                return Date::create($value)->toDateStr();
            case self::TYPE_DECIMAL:
            case self::TYPE_INTEGER:
            default:
                return (string) $value;
        }
    }

    public function format($attributeType, $value)
    {
        if ($attributeType === self::TYPE_ARRAY) {
            if (empty($value)) {
                $value = [];
            }
            return is_string($value) ? $value : Json::encode($value);
        }
        $format = $attributeType;
        if ($attributeType === self::TYPE_OPTION) {
            $format = 'text';
        }
        $method = 'as' . ucfirst($format);
        return $this->formatter->$method($value);
    }

    /**
     * @return bool
     */
    protected function isParentModule(): bool
    {
        $mainModules = [
            'app-backend',
            'app-frontend',
            'app-console'
        ];
        return !$this->module->module || in_array($this->module->module->id, $mainModules);
    }
}
