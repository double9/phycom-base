<?php

namespace Phycom\Base\Models;


use Phycom\Base\Models\Behaviors\TimestampBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "{{%partner_contract}}".
 *
 * @property integer $id
 * @property integer $created_by
 * @property string $company_name
 * @property array $address
 * @property string $contact_email
 * @property string $phone
 * @property string $category
 * @property \DateTime $contract_start
 * @property \DateTime $contract_end
 * @property string $consent_required_on
 * @property integer $max_consent_days
 * @property boolean $mandatory
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property User $createdBy
 * @property UserConsent[] $userConsents
 */
class PartnerContract extends ActiveRecord
{
    const CATEGORY_AUTHENTICATION = 'authentication';
    const CATEGORY_COMMUNICATION = 'communication';
    const CATEGORY_MARKETING = 'marketing';
    const CATEGORY_PAYMENTS = 'payments';
    const CATEGORY_SUPPORT = 'support';

    const CONSENT_REGISTRATION = 'registration';
    const CONSENT_ORDER = 'order';

    const CONSENT_EMAIL_MARKETING = 'email_marketing';
    const CONSENT_SMS_MARKETING = 'sms_marketing';
    const CONSENT_PHONE_MARKETING = 'phone_marketing';
    const CONSENT_AUTOMATED_MARKETING = 'automated_marketing';

    const CONSENT_EMAIL = 'email';
    const CONSENT_PHONE = 'phone';
    const CONSENT_SMS = 'sms';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_contract}}';
    }

    /**
     * @inheritdoc
     * @return PartnerContractQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerContractQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
                    ['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'contract_start',
                    'contract_end'
                ]
            ]
        ]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->created_by && !Yii::$app->user->isGuest) {
            $this->created_by = Yii::$app->user->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'contract_start', 'consent_required_on', 'max_consent_days', 'mandatory', 'category'], 'required'],
            [['address'], 'string'],
            [['mandatory'], 'boolean'],
            [['company_name', 'phone', 'consent_required_on', 'contact_email', 'category'], 'string', 'max' => 255],
            [['contact_email'], 'email'],
            [['contract_start', 'contract_end'],
                'date',
                'format' => 'php:Y-m-d'
            ],
            [['max_consent_days'], 'integer', 'max' => 3653, 'min' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('phycom/base/gdpr', 'ID'),
            'created_by' => Yii::t('phycom/base/gdpr', 'Created By'),
            'company_name' => Yii::t('phycom/base/gdpr', 'Company Name'),
            'address' => Yii::t('phycom/base/gdpr', 'Address'),
            'contact_email' => Yii::t('phycom/base/gdpr', 'DPO email'),
            'phone' => Yii::t('phycom/base/gdpr', 'Phone'),
            'category' => Yii::t('phycom/base/gdpr', 'Category'),
            'contract_start' => Yii::t('phycom/base/gdpr', 'Contract Start'),
            'contract_end' => Yii::t('phycom/base/gdpr', 'Contract End'),
            'consent_required_on' => Yii::t('phycom/base/gdpr', 'Consent Required On'),
            'max_consent_days' => Yii::t('phycom/base/gdpr', 'Max Consent Days'),
            'mandatory' => Yii::t('phycom/base/gdpr', 'Mandatory'),
            'created_at' => Yii::t('phycom/base/gdpr', 'Created At'),
            'updated_at' => Yii::t('phycom/base/gdpr', 'Updated At')
        ];
    }

    public function attributeHints()
    {
        return [
            'contact_email' => Yii::t('phycom/base/gdpr', 'Data protection officer email address'),
        ];
    }

    /**
     * Array of all categories and their label
     * @return array
     */
    public static function getCategories()
    {
        return [
            self::CATEGORY_AUTHENTICATION => Yii::t('phycom/base/gdpr', 'Authentication'),
            self::CATEGORY_COMMUNICATION => Yii::t('phycom/base/gdpr', 'Communications'),
            self::CATEGORY_MARKETING => Yii::t('phycom/base/gdpr', 'Marketing'),
            self::CATEGORY_PAYMENTS => Yii::t('phycom/base/gdpr', 'Payments'),
            self::CATEGORY_SUPPORT => Yii::t('phycom/base/gdpr', 'Support'),
        ];
    }

    public static function getRequiredForFields()
    {
        return [
            self::CONSENT_REGISTRATION => Yii::t('phycom/base/gdpr', 'Registration'),
            self::CONSENT_ORDER => Yii::t('phycom/base/gdpr', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserConsents()
    {
        return $this->hasMany(UserConsent::class, ['partner_contract_id' => 'id']);
    }

    /**
     * @param string $contractName
     * @return array|PartnerContract|null
     */
    public static function findByName(string $contractName)
    {
        return static::find()->where(['like', 'company_name', $contractName])->one();
    }



    public static function excludedContracts()
    {
        return [
            self::CONSENT_EMAIL_MARKETING,
            self::CONSENT_PHONE_MARKETING,
            self::CONSENT_SMS_MARKETING,
            self::CONSENT_AUTOMATED_MARKETING,
            self::CONSENT_SMS,
            self::CONSENT_EMAIL,
            self::CONSENT_PHONE
        ];
    }

    /**
     * @param array $contracts
     *
     * @return array
     */
    public function visibleContracts($contracts)
    {
        foreach ($contracts as $index => $contract) {
            if (in_array($contract->company_name, self::excludedContracts())) {
                ArrayHelper::remove($contracts, $index);
            }
        }

        $result = [];
        foreach ($contracts as $contract) {
            $result[$contract->category][] = $contract;
        }

        return $result;
    }
}
