<?php

namespace Phycom\Base\Models;


use Phycom\Base\Helpers\f;
use Phycom\Base\Helpers\ProductHelper;
use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartItemDiscountInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Base\Models\Attributes\DiscountRuleType;
use Phycom\Base\Models\Attributes\DiscountRuleStatus;
use Phycom\Base\Models\Behaviors\CurrencyBehavior;
use Phycom\Base\Models\Behaviors\DateBehavior;
use Phycom\Base\Models\Behaviors\StatusBehavior;
use Phycom\Base\Models\Behaviors\TimestampBehavior;
use Phycom\Base\Helpers\Coupon;
use Phycom\Base\Helpers\c;

use yii\helpers\ArrayHelper;
use yii\db\Query;
use Yii;

/**
 * This is the model class for table "discount_rule".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property integer $vendor_id
 * @property string $code
 * @property string $discount_rate
 * @property string $birthday_discount_rate
 * @property int $discount_amount
 * @property int $min_purchase
 * @property string $rule
 * @property string $name
 * @property string $personal_code
 * @property \DateTime $birthday
 * @property integer $max_usage
 * @property integer $used
 * @property DiscountRuleType $type
 * @property DiscountRuleStatus $status
 * @property \Datetime $starts_at
 * @property \DateTime|null $expires_at
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Vendor $vendor
 * @property Shop $shop
 * @property User $createdBy
 * @property DiscountRuleCategory[] $categories
 *
 * @property-read bool $isGlobal - when true discount applies to all vendors and shops
 * @property-read bool $isBirthdayPeriod
 * @property-read float|null $rate
 * @property-read bool $isValid
 *
 * @method bool updateStatus($status, $save = true)
 */
class DiscountRule extends ActiveRecord implements CartItemDiscountInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_rule';
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
            'status' => StatusBehavior::class,
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'starts_at',
					'expires_at'
				]
			],
            'date' => [
                'class' => DateBehavior::class,
                'attributes' => [
                    'birthday'
                ]
            ],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => DiscountRuleStatus::class,
					'type' => DiscountRuleType::class
				]
			],
            'currency' => [
                'class' => CurrencyBehavior::class,
                'attributes' => ['discount_amount', 'min_purchase']
            ],
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'vendor_id', 'max_usage', 'used', 'created_by'], 'integer'],
            [['type', 'status'], 'required'],
            [['discount_rate', 'birthday_discount_rate', 'discount_amount', 'min_purchase'], 'number'],
            [['birthday', 'starts_at', 'expires_at', 'created_at', 'updated_at'], 'safe'],
            [['code', 'name', 'personal_code', 'rule'], 'string', 'max' => 255],
            [['type', 'status'], 'safe'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                     => Yii::t('phycom/base/discount-rule', 'ID'),
            'shop_id'                => Yii::t('phycom/base/discount-rule', 'Shop ID'),
            'vendor_id'              => Yii::t('phycom/base/discount-rule', 'Vendor ID'),
            'code'                   => Yii::t('phycom/base/discount-rule', 'Code'),
            'rule'                   => Yii::t('phycom/base/discount-rule', 'Rule'),
            'discount_rate'          => Yii::t('phycom/base/discount-rule', 'Discount Rate'),
            'discount_amount'        => Yii::t('phycom/base/discount-rule', 'Discount Amount'),
            'min_purchase'           => Yii::t('phycom/base/discount-rule', 'Minimum Purchase'),
            'birthday_discount_rate' => Yii::t('phycom/base/discount-rule', 'Birthday Discount Rate'),
            'personal_code'          => Yii::t('phycom/base/discount-rule', 'Personal Code'),
            'name'                   => Yii::t('phycom/base/discount-rule', 'Name'),
            'birthday'               => Yii::t('phycom/base/discount-rule', 'Birthday'),
            'max_usage'              => Yii::t('phycom/base/discount-rule', 'Max Usage'),
            'used'                   => Yii::t('phycom/base/discount-rule', 'Used'),
            'type'                   => Yii::t('phycom/base/discount-rule', 'Type'),
            'status'                 => Yii::t('phycom/base/discount-rule', 'Status'),
            'starts_at'              => Yii::t('phycom/base/discount-rule', 'Starts At'),
            'expires_at'             => Yii::t('phycom/base/discount-rule', 'Expires At'),
            'created_by'             => Yii::t('phycom/base/discount-rule', 'Created By'),
            'created_at'             => Yii::t('phycom/base/discount-rule', 'Created At'),
            'updated_at'             => Yii::t('phycom/base/discount-rule', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->type === DiscountRuleType::COUPON || $this->type === DiscountRuleType::VOUCHER) {
            $this->max_usage = 1;
        }
        return parent::beforeSave($insert);
    }

    public function getUniqueId()
    {
        return $this->id;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function getPrice()
    {
        return -$this->calculateDiscount();
    }

    /**
     * @return int
     * @throws yii\base\InvalidConfigException
     */
    public function calculateDiscount()
    {
        $totalPurchase = 0;
        $cart = $this->getCart();
        /**
         * @var CartItemProductInterface[] $cartItems
         */
        $cartItems = $cart->getItems(CartItemProductInterface::class);
        foreach ($cartItems as $item) {
            $totalPurchase += $item->getTotalPrice();
        }

        if ($this->min_purchase && $this->min_purchase > $totalPurchase) {
            return 0;
        }

        $items = [];
        if (!empty($this->categories)) {
            foreach ($cartItems as $item) {
                if (isset($item->product_id)) {
                    $categories = ProductHelper::getAllCategoriesRecursive($item->product_id);
                    $categoryIds = ArrayHelper::getColumn($categories, 'id');
                    foreach ($this->categories as $c) {
                        if (in_array($c->product_category_id, $categoryIds)) {
                            $items[] = $item;
                            break;
                        }
                    }
                }
            }
        } else {
            $items = $cartItems;
        }

        // filter by vendor and shop if set
        $shops = [];
        if ($this->vendor_id) {
            $shops = (new Query())->select('id')->from(Shop::tableName())->where(['vendor_id' => $this->vendor_id])->column();
        }
        if ($this->shop_id) {
            $shops[] = $this->shop_id;
        }
        if (!empty($shops)) {
//            foreach ($items as $key => $item) {
//                if ($item->order && $item instanceof OrderItem) {
//                    if (!in_array($item->order->shop_id, $shops)) {
//                        unset($items[$key]);
//                    }
//                }
//            }
        }

        $totalPurchase = 0;
        foreach ($items as $item) {
            $totalPurchase += $item->getTotalPrice();
        }

        $currentDiscount = 0;

        foreach ($cart->getItems(CartItemDiscountInterface::class) as $item) {
            /**
             * @var CartItemDiscountInterface $item
             */
            if ($item->getUniqueId() === $this->getUniqueId()) {
                break;
            }
            $currentDiscount += $item->calculateDiscount();
        }

        $discount = (int) $this->discount_amount ?: $this->rate * $totalPurchase;
        $discount = (int) min($totalPurchase - $currentDiscount, $discount);

        return $discount;
    }

    public function getLabel()
    {
        if ($this->rate) {
            return Yii::t('phycom/base/discount-rule', 'Discount {rate}', ['rate' => f::percent($this->rate)]);
        }
        return Yii::t('phycom/base/discount-rule', 'Discount');
    }

    /**
     * @param int $length
     * @param string $prefix
     * @param string $suffix
     * @return string
     */
    public function generateCode($length = null, $prefix = null, $suffix = null)
    {
        if (!$prefix && $this->type->is(DiscountRuleType::CLIENT_CARD)) {
            $prefix = 'CC-';
        }
        if (!$prefix && $this->type->is(DiscountRuleType::COUPON)) {
            $prefix = 'C-';
        }
        if (!$prefix && $this->type->is(DiscountRuleType::VOUCHER)) {
            $prefix = 'V-';
        }
        $coupon = new Coupon();
        $coupon->prefix = $prefix;
        $coupon->useNumbers = true;
        $coupon->useLetters = true;
        return $coupon->generateOne($length ?: Yii::$app->commerce->promoCodes->defaultCouponCodeLength);
    }

    public function getRate()
    {
        switch ((string)$this->type) {
            case DiscountRuleType::COUPON:
                return $this->discount_rate;
            case DiscountRuleType::CLIENT_CARD:
                return Yii::$app->commerce->clientCards->useBirthdayDiscount && $this->isBirthdayPeriod
                    ? $this->birthday_discount_rate
                    : $this->discount_rate;
            default:
                return null;
        }
    }

    public function softDelete(): bool
    {
        if ($this->status->is(DiscountRuleStatus::DELETED)) {
            return true;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->updateStatus(DiscountRuleStatus::DELETED, false);
            $code = $this->code . '_' . Yii::$app->security->generateRandomString(16);
            if (strlen($code) > 255) {
               $code = substr($code, 0, 255);
            }
            $this->code = $code;
            if ($this->save()) {
                $transaction->commit();
                return true;
            }
            $transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function getIsBirthdayPeriod(): bool
    {
        if (!$this->birthday) {
            return false;
        }
        $birthdayWeek = $this->birthday->format('W');
        $currentWeek = (new \DateTime())->format('W');
        return $currentWeek === $birthdayWeek;
    }

    /**
     * Checks if the given model is eligible for discount
     * @return bool
     * @throws \Exception
     */
    public function getIsValid()
    {
        if (!$this->status->is(DiscountRuleStatus::ACTIVE)) {
            return false;
        }
        if ($this->type->in([DiscountRuleType::COUPON, DiscountRuleType::VOUCHER]) && $this->used >= 1) {
            $this->updateStatus(DiscountRuleStatus::CLOSED);
            return false;
        }
        if ($this->expires_at && $this->expires_at < new \DateTime()) {
            $this->updateStatus(DiscountRuleStatus::CLOSED);
            return false;
        }
        if ($this->starts_at && $this->starts_at > new \DateTime()) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function getIsGlobal()
    {
        return !$this->shop_id && !$this->vendor_id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(DiscountRuleCategory::class, ['discount_rule_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return CartInterface|object
     * @throws yii\base\InvalidConfigException
     */
    protected function getCart() : CartInterface
    {
        return yii\di\Instance::ensure('cart', CartInterface::class);
    }
}
