<?php

namespace Phycom\Base\Models;


use Phycom\Base\Models\Attributes\UnitType;

use Phycom\Base\Models\Product\Product;
use yii\base\Model;

/**
 * Class OrderLine
 *
 * @package Phycom\Base\Models
 */
class OrderLine extends Model
{
    const TYPE_DEFAULT = 'default';
    const TYPE_VARIANT = 'variant';

    /**
     * @var string
     */
    public string $type = self::TYPE_DEFAULT;

    /**
     * @var string
     */
    public string $id;

    /**
     * @var string|null
     */
    public ?string $parentId;

    /**
     * @var int|null
     */
    public ?int $internalId;

    /**
     * @var Product|null
     */
    public ?Product $product = null;

    /**
     * @var string
     */
    public string $code;

    /**
     * @var string
     */
    public string $title;

    /**
     * @var int
     */
    public int $quantity = 1;

    /**
     * @var int
     */
    public int $price;

    /**
     * @var bool
     */
    public bool $unitPrice = false;

    /**
     * @var int|float|null
     */
    public $numUnits = null;

    /**
     * @var UnitType|null
     */
    public ?UnitType $unitType = null;

    /**
     * @var int
     */
    public $discount = 0;

    /**
     * @var array
     */
    public array $options = [];

    /**
     * @return int
     */
    public function getSubtotal(): int
    {
        if ($this->unitPrice) {
            $totalBeforeDiscount = (string)($this->price * $this->numUnits * $this->quantity);
        } else {
            $totalBeforeDiscount = (string)($this->price * $this->quantity);
        }
        return (int) bcmul((string)(1 - $this->discount), $totalBeforeDiscount, 4);
    }

    /**
     * @return string
     */
    public function printOptions(): string
    {
        return implode(', ', array_values($this->options));
    }
}
