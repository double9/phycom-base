<?php

namespace Phycom\Base\Models;

use yii;

/**
 * This is the model class for table "currency".
 *
 * @property string $code
 * @property string $name
 * @property string $symbol
 */
class Currency extends ActiveRecord
{
	public function behaviors()
	{
		return [];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code'], 'string', 'max' => 3],
            [['name', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('phycom/base/main', 'Code'),
            'name' => Yii::t('phycom/base/main', 'Name'),
            'symbol' => Yii::t('phycom/base/main', 'Symbol'),
        ];
    }
}
