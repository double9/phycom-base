<?php

namespace Phycom\Base\Models\Traits;


use Phycom\Base\Models\ModuleSettingsForm;

use Yii;

/**
 * Trait ModuleSettingsTrait
 * @package Phycom\Base\Models\Traits
 *
 * @property bool $useSettings
 * @property-read ModuleSettingsForm $settings
 */
trait ModuleSettingsTrait
{
    private bool $useSettings = false;

    private $settings;

    /**
     * @return string|array|callable
     */
    abstract public function getSettingsForm();


    public function init()
    {
        $this->initSettings();
        parent::init();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function initSettings()
    {
        if ($this->useSettings) {
            $this->getSettings()->populateModule();
        }
    }

    /**
     * @param bool $value
     */
    public function setUseSettings(bool $value)
    {
        $this->useSettings = $value;
    }

    /**
     * @return bool
     */
    public function getUseSettings() : bool
    {
        return $this->useSettings;
    }

    /**
     * @return ModuleSettingsForm|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getSettings()
    {
        if ($this->useSettings && !$this->settings) {
            /**
             * @var ModuleSettingsForm $settings
             */
            $settings = Yii::createObject($this->getSettingsForm(), [$this]);
            $this->settings = $settings->populate();
        }
        return $this->settings;
    }
}
