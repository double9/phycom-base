<?php

namespace Phycom\Base\Models\Traits;

use yii;

/**
 * Class ApplicationFactoryTrait
 * For bootstrapping correct application code
 *
 * NOTE: properties here used for IDE code auto-completion for both common Web and Console application:
 *
 *
 * @property-read \Phycom\Base\Models\User $systemUser
 *
 * @property-read \Phycom\Base\Components\Site $site
 * @property-read \Phycom\Base\Components\ModelFactory $modelFactory
 * @property-read \Phycom\Base\Components\AuthManager $authManager
 * @property-read \Phycom\Base\Components\Formatter $formatter
 * @property-read \Phycom\Base\Components\User $user
 * @property-read \Phycom\Base\Components\FileStorage $fileStorage
 * @property-read \Phycom\Base\Components\Queue\Beanstalk $queue1
 * @property-read \Phycom\Base\Components\Queue\Beanstalk $queue2
 * @property-read \Phycom\Base\Components\LanguageManager $lang
 * @property-read \Phycom\Base\Components\Country $country
 * @property-read \Phycom\Base\Components\PageSpaceCollection $pages
 * @property-read \Phycom\Base\Components\PageSpaceCollection $landingPages
 * @property-read \Phycom\Base\Components\Commerce\Commerce $commerce
 * @property-read \Phycom\Base\Components\Admin $admin
 * @property-read \Phycom\Base\Components\Blog $blog
 * @property-read \Phycom\Base\Components\Review $reviews
 * @property-read \Phycom\Base\Components\Subscription $subscription
 * @property-read \Phycom\Base\Components\Message $messages
 * @property-read \Phycom\Base\Components\Comment $comments
 * @property-read \Phycom\Base\Components\PartnerContract $partnerContracts
 * @property-read \Phycom\Base\Components\Geocoder $geocoder
 * @property-read \Phycom\Base\Models\Vendor $vendor
 *
 * @property-read yii\i18n\Locale $locale
 *
 * @property-read \yii\web\UrlManager $urlManagerFrontend The URL manager for this application.
 * @property-read \yii\web\UrlManager $urlManagerBackend The URL manager for this application.
 */
trait ApplicationFactoryTrait
{
    public $shortName;

    public $frameworkName;

    public $frameworkShortName;

    public $vendorName;

    public $vendorId;

    private $namespace = 'Phycom';

    private $_vendor;

    private $_systemUser;

    private $_settings = [];

    /**
     * @return \Phycom\Base\Models\User|null
     */
    public function getSystemUser()
    {
        if (!$this->_systemUser) {
            $this->_systemUser = $this->modelFactory->getUser()::findOne(['username' => \yii\helpers\Inflector::slug($this->name) . '_system']);
        }
        return $this->_systemUser;
    }

    public function getVendor()
    {
        if (!$this->_vendor) {
            if ($this->vendorId) {
                $this->_vendor = \Phycom\Base\Models\Vendor::findOne(['id' => $this->vendorId, 'main' => true]);
            } elseif ($this->vendorName) {
                $this->_vendor = \Phycom\Base\Models\Vendor::findOne(['name' => $this->vendorName, 'main' => true]);
            }
            if (!$this->_vendor) {
                $this->_vendor = \Phycom\Base\Models\Vendor::findOne(['name' => $this->name, 'main' => true]);
            }
        }
        return $this->_vendor;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getSetting($key)
    {
        if (!isset($this->_settings[$key])) {
            $setting = \Phycom\Base\Models\Setting::findOne(['key' => $key]);
            $this->_settings[$key] = $setting ? $setting->value : null;
        }
        return $this->_settings[$key];
    }

    public function unsetSettings()
    {
        $this->_settings = [];
    }

    public function createControllerByID($id)
    {
        $pos = strrpos($id, '/');
        if ($pos === false) {
            $prefix = '';
            $className = $id;
        } else {
            $prefix = substr($id, 0, $pos + 1);
            $className = substr($id, $pos + 1);
        }

        if (!preg_match('%^[a-z][a-z0-9\\-_]*$%', $className)) {
            return null;
        }
        if ($prefix !== '' && !preg_match('%^[a-z0-9_/]+$%i', $prefix)) {
            return null;
        }

        $className = str_replace(' ', '', ucwords(str_replace('-', ' ', $className))) . 'Controller';
        $className = ltrim($this->controllerNamespace . '\\' . str_replace('/', '\\', $prefix)  . $className, '\\');
        $originalControllerNamespace = $this->controllerNamespace;

        if (strpos($className, '-') !== false || !class_exists($className)) {
            // check if this is already base namespace
            $namespaceTokens = explode('\\', $className);
            if ($namespaceTokens[0] === $this->namespace || ($namespaceTokens[0] === '' && $namespaceTokens[1] === $this->namespace)) {
                return null;
            }
            // in not then fallback to base namespace
            $className = $this->namespace . '\\' . $className;
            $this->controllerNamespace = $this->namespace . '\\' . $this->controllerNamespace;

            if (strpos($className, '-') !== false || !class_exists($className)) {
                $this->controllerNamespace = $originalControllerNamespace;
                return null;
            }
        }

        if (is_subclass_of($className, 'yii\base\Controller')) {
            return Yii::createObject($className, [$id, $this]);
        } elseif (YII_DEBUG) {
            throw new \yii\base\InvalidConfigException("Controller class must extend from \\yii\\base\\Controller.");
        } else {
            return null;
        }
    }

    public function setBasePath($path)
    {
        parent::setBasePath($path);
        $basePath = Yii::getAlias('@' . $this->namespace) . DIRECTORY_SEPARATOR . basename($path);
        Yii::setAlias('@base-app', $basePath);
    }
}
