<?php

namespace Phycom\Base\Models\Traits;

use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Widgets\FlashAlert;

use yii\helpers\ArrayHelper;
use yii\base\InvalidArgumentException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii;

/**
 * Trait WebControllerTrait
 * @package Phycom\Base\Models\Traits
 */
trait WebControllerTrait
{
	/**
	 * @param null $message
	 * @throws \yii\web\ForbiddenHttpException
	 */
	public function denyAccess($message = null)
	{
		$message = $message ?: Yii::t('phycom/base/error', 'You are not allowed to perform this action.');
		throw new ForbiddenHttpException($message);
	}

	/**
	 * @param string $permission
	 * @param array $params
     * @throws ForbiddenHttpException
	 */
	protected function checkPermission($permission, $params = []){
		if (!Yii::$app->user->can($permission, $params)) {
			$this->denyAccess();
		}
	}

	/**
	 * Check if the request is ajax request
	 * @param string $responseFormat
	 * @throws NotFoundHttpException
	 */
	protected function ajaxOnly($responseFormat = null)
	{
		if (!Yii::$app->request->isAjax) {
			Yii::error('Non-ajax request for ajax action encountered. ' . Yii::$app->request->absoluteUrl, 'invalid-request');
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		if ($responseFormat) {
			Yii::$app->response->format = $responseFormat;
		}
	}

    /**
     * @param null $errors
     * @param array $params
     * @param array $alertOptions
     * @return array
     * @throws \Exception
     */
	protected function ajaxError($errors = null, array $params = [], array $alertOptions = [])
	{
		if ($errors && is_string($errors)) {
			$errors = [$errors];
		}
		if ($errors) {
			FlashMsg::error($errors);
		}
		return ArrayHelper::merge(['error' => FlashAlert::widget($alertOptions)], $params);
	}

    /**
     * @param array $response
     * @param array $alertOptions
     * @return array
     * @throws \Exception
     */
	protected function ajaxSuccess($response = [], array $alertOptions = [])
	{
		if (is_string($response)) {
			$response = [$response];
		}
		return ArrayHelper::merge(['msg' => FlashAlert::widget($alertOptions)], $response);
	}
}
