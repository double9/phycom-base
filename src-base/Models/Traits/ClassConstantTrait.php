<?php

namespace Phycom\Base\Models\Traits;

use Phycom\Base\Helpers\Constant;
/**
 * Class ClassConstantTrait
 * @package Phycom\Base\Models\Traits
 */
trait ClassConstantTrait
{
    /**
     * Return array of constants for a class
     *
     * @param null|string $prefix Prefix like e.g. "KEY_"
     * @param array       $exclude Array of excluding values
     * @param boolean     $assoc  Return associative array with constant name as key
     *
     * @return array Assoc array of constants
     */
    public static function getConstants($prefix = null, $exclude = [], $assoc = false)
    {
        return Constant::getClassConstants(get_called_class(), $prefix, $exclude, $assoc);
    }

}
