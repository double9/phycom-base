<?php
namespace Phycom\Base\Models\Traits;


use Phycom\Base\Components\ActiveQuery;

use yii\base\Exception;

/**
 * Class SearchQueryFilter
 * @package Phycom\Base\Models\Traits
 */
trait SearchQueryFilter
{
	/**
	 * @return ActiveQuery
     * @throws Exception
	 */
	public static function find()
	{
		return \Yii::createObject(ActiveQuery::class, [get_called_class()]);
	}
}
