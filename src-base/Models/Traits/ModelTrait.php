<?php

namespace Phycom\Base\Models\Traits;


use yii\db\Transaction;

/**
 * Trait ModelTrait
 * @package Phycom\Base\Models\Traits
 *
 * @property-read string|null $lastError
 * @property-read array $globalErrors
 */
trait ModelTrait
{
    /**
     * @var string - default error attribute name used
     */
    protected $defaultErrorAttribute = 'error';

	public function getLastError()
	{
		$errors = array_values($this->getFirstErrors());
		if (!empty($errors)) {
			return end($errors);
		}
		return null;
	}

    /**
     * @return bool
     */
	public function hasGlobalErrors()
    {
        foreach ($this->getErrors() as $attribute => $errors) {
            if (!$attribute || $attribute === $this->defaultErrorAttribute) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getGlobalErrors()
    {
        $globalErrors = [];
        foreach ($this->getErrors() as $attribute => $errors) {
            if (!$attribute || $attribute === $this->defaultErrorAttribute) {
                $globalErrors = array_merge($globalErrors, $errors);
            }
        }
        return $globalErrors;
    }


	/**
	 * Add underlying model errors to this model. So that the error messages could be shown in UI
	 *
	 * @param array $errors
	 * @param string|null $attribute - error attribute name
	 */
	protected function setErrors(array $errors, $attribute = null)
	{
	    if ($attribute === null) {
	        $attribute = $this->defaultErrorAttribute;
        }
		foreach ($errors as $errorAttribute => $errorMessages) {
			foreach ($errorMessages as $message) {
				$this->addError($attribute, $message);
			}
		}
	}

    /**
     * Rollback the transaction with error message(s)
     *
     * @param Transaction $transaction
     * @param array $errors
     * @param string|null $attribute - error attribute name
     * @return bool
     */
	protected function rollback(Transaction $transaction, array $errors = [], $attribute = null)
	{
		$this->setErrors($errors, $attribute);
		$transaction->rollBack();
		return false;
	}
}
