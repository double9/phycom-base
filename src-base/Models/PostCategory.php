<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\TranslationStatus;
use Phycom\Base\Models\Behaviors\Sortable;
use Phycom\Base\Models\Attributes\CategoryStatus;
use Phycom\Base\Models\Traits\ModelTranslationTrait;
use Phycom\Base\Models\Translation\PostCategoryTranslation;

use Phycom\Base\Models\Translation\Translation;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "post_category".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property integer $parent_id
 * @property CategoryStatus $status
 * @property bool $featured
 * @property integer $order
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property PostCategory $parent
 * @property PostCategory[] $children
 * @property Shop $shop
 * @property User $createdBy
 * @property PostCategory[] $relatedCategories
 * @property PostCategoryTranslation[] $translations
 * @property post[] $posts
 * @property post[] $publishedPosts
 *
 * @method insertBefore(PostCategory $target)
 * @method appendTo(PostCategory $target)
 * @method insertAfter(PostCategory $target)
 */
class PostCategory extends ActiveRecord
{
    use ModelTranslationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_category';
    }

    /**
     * @param string urlKey
     * @return static|null
     */
    public static function findByUrlKey(string $urlKey)
    {
        if (!strlen($urlKey)) {
            throw new yii\base\InvalidArgumentException('Url key cannot be empty');
        }
        $query = static::find()
            ->select('c.*')
            ->from(['ct' => PostCategoryTranslation::tableName()])
            ->where(['ct.url_key' => $urlKey])
            ->innerJoin(['c' => static::tableName()], [
                'and',
                'c.id = ct.post_category_id',
                ['not', ['c.status' => CategoryStatus::DELETED]]
            ]);

        return $query->one();
    }


	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => CategoryStatus::class
				]
			],
			'sortable' => ['class' => Sortable::class],
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'created_by', 'order'], 'integer'],
            [['status'], 'required'],
            [['featured'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['created_at', 'updated_at', 'status', 'featured'], 'safe'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('phycom/base/main', 'ID'),
            'shop_id' => Yii::t('phycom/base/main', 'Shop ID'),
            'parent_id' => Yii::t('phycom/base/main', 'Parent ID'),
            'status' => Yii::t('phycom/base/main', 'Status'),
            'featured' => Yii::t('phycom/base/main', 'Featured'),
	        'order' => Yii::t('phycom/base/main', 'Order'),
            'created_by' => Yii::t('phycom/base/main', 'Created By'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function delete()
    {
	    $this->status = CategoryStatus::DELETED;
	    return $this->save();
    }

    /**
     * @param string $languageCode
     * @param bool $fallback
     * @return PostCategoryTranslation|Translation
     */
    public function getTranslation($languageCode = null, $fallback = true)
    {
        return $this->getTranslationModel(PostCategoryTranslation::class, $languageCode, $fallback);
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
	public function getTitle($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->title : null;
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
    public function getDescription($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->description : null;
    }

    /**
     * @param null $languageCode
     * @return string|null
     */
    public function getUrlKey($languageCode = null)
    {
        $translation = $this->getTranslation($languageCode);
        return $translation ? $translation->url_key : null;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::class, ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(PostCategory::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedCategories()
    {
        return $this->hasMany(PostCategory::class, ['id' => 'category_id'])->viaTable('post_category_relation', ['related_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PostCategoryTranslation::class, ['post_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::class, ['id' => 'post_id'])
	        ->viaTable('post_in_post_category', ['category_id' => 'id'])
	        ->andWhere(['not', ['post.status' => PostStatus::DELETED]]);
    }

    public function getPublishedPosts()
    {
    	return $this->getPosts()
		    ->andWhere(['post.status' => PostStatus::PUBLISHED])
		    ->orderBy(['post.created_at' => SORT_DESC]);
    }
}
