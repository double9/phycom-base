<?php

namespace Phycom\Base\Models;

use yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class Setting extends ActiveRecord
{
    /**
     * @param string $prefix
     * @param callable|null $parseKeys
     * @return Setting[]
     */
    public static function findByKeyPrefix(string $prefix, callable $parseKeys = null): array
    {
        $result = [];
        /**
         * @var Setting[] $settings
         */
        $settings = static::find()->where(new yii\db\Expression("key like :prefix", [
            'prefix' => $prefix . '%'
        ]))->all();

        if (!empty($settings)) {
            foreach ($settings as $model) {
                $key = $parseKeys ? $parseKeys($model->key) : $model->key;
                $result[$key] = $model->value;
            }
        }

        return $result;
    }

	/**
	 * @param mixed $key
	 * @param null $defaultValue
	 * @return null|string
	 */
	public static function get($key, $defaultValue = null)
	{
		/**
		 * @var Setting $model
		 */
		$model = static::findByKey($key);
		return $model ? $model->value : $defaultValue;
	}

	/**
	 * @param mixed $key
	 * @param string $value
	 * @return static
	 */
	public static function set($key, $value)
	{
		$model = static::findByKey($key);
		if (!$model) {
			$model = new static;
			$model->key = $key;
		}
		$model->value = $value;
		$model->save();
		return $model;
	}

	/**
	 * @param $key
	 * @return static
	 */
	public static function findByKey($key)
	{
		return static::find()->where(['key' => $key])->one();
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('phycom/base/main', 'ID'),
            'key'        => Yii::t('phycom/base/main', 'Key'),
            'value'      => Yii::t('phycom/base/main', 'Value'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }
}
