<?php

namespace Phycom\Base\Models;

use Phycom\Base\Components\FileStorage;
use Phycom\Base\Helpers\c;
use Phycom\Base\Helpers\Checksum731;
use Phycom\Base\Jobs\InvoicePdfJob;
use Phycom\Base\Models\Attributes\InvoiceStatus;
use Phycom\Base\Models\Attributes\CustomerType;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Attributes\PaymentStatus;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property string $number
 * @property integer $order_id
 * @property InvoiceStatus $status
 * @property string $file
 * @property string $checksum
 * @property string $customer
 * @property CustomerType $customer_type
 * @property string $reg_no
 * @property array $address
 * @property string $notes
 * @property \DateTime $created_at
 * @property \DateTime $due_date
 *
 * @property int $total
 * @property int $totalPaid
 * @property Order $order
 * @property Payment[] $payments
 * @property Payment $lastPendingPayment
 * @property Shipment[] $shipments
 * @property InvoiceItem[] $items
 * @property OrderItem[] $orderItems
 * @property string $fileUrl
 * @property bool $isPaid
 */
class Invoice extends ActiveRecord
{
    const EVENT_AFTER_INVOICE_CREATED = 'afterInvoiceCreated';
    const EVENT_AFTER_INVOICE_FILE_CREATED = 'afterInvoiceFileCreated';

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => InvoiceStatus::class,
					'customer_type' => CustomerType::class
				]
			],
            'timestamp' => [
                'attributes' => [
                    ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
                    ['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'due_date'
                ]
            ]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['order_id'], 'integer'],
            [['status', 'customer', 'customer_type', 'order_id'], 'required'],
            [['file', 'notes'], 'string'],
            [['address', 'created_at', 'due_date', 'status', 'customer_type'], 'safe'],
            [['number', 'customer', 'reg_no', 'checksum'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrder()), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('phycom/base/main', 'ID'),
            'number'        => Yii::t('phycom/base/main', 'Number'),
            'order_id'      => Yii::t('phycom/base/main', 'Order ID'),
            'status'        => Yii::t('phycom/base/main', 'Status'),
            'file'          => Yii::t('phycom/base/main', 'File'),
            'checksum'      => Yii::t('phycom/base/main', 'Checksum'),
            'customer'      => Yii::t('phycom/base/main', 'Customer'),
            'customer_type' => Yii::t('phycom/base/main', 'Customer Type'),
            'reg_no'        => Yii::t('phycom/base/main', 'Reg No'),
            'notes'         => Yii::t('phycom/base/main', 'Notes'),
            'address'       => Yii::t('phycom/base/main', 'Address'),
            'created_at'    => Yii::t('phycom/base/main', 'Created At'),
            'due_date'      => Yii::t('phycom/base/main', 'Due date'),
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INVOICE_CREATED, function ($e) {
            /**
             * @var InvoicePdfJob|object $job
             */
            $job = Yii::createObject([
                'class'         => InvoicePdfJob::class,
                'invoiceNumber' => $this->number
            ]);

            $jobId = Yii::$app->queue2->delay(1)->push($job);
            Yii::info('Invoice pdf job ' . $jobId . ' queued', 'invoice');
        });
    }

    /**
     * @return string|null
     */
    public function getFullFilePath(): ?string
    {
        if ($this->file) {
            return Yii::getAlias(Yii::$app->commerce->order->invoiceFilePath . '/' . $this->file);
        }
        return null;
    }

    /**
     * Calculates checksum for the invoice
     * This is mainly to determine if invoice file generated needs to be been changed or not
     *
     * @return string
     */
    public function calculateChecksum(): string
    {
        $items = [];
        foreach ($this->items as $item) {
            $itemData = [
                (string) $item->orderItem->id,
                (string) $item->orderItem->code,
                Json::encode($item->orderItem->product_attributes),
                Json::encode($item->orderItem->meta),
                $item->quantity == $item->orderItem->quantity ? '1' : '0',
                (string) $item->orderItem->price,
                (string) $item->orderItem->discount
            ];
            $items[] = implode('', $itemData);
        }

        $shipments = [];
        foreach ($this->shipments as $shipment) {
            $itemData = [
                (string) $shipment->method,
                $shipment->carrier_area ?: '',
                Json::encode($shipment->to_address),
                $shipment->delivery_date ? $shipment->delivery_date->format('Ymd') : '',
                $shipment->delivery_time ?: ''
            ];
            $shipments[] = implode('', $itemData);
        }

        $string = implode($items) . '-' . implode($shipments);
        Yii::debug('Invoice checksum str: ' . $string, __METHOD__);
        return md5($string);
    }


    /**
	 * Creates the pdf invoice document
	 * TODO
	 */
    public function createFile()
    {

    }

	/**
	 * Sends the invoice to user email
	 * TODO
	 */
    public function send()
    {

    }

    /**
     * @param mixed $status
     * @return bool
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
	public function updateStatus($status)
	{
		$this->status = (string) $status;
		if (!$this->update(true, ['status'])) {
			Yii::error('Error updating invoice ' . $this->id . ' status: ' . json_encode($this->errors), __METHOD__);
			return false;
		}
		return true;
	}

	public function generateNumber()
	{
        Yii::debug('Generating invoice number', __METHOD__);
		if (!$this->isNewRecord) {
			throw new yii\base\InvalidCallException('Can\'t generate invoice number for existing invoice ' . $this->id);
		}
		if ($lastInvoice = Yii::$app->modelFactory->getInvoice()::find()->orderBy(['id' => SORT_DESC])->one()) {
			/**
			 * @var Invoice $lastInvoice
			 */
			$lastInvoiceNr = (int) substr($lastInvoice->number, 5, -1);
		} else {
			$lastInvoiceNr = c::param('invoiceNoBegin');
		}
		$today = new \DateTime();
		$invoiceNr = $today->format('ym') . '-' . (string)($lastInvoiceNr + 1);
		$this->number = $invoiceNr . Checksum731::calculate($invoiceNr);
	}

	public function getIsPaid()
	{
		return $this->status->is(InvoiceStatus::PAID);
	}

	/**
	 * Checks if invoice has been paid and updates invoice status
	 */
	public function checkPaid()
	{
		if (!$this->isPaid) {
			unset($this->payments);
			$totalPaid = $this->getTotalPaid();
			if ($totalPaid >= $this->getTotal()) {
				$this->updateStatus(InvoiceStatus::PAID);
				$order = $this->order;
				$order->status = OrderStatus::PAYMENT_COMPLETE;
				$order->paid_at = new \DateTime();
				Yii::info('Update order ' . $order->number . ' status ' . OrderStatus::PAYMENT_COMPLETE . ' on ' . __METHOD__, 'order');
				$order->update();
			}
		}
	}

	public function getTotalPaid()
	{
		$paid = 0;
		/**
		 * @var Payment[] $payments
		 */
		$payments = $this->getPayments()->where(['status' => PaymentStatus::COMPLETED])->all();
		foreach ($payments as $payment) {
			$paid += $payment->amount;
		}
		return $paid;
	}

	public function getTotal()
	{
		$total = 0;
		foreach ($this->items as $line) {
			$total += $line->orderItem->total;
		}
		return $total;
	}

    /**
     * @param string $paymentMethod
     * @return array|Payment|null|yii\db\ActiveRecord
     * @throws \Throwable
     * @throws yii\base\Exception
     * @throws yii\db\StaleObjectException
     */
	public function createPayment($paymentMethod)
	{
		if ($this->isNewRecord) {
			throw new yii\base\InvalidCallException('Error creating payment. Invoice must be saved first');
		}
		if ($this->isPaid) {
			throw new yii\base\InvalidCallException('Error creating payment. Invoice is already paid');
		}
		/**
		 * the amount needed to be paid when subtracting other payments
		 */
		$amount = max(0, $this->total - $this->totalPaid);


		if ($payment = $this->getLastPendingPayment()) {
			$payment->payment_method = $paymentMethod;
			$payment->amount = $amount;
			$payment->update();
			return $payment;
		}

		$payment = Yii::$app->modelFactory->getPayment();
		$payment->payment_method = $paymentMethod;
		if ($this->order->user_id) {
            $payment->reference_number = (string) $this->order->user->reference_number;
        }
		$payment->invoice_id = $this->id;
		$payment->amount = $amount;
		$payment->status = PaymentStatus::PENDING;

		if (!$payment->save()) {
			throw new yii\base\Exception('Error saving payment: ' . json_encode($payment->errors));
		}

		return $payment;
	}

    /**
     * @param array $config
     * @return OrderLineCollection
     */
	public function getOrderLines(array $config = []): OrderLineCollection
    {
        $orderItems = [];
        foreach ($this->items as $invoiceItem) {
            $orderItem = $invoiceItem->orderItem;
            $orderItem->quantity = $invoiceItem->quantity;
            $orderItems[] = $orderItem;
        }

        return Yii::$app->modelFactory->getOrderLineCollection($orderItems, $config);
    }



	public function getLastPendingPayment()
	{
		return $this->getPayments()->where(['status' => PaymentStatus::PENDING])->orderBy(['id' => SORT_DESC])->one();
	}

	/**
	 * @return string
	 */
	public function getFileUrl()
	{
		return Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_INVOICES)->getFileUrl($this->file);
	}

    /**
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrder()), ['id' => 'order_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getPayment()), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getShipments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipment()), ['order_id' => 'id'])->viaTable('order', ['id' => 'order_id']);
    }

	/**
	 * @return ActiveQuery
	 */
	public function getItems()
	{
		return $this->hasMany(InvoiceItem::class, ['invoice_id' => 'id']);
	}

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id'])->viaTable('invoice_item', ['invoice_id' => 'id']);
    }
}
