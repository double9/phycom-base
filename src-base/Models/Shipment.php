<?php

namespace Phycom\Base\Models;

use Phycom\Base\Events\StatusUpdateEvent;
use Phycom\Base\Helpers\f;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Attributes\ShipmentStatus;
use Phycom\Base\Models\Attributes\ShipmentType;
use Phycom\Base\Models\Behaviors\DateBehavior;
use Phycom\Base\Models\Behaviors\TimestampBehavior;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Models\CarrierToken;

use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\Label\NoLabelStrategy;
use Phycom\Base\Modules\Delivery\Models\Label\PostageLabel;

use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii;

/**
 * This is the model class for table "shipment".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $method - Delivery module method id
 *
 * @property string $shipment_id - shipment id in delivery provider system
 * @property string $rate_id - shipping rate id in delivery provider system
 * @property string $carrier_name
 * @property string $carrier_service
 * @property string $carrier_area
 * @property string $carrier_delivery_days
 * @property string $duration_terms
 *
 * @property \DateTime $delivery_date
 * @property string $delivery_time
 *
 * @property array $from_address
 * @property array $to_address
 * @property array $return_address
 *
 * @property string $transaction_id - shipping label purchase id in delivery provider system
 * @property array $postage_label
 * @property string $tracking_number
 * @property string $tracking_status
 * @property string $tracking_url
 * @property \DateTime $dispatch_at - time (estimate) when the parcel should be collected by carrier
 * @property \DateTime $eta
 * @property \DateTime $original_eta
 *
 * @property ShipmentStatus $status
 * @property ShipmentType $type
 * @property \DateTime $shipped_at - time when the parcel was shipped by carrier
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Order $order
 * @property ShipmentItem[] $shipmentItems
 * @property OrderItem[] $orderItems
 * @property-read string $label
 * @property-read string $carrierNameLabel
 * @property-read bool $isDelivery
 * @property-read DeliveryArea $deliveryArea
 * @property-read AddressField $toAddress
 * @property-read bool $isDeliveryAddressSaved
 * @property-read DeliveryMethodInterface|DeliveryMethod|null $deliveryMethod
 * @property-read array|null $postageLabelData
 * @property-read string $recipientName
 * @property-read string $recipientCompany
 * @property-read string $recipientEmail
 * @property-read string $recipientPhone
 * @property-read string $estimatedArrival
 *
 * @property-read array $messages
 *
 */
class Shipment extends ActiveRecord
{
	const EVENT_AFTER_STATUS_UPDATE = 'afterStatusUpdate';

	protected $messages = [];
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'shipped_at',
                    'dispatch_at',
                    'eta',
                    'original_eta'
				]
			],
			'date' => [
			    'class' => DateBehavior::class,
                'attributes' => ['delivery_date']
            ],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ShipmentStatus::class,
					'type' => ShipmentType::class
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'status', 'type'], 'required'],
            [['order_id', 'carrier_delivery_days'], 'integer'],
            [['duration_terms'], 'string'],
            [['from_address', 'to_address', 'return_address', 'postage_label', 'shipped_at', 'created_at', 'updated_at', 'status', 'eta', 'original_eta', 'dispatch_at', 'delivery_date', 'type'], 'safe'],
            [[
                'tracking_number',
                'tracking_status',
                'tracking_url',
                'method',
                'carrier_name',
                'carrier_service',
                'carrier_area',
                'shipment_id',
                'rate_id',
                'transaction_id',
                'delivery_time'
                ],
                'string', 'max' => 255
            ],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrder()), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => Yii::t('phycom/base/main', 'ID'),
            'order_id'              => Yii::t('phycom/base/main', 'Order ID'),
            'method'                => Yii::t('phycom/base/main', 'Delivery method'),
            'shipment_id'           => Yii::t('phycom/base/main', 'Delivery method ID'),
            'rate_id'               => Yii::t('phycom/base/main', 'Rate ID'),
            'transaction_id'        => Yii::t('phycom/base/main', 'Transaction ID'),
            'carrier_name'          => Yii::t('phycom/base/main', 'Carrier'),
            'carrier_service'       => Yii::t('phycom/base/main', 'Carrier Service'),
            'carrier_area'          => Yii::t('phycom/base/main', 'Area'),
            'carrier_delivery_days' => Yii::t('phycom/base/main', 'Delivery Days'),
            'postage_label'         => Yii::t('phycom/base/main', 'Postage label'),
            'tracking_number'       => Yii::t('phycom/base/main', 'Tracking Number'),
            'tracking_status'       => Yii::t('phycom/base/main', 'Tracking Status'),
            'tracking_url'          => Yii::t('phycom/base/main', 'Tracking Url'),
            'dispatch_at'           => Yii::t('phycom/base/main', 'Dispatch At'),
            'delivery_date'         => Yii::t('phycom/base/main', 'Delivery Date'),
            'delivery_time'         => Yii::t('phycom/base/main', 'Delivery Time'),
            'eta'                   => Yii::t('phycom/base/main', 'Current ETA'),
            'original_eta'          => Yii::t('phycom/base/main', 'Original ETA'),
            'from_address'          => Yii::t('phycom/base/main', 'Sender Address'),
            'to_address'            => Yii::t('phycom/base/main', 'Destination Address'),
            'return_address'        => Yii::t('phycom/base/main', 'Return Address'),
            'status'                => Yii::t('phycom/base/main', 'Status'),
            'type'                  => Yii::t('phycom/base/main', 'Type'),
            'duration_terms'        => Yii::t('phycom/base/main', 'Duration terms'),
            'shipped_at'            => Yii::t('phycom/base/main', 'Shipped At'),
            'created_at'            => Yii::t('phycom/base/main', 'Created At'),
            'updated_at'            => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(self::EVENT_AFTER_STATUS_UPDATE, function ($event) {
		    /**
		     * @var StatusUpdateEvent $event
		     */
		    switch ((string) $this->status) {
			    case ShipmentStatus::DISPATCHED:
			    	if (!$this->order->status->is(OrderStatus::SHIPPED)) {
			    		$this->order->updateStatus(OrderStatus::SHIPPED);
				    }
				    break;
			    case ShipmentStatus::DELIVERED:
			    	if ($this->order->isPaid() && $this->order->shipmentsDelivered()) {
			    		$this->order->updateStatus(OrderStatus::COMPLETE);
				    } else if (!$this->order->status->is(OrderStatus::SHIPPED)) {
					    $this->order->updateStatus(OrderStatus::SHIPPED);
				    }
			    	break;
		    }
	    });
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws yii\base\InvalidConfigException
     */
	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		if (in_array('status', array_keys($changedAttributes))) {

            /**
             * @var StatusUpdateEvent|object $event
             */
            $event = Yii::createObject([
                'class'      => StatusUpdateEvent::class,
                'prevStatus' => $changedAttributes['status']
            ]);

			$this->trigger(self::EVENT_AFTER_STATUS_UPDATE, $event);
		}
	}

    /**
     * @return DeliveryArea|null
     */
    public function getDeliveryArea(): ?DeliveryArea
    {
        if (is_numeric($this->rate_id)) {
            return DeliveryArea::findOne(['id' => $this->rate_id]);
        }
        return null;
    }

    /**
     * @return AddressField|object
     * @throws yii\base\InvalidConfigException
     */
    public function getToAddress(): ?AddressField
    {
        return Yii::createObject(AddressField::class, [$this->to_address]);
    }

	public function getRecipientName()
    {
        if (isset($this->to_address['name'])) {
            return $this->to_address['name'];
        }
        if (isset($this->to_address['first_name']) && isset($this->to_address['last_name'])) {
            return $this->to_address['first_name'] . ' ' . $this->to_address['last_name'];
        }
        return null;
    }

    public function getRecipientCompany()
    {
        return $this->to_address['company'] ?? null;
    }

    public function getRecipientEmail()
    {
        return $this->to_address['email'] ?? null;
    }

    public function getRecipientPhone()
    {
        return $this->to_address['phone'] ?? $this->order->phone_code . $this->order->phone_number ?? null;
    }

    public function getPostageLabelData()
    {
        return $this->postage_label;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return PostageLabel|false|null
     */
    public function createPostageLabel()
    {
        if (!$this->postage_label) {
            try {

                $labelStrategy = $this->deliveryMethod->invokeLabelStrategy([
                    'shippingRateId' => $this->rate_id
                ]);

                if ($labelStrategy instanceof NoLabelStrategy) {
                    Yii::warning('Label strategy is not defined for this delivery method!', __METHOD__);
                    return false;
                }

                $label = $labelStrategy->createLabel($this);

                if ($label->status !== $label::STATUS_SUCCESS) {
                    foreach ($label->messages as $message) {
                        $this->addError('postage_label', $message->text);
                    }
                    return false;
                } elseif (!empty($label->messages)) {
                    foreach ($label->messages as $message) {
                        $this->addMessage($message->text);
                    }
                }

                $this->postage_label = $label->export();

                if ($this->save()) {
                    return $label;
                } else {
                    Yii::error('Error saving postage label on shipment ' . $this->id . ' errors: ' . Json::encode($this->errors), __METHOD__);
                }

            } catch (\Exception $e) {
                Yii::error('Error saving postage label on shipment ' . $this->id . ' ' . $e->getMessage() . ' - ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
                Yii::error($e->getTraceAsString(), __METHOD__);
                $this->addError('postage_label', 'Error creating postage label: ' . $e->getMessage());
            }
        }
        return false;
    }

	/**
	 * @param $status
	 * @return bool
     * @throws \Exception|\Throwable in case update failed.
	 */
	public function updateStatus($status)
	{
		$this->status = (string) $status;
		if (!$this->update(true, ['status'])) {
			Yii::error('Error updating shipment ' . $this->id . ' status: ' . Json::encode($this->errors), __METHOD__);
			return false;
		}
		return true;
	}

    public function generateTrackingNumber()
    {
    	$this->tracking_number = Yii::$app->security->generateRandomString();
    }

    /**
     * @return Address|bool
     * @throws InvalidConfigException
     */
    public function getIsDeliveryAddressSaved()
    {
        if ($this->to_address && !empty($this->order->user->addresses)) {
            $deliveryAddress = (string) Address::create($this->to_address)->export();
            foreach ($this->order->user->addresses as $address) {
                if ((string)$address->export() === $deliveryAddress) {
                    return $address;
                }
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getIsDelivery(): bool
    {
        return (string)$this->type === ShipmentType::DELIVERY;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getDeliveryTime()
    {
        $deliveryTime = $this->delivery_date ? clone $this->delivery_date : new \DateTime();
        if ($this->delivery_time) {
            $deliveryTime->setTime((int)substr($this->delivery_time, 0, 2), (int)substr($this->delivery_time, 2, 2));
        }
        return $deliveryTime;
    }

	/**
	 * @return string
	 */
    public function getLabel()
    {
    	return $this->carrier_name ?
            CarrierToken::label($this->carrier_name) :
            ($this->deliveryMethod ? $this->deliveryMethod->getLabel() : $this->method);
    }

    public function getCarrierNameLabel()
    {
        return $this->carrier_name ? CarrierToken::label($this->carrier_name) : null;
    }

    /**
     * @return string
     */
    public function getEstimatedArrival()
    {
        if ($this->eta) {
            return Yii::t('phycom/base/shipment', 'approximately {time}', ['time' => f::datetime($this->eta)]);
        }
        $est = '';
        if ($this->carrier_delivery_days) {
            $est .= f::days($this->carrier_delivery_days);
        }
        if ($this->delivery_time) {
            $est .= (strlen($est) ? ' ' : '')  . Yii::t('phycom/base/shipment', 'at {time}', ['time' => $this->delivery_time]);
        }
        return strlen($est) ? $est : null;
    }

    /**
     * @return DeliveryMethodInterface|DeliveryMethod|yii\base\Module|null
     */
    public function getDeliveryMethod()
    {
        return Yii::$app->getModule('delivery')->getModule($this->method);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrder()), ['id' => 'order_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getShipmentItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipmentItem()), ['shipment_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id'])->viaTable('shipment_item', ['shipment_id' => 'id']);
    }
}
