<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "shop_open".
 *
 * @property integer $id
 * @property integer $shop_id
 * @property int $day_of_week
 * @property \DateTime $opened_at
 * @property \DateTime $closed_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property bool $open
 *
 * @property Shop $shop
 */
class ShopOpen extends ActiveRecord
{

    private $open;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_open';
    }

    public function init()
    {
        parent::init();
        $this->on(ActiveRecord::EVENT_AFTER_FIND, [$this, 'checkClosedDate']);
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE, [$this, 'checkClosedDate']);
        $this->on(ActiveRecord::EVENT_AFTER_INSERT, [$this, 'checkClosedDate']);
    }


    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'opened_at',
					'closed_at'
				]
			]
		]);
	}

	public function checkClosedDate()
    {
        if ($this->closed_at < $this->opened_at) {
            $this->closed_at->add(new \DateInterval('P1D'));
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_id', 'day_of_week', 'opened_at', 'closed_at'], 'required'],
            [['shop_id', 'day_of_week'], 'integer'],
            [['open', 'opened_at', 'closed_at', 'created_at', 'updated_at'], 'safe'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('phycom/base/main', 'ID'),
            'shop_id'     => Yii::t('phycom/base/main', 'Shop ID'),
            'day_of_week' => Yii::t('phycom/base/main', 'Day Of Week'),
            'opened_at'   => Yii::t('phycom/base/main', 'Opened At'),
            'closed_at'   => Yii::t('phycom/base/main', 'Closed At'),
            'created_at'  => Yii::t('phycom/base/main', 'Created At'),
            'updated_at'  => Yii::t('phycom/base/main', 'Updated At'),
            'open'        => Yii::t('phycom/base/main', 'Is Open'),
        ];
    }

    public function getOpen()
    {
        return $this->open;
    }

    public function setOpen($value)
    {
        $this->open = $value ? 1 : 0;
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->open = 1;
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
        if (!$this->opened_at || !$skipIfSet) {
            $this->opened_at = \DateTime::createFromFormat('H:i', '08:00', new \DateTimeZone('UTC'));
        }
        if (!$this->closed_at || !$skipIfSet) {
            $this->closed_at = \DateTime::createFromFormat('H:i', '17:00', new \DateTimeZone('UTC'));
        }
        $this->open = 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }
}
