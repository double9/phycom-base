<?php

namespace Phycom\Base\Models;

use Phycom\Base\Events\EventWithPayload;
use Phycom\Base\Models\Traits\ClassConstantTrait;

use yii\base\Model;
use yii;

/**
 * Class CustomFieldOption
 * @package \Phycom\Base\Models
 *
 * @property CustomField $customField
 */
abstract class CustomFieldOption extends Model
{
    const COMPOSITE_KEY_DELIMITER = '__';

    const EVENT_BEFORE_CONFIGURE = 'beforeConfigure';

    use ClassConstantTrait;

    /**
     * @var mixed
     */
    public $key;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $publicLabel;

    /**
     * @var CustomField
     */
    public $customField;

    /**
     * @var bool
     */
    public bool $isVisible = true;

    /**
     * @var callable|mixed
     */
    protected $value;

    /**
     * Finds model by key
     *
     * @param string $customFieldName
     * @param string $key
     * @return null|static
     * @throws yii\base\InvalidConfigException
     */
    public static function findByKey($customFieldName, $key)
    {
        /**
         * @var CustomField|string $CustomField
         */
        $CustomField = (new static())->getCustomFieldClassName();
        if ($customField = $CustomField::findByName($customFieldName)) {
            foreach ($customField->getOptions() as $option) {
                if ($option->key == $key && $option instanceof static) {
                    return $option;
                }
            }
        }
        return null;
    }

    /**
     * @return static[]
     * @throws yii\base\InvalidConfigException
     * @var string $customFieldName - custom field name
     */
    public static function findAll($customFieldName = null)
    {
        /**
         * @var CustomField|string $CustomField
         */
        $CustomField = (new static())->getCustomFieldClassName();
        $options = [];
        if (null === $customFieldName) {
            $customFields = $CustomField::findAll();
        } else {
            $customField = $CustomField::findByName($customFieldName);
            $customFields = $customField ? [$customField] : [];
        }
        foreach ($customFields as $customField) {
            foreach ($customField->options as $option) {
                if ($option instanceof static) {
                    $options[] = $option;
                }
            }
        }
        return $options;
    }

    public function __construct($config = [])
    {
        parent::__construct($this->loadConfig($config));
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'fieldName', 'label'], 'required'],
            [['key', 'label', 'group'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key'       => Yii::t('phycom/base/product/option', 'Key'),
            'fieldName' => Yii::t('phycom/base/product/option', 'Field Name'),
            'label'     => Yii::t('phycom/base/product/option', 'Label')
        ];
    }

    /**
     * @return string
     */
    public function getCompositeKey()
    {
        return $this->customField->name . static::COMPOSITE_KEY_DELIMITER . $this->key;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        if (is_callable($this->value)) {
            return call_user_func($this->value, $this);
        }
        if (is_callable($this->customField->optionValue)) {
            return call_user_func($this->customField->optionValue, $this);
        }
        if (in_array($this->customField->type, [CustomField::TYPE_OPTION, CustomField::TYPE_SELECT])) {
            return $this->key;
        }
        return null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->key;
    }

    /**
     * @return string|mixed
     */
    abstract public function getCustomFieldClassName();


    /**
     * @param array $config
     * @return array
     */
    protected function loadConfig(array $config)
    {
        if (!isset($config['publicLabel']) && isset($config['label'])) {
            $config['publicLabel'] = $config['label'];
        }
        $this->trigger(self::EVENT_BEFORE_CONFIGURE, new EventWithPayload(['payload' => $config]));
        return $config;
    }
}
