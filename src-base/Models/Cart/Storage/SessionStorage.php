<?php

namespace Phycom\Base\Models\Cart\Storage;


use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartStorageInterface;

use yii\base\BaseObject;
use yii;

/**
 * Class SessionStorage is a session adapter for cart data storage.
 *
 * @property \yii\web\Session session
 */
class SessionStorage extends BaseObject implements CartStorageInterface
{
    /**
     * @var string
     */
    public string $key = 'cart';

    /**
     * @inheritdoc
     */
    public function load(CartInterface $cart)
    {
        $cartData = [];

        if (false !== ($session = ($this->session->get($this->key, false)))) {
            $cartData = unserialize($session);
        }

        return $cartData;
    }

    /**
     * @inheritdoc
     */
    public function save(CartInterface $cart)
    {
        $sessionData = serialize($cart->getItems());

        $this->session->set($this->key, $sessionData);
    }


    /**
     * @return \yii\web\Session
     * @throws yii\base\InvalidConfigException
     */
    public function getSession()
    {
        return Yii::$app->get('session');
    }
}
