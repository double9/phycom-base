<?php

namespace Phycom\Base\Models;


use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Creates OrderLines from OrderItems. OrderLine is used to visualize order in backoffice and/or integrations
 * The reasoning for OrderLines is to provide flexibility to create order lines however is needed by business rules
 * For example we might want to render each order item attribute as separate line
 *
 * Default implementation will create on line for each order item
 *
 * Class OrderLineCollection
 *
 * @package Phycom\Base\Models
 */
class OrderLineCollection extends BaseObject implements \Iterator
{
    /**
     * @var array
     */
    public array $orderLineConfig = [];

    /**
     * @var array|OrderItem[]
     */
    protected array $orderItems = [];

    /**
     * @var array|OrderLine[]
     */
    protected array $orderLines = [];

    /**
     * @var int
     */
    private int $key = 0;

    public function __construct(array $orderItems, $config = [])
    {
        $this->orderItems = $orderItems;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->build();
    }

    /**
     * @return OrderLine|null
     */
    public function current(): ?OrderLine
    {
        return $this->orderLines[$this->key] ?? null;
    }

    public function next(): void
    {
        $this->key++;
    }

    public function key(): mixed
    {
        return $this->key;
    }

    public function valid(): bool
    {
        return $this->key < count($this->orderLines) && $this->key >= 0;
    }

    public function rewind(): void
    {
        $this->key = 0;
    }

    /**
     * @return $this
     * @throws InvalidConfigException
     */
    protected function build(): OrderLineCollection
    {
        $orderLines = [];
        foreach ($this->orderItems as $orderItem) {

            $lineConfig = ArrayHelper::merge([
                'id'         => $orderItem->getUniqueId(),
                'internalId' => $orderItem->id,
                'code'       => $orderItem->code,
                'product'    => $orderItem->product,
                'title'      => $orderItem->getTitle(),
                'quantity'   => $orderItem->quantity,
                'price'      => $orderItem->price,
                'numUnits'   => $orderItem->num_units,
                'unitType'   => $orderItem->product ? $orderItem->product->price_unit : null,
                'discount'   => $orderItem->discount,
                'options'    => $orderItem->product_attributes
            ], $this->orderLineConfig);

            $orderLines[] = isset($lineConfig['class'])
                ? Yii::createObject($lineConfig)
                : Yii::$app->modelFactory->getOrderLine($lineConfig);
        }
        $this->orderLines = $orderLines;
        return $this;
    }
}
