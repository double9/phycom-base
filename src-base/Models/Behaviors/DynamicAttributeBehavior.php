<?php

namespace Phycom\Base\Models\Behaviors;

use Phycom\Base\Interfaces\DynamicAttributeInterface;
use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\Attributes\EnumAttribute;
use yii\base\Behavior;
use yii\base\InvalidArgumentException;

/**
 * This is the Behavior implementation used to load/transform the EnumAttributes
 *
 * Class DynamicAttributeBehavior
 * @package Phycom\Base\Models\Behaviors
 *
 * @property ActiveRecord $owner
 */
class DynamicAttributeBehavior extends Behavior
{
	private $_dynamicAttributes = [];

    private $useDynamicAttribues = true;

	public function setAttributes(array $attributes = [])
	{
		$this->_dynamicAttributes = $attributes;
	}

    public function disableDynamicAttributes()
    {
        $this->useDynamicAttribues = false;
    }

    public function enableDynamicAttributes()
    {
        $this->useDynamicAttribues = true;
    }

	public function attach($owner)
	{
		parent::attach($owner);

		/**
		 * @var EnumAttribute $className
		 */
		foreach ($this->_dynamicAttributes as $attribute => $className) {

			/**
			 * Transform the Enum value to string before the attribute is saved
			 */
			$toString = function ($e) {
				$attribute = $e->data;
				if ($this->owner->$attribute instanceof DynamicAttributeInterface) {
                    $this->owner->disableDynamicAttributes();
					$this->owner->setAttribute($attribute, $this->owner->$attribute->getValue());
                    $this->owner->enableDynamicAttributes();
                }
			};
			$owner->on(ActiveRecord::EVENT_BEFORE_INSERT, $toString, $attribute);
			$owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, $toString, $attribute);

			/**
			 * Transform the value to correct DynamicAttributeInterface class implementation whenever the attribute has been saved or populated
			 */
			$toObject = function ($e) use ($className) {
				$attribute = $e->data;
				if (!$this->owner->$attribute instanceof DynamicAttributeInterface) {
                    $this->owner->disableDynamicAttributes();
                    $this->owner->setAttribute($attribute, $className::create($this->owner->$attribute));
                    $this->owner->enableDynamicAttributes();
				}
			};
			$owner->on(ActiveRecord::EVENT_AFTER_INSERT, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_UPDATE, $toObject, $attribute);
			$owner->on(ActiveRecord::EVENT_AFTER_FIND, $toObject, $attribute);
		}
	}

	/**
	 * @param string $name
	 * @param string|EnumAttribute $value
	 * @return EnumAttribute
	 * @throws InvalidArgumentException
	 */
	public function createDynamicAttribute($name, $value)
	{
		foreach ($this->_dynamicAttributes as $attribute => $className) {
			if ($name === $attribute) {
				if (!$value instanceof $className) {
					$value = $className::create($value);
				}
				return $value;
			}
		}
		throw new InvalidArgumentException('Dynamic attribute ' . $name . ' is not found');
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public function hasDynamicAttribute($name)
	{
		return $this->useDynamicAttribues && (isset($this->_dynamicAttributes[$name]) || in_array($name, $this->_dynamicAttributes, true));
	}
}
