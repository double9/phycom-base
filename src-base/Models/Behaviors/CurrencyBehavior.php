<?php

namespace Phycom\Base\Models\Behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use Phycom\Base\Helpers\Currency;

class CurrencyBehavior extends Behavior
{
    private $_attributes = [];

    public function setAttributes($attributes=[])
    {
        $this->_attributes = $attributes;
    }

    public function init()
    {
        parent::init();
    }

    public function initCurrencyAttributes()
    {
	    foreach ($this->_attributes as $attribute) {
		    $this->owner->$attribute = Currency::toInteger($this->owner->$attribute);
	    }
    }

    public function attach($owner)
    {
        parent::attach($owner);
        foreach ($this->_attributes as $attribute) {

            $owner->on(ActiveRecord::EVENT_BEFORE_INSERT, function($e){
                $attribute = $e->data;
                $this->owner->$attribute = Currency::toDecimal($this->owner->$attribute);
            }, $attribute);

            $owner->on(ActiveRecord::EVENT_BEFORE_UPDATE, function($e){
                $attribute = $e->data;
                $this->owner->$attribute = Currency::toDecimal($this->owner->$attribute);
            }, $attribute);

            $owner->on(ActiveRecord::EVENT_AFTER_UPDATE, function($e){
                $attribute = $e->data;
                $this->owner->$attribute = Currency::toInteger($this->owner->$attribute);
            }, $attribute);

            $owner->on(ActiveRecord::EVENT_AFTER_INSERT, function($e){
                $attribute = $e->data;
                $this->owner->$attribute = Currency::toInteger($this->owner->$attribute);
            }, $attribute);

            $owner->on(ActiveRecord::EVENT_AFTER_FIND, function($e){
                $attribute = $e->data;
                $this->owner->$attribute = Currency::toInteger($this->owner->$attribute);
            }, $attribute);
        }
    }
}
