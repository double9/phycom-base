<?php

namespace Phycom\Base\Models;


use Phycom\Base\Helpers\Date;
use Phycom\Base\Jobs\MessageJob;
use Phycom\Base\Models\Attributes\MessagePriority;
use Phycom\Base\Models\Attributes\MessageStatus;
use Phycom\Base\Models\Attributes\MessageType;
use Phycom\Base\Models\Behaviors\TimestampBehavior;

use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\base\Exception;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 *
 * @property integer $user_from
 * @property string $from
 * @property string $from_name
 *
 * @property integer $user_to
 * @property string $to
 * @property string $to_name
 *
 * @property string $subject
 * @property string $content
 *
 * @property MessageStatus $status
 * @property MessageType $type
 * @property MessagePriority $priority
 * @property integer $parent_id
 * @property \DateTime $delivery_time
 * @property \DateTime $dispatched_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Message $parent
 * @property User $userFrom
 * @property User $userTo
 * @property MessageAttachment[] $attachments
 */
class Message extends ActiveRecord
{
	const EVENT_AFTER_DISPATCHED = 'after_dispatched';
	const EVENT_AFTER_QUEUED = 'after_queued';

	const PRIORITY_HIGH = 1;
	const PRIORITY_LOW = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @param string $templateId
     * @param array $params
     * @param string|null $language
     * @return Message
     * @throws yii\base\NotSupportedException
     */
    public static function createByTemplateId(string $templateId, array $params = [], $language = null)
    {
    	if (!$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($templateId)) {
    	    throw new InvalidArgumentException('Template ' . $templateId . ' not found');
	    }
    	return static::createByTemplate($template, $params, $language);
    }

    /**
     * @param MessageTemplate $template
     * @param array $params
     * @param string|null $language
     * @return Message
     * @throws InvalidConfigException
     * @throws NotSupportedException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function createByTemplate(MessageTemplate $template, array $params = [], string $language = null)
    {
        /**
         * @var static|object $message
         */
	    $message = Yii::createObject(static::class);
	    $message->type = $template->type;
        $message->subject = $template->renderTitle($params, $language);
	    $message->content = $template->render($params, $language);
	    $message->status = MessageStatus::NEW;
	    $message->user_from = Yii::$app->systemUser->id;
	    return $message;
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'delivery_time',
					'dispatched_at'
				]
			],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => MessageStatus::class,
					'type' => MessageType::class,
					'priority' => MessagePriority::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'status', 'type'], 'required'],
            ['user_from', 'required', 'when' => function () {
                return empty($this->from);
            }],
            ['from', 'required', 'when' => function () {
                return !$this->user_from;
            }],
            ['user_to', 'required', 'when' => function () {
                return empty($this->to);
            }],
            ['to', 'required', 'when' => function () {
                return !$this->user_to;
            }],
            [['user_from', 'user_to', 'parent_id'], 'integer'],
            [['content'], 'string'],
            [['dispatched_at', 'created_at', 'updated_at', 'priority'], 'safe'],
            [['subject', 'from', 'to', 'from_name', 'to_name'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['user_from'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_from' => 'id']],
            [['user_to'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_to' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('phycom/base/message', 'ID'),
            'user_from'     => Yii::t('phycom/base/message', 'User From'),
            'from'          => Yii::t('phycom/base/message', 'From'),
            'user_to'       => Yii::t('phycom/base/message', 'User To'),
            'to'            => Yii::t('phycom/base/message', 'To'),
            'subject'       => Yii::t('phycom/base/message', 'Subject'),
            'content'       => Yii::t('phycom/base/message', 'Content'),
            'status'        => Yii::t('phycom/base/message', 'Status'),
            'type'          => Yii::t('phycom/base/message', 'Type'),
            'priority'      => Yii::t('phycom/base/message', 'Priority'),
            'parent_id'     => Yii::t('phycom/base/message', 'Parent ID'),
            'delivery_time' => Yii::t('phycom/base/message', 'Delivery Time'),
            'dispatched_at' => Yii::t('phycom/base/message', 'Dispatched At'),
            'created_at'    => Yii::t('phycom/base/message', 'Created At'),
            'updated_at'    => Yii::t('phycom/base/message', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
    	if (!$this->priority) {
    		$this->priority = MessagePriority::PRIORITY_DEFAULT;
	    }
	    return parent::beforeSave($insert);
    }

    /**
     * @return bool
     * @throws Exception
     */
	public function queue(): bool
    {
    	if ($this->isNewRecord) {
    		throw new Exception('Message must be saved before it can be queued');
	    }
	    $queue = $this->priority->isUrgent
            ? Yii::$app->queue1
            : Yii::$app->queue2;
        /**
         * @var MessageJob|object $job
         */
        $job = Yii::createObject(['class' => MessageJob::class, 'id' => $this->id]);

        if ($this->delivery_time) {
		    $timeout = max(0, ($this->delivery_time->getTimestamp() - (new \DateTime())->getTimestamp()));
    		if ($timeout > 0) {
    		    $jobId = $queue->delay($timeout);
    		    return $this->afterMessageQueued($jobId);
			}
	    }
	    $jobId = $queue->push($job);
	    return $this->afterMessageQueued($jobId);
    }

    /**
     * @return bool
     *
     * @throws \Phycom\Base\Modules\Email\EmailException
     * @throws \Phycom\Base\Modules\Sms\SmsException
     * @throws yii\base\NotSupportedException
     */
    public function dispatch()
    {
	    if ($this->type->isEmail) {
            /**
             * @var \Phycom\Base\Modules\Email\Module $emailModule
             */
            $emailModule = Yii::$app->getModule('email');
            $emailModule->send($this);
            return $this->afterMessageDispatched();

        } elseif ($this->type->isSms) {

            /**
             * @var \Phycom\Base\Modules\Sms\Module $smsModule
             */
            $smsModule = Yii::$app->getModule('sms');
            $smsModule->sendMessage($this);
            return $this->afterMessageDispatched();

	    } else {
		    throw new NotSupportedException('Message type ' . $this->type . ' transport is not yet supported');
	    }
    }

	protected function afterMessageQueued($jobId)
	{
		$this->status = MessageStatus::QUEUED;
		Yii::debug('Message ' . $this->id . ' was queued at ' . Date::now()->format('Y-m-d') . ' with job id ' . $jobId, $this->getLogCategory());
		if (!$this->save()) {
			Yii::error($this->errors, __CLASS__);
			Yii::error($this->attributes, __CLASS__);
			return false;
		}
		$this->trigger(self::EVENT_AFTER_QUEUED);
		return $jobId;
	}

	protected function afterMessageDispatched()
	{
		$this->status = MessageStatus::DISPATCHED;
		$this->dispatched_at = Date::now();
		Yii::debug('Message ' . $this->id . ' was dispatched at ' . $this->dispatched_at->format('Y-m-d'), $this->getLogCategory());
		if (!$this->save()) {
			Yii::error($this->errors, __CLASS__);
			Yii::error($this->attributes, __CLASS__);
			return false;
		}
		$this->trigger(self::EVENT_AFTER_DISPATCHED);
		return true;
	}

    /**
     * @return string|null
     */
    public function getSenderName()
    {
        if ($this->from_name) {
            return $this->from_name;
        }
        if ($this->user_from) {
            return $this->userFrom->getFullName();
        }
        return null;
    }

    /**
     * @return string|null
     */
	public function getRecipientName()
    {
        if ($this->to_name) {
            return $this->to_name;
        }
        if ($this->user_to) {
            return $this->userTo->getFullName();
        }
        return null;
    }

    /**
     * @param string[] $files
     * @return $this
     * @throws InvalidConfigException
     */
    public function setAttachmentFiles(array $files): Message
    {
        if ($this->isNewRecord) {
            throw new yii\base\InvalidCallException('Message must be saved before attachments can be added');
        }
        foreach ($files as $file) {
            if (!$file) {
                Yii::warning('Empty message ' . $this->id . ' attachment file passed', $this->getLogCategory());
                continue;
            }
            if ($messageAttachment = MessageAttachment::create($this->id, $file)) {
                if (!$messageAttachment->save()) {
                    Yii::error('Error saving message attachment: ' . Json::encode($messageAttachment->errors), $this->getLogCategory());
                }
            }
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getLogCategory(): string
    {
        return $this->type ? (string)$this->type : 'message';
    }

    /**
     * @return string[]
     */
	public function getAttachmentFiles(): array
	{
		$res = [];
		if ($attachments = $this->getAttachments()->all()) {
            /**
             * @var MessageAttachment[] $attachments
             */
			foreach ($attachments as $attachment) {
				$res[] = $attachment->getFullFilename();
			}
		}
		return $res;
	}

    /**
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(static::class, ['id' => 'parent_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_from']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_to']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(MessageAttachment::class, ['message_id' => 'id']);
    }
}
