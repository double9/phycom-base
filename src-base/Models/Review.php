<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Attributes\ReviewStatus;
use Phycom\Base\Models\Product\Product;

use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property integer $approved_by
 * @property integer $score
 * @property string $title
 * @property string $description
 * @property ReviewStatus $status
 * @property integer $created_by
 * @property bool $verified_purchase
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property User $createdBy
 * @property User $approvedBy
 * @property Product $product
 * @property Shop $shop
 */
class Review extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ReviewStatus::class,
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approved_by', 'created_by', 'score'], 'integer'],
            [['score', 'status'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['status', 'created_at', 'updated_at'], 'safe'],
            [['approved_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['approved_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('phycom/base/main', 'ID'),
            'score' => Yii::t('phycom/base/main', 'Score'),
            'description' => Yii::t('phycom/base/main', 'Description'),
            'status' => Yii::t('phycom/base/main', 'Status'),
            'approved_by' => Yii::t('phycom/base/main', 'Approved By'),
            'created_by' => Yii::t('phycom/base/main', 'Created By'),
            'verified_purchase' => Yii::t('phycom/base/main', 'Verified purchase'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id'])->viaTable('product_review', ['review_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id'])->viaTable('shop_review', ['review_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'approved_by']);
    }
}
