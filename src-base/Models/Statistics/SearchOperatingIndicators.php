<?php

namespace Phycom\Base\Models\Statistics;

use Phycom\Base\Models\Attributes\PaymentStatus;
use Phycom\Base\Models\Payment;

use yii\db\Query;

/**
 * Class SearchOperatingIndicators
 * @package Phycom\Base\Models\Statistics
 */
class SearchOperatingIndicators extends SearchStatistics
{

    public function search(array $params = [])
    {
        return null;
    }

    /**
     * @return Query
     */
    protected function createSearchQuery()
    {
        return (new Query())
            ->select([
                'MAX((p.transaction_time) :: date) AS date',
                'SUM(CASE WHEN p.status = :completed THEN p.amount ELSE 0 END) AS received',
            ])
            ->from(['p' => Payment::tableName()])
            ->where(['>=', '(p.transaction_time) :: date', $this->from])
            ->andWhere(['<=', '(p.transaction_time) :: date', $this->to])
            ->andWhere(['not', ['p.status' => PaymentStatus::DELETED]])
            ->groupBy(['(p.transaction_time) :: date'])
            ->addParams([
                'completed' => PaymentStatus::COMPLETED,
            ]);
    }
}
