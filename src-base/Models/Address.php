<?php

namespace Phycom\Base\Models;


use Geocoder\Model\Coordinates;
use Phycom\Base\Components\AddressFormatter;
use Phycom\Base\Helpers\c;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;

use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Http\Client\Curl\Client;

use yii\helpers\Json;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $vendor_id
 * @property integer $shop_id
 * @property string $country
 * @property string $state
 * @property string $province
 * @property string $city
 * @property string $locality
 * @property string $district
 * @property string $street
 * @property string $house;
 * @property string $room
 * @property string $postcode
 * @property AddressType $type
 * @property ContactAttributeStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Vendor $vendor
 * @property Shop $shop
 * @property User $user
 * @property AddressMeta $meta
 *
 * @property-read array $l2
 * @property-read array $l3
 */
class Address extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => ContactAttributeStatus::class,
					'type' => AddressType::class
				]
			]
		]);
	}

    /**
     * @param array|string $data - array of address attributes or json string
     * @return static
     * @throws yii\base\InvalidConfigException
     */
	public static function create($data)
	{
        /**
         * @var static $model
         */
		$model = Yii::createObject(static::class);
		$model->attributes = (Yii::createObject(AddressField::class, [$data]))->attributes;

		return $model;
	}

    /**
     * @param string $cityName
     * @return null|string
     */
	public static function generateCityCode(string $cityName)
    {
        $code = Inflector::transliterate($cityName);
        $code = substr($code, 0, 1) . substr($code, 2, 2);
        $code = preg_replace('/[\s+]|[\d+]/', '', strtoupper($code));
        return $code;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'vendor_id', 'shop_id'], 'integer'],
            [['country', 'street', 'type', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['country', 'state', 'province', 'locality', 'city', 'district', 'street', 'postcode', 'house', 'room'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => ContactAttributeStatus::all()],
	        ['type', 'in', 'range' => AddressType::all()],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('phycom/base/main', 'ID'),
            'user_id'    => Yii::t('phycom/base/main', 'User ID'),
            'vendor_id'  => Yii::t('phycom/base/main', 'Vendor ID'),
            'shop_id'    => Yii::t('phycom/base/main', 'Shop ID'),
            'country'    => Yii::t('phycom/base/main', 'Country'),
            'state'      => Yii::t('phycom/base/main', 'State'),
            'province'   => Yii::t('phycom/base/main', 'Province'),
            'locality'   => Yii::t('phycom/base/main', 'Locality'),
            'city'       => Yii::t('phycom/base/main', 'City'),
            'district'   => Yii::t('phycom/base/main', 'District'),
            'street'     => Yii::t('phycom/base/main', 'Street'),
            'house'      => Yii::t('phycom/base/main', 'House'),
            'room'       => Yii::t('phycom/base/main', 'Room'),
            'postcode'   => Yii::t('phycom/base/main', 'Postcode'),
            'type'       => Yii::t('phycom/base/main', 'Type'),
            'status'     => Yii::t('phycom/base/main', 'Status'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        if (!$this->isNewRecord) {
            $this->status = ContactAttributeStatus::DELETED;
            return $this->save(false, ['status']);
        }
        return false;
    }

    public function updateJson($json)
    {
        $this->setAttributes(Json::decode($json));
    }

	/**
	 * Finds similar addresses from the db.
	 *
	 * @param null $relatedAttribute
	 * @param bool $strictComparison
	 * @return static[]
	 */
    public function getSimilar($relatedAttribute = null, $strictComparison = false)
    {
    	$query = static::find()
            ->where([
                'country'  => $this->country,
                'street'   => $this->street,
                'postcode' => $this->postcode
            ])
	        ->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);

    	if ($relatedAttribute) {
    		$query->andWhere([$relatedAttribute => $this->$relatedAttribute]);
	    }
	    if ($strictComparison) {
            $query->andWhere([
                'province' => $this->province,
                'locality' => $this->locality,
                'city'     => $this->city,
                'district' => $this->district
            ]);
	    }
	    return $query->all();
    }

    /**
     * @return AddressField
     * @throws yii\base\InvalidConfigException
     */
    public function export()
    {
        /**
         * @var AddressField|object $a
         */
    	$a = Yii::createObject(AddressField::class);
    	$a->country = $this->country;
    	$a->state = $this->state;
    	$a->province = $this->province;
    	$a->city = $this->city;
    	$a->locality = $this->locality;
    	$a->district = $this->district;
    	$a->street = $this->street;
    	$a->house = $this->house;
    	$a->room = $this->room;
    	$a->postcode = $this->postcode;
    	return $a;
    }

    /**
     * @throws yii\base\InvalidConfigException
     */
    public function exportArray() : array
    {
	    return $this->export()->exportArray(true);
    }

    /**
     * @return string
     * @throws yii\base\InvalidConfigException
     *
     * @deprecated  use Yii::$app->formatter->asAddress($address) instead
     */
    public function getLabel() : ?string
    {
	    return  $this->getFormatter()->asLabel();
    }

    /**
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function getL2() : array
    {
        return $this->getFormatter()->asL2();
    }

    /**
     * @return array
     * @throws yii\base\InvalidConfigException
     */
    public function getL3() : array
    {
        return $this->getFormatter()->asL3();
    }

    /**
     * @param bool $save
     * @return Coordinates|null
     * @throws \Geocoder\Exception\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function geocode(bool $save = false): ?Coordinates
    {
        if (Yii::$app->geocoder->isEnabled()) {

            $response = Yii::$app->geocoder->geocode($this->export());
            if (!$response->isEmpty()) {
                $location = $response->first();
                $coordinates = $location->getCoordinates();

                if ($save) {
                    $addressMeta = $this->meta ?: new AddressMeta(['address_id' => $this->id]);
                    $addressMeta->lat = $coordinates->getLatitude();
                    $addressMeta->lng = $coordinates->getLongitude();
                    $addressMeta->save();
                }

                return $coordinates;
            }
        }
        return null;
    }


    /**
     * @return AddressFormatter|object
     * @throws yii\base\InvalidConfigException
     */
    protected function getFormatter() : AddressFormatter
    {
        return Yii::createObject(AddressFormatter::class, [$this->export()]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return implode(', ', $this->exportArray());
        } catch (yii\base\InvalidConfigException $e) {
            Yii::error('Error converting address ' . $this->id . ' to string: ' . $e->getMessage(), __METHOD__);
            return '';
        }
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeta()
    {
        return $this->hasOne(AddressMeta::class, ['address_id' => 'id']);
    }
}
