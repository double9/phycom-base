<?php

namespace Phycom\Base\Models;

use Phycom\Base\Events\StatusUpdateEvent;
use Phycom\Base\Helpers\c;
use Phycom\Base\Helpers\Checksum731;
use Phycom\Base\Helpers\TransactionHelper;
use Phycom\Base\Jobs\EmailJob;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\CustomerType;
use Phycom\Base\Models\Attributes\InvoiceStatus;
use Phycom\Base\Models\Attributes\MessageType;
use Phycom\Base\Models\Attributes\MessagePriority;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Attributes\ShipmentStatus;
use Phycom\Base\Models\Behaviors\StatusBehavior;
use Phycom\Base\Models\Behaviors\TimestampBehavior;
use Phycom\Base\Models\Product\Variant;
use Phycom\Base\Models\Product\VariantOption;
use Phycom\Base\Modules\Email\Helpers\EmailParser;

use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $number
 * @property string $first_name
 * @property string $last_name
 * @property string $company_name
 * @property string $email
 * @property integer $user_id
 * @property integer $shop_id
 * @property string $promotion_code
 * @property OrderStatus $status
 * @property string $comment
 * @property string $phone_number
 * @property string $phone_code
 * @property string $language_code
 * @property \DateTime $paid_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Invoice[] $invoices
 * @property OrderItem[] $orderItems
 * @property Shipment[] $shipments
 *
 * @property-read integer $total
 * @property-read Invoice $invoice
 * @property-read Invoice $hasValidInvoice
 * @property-read Shop $shop
 * @property-read User $user
 * @property-read Shipment $shipment
 * @property-read bool $canChange
 * @property-read int $totalProcessingTime
 * @property-read bool $isDelivery
 * @property-read Phone $phone
 * @property-read \DateTime $deliveryTime
 *
 * @method bool updateStatus($status, $save = true)
 */
class Order extends ActiveRecord
{
	const EVENT_AFTER_STATUS_UPDATE = 'afterStatusUpdate';

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
					'delivery_time',
					'paid_at'
				]
			],
			'dynamic-attribute' => [
				'attributes' => [
					'status' => OrderStatus::class
				]
			],
            'status' => StatusBehavior::class
		]);
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'status'], 'required'],
            [['user_id', 'shop_id', 'phone_number', 'phone_code'], 'integer'],
            [['comment'], 'string'],
            [['paid_at', 'created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'company_name', 'email', 'number', 'promotion_code'], 'string', 'max' => 255],
	        [['status'], 'safe'],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('phycom/base/main', 'ID'),
            'number'         => Yii::t('phycom/base/main', 'Order number'),
            'user_id'        => Yii::t('phycom/base/main', 'User ID'),
            'shop_id'        => Yii::t('phycom/base/main', 'Shop ID'),
            'first_name'     => Yii::t('phycom/base/main', 'First name'),
            'last_name'      => Yii::t('phycom/base/main', 'Last name'),
            'company_name'   => Yii::t('phycom/base/main', 'Company name'),
            'email'          => Yii::t('phycom/base/main', 'Email'),
            'promotion_code' => Yii::t('phycom/base/main', 'Promotion Code'),
            'status'         => Yii::t('phycom/base/main', 'Status'),
            'comment'        => Yii::t('phycom/base/main', 'Comment'),
            'phone_number'   => Yii::t('phycom/base/main', 'Phone number'),
            'phone_code'     => Yii::t('phycom/base/main', 'Phone code'),
            'paid_at'        => Yii::t('phycom/base/main', 'Paid At'),
            'created_at'     => Yii::t('phycom/base/main', 'Created At'),
            'updated_at'     => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function init()
    {
	    parent::init();
	    $this->on(self::EVENT_AFTER_STATUS_UPDATE, function ($event) {
		    /**
		     * @var StatusUpdateEvent $event
		     */
		    switch ((string) $this->status) {
			    case OrderStatus::PENDING:
			    case OrderStatus::PAYMENT_COMPLETE:
                    if ($this->isPaid() && Yii::$app->commerce->shipment->autoPostageLabel) {
                        $this->shipment->createPostageLabel();
                    }
                    $this->notifyAdmin('new_order');
                    if (Yii::$app->commerce->order->sendOrderNotificationSms) {
                        $this->notifyAdmin('sms/new_order');
                    }
                    $this->updateStatistics();
			    	break;

			    case OrderStatus::CANCELED:
			    	foreach ($this->shipments as $shipment) {
			    		$shipment->updateStatus(ShipmentStatus::DELETED);
				    }
				    foreach ($this->invoices as $invoice) {
			    		$invoice->updateStatus(InvoiceStatus::CANCELED);
				    }
			    	break;
		    }
	    });
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws yii\base\InvalidConfigException
     */
	public function afterSave($insert, $changedAttributes)
    {
	    parent::afterSave($insert, $changedAttributes);
	    if (in_array('status', array_keys($changedAttributes))) {

            Yii::info('Order ' . $this->id . ' status was changed to ' . $this->status, 'order');
            /**
             * @var StatusUpdateEvent|object $event
             */
            $event = Yii::createObject([
                'class'      => StatusUpdateEvent::class,
                'prevStatus' => $changedAttributes['status']
            ]);

			$this->trigger(self::EVENT_AFTER_STATUS_UPDATE, $event);
	    }
    }

	public function generateNumber()
    {
		if (!$this->isNewRecord) {
			throw new yii\base\InvalidCallException('Can\'t generate order number for existing order ' . $this->id);
		}

        if ($lastOrder = Yii::$app->modelFactory->getOrder()::find()->orderBy(['id' => SORT_DESC])->one()) {
            /**
             * @var Order $lastOrder
             */
            $baseOrderNr = c::param('orderNoBegin');
            $baseCount = strlen($baseOrderNr);
            $lastOrderNr = substr(preg_replace('/[^0-9.]/', '', $lastOrder->number), 0, -1);
            $lastCount = strlen($lastOrderNr);
            // double check if the last order number has been properly made and is same length as the
            if ($lastCount < $baseCount) {
                $lastOrderNr = (int) substr($baseOrderNr, 0, $baseCount - $lastCount) . $lastOrderNr;
            } else {
                $lastOrderNr = (int) $lastOrderNr;
            }
        } else {
            $lastOrderNr = c::param('orderNoBegin');
        }

		$orderNr = (string)($lastOrderNr + 1);
		$this->number = $orderNr . Checksum731::calculate($orderNr);
    }


    /**
     * Creates an email message to client who made the order.
     *
     * @param string $template
     * @param array $params
     * @param string $priority
     * @return Message|bool
     * @throws yii\base\NotSupportedException
     */
    public function createEmail(string $template, array $params = [], $priority = MessagePriority::PRIORITY_1)
    {
        $params = ArrayHelper::merge([
            'appname'       => Yii::$app->name,
            'firstname'     => $this->getFirstName(),
            'contact_email' => Yii::$app->systemUser->email,
            'order'         => $this,
            'orderno'       => $this->number // deprecated - use order instead
        ], $params);

        Yii::info('Creating order ' . $this->id . ' #' . $this->number . ' email ' . $template . ' with params: ' . Json::encode($params), 'order');
        $message = Message::createByTemplateId($template, $params, $this->getLanguageCode());


	    if ($this->user_id) {
	        $message->user_to = $this->user_id;
        } else {
	        $message->to = $this->email;
	        $message->to_name = $this->getFullName();
        }
	    $message->type = MessageType::EMAIL;
	    $message->priority = $priority;

	    if (!$message->save()) {
	    	Yii::error('Error creating order ' . $this->id . ' notification email message: ' . Json::encode($message->errors));
	    	return false;
	    }
	    return $message;
    }

    /**
     * Notifies admin users or explicitly configured subscribers for an order change
     *
     * @param string $template
     * @param array $params
     * @param string $priority
     * @return bool
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws yii\base\Exception
     * @throws yii\base\InvalidConfigException
     * @throws yii\base\NotSupportedException
     */
    public function notifyAdmin(string $template, array $params = [], $priority = MessagePriority::PRIORITY_1)
    {
        $query = Yii::$app->admin->getMessageSubscribers()
            ->andWhere('(settings #>> \'{"receiveOrderNotifications"}\') :: text = \'true\'');

        $params = ArrayHelper::merge([
            'appname'           => Yii::$app->name,
            'link'              => Yii::$app->urlManagerBackend->createAbsoluteUrl(['/order/edit', 'id' => $this->id]),
            'order'             => $this,
            'postageLabel'      => $this->shipment->postage_label ? $this->shipment->postageLabelData['url'] : false,
            'commercialInvoice' => $this->shipment->postage_label ? $this->shipment->postageLabelData['commercialInvoiceUrl'] : false
        ], $params);

        if ($this->shipment->hasErrors('postage_label')) {
            $params['postageLabelErrors'] = $this->shipment->getErrors('postage_label');
        }

        foreach ($query->batch() as $subscribers) {
            /**
             * @var User[] $subscribers
             */
            foreach ($subscribers as $subscriber) {
                $message = Message::createByTemplateId($template, ArrayHelper::merge($params, ['firstname' => $subscriber->first_name]), $subscriber->settings->language);
                $message->user_to = $subscriber->id;
                $message->priority = $priority;

                if (!$message->save()) {
                    Yii::error('Error saving order ' . $this->id . ' admin notification email message: ' . Json::encode($message->errors));
                    return false;
                }
                $message->queue();
            }
        }
        if (!$tpl = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($template)) {
            throw new InvalidArgumentException('Template ' . $template . ' not found');
        }
        if ($tpl->type === MessageTemplate::TYPE_EMAIL) {
            foreach (Yii::$app->commerce->order->orderNotificationEmails as $recipient) {

                /**
                 * @var EmailParser|object $emailParser
                 */
                $emailParser = Yii::createObject(EmailParser::class, [$recipient]);
                $emailParams = ArrayHelper::merge($params, ['firstname' => $emailParser->name]);

                /**
                 * @var EmailJob|object $job
                 */
                $job = Yii::createObject(EmailJob::class);
                $job->to = $emailParser->email;
                $job->subject = $tpl->renderTitle($emailParams, Yii::$app->lang->default->code);
                $job->content = $tpl->render($emailParams, Yii::$app->lang->default->code);

                $id = Yii::$app->queue1->push($job);
                Yii::info('email job ' . $id . ' queued', 'email');
            }
        }
    }

    /**
     * Updates product statistical values after order has been placed
     */
    public function updateStatistics()
    {
        foreach ($this->orderItems as $orderItem) {
            if ($orderItem->product) {

                if (!$productStatistics = $orderItem->product->productStatistics) {
                    $productStatistics = Yii::$app->modelFactory->getProductStatistics();
                    $productStatistics->product_id = $orderItem->product_id;
                }
                $productStatistics->num_transactions++;
                $productStatistics->total_quantity_ordered += $orderItem->quantity;
                $productStatistics->revenue += $orderItem->totalPrice;
                $productStatistics->save();
            }
        }
    }

    public function getFirstName()
    {
        return $this->user_id ? $this->user->first_name : $this->first_name;
    }

    public function getFullName()
    {
        return $this->user_id ? $this->user->fullName : trim($this->first_name . ' ' . $this->last_name);
    }

    public function getLanguageCode()
    {
        return $this->language_code ?: ($this->user_id ? substr($this->user->settings->language, 0, 2) : null);
    }

    /**
     * @return Phone|null
     */
    public function getPhone()
    {
        if ($this->user) {
            foreach ($this->user->phones as $phone) {
                if ($phone->phone_nr === $this->phone_number && $phone->country_code === $this->phone_code) {
                    return $phone;
                }
            }
        }
        if ($this->phone_number && $this->phone_code) {
            $phone = new Phone();
            $phone->country_code = $this->phone_code;
            $phone->phone_nr = $this->phone_number;
            $phone->user_id = $this->user_id;
            return $phone;
        }
        return null;
    }


    public function getCanChange()
	{
		return !in_array($this->status, [
			OrderStatus::DELETED,
			OrderStatus::CLOSED,
			OrderStatus::COMPLETE
		]);
	}

    public function getTotal()
    {
    	$total = 0;
    	foreach ($this->orderItems as $line) {
    		$total += $line->total;
	    }
	    return $total;
    }

	/**
	 * @return bool
	 */
    public function isPaid()
    {
    	$paid = 0;
	    foreach ($this->invoices as $invoice) {
	    	$paid += $invoice->totalPaid;
	    }
	    return $this->total <= $paid;
    }

    public function getIsDelivery()
    {
        if (empty($this->shipments)) {
            return false;
        }
        return $this->shipments[0]->isDelivery;
    }

    /**
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getDeliveryTime()
    {
        $deliveryTime = null;
        foreach ($this->shipments as $shipment) {
            if (!$deliveryTime || $shipment->getDeliveryTime() > $deliveryTime) {
                $deliveryTime = $shipment->getDeliveryTime();
            }
        }
        return $deliveryTime;
    }

	/**
	 * @return int - minutes
	 */
    public function getTotalProcessingTime()
    {
    	$total = 0;
    	foreach ($this->orderItems as $item) {
    		if ($item->product && $item->product->processingTime > $total) {
				$total = $item->product->processingTime;
		    }
	    }
	    return $total;
    }

	/**
	 * Check if all shipments have been delivered
	 * @return bool
	 */
    public function shipmentsDelivered()
    {
    	foreach ($this->shipments as $shipment) {
    		if (!$shipment->status->is(ShipmentStatus::DELIVERED)) {
				return false;
		    }
	    }
	    return true;
    }

    public function getHasValidInvoice($cancelInvalid = false)
    {
        foreach ($this->invoices as $invoice) {
	        $itemFound = false;
        	foreach ($this->orderItems as $orderItem) {
		        $itemFound = false;
        		foreach ($invoice->items as $item) {
					if ($item->order_item_id === $orderItem->id && $item->quantity === $orderItem->quantity) {
						$itemFound = $item->order_item_id;
						break;
					}
		        }
		        if (!$itemFound) {
					break;
		        }
	        }

        	$checksum = $invoice->calculateChecksum();

	        if ($itemFound && $invoice->checksum === $checksum) {
        		return $invoice;
	        } else if ($cancelInvalid) {

	            if ($invoice->checksum !== $checksum) {
                    Yii::info('Invoice ' . $this->id . ' checksum mismatch ' . $invoice->checksum . ' != ' . $checksum , __METHOD__);
                }

                $invoice->updateStatus(InvoiceStatus::CANCELED);
            }
        }
        return false;
    }

    public function createInvoice(AddressField $address = null, $status = InvoiceStatus::ISSUED)
    {
        Yii::info('Creating invoice for order no ' . $this->number . ' / id ' . $this->id, __METHOD__);
	    $transactionHelper = new TransactionHelper();

	    $invoice = Yii::$app->modelFactory->getInvoice();
	    $invoice->due_date = (new \DateTime('today'))->add(new \DateInterval('P' . Yii::$app->commerce->invoice->defaultDueDateTerm . 'D'))->setTime(23,59,59);
	    $invoice->order_id = $this->id;
	    $invoice->status = $status;

	    if ($this->company_name) {
		    $invoice->customer = $this->company_name;
		    $invoice->customer_type = CustomerType::COMPANY;
	    } else {
		    $invoice->customer = $this->getFullName();
		    $invoice->customer_type = CustomerType::INDIVIDUAL;
	    }

	    // if address is not provided try to find user's address
	    if (!$address && $this->user_id) {
            if (!$addressModel = $this->user->getAddresses()->where(['type' => AddressType::INVOICE])->one()) {
                $addressModel = $this->user->getAddresses()->one();
            }
            /**
             * @var Address $addressModel
             */
            if ($addressModel) {
                $address = $addressModel->export();
            }
        }
	    /**
	     * @var Address $address
	     */
	    if ($address) {
		    $invoice->address = (string)$address;
	    }

	    return $transactionHelper->run(function (yii\db\Transaction $transaction) use ($invoice) {

		    $invoice->generateNumber();
		    if (!$invoice->save()) {
			    $transaction->rollBack();
			    Yii::error('Error creating invoice for order ' . $this->number . ' / id ' . $this->id . ': ' . Json::encode($invoice->errors), __METHOD__);
			    return false;
		    }

		    foreach ($this->orderItems as $item) {
                /**
                 * @var InvoiceItem|object $invoiceItem
                 */
			    $invoiceItem = Yii::createObject(InvoiceItem::class);
			    $invoiceItem->invoice_id = $invoice->id;
			    $invoiceItem->order_item_id = $item->id;
			    $invoiceItem->quantity = $item->quantity;

			    if (!$invoiceItem->save()) {
				    $transaction->rollBack();
				    Yii::error('Error creating invoice item. Order ' . $this->number . ' / id ' . $this->id . ': ' . Json::encode($invoiceItem->errors), __METHOD__);
				    return false;
			    }
		    }
		    unset($this->invoices);

		    $invoice->checksum = $invoice->calculateChecksum();
            if (!$invoice->save(true, ['checksum'])) {
                $transaction->rollBack();
                Yii::error('Error saving invoice ' . $this->id . ' checksum: ' . Json::encode($invoice->errors), __METHOD__);
                return false;
            }

		    $invoice->trigger($invoice::EVENT_AFTER_INVOICE_CREATED);
		    return $invoice;
	    });
    }

    /**
     * @param array $config
     * @return OrderLineCollection
     */
    public function getOrderLines(array $config = []): OrderLineCollection
    {
        return Yii::$app->modelFactory->getOrderLineCollection($this->orderItems, $config);
    }


    public function getShipment()
    {
        return !empty($this->shipments) ? $this->shipments[0] : null;
    }


	public function getInvoice()
	{
		return !empty($this->invoices) ? $this->invoices[0] : null;
	}

	/**
     * @param mixed $statuses
	 * @return \yii\db\ActiveQuery
	 */
	public function getInvoices($statuses = null)
	{
		$query = $this->hasMany(get_class(Yii::$app->modelFactory->getInvoice()), ['order_id' => 'id'])
			->orderBy(['created_at' => SORT_DESC, 'id' => SORT_DESC])
			->andWhere(['not', ['invoice.status' => InvoiceStatus::DELETED]]);

		if ($statuses) {
		    $query->andWhere(['invoice.status' => $statuses]);
        }
		return $query;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getShop()), ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getOrderItem()), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipments()
    {
        return $this->hasMany(get_class(Yii::$app->modelFactory->getShipment()), ['order_id' => 'id'])->orderBy(['created_at' => SORT_DESC])->andWhere(['not', ['shipment.status' => ShipmentStatus::DELETED]]);
    }
}
