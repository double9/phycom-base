<?php

namespace Phycom\Base\Models;


use Phycom\Base\Models\Traits\ClassConstantTrait;
use Phycom\Base\Models\Behaviors\TimestampBehavior;

/**
 * This is the base Active Record class all AR models should extend.
 * It uses the AuditTrail, Enum and Error Behavior
 *
 * Class ActiveRecord
 * @package Phycom\Base\Models
 *
 * @method hasDynamicAttribute($name)
 * @method createDynamicAttribute($name, $value)
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
	use ClassConstantTrait;

	/**
	 * @return array
	 */
	public function modelEvents()
	{
		return static::getConstants('EVENT_');
	}

	public function behaviors()
	{
		return [
			'audit-trail' => [
				'class' => Behaviors\AuditTrailBehavior::class,
				'ignored' => [
					'created_at',
					'updated_at',
				],
				'dateFormat' => 'Y-m-d H:i:s'
			],
			'timestamp' => [
				'class' => TimestampBehavior::class,
				'attributes' => [
					['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
					['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE]
				]
			],
			'dynamic-attribute' => [
				'class' => Behaviors\DynamicAttributeBehavior::class
			],
			'error' => [
				'class' => Behaviors\ErrorBehavior::class
			]
		];
	}

	/**
	 * Check here if dynamic attribute behavior is used and overload the set method accordingly.
	 * This is so that we can set strings instead of objects as dynamic attribute
	 *
	 * @param string $name
	 * @param mixed $value
	 */
	public function __set($name, $value)
	{
		parent::__set($name, $this->ensureDynamicAttributeValue($name, $value));
	}

    public function setAttribute($name, $value)
    {
        parent::setAttribute($name, $this->ensureDynamicAttributeValue($name, $value));
    }

    protected function ensureDynamicAttributeValue($name, $value): mixed
    {
        if ($this->hasDynamicAttributeBehavior() && $this->hasDynamicAttribute($name)) {
            $value = $this->createDynamicAttribute($name, $value);
        }
        return $value;
    }

    private function hasDynamicAttributeBehavior(): bool
    {
        foreach ($this->behaviors as $behavior) {
            if ($behavior instanceof Behaviors\DynamicAttributeBehavior) {
                return true;
            }
        }
        return false;
    }
}
