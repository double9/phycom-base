<?php

namespace Phycom\Base\Models;


use Phycom\Base\Helpers\Url;
use Phycom\Base\Models\Attributes\PostType;

use yii\base\Exception;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use Yii;

/**
 * Class PageSpace
 * @package Phycom\Base\Models
 *
 * @property-read string $label
 * @property-read string $identifier
 */
class PageSpace extends BaseObject
{
    /**
     * @var string|null
     */
    public ?string $name = null;

    /**
     * @var bool - If true multiple pages can use same PageSpace otherwise only one page can use a certain PageSpace
     */
    public bool $multiple = false;

    /**
     * @var string
     */
    protected string $identifier;

    /**
     * @var string
     */
    protected string $postType;


    public function __construct(string $identifier, string $postType, $config = [])
    {
        $this->identifier = $identifier;
        $this->postType = $postType;
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return Inflector::humanize($this->name ?: $this->identifier);
    }

    /**
     * @return array
     */
    public function getRoute(): array
    {
        if ($this->postType === PostType::PAGE) {
            return ['page/' . $this->identifier, 'url-language' => Yii::$app->language];
        }
        return [];
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        if ($this->postType === PostType::PAGE) {
            return Url::toFeRoute(['/page/view', 'key' => $this->identifier]);
        }
        return '';
    }

    /**
     * @return \Phycom\Frontend\Models\Post\SearchPost|\Phycom\Base\Models\Post|null
     * @throws Exception
     */
    public function getPage(): ?Post
    {
        if (!Yii::$app instanceof \FrontendApplication) {
            return Post::findOne(['identifier' => $this->identifier, 'type' => $this->postType]);
        }
        return Yii::$app->modelFactory->getSearchPost()::findByKey($this->identifier, $this->postType);
    }
}
