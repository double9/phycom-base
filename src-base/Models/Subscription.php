<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\SubscriptionStatus;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "newsletter_subscription".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property integer $emails_sent
 * @property SubscriptionStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 */
class Subscription extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => SubscriptionStatus::class,
				]
			]
		]);
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletter_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
	        [['emails_sent'], 'integer'],
            [['email', 'first_name', 'last_name'], 'string', 'max' => 255],
	        ['status', 'in', 'range' => SubscriptionStatus::all()],
	        ['email', 'unique', 'message' => Yii::t('phycom/base/main', '{email} is already subscribed', ['email' => $this->email])]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('phycom/base/main', 'ID'),
            'first_name'  => Yii::t('phycom/base/main', 'First name'),
            'last_name'   => Yii::t('phycom/base/main', 'Last name'),
            'email'       => Yii::t('phycom/base/main', 'Email'),
            'emails_sent' => Yii::t('phycom/base/main', 'Emails sent'),
            'status'      => Yii::t('phycom/base/main', 'Status'),
            'created_at'  => Yii::t('phycom/base/main', 'Created At'),
            'updated_at'  => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function __toString()
    {
	    return $this->email;
    }
}
