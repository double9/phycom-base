<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Language model
 *
 * Class UserStatus
 * @package Phycom\Base\Models\Attributes
 */
class LanguageStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const UNUSED = 'unused';

    public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('phycom/base/main', 'Visible'),
            self::HIDDEN  => Yii::t('phycom/base/main', 'Hidden'),
            self::UNUSED  => Yii::t('phycom/base/main', 'Unused')
        ];
    }
}
