<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Order model
 *
 * Class OrderStatus
 * @package Phycom\Base\Models\Attributes
 */
class OrderStatus extends EnumAttribute
{
	const NEW = 'new'; //customer started the checkout process, but did not complete it.
	const PENDING_PAYMENT = 'pending_payment'; // customer has completed checkout process, but payment has yet to be confirmed.
	const EXPIRED = 'expired'; // no payment received after n days

	const PAYMENT_COMPLETE = 'payment_complete'; // customer has completed the checkout process and payment has been confirmed

	const PENDING = 'pending'; // customer has completed the checkout process but immediate payment is not required (payment on delivery)

	const PROCESSING = 'processing'; // order is being processed
	const PROCESSING_COMPLETE = 'processing_complete'; // processing of the order has been completed


	const SHIPPED = 'shipped'; // order has been forwarded to courier;
	const COMPLETE = 'complete'; //  order has been shipped/picked up/delivered

	// special cases
    const PARTIALLY_SHIPPED = 'partially_shipped'; // only some items in the order have been shipped, due to some products being pre-order only or other reasons
    const ON_HOLD = 'on_hold';  // order is put on hold
    const CLOSED = 'closed'; // order has been refunded or partially refunded
	const CANCELED = 'canceled'; // order is canceled
	const DELETED = 'deleted'; // order is deleted


	public function getLabelClass()
	{
		if (in_array($this->value, [static::NEW, static::PROCESSING, static::PENDING_PAYMENT])) {
			return $this->cssClassPrefix . 'warning';
		}
		if (in_array($this->value, [static::PENDING])) {
		    return $this->cssClassPrefix . 'info';
        }
		if (in_array($this->value, [static::DELETED])) {
			return $this->cssClassPrefix . 'danger';
		}
        if (in_array($this->value, [static::PAYMENT_COMPLETE])) {
            return $this->cssClassPrefix . 'primary';
        }
		if (in_array($this->value, [static::SHIPPED, static::COMPLETE, static::CLOSED])) {
			return $this->cssClassPrefix . 'success';
		}
		return parent::getLabelClass();
	}

	public function attributeLabels()
    {
        return [
            static::NEW                 => Yii::t('phycom/base/order', 'New'),
            static::PENDING_PAYMENT     => Yii::t('phycom/base/order', 'Pending payment'),
            static::EXPIRED             => Yii::t('phycom/base/order', 'Expired'),
            static::PAYMENT_COMPLETE    => Yii::t('phycom/base/order', 'Payment complete'),
            static::PENDING             => Yii::t('phycom/base/order', 'Pending'),
            static::PROCESSING          => Yii::t('phycom/base/order', 'Processing'),
            static::PROCESSING_COMPLETE => Yii::t('phycom/base/order', 'Processing complete'),
            static::SHIPPED             => Yii::t('phycom/base/order', 'Shipped'),
            static::COMPLETE            => Yii::t('phycom/base/order', 'Complete'),
            static::PARTIALLY_SHIPPED   => Yii::t('phycom/base/order', 'Partially shipped'),
            static::CLOSED              => Yii::t('phycom/base/order', 'Closed'),
            static::ON_HOLD             => Yii::t('phycom/base/order', 'On hold'),
            static::CANCELED            => Yii::t('phycom/base/order', 'Cancelled')
        ];
    }
}
