<?php

namespace Phycom\Base\Models\Attributes;

/**
 * Represents the type attribute on File model
 *
 * Class FileType
 * @package Phycom\Base\Models\Attributes
 *
 * @property bool $isImage
 */
class FileType extends EnumAttribute
{
	const JPG = 'image/jpg';
	const JPEG = 'image/jpeg';
	const PNG = 'image/png';
	const GIF = 'image/gif';
	const TXT = 'text/plain';
	const PDF = 'application/pdf';
	const HTML = 'text/html';
	const XLS = 'application/excel';

	public function getIsImage()
	{
		return substr($this->value, 0, 5) === 'image';
	}

	/**
	 * @return array
	 */
	public static function getImageTypes()
	{
		$images = [];
		foreach (static::getConstants() as $value) {
			if (substr($value, 0, 5) === 'image') {
				$images[] = $value;
			}
		}
		return $images;
	}
}
