<?php

namespace Phycom\Base\Models\Attributes;

use Phycom\Base\Models\User;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class UserSetting
 * @package Phycom\Base\Models\Attributes
 *
 * @property-read array $clientAttributes
 * @property-read array $adminAttributes
 */
class UserSetting extends JsonAttribute
{
	public $country;
	public $language;
	public $currency;
	public $timezone;
	public $formatDate;
	public $formatDatetime;
	public $advertise;

    ### Notification handling settings
	public $receiveOrderNotifications;
    public $receiveErrorNotifications;
	public $receiveShipmentNotifications;

	### UI settings
    public bool $sidebarCollapse = false;

    /**
     * @var User
     */
	protected $owner;
    /**
     * @var array
     */
    protected array $adminAttributeNames = [
        'receiveOrderNotifications',
        'receiveErrorNotifications',
        'receiveShipmentNotifications',
        'sidebarCollapse'
    ];

    public function attributeLabels()
	{
        return [
            'country'                      => Yii::t('phycom/base/main', 'Country'),
            'language'                     => Yii::t('phycom/base/main', 'Language'),
            'currency'                     => Yii::t('phycom/base/main', 'Currency'),
            'timezone'                     => Yii::t('phycom/base/main', 'Timezone'),
            'formatDate'                   => Yii::t('phycom/base/main', 'Date format'),
            'formatDatetime'               => Yii::t('phycom/base/main', 'Datetime format'),
            'advertise'                    => Yii::t('phycom/base/main', 'Advertise'),
            'receiveOrderNotifications'    => Yii::t('phycom/base/main', 'Receive order notifications'),
            'receiveErrorNotifications'    => Yii::t('phycom/base/main', 'Receive error notifications'),
            'receiveShipmentNotifications' => Yii::t('phycom/base/main', 'Receive shipment notifications'),
        ];
	}

	public function rules()
    {
        return [
            [
                [
                    'country',
                    'language',
                    'currency',
                    'timezone',
                    'formatDate',
                    'formatDatetime'
                ], 'string'],
            [
                [
                    'receiveOrderNotifications',
                    'receiveErrorNotifications',
                    'receiveShipmentNotifications',
                    'sidebarCollapse'
                ], 'boolean']
        ];
    }

    public function getClientAttributes(array $except = [])
    {
        $except = ArrayHelper::merge($except, $this->adminAttributeNames);
        return $this->getAttributes(null, $except);
    }

    public function getAdminAttributes(array $except = [])
    {
        return $this->getAttributes($this->adminAttributeNames, $except);
    }

    public function getAttributes($names = null, $except = [])
    {
        if (!$this->owner || (string)$this->owner->type === UserType::CLIENT) {
            $except = ArrayHelper::merge($except, $this->adminAttributeNames);
        }
        return parent::getAttributes($names, $except);
    }


    public function assignDefaults()
	{
		$this->timezone = Yii::$app->timeZone;
		$this->language = Yii::$app->language;

		if ($this->owner && $this->owner->type->isAdmin) {
		    $this->receiveOrderNotifications = true;
            $this->receiveErrorNotifications = true;
		    $this->receiveShipmentNotifications = true;
		    $this->sidebarCollapse = false;
        }
	}
}
