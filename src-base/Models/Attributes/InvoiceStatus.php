<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the status attribute on Invoice model
 *
 * Class InvoiceStatus
 * @package Phycom\Base\Models\Attributes
 */
class InvoiceStatus extends EnumAttribute
{
	const ISSUED = 'issued';
	const PAID = 'paid';
	const CANCELED = 'canceled';
	const DELETED = 'deleted';

    public function getLabelClass()
    {
        if (in_array($this->value, [self::ISSUED])) {
            return $this->cssClassPrefix . 'warning';
        }
        if (in_array($this->value, [self::DELETED, self::CANCELED])) {
            return $this->cssClassPrefix . 'danger';
        }
        if (in_array($this->value, [self::PAID])) {
            return $this->cssClassPrefix . 'success';
        }
        return parent::getLabelClass();
    }

    public function attributeLabels()
    {
        return [
            self::ISSUED   => Yii::t('phycom/base/invoice', 'Issued'),
            self::PAID     => Yii::t('phycom/base/invoice', 'Paid'),
            self::CANCELED => Yii::t('phycom/base/invoice', 'Canceled'),
        ];
    }
}
