<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on File model
 *
 * Class FileStatus
 * @package Phycom\Base\Models\Attributes
 */
class FileStatus extends EnumAttribute
{
	const PROCESSING = 'processing';
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::PROCESSING => Yii::t('phycom/base/file', 'Processing'),
            self::VISIBLE    => Yii::t('phycom/base/file', 'Visible'),
            self::HIDDEN     => Yii::t('phycom/base/file', 'Hidden')
        ];
    }
}
