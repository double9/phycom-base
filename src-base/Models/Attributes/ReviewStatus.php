<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Review model
 *
 * Class ReviewStatus
 * @package Phycom\Base\Models\Attributes
 */
class ReviewStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const APPROVED = 'approved';
	const REJECTED = 'rejected';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::PENDING  => Yii::t('phycom/base/review', 'Pending'),
            self::APPROVED => Yii::t('phycom/base/review', 'Approved'),
            self::REJECTED => Yii::t('phycom/base/review', 'Rejected'),
        ];
    }

    public function isPending()
    {
        return $this->value === self::PENDING;
    }
}
