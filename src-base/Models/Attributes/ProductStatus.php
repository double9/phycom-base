<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the status attribute on Product model
 *
 * Class ProductStatus
 * @package Phycom\Base\Models\Attributes
 */
class ProductStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const OUT_OF_STOCK = 'out_of_stock';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function getLabelClass()
	{
		switch ($this->value) {
			case self::ACTIVE:
				return $this->cssClassPrefix . 'success';
			case self::OUT_OF_STOCK:
				return $this->cssClassPrefix . 'danger';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}

	public function attributeLabels()
    {
        return [
            self::DRAFT        => Yii::t('phycom/base/product-status', 'Draft'),
            self::ACTIVE       => Yii::t('phycom/base/product-status', 'Active'),
            self::INACTIVE     => Yii::t('phycom/base/product-status', 'Inactive'),
            self::OUT_OF_STOCK => Yii::t('phycom/base/product-status', 'Out Of Stock'),
            self::HIDDEN       => Yii::t('phycom/base/product-status', 'Hidden'),
            self::DELETED      => Yii::t('phycom/base/product-status', 'Deleted')
        ];
    }
}
