<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Comment model
 *
 * Class CommentStatus
 * @package Phycom\Base\Models\Attributes
 */
class CommentStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const APPROVED = 'approved';
    const REJECTED = 'rejected';
	const SPAM = 'spam';
	const DELETED = 'deleted';

    public function isPending()
    {
        return $this->value === self::PENDING;
    }

    public function attributeLabels()
    {
        return [
            self::PENDING  => Yii::t('phycom/base/comment', 'Pending'),
            self::APPROVED => Yii::t('phycom/base/comment', 'Approved'),
            self::REJECTED => Yii::t('phycom/base/comment', 'Rejected'),
            self::SPAM     => Yii::t('phycom/base/comment', 'Spam')
        ];
    }
}
