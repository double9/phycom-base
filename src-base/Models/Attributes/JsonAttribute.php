<?php

namespace Phycom\Base\Models\Attributes;

use yii\helpers\Json;
use yii\base\InvalidArgumentException;
use yii\base\Component;
use yii\base\Model;
use yii;

/**
 * This is the base class for JSON string model attributes. It handles the JSON conversion and acts as a regular model having the Labels and attributes
 * Also it is possible to assign default values when constructing the object
 *
 * Class JsonAttribute
 * @package Phycom\Base\Models\Attributes
 *
 * @property-write Component $owner
 */
abstract class JsonAttribute extends Model
{
	/**
	 * @var bool - by default all fields will be exported does not matter if the field is populated or not
	 */
	protected bool $exportOnlyPopulated = false;
	/**
	 * @var Component
	 */
	protected $owner = null;

	/**
	 * JsonAttribute constructor.
	 * @param array|string|null $data - either a json string assoc. array or null
	 * @param array $config
	 */
	public function __construct($data = null, array $config = [])
	{
		if ($data) {
            $this->populate($data);
        }

		parent::__construct($config);

		if ($data === null) {
            $this->assignDefaults();
        }
	}

	public function setOwner(Component $component)
	{
		$this->owner = $component;
	}

	/**
	 * Populates model attributes from the JSON string or array
	 * @param array|string $data
	 */
	public function populate($data)
	{
		try {
			$attributes = is_array($data) ? $data : Json::decode($data, true);
			$this->setAttributes($attributes, false);
		} catch (\Exception $e) {
			Yii::error('Invalid JSON: ' . Json::encode($data));
			throw new InvalidArgumentException('Error decoding JSON string: ' . $e->getMessage());
		}
	}

    /**
     * Converts the model into assoc. array
     *
     * @param array fields
     * @param array $expand
     * @param bool $recursive
     *
     * @return array
     */
	public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $attributes = [];
        if (empty($fields)) {
            foreach ($this->attributes as $attribute => $value) {
                if ($value === null && $this->exportOnlyPopulated) {
                    continue;
                }
                $attributes[$attribute] = $value;
            }
        } else {
            foreach ($fields as $attributeName) {
                if (isset($this->$attributeName)) {
                    if ($this->$attributeName === null && $this->exportOnlyPopulated) {
                        continue;
                    }
                    $attributes[$attributeName] = $this->$attributeName;
                }
            }
        }

        if (!empty($expand)) {
            $attributes = yii\helpers\ArrayHelper::merge($attributes, $expand);
        }
        return $attributes;
    }

    /**
     * @param array fields
     * @param array $expand
     * @param bool $recursive
     *
     * @return string - a JSON representation of the model
     */
    public function toJson(array $fields = [], array $expand = [], $recursive = true)
    {
        return Json::encode($this->toArray($fields, $expand, $recursive));
    }


	public function __toString()
	{
		return $this->toJson();
	}

	/**
	 * Extend this in child model to set default attribute values
	 */
	public function assignDefaults()
	{

	}
}
