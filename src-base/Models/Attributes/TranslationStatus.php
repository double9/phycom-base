<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the status attribute on all Translation models
 *
 * Class TranslationStatus
 * @package Phycom\Base\Models\Attributes
 */
class TranslationStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const PUBLISHED = 'published';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::DRAFT     => Yii::t('phycom/base/translation', 'Draft'),
            self::PUBLISHED => Yii::t('phycom/base/translation', 'Published'),
            self::DELETED   => Yii::t('phycom/base/translation', 'Deleted')
        ];
    }
}
