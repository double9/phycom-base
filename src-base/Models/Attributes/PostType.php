<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the type attribute on Post model
 *
 * Class PostType
 * @package Phycom\Base\Models\Attributes
 */
class PostType extends EnumAttribute
{
	const POST = 'post';    // blog post
	const PAGE = 'page';    // regular page
	const LAND = 'land';    // landing page
    const SHOP = 'shop';    // shop information

    public function attributeLabels()
    {
        return [
            self::POST => Yii::t('phycom/base/post', 'Post'),
            self::PAGE => Yii::t('phycom/base/post', 'Page'),
            self::LAND => Yii::t('phycom/base/main', 'Landing page'),
        ];
    }
}
