<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Vendor model
 *
 * Class VendorStatus
 * @package Phycom\Base\Models\Attributes
 */
class VendorStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::ACTIVE   => Yii::t('phycom/base/main', 'Active'),
            self::INACTIVE => Yii::t('phycom/base/main', 'Inactive'),
        ];
    }
}
