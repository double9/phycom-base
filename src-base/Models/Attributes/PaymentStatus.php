<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the status attribute on Payment model
 *
 * Class PaymentStatus
 * @package Phycom\Base\Models\Attributes
 *
 * @property-read bool $isPending
 */
class PaymentStatus extends EnumAttribute
{
	const PENDING = 'pending';
	const COMPLETED = 'completed';
	const CANCELED = 'cancelled';
	const FAILED = 'failed';
	const EXPIRED = 'expired';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::PENDING   => Yii::t('phycom/base/payment', 'Pending'),
            self::COMPLETED => Yii::t('phycom/base/payment', 'Completed'),
            self::CANCELED  => Yii::t('phycom/base/payment', 'Canceled'),
            self::FAILED    => Yii::t('phycom/base/payment', 'Failed'),
            self::EXPIRED   => Yii::t('phycom/base/payment', 'Expired'),
        ];
    }

    public function getLabelClass()
	{
		switch ($this->value) {
			case self::PENDING:
				return $this->cssClassPrefix . 'warning';
			case self::COMPLETED:
				return $this->cssClassPrefix . 'success';
			case self::DELETED:
			case self::CANCELED:
            case self::EXPIRED:
            case self::FAILED:
				return $this->cssClassPrefix . 'danger';
			default:
				return $this->cssClassPrefix . 'default';
		}
	}

	public function getIsPending()
    {
        return $this->value === self::PENDING;
    }
}
