<?php

namespace Phycom\Base\Models\Attributes;

/**
 * Represents the status attribute on Message model
 *
 * Class MessageStatus
 * @package Phycom\Base\Models\Attributes
 */
class MessageStatus extends EnumAttribute
{
	const NEW = 'new';
	const QUEUED = 'queued';
	const DISPATCHED = 'dispatched';
	const BOUNCED = 'bounced';
	const SENT = 'sent';
	const DELIVERED = 'delivered';
	const DELETED = 'deleted';
 }
