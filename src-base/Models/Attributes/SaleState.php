<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Class SaleState
 * @package Phycom\Base\Models\Attributes
 */
class SaleState extends EnumAttribute
{
	const OPEN = 'open';
	const CLOSED = 'closed';

    public function attributeLabels()
    {
        return [
            self::OPEN   => Yii::t('phycom/base/main', 'Open'),
            self::CLOSED => Yii::t('phycom/base/main', 'Closed'),
        ];
    }
}
