<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on ProductVariant model
 *
 * Class ProductVariantStatus
 * @package Phycom\Base\Models\Attributes
 */
class ProductVariantStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';

    public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('phycom/base/variant', 'Visible'),
            self::HIDDEN  => Yii::t('phycom/base/variant', 'Hidden')
        ];
    }
}
