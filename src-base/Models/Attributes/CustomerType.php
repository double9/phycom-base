<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the customer_type attribute on Invoice model
 *
 * Class CustomerType
 * @package Phycom\Base\Models\Attributes
 */
class CustomerType extends EnumAttribute
{
	const INDIVIDUAL = 'individual';
	const COMPANY = 'company';

    public function attributeLabels()
    {
        return [
            self::INDIVIDUAL => Yii::t('phycom/base/main', 'Individual'),
            self::COMPANY    => Yii::t('phycom/base/main', 'Company')
        ];
    }
}
