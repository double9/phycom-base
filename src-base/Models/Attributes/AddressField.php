<?php

namespace Phycom\Base\Models\Attributes;

use Phycom\Base\Models\Country;

use Yii;

/**
 * Class AddressField
 * @package Phycom\Base\Models
 */
class AddressField extends JsonAttribute
{
    ### Primary address fields
	public $country;
	public $state;
	public $province;
	public $locality;
	public $city;
	public $district;
	public $street;
	public $house;
	public $room;
	public $postcode;

	### Contact Meta fields
    public $name;
    public $firstName;
    public $lastName;
    public $company;
    public $email;
    public $phone;

    ### Other Meta fields
    public $lat;
    public $lng;
    public $mapUrl;
    public $info;       // custom description to hint the accurate place

    /**
     * @var bool - In most of times we want o export only those address fields that have a value not to write empty fields to database
     */
	protected bool $exportOnlyPopulated = true;


	public function attributeLabels()
	{
        return [
            'country'  => Yii::t('phycom/base/main', 'Country'),
            'state'    => Yii::t('phycom/base/main', 'State'),
            'province' => Yii::t('phycom/base/main', 'Province'),
            'locality' => Yii::t('phycom/base/main', 'Locality'),
            'city'     => Yii::t('phycom/base/main', 'City'),
            'district' => Yii::t('phycom/base/main', 'District'),
            'street'   => Yii::t('phycom/base/main', 'Street'),
            'house'    => Yii::t('phycom/base/main', 'House'),
            'room'     => Yii::t('phycom/base/main', 'Room'),
            'postcode' => Yii::t('phycom/base/main', 'Postcode'),

            'name'      => Yii::t('phycom/base/main', 'Name'),
            'firstName' => Yii::t('phycom/base/main', 'First Name'),
            'lastName'  => Yii::t('phycom/base/main', 'Last Name'),
            'company'   => Yii::t('phycom/base/main', 'Company'),
            'email'     => Yii::t('phycom/base/main', 'Email'),
            'phone'     => Yii::t('phycom/base/main', 'Phone'),

            'lat'    => Yii::t('phycom/base/main', 'Latitude'),
            'lng'    => Yii::t('phycom/base/main', 'Longitude'),
            'mapUrl' => Yii::t('phycom/base/main', 'Map url'),
            'info'   => Yii::t('phycom/base/main', 'Info')
        ];
	}

	public function rules()
	{
		return [array_keys($this->attributes), 'safe'];
	}

	public function assignDefaults()
	{
		$this->country = (string)Yii::$app->country;
	}


    /**
     * @param bool $fetchCountryName
     * @return array
     */
    public function exportArray(bool $fetchCountryName = false)
    {
        $address = [$this->street];

        if ($this->house) {
            $number = $this->house;
            if ($this->room) {
                $number .= '-' . $this->room;
            }
            $address[] = $number;
        }

        if ($this->district) {
            $address[] = $this->district;
        }
        if ($this->city) {
            $address[] = $this->city;
        }
        if ($this->locality) {
            $address[] = $this->locality;
        }
        if ($this->province) {
            $address[] = $this->province;
        }
        if ($this->state) {
            $address[] = $this->state;
        }
        if ($this->postcode) {
            $address[] = $this->postcode;
        }
        if ($this->country !== (string)Yii::$app->country && $fetchCountryName) {
            if ($country = Country::findOne(['code' => $this->country])) {
                $address[] = $country->name;
            }
        }
        return $address;
    }
}
