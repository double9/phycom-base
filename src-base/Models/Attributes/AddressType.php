<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the type attribute on Address model
 *
 * Class AddressType
 * @package Phycom\Base\Models\Attributes
 */
class AddressType extends EnumAttribute
{
	const MAIN = 'main';
	const INVOICE = 'invoice';
	const SHIPPING = 'shipping';
	const OTHER = 'other';

	public function attributeLabels()
    {
        return [
            self::MAIN     => Yii::t('phycom/base/address', 'Main'),
            self::INVOICE  => Yii::t('phycom/base/address', 'Invoice'),
            self::SHIPPING => Yii::t('phycom/base/address', 'Shipping'),
            self::OTHER    => Yii::t('phycom/base/address', 'Other')
        ];
    }
}
