<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the type attribute on Vendor model
 *
 * Class VendorType
 * @package Phycom\Base\Models\Attributes
 */
class VendorType extends EnumAttribute
{
	const SITE_OWNER = 'site_owner';
	const RESELLER = 'reseller';

    public function attributeLabels()
    {
        return [
            self::SITE_OWNER => Yii::t('phycom/base/vendor', 'Site owner'),
            self::RESELLER   => Yii::t('phycom/base/vendor', 'Reseller'),
        ];
    }
}
