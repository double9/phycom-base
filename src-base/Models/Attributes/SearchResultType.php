<?php

namespace Phycom\Base\Models\Attributes;

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii;
/**
 * Class SearchResultType
 * @package Phycom\Base\Models\Attributes
 */
class SearchResultType extends EnumAttribute
{
    const PRODUCT = 'product';
    const POST = PostType::POST;
    const PAGE = PostType::PAGE;
    const COMMENT = 'comment';
    const ORDER = 'order';
    const INVOICE = 'invoice';
    const PAYMENT = 'payment';
    const USER = 'user';
    const SHIPMENT = 'shipment';
    const COUPON = 'coupon';

    public function attributeLabels()
    {
        return [
            self::PRODUCT  => Yii::t('phycom/base/main', 'Product'),
            self::POST     => Yii::t('phycom/base/main', 'Post'),
            self::PAGE     => Yii::t('phycom/base/main', 'Page'),
            self::COMMENT  => Yii::t('phycom/base/main', 'Comment'),
            self::ORDER    => Yii::t('phycom/base/main', 'Order'),
            self::INVOICE  => Yii::t('phycom/base/main', 'Invoice'),
            self::PAYMENT  => Yii::t('phycom/base/main', 'Payment'),
            self::USER     => Yii::t('phycom/base/main', 'User'),
            self::SHIPMENT => Yii::t('phycom/base/main', 'Shipment'),
            self::COUPON   => Yii::t('phycom/base/main', 'Coupon'),
        ];
    }

    public function getIcon()
    {
        switch ($this->value) {
            case self::PRODUCT:
                return FAS::i(FAS::_BARCODE);
            case self::USER:
                return FAR::i(FAR::_USER);
            case self::POST:
                return FAR::i(FAR::_BOOK);
            case self::PAGE:
                return FAR::i(FAR::_EDIT);
            case self::COMMENT:
                return FAR::i(FAR::_COMMENT);
            case self::ORDER:
                return FAS::i(FAS::_SHOPPING_CART);
            case self::INVOICE:
                return FAR::i(FAS::_FILE);
            case self::PAYMENT:
                return FAR::i(FAR::_EURO_SIGN);
            case self::SHIPMENT:
                return FAS::i(FAS::_TRUCK);
            case self::COUPON:
                return FAS::i(FAS::_QRCODE);
            default:
                return FAR::i(FAR::_CIRCLE);
        }
    }
}
