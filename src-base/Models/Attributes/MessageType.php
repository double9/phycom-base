<?php

namespace Phycom\Base\Models\Attributes;

/**
 * Represents the type attribute on Message model
 *
 * Class MessageType
 * @package Phycom\Base\Models\Attributes
 *
 * @property bool $isEmail
 * @property bool $isSms
 */
class MessageType extends EnumAttribute
{
	const EMAIL = 'email';
	const SMS = 'sms';
	const MESSAGE = 'message';
	const ALERT	= 'alert';

	/**
	 * @return bool
	 */
	public function getIsEmail()
	{
		return $this->is(static::EMAIL);
	}

	/**
	 * @return bool
	 */
	public function getIsSms()
	{
		return $this->is(static::SMS);
	}
}
