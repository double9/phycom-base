<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Shipment model
 *
 * Class ShipmentStatus
 * @package Phycom\Base\Models\Attributes
 */
class ShipmentStatus extends EnumAttribute
{
	const PENDING = 'pending'; // shipment is scheduled but the order is not processed yet
	const ASSEMBLED = 'assembled'; // order is processed and the package has been assembled
	const DISPATCHED = 'dispatched'; // courier has started delivery
	const DELIVERED = 'delivered'; // shipment has been delivered to customer
	const LOST = 'lost';
	const DAMAGED = 'damaged';
	const FAILURE = 'failure';
	const RETURNED = 'returned'; // shipment has been returned due customer was not contacted in time
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::PENDING    => Yii::t('phycom/base/shipment', 'Pending'),
            self::ASSEMBLED  => Yii::t('phycom/base/shipment', 'Assembled'),
            self::DISPATCHED => Yii::t('phycom/base/shipment', 'Dispatched'),
            self::DELIVERED  => Yii::t('phycom/base/shipment', 'Delivered'),
            self::LOST       => Yii::t('phycom/base/shipment', 'Lost'),
            self::DAMAGED    => Yii::t('phycom/base/shipment', 'Damaged'),
            self::FAILURE    => Yii::t('phycom/base/shipment', 'Failure'),
            self::RETURNED   => Yii::t('phycom/base/shipment', 'Returned')
        ];
    }

    public function getLabelClass()
	{
		if (in_array($this->value, [self::PENDING, self::ASSEMBLED])) {
			return $this->cssClassPrefix . 'warning';
		}
		if (in_array($this->value, [self::DELETED, self::LOST, self::DAMAGED])) {
			return $this->cssClassPrefix . 'danger';
		}
		if ($this->value === self::DISPATCHED) {
			return $this->cssClassPrefix . 'primary';
		}
		if ($this->value === self::DELIVERED) {
			return $this->cssClassPrefix . 'success';
		}
		return parent::getLabelClass();
	}
}
