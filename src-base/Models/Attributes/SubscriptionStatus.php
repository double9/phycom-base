<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Subscription model
 *
 * Class SubscriptionStatus
 * @package Phycom\Base\Models\Attributes
 */
class SubscriptionStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const INACTIVE = 'inactive';
	const OPT_OUT = 'opt_out';

    public function attributeLabels()
    {
        return [
            self::ACTIVE   => Yii::t('phycom/base/subscription', 'Active'),
            self::INACTIVE => Yii::t('phycom/base/subscription', 'Inactive'),
            self::OPT_OUT  => Yii::t('phycom/base/subscription', 'Opt out'),
        ];
    }
}
