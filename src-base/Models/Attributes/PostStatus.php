<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on Post model
 *
 * Class PostStatus
 * @package Phycom\Base\Models\Attributes
 */
class PostStatus extends EnumAttribute
{
	const DRAFT = 'draft';
	const PUBLISHED = 'published';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::DRAFT     => Yii::t('phycom/base/post', 'Draft'),
            self::PUBLISHED => Yii::t('phycom/base/post', 'Published'),
            self::HIDDEN    => Yii::t('phycom/base/post', 'Hidden'),
        ];
    }


    public function getLabelClass()
    {
        if (in_array($this->value, [self::DELETED])) {
            return $this->cssClassPrefix . 'danger';
        }
        if (in_array($this->value, [self::PUBLISHED])) {
            return $this->cssClassPrefix . 'success';
        }
        return parent::getLabelClass();
    }
}
