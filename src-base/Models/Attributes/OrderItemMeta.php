<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Class OrderItemMeta
 * @package Phycom\Base\Models
 */
class OrderItemMeta extends JsonAttribute
{
	public $title;

	public function attributeLabels()
	{
		return [
			'title' => Yii::t('phycom/base/main', 'Title'),
		];
	}
}
