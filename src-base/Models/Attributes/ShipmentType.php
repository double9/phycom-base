<?php

namespace Phycom\Base\Models\Attributes;


use yii;
/**
 * Represents the status attribute on Shipment model
 *
 * Class ShipmentType
 * @package Phycom\Base\Models\Attributes
 */
class ShipmentType extends EnumAttribute
{
	const DELIVERY = 'delivery';
	const SELF_PICKUP = 'pickup';

    public function attributeLabels()
    {
        return [
            self::DELIVERY    => Yii::t('phycom/base/shipment', 'Delivery'),
            self::SELF_PICKUP => Yii::t('phycom/base/shipment', 'Self pickup'),
        ];
    }
}
