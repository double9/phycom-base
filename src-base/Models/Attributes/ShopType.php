<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the type attribute on Shop model
 *
 * Class ShopType
 * @package Phycom\Base\Models\Attributes
 */
class ShopType extends EnumAttribute
{
	const SHOP = 'shop';
	const CAFETERIA = 'cafeteria';
	const SHOWROOM = 'showroom';
	const STORE = 'store';
	const MARKET = 'market';
	const POINT_OF_SALE = 'point_of_sale';

    public function attributeLabels()
    {
        return [
            self::SHOP          => Yii::t('phycom/base/shop', 'Shop'),
            self::CAFETERIA     => Yii::t('phycom/base/shop', 'Cafeteria'),
            self::SHOWROOM      => Yii::t('phycom/base/shop', 'Showroom'),
            self::STORE         => Yii::t('phycom/base/shop', 'Store'),
            self::MARKET        => Yii::t('phycom/base/shop', 'Market'),
            self::POINT_OF_SALE => Yii::t('phycom/base/shop', 'Point of sale')
        ];
    }
}
