<?php

namespace Phycom\Base\Models\Attributes;

/**
 * Represents the type attribute on ProductCategoryRelation model
 *
 * Class RelatedCategoryType
 * @package Phycom\Base\Models\Attributes
 */
class RelatedCategoryType extends EnumAttribute
{
	const SIMILAR = 'similar';
	const ACCESSORIES = 'accessories';
}
