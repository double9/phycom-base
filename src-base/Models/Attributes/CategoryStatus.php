<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on ProductCategory model
 *
 * Class ProductCategoryStatus
 * @package Phycom\Base\Models\Attributes
 *
 * @property bool $isHidden
 * @property bool $isVisible
 */
class CategoryStatus extends EnumAttribute
{
	const VISIBLE = 'visible';
	const HIDDEN = 'hidden';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::VISIBLE => Yii::t('phycom/base/category', 'Visible'),
            self::HIDDEN  => Yii::t('phycom/base/category', 'Hidden'),
            self::DELETED => Yii::t('phycom/base/category', 'Deleted')
        ];
    }

    public function getLabelClass()
	{
		switch ($this->value) {
			case self::VISIBLE:
				return $this->cssClassPrefix . 'success';
			default:
				return parent::getLabelClass();
		}
	}

	public function getIsHidden()
	{
		return $this->is(self::HIDDEN);
	}

	public function getIsVisible()
	{
		return $this->is(self::VISIBLE);
	}
}
