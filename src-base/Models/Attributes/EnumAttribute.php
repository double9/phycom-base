<?php

namespace Phycom\Base\Models\Attributes;

use Phycom\Base\Helpers\c;
use Phycom\Base\Models\Traits\ClassConstantTrait;
use Phycom\Base\Interfaces\DynamicAttributeInterface;

use yii\helpers\Inflector;
use yii\base\InvalidArgumentException;
use yii\base\BaseObject;

use Yii;

/**
 * Base class for any of the enum type attributes.
 *
 * Class EnumAttribute
 * @package Phycom\Base\Models\Attributes
 *
 * @property mixed value
 * @property array values
 * @property-read string prefix
 * @property-read string label
 * @property-read string labelClass
 */
abstract class EnumAttribute extends BaseObject implements DynamicAttributeInterface
{
	use ClassConstantTrait;

	protected $value;

	protected $cssClassPrefix;

    /**
     * @param array $exclude
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
	public static function all(array $exclude = [])
	{
	    if (!empty($exclude)) {
	        return array_diff(static::create()->values, $exclude);
        }
		return static::create()->values;
	}

    /**
     * @param array $exclude
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
	public static function displayValues(array $exclude = ['deleted'])
	{
		$result = [];
        /**
         * @var static $instance
         */
		$instance = static::create();
		$labels = $instance->attributeLabels();
		foreach ($instance->values as $value) {
			if (!in_array($value, $exclude)) {
				$result[$value] = $labels[$value] ?? Inflector::titleize($value);
			}
		}
		return $result;
	}

    /**
     * @param null $value
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
	public static function create($value = null)
	{
		return Yii::createObject(static::class, [$value]);
	}

	public function __construct($value = null, array $config = [])
	{
		$this->value = $value;
		parent::__construct($config);
	}

	public function init()
    {
        parent::init();
        $this->cssClassPrefix = c::param('labelCssClassName', 'label-');
    }

    public function getPrefix()
	{
		return null;
	}

	public function setValue($value)
	{
		if ($value === '') {
			$value = null;
		}
		if ($value !== null && !in_array($value, $this->getValues())) {
			throw new InvalidArgumentException('Invalid ' . static::class . ' value ' . json_encode($value));
		}
		$this->value = $value;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function getValues()
	{
		return static::getConstants($this->prefix);
	}

	public function attributeLabels()
    {
        return [
            'deleted' => Yii::t('phycom/base/main', 'Deleted')
        ];
    }

	public function getLabel()
	{
	    $labels = $this->attributeLabels();
		return $labels[$this->getValue()] ?? Inflector::titleize($this->getValue());
	}

	public function getLabelClass()
	{
		return $this->cssClassPrefix . 'default';
	}

	public function getCssClass($prefix = '')
    {
        $originalPrefix = $this->cssClassPrefix;
        $this->cssClassPrefix = '';
        $class = $prefix . $this->getLabelClass();
        $this->cssClassPrefix = $originalPrefix;
        return $class;
    }

	/**
	 * @param mixed $value
	 * @return bool
	 */
	public function is($value)
	{
		if ($value instanceof static) {
			$value = $value->value;
		}
		return $this->value === $value;
	}

	/**
	 * @param array $values
	 * @return bool
	 */
	public function in(array $values)
	{
		return in_array($this->value, $values);
	}

	public function __toString()
	{
		return (string) $this->value;
	}
}
