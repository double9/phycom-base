<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the type attribute on UserFile model
 *
 * Class UserFileType
 * @package Phycom\Base\Models\Attributes
 */
class UserFileType extends EnumAttribute
{
	const PROFILE_IMAGE = 'profile_image';
	const AVATAR = 'avatar';
	const DOCUMENT = 'document';
	const OTHER = 'other';

    public function attributeLabels()
    {
        return [
            self::PROFILE_IMAGE => Yii::t('phycom/base/file', 'Profile image'),
            self::AVATAR        => Yii::t('phycom/base/file', 'Avatar'),
            self::DOCUMENT      => Yii::t('phycom/base/file', 'Document'),
            self::OTHER         => Yii::t('phycom/base/file', 'Other'),
        ];
    }
}
