<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on DiscountCard model
 *
 * Class DiscountCardStatus
 * @package Phycom\Base\Models\Attributes
 */
class DiscountRuleStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const CLOSED = 'closed';
	const DELETED = 'deleted';

	public function attributeLabels()
    {
        return [
            self::ACTIVE => Yii::t('phycom/base/discount', 'Active'),
            self::CLOSED => Yii::t('phycom/base/discount', 'Closed'),
        ];
    }
}
