<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the status attribute on models representing a contact attribute like Email, Address, Phone
 *
 * Class ContactAttributeStatus
 * @package Phycom\Base\Models\Attributes
 */
class ContactAttributeStatus extends EnumAttribute
{
	const UNVERIFIED = 'unverified';
	const VERIFIED = 'verified';
	const DELETED = 'deleted';

    public function attributeLabels()
    {
        return [
            self::UNVERIFIED => Yii::t('phycom/base/main', 'Unverified'),
            self::VERIFIED   => Yii::t('phycom/base/main', 'Verified'),
        ];
    }
}
