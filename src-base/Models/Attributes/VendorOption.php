<?php

namespace Phycom\Base\Models\Attributes;

use Phycom\Base\Components\FileStorage;
use Phycom\Base\Models\AttachmentUrl;

use yii\base\InvalidConfigException;
use Yii;

/**
 * Class VendorOption
 * @package Phycom\Base\Models\Attributes
 *
 * @property-read AttachmentUrl $logoUrl
 */
class VendorOption extends JsonAttribute
{
    /**
     * @var string|null
     */
    public ?string $vatNumber = null;

    /**
     * @var string|null
     */
    public ?string $logo = null;

    /**
     * @var string|null
     */
    public ?string $website = null;

    /**
     * @var array
     */
    public array $paymentAccounts = [];

    /**
     * @var string
     */
    protected string $bucket = FileStorage::BUCKET_CONTENT_FILES;


    public function attributeLabels()
    {
        return [
            'vatNumber'       => Yii::t('phycom/base/main', 'VAT Number'),
            'logo'            => Yii::t('phycom/base/main', 'Logo'),
            'website'         => Yii::t('phycom/base/main', 'Website'),
            'paymentAccounts' => Yii::t('phycom/base/main', 'Payment accounts')
        ];
    }

    /**
     * @return string
     */
    public function getFileBucket(): string
    {
        return $this->bucket;
    }

    /**
     * @return AttachmentUrl|object
     * @throws InvalidConfigException
     */
    public function getLogoUrl(): AttachmentUrl
    {
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->logo ? $this->bucket : null,
            'filename' => $this->logo ?: null,
        ]);
    }
}
