<?php

namespace Phycom\Base\Models\Attributes;

/**
 * Represents the type attribute on UserToken model
 *
 * Class UserTokenType
 * @package Phycom\Base\Models\Attributes
 */
class UserTokenType extends EnumAttribute
{
	const PASSWORD_RESET = 'password_reset';
	const ACCESS_TOKEN = 'access';
	const REGISTRATION_REQUEST = 'reg_request';
}
