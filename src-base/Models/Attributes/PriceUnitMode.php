<?php

namespace Phycom\Base\Models\Attributes;

use yii;

/**
 * Represents the unit mode of the product pricing. Unit mode defines rules for which unit selection is available for the product.
 *
 * Class PriceUnitMode
 * @package Phycom\Base\Models\Attributes
 *
 * @property-read string $unitLabel
 */
class PriceUnitMode extends EnumAttribute
{
	const ANY = 'any';          // all units can be selected regardless of the product_price record exists or not
	const STRICT = 'strict';    // only units defined in product_price table can be selected
    const LIMIT = 'limit';      // units limited by min max value


	public function attributeLabels()
    {
        return [
            self::ANY    => Yii::t('phycom/base/product', 'Any'),
            self::STRICT => Yii::t('phycom/base/product', 'Strict'),
            self::LIMIT  => Yii::t('phycom/base/product', 'Limit'),
        ];
    }
}
