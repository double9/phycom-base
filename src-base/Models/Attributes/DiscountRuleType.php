<?php

namespace Phycom\Base\Models\Attributes;

use yii;
/**
 * Represents the type attribute on DiscountRule model
 *
 * Class DiscountRuleType
 * @package Phycom\Base\Models\Attributes
 */
class DiscountRuleType extends EnumAttribute
{
    const RULE = 'rule';                // discount rule that applies without the code
	const COUPON = 'coupon';            // offers one-time discount by percentage
	const VOUCHER = 'voucher';          // one-time discount card by a certain amount
	const CLIENT_CARD = 'client_card';  // personalized coupons for using multiple times until expired/deleted

    public function attributeLabels()
    {
        return [
            self::RULE        => Yii::t('phycom/base/discount', 'Rule'),
            self::COUPON      => Yii::t('phycom/base/discount', 'Coupon'),
            self::VOUCHER     => Yii::t('phycom/base/discount', 'Voucher'),
            self::CLIENT_CARD => Yii::t('phycom/base/discount', 'Client Card'),
        ];
    }
}
