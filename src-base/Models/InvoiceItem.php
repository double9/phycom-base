<?php

namespace Phycom\Base\Models;

use yii;

/**
 * This is the model class for table "invoice_item".
 *
 * @property integer $invoice_id
 * @property integer $order_item_id
 * @property integer $quantity
 *
 * @property OrderItem $orderItem
 * @property Invoice $invoice
 */
class InvoiceItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_item';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'order_item_id'], 'required'],
            [['invoice_id', 'order_item_id', 'quantity'], 'integer'],
            [['order_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getOrderItem()), 'targetAttribute' => ['order_item_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getInvoice()), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('phycom/base/main', 'Invoice ID'),
            'order_item_id' => Yii::t('phycom/base/main', 'Order Item ID'),
	        'quantity' => Yii::t('phycom/base/main', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getOrderItem()), ['id' => 'order_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getInvoice()), ['id' => 'invoice_id']);
    }
}
