<?php

namespace Phycom\Base\Models\Translation;


use Phycom\Base\Models\Attributes\TranslationStatus;
use Phycom\Base\Models\Language;
use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\Behaviors\UrlKeyBehavior;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Base class for the Translation Active Record models
 *
 * Class Translation
 * @package Phycom\Base\Models\Translation
 *
 * @property integer $id
 * @property string $language
 * @property string $url_key
 * @property string $title
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property TranslationStatus $status
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Language $languageModel
 * @property ActiveRecord $model
 *
 * @method generateUrlKey()
 */
abstract class Translation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'dynamic-attribute' => [
                'attributes' => [
                    'status' => TranslationStatus::class
                ]
            ],
            'url-key' => UrlKeyBehavior::class
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'status'], 'required', 'enableClientValidation' => false],
            [['title', 'meta_title', 'meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['language'], 'string', 'max' => 2],
            [['url_key'], 'unique'],
            [['url_key'], 'string', 'max' => 255],
            [['language'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language' => 'code']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('phycom/base/translation', 'ID'),
            'language'         => Yii::t('phycom/base/translation', 'Language'),
            'url_key'          => Yii::t('phycom/base/translation', 'Url Key'),
            'title'            => Yii::t('phycom/base/translation', 'Title'),
            'meta_title'       => Yii::t('phycom/base/translation', 'Meta title'),
            'meta_keywords'    => Yii::t('phycom/base/translation', 'Meta keywords'),
            'meta_description' => Yii::t('phycom/base/translation', 'Meta description'),
            'status'           => Yii::t('phycom/base/translation', 'Translation status'),
            'created_at'       => Yii::t('phycom/base/translation', 'Created At'),
            'updated_at'       => Yii::t('phycom/base/translation', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguageModel()
    {
        return $this->hasOne(Language::class, ['code' => 'language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    abstract public function getModel();
}
