<?php

namespace Phycom\Base\Models;

use Phycom\Base\Events\StatusUpdateEvent;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Behaviors\StatusBehavior;
use Phycom\Base\Models\Traits\ModelTranslationTrait;
use Phycom\Base\Models\Translation\PostTranslation;
use Phycom\Base\Models\Translation\Translation;
use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Helpers\Url;
use Phycom\Base\Helpers\c;

use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $identifier
 * @property integer $created_by
 * @property integer $vendor_id
 * @property integer $shop_id
 * @property PostType $type
 * @property PostStatus $status
 * @property \DateTime $published_at
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property Comment[] $comments
 * @property Vendor $vendor
 * @property File $image
 * @property Shop $shop
 * @property User $createdBy
 * @property PostAttachment $attachmentImage
 * @property PostAttachment[] $attachments
 * @property File[] $attachmentFiles
 * @property PostCategory[] $categories
 * @property PostTag[] $tags
 * @property PostTranslation[] $translations
 * @property PostTranslation $translation
 *
 * @property-read bool $isPost
 * @property-read bool $isPage
 */
class Post extends ActiveRecord
{
    use ModelTranslationTrait;

    const EVENT_AFTER_STATUS_UPDATE = 'afterStatusUpdate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => PostStatus::class,
                    'type' => PostType::class
				]
			],
            'timestamp' => [
                'attributes' => [
                    ['created_at', ActiveRecord::EVENT_BEFORE_INSERT],
                    ['updated_at', ActiveRecord::EVENT_BEFORE_INSERT, ActiveRecord::EVENT_BEFORE_UPDATE],
                    'published_at'
                ]
            ],
            'status' => StatusBehavior::class
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'vendor_id', 'type', 'status'], 'required'],
            [['created_by', 'vendor_id', 'shop_id'], 'integer'],
            [['created_at', 'updated_at', 'published_at', 'type', 'status'], 'safe'],
            [['identifier'], 'string', 'max' => 255],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getVendor()), 'targetAttribute' => ['vendor_id' => 'id']],
            [['shop_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getShop()), 'targetAttribute' => ['shop_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        $this->status = PostStatus::DELETED;
        if ($this->identifier) {
            $this->identifier = uniqid($this->identifier . '__');
        }
        return $this->save();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('phycom/base/post', 'ID'),
            'identifier'   => Yii::t('phycom/base/post', 'Location identifier'),
            'image'        => Yii::t('phycom/base/post', 'Image'),
            'created_by'   => Yii::t('phycom/base/post', 'Created By'),
            'type'         => Yii::t('phycom/base/post', 'Type'),
            'published_at' => Yii::t('phycom/base/post', 'Published At'),
            'created_at'   => Yii::t('phycom/base/main', 'Created At'),
            'updated_at'   => Yii::t('phycom/base/main', 'Updated At'),
            'status'       => Yii::t('phycom/base/main', 'Status'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $isStatusUpdate = in_array('status', array_keys($changedAttributes));
        if ($isStatusUpdate && (string) $this->status === PostStatus::PUBLISHED) {
            $changedAttributes['published_at'] = $this->published_at;
            $this->published_at = $this->updated_at;

            Yii::$app->db->createCommand()
                ->update($this::tableName(), ['published_at' => $this->published_at], ['id' => $this->id])
                ->execute();
        }

        parent::afterSave($insert, $changedAttributes);

        if ($isStatusUpdate) {
            $this->trigger(self::EVENT_AFTER_STATUS_UPDATE, new StatusUpdateEvent([
                'prevStatus' => $changedAttributes['status']
            ]));
        }
    }

    public function getIsPost()
    {
        return (string)$this->type === PostType::POST;
    }

    public function getIsPage()
    {
        return (string)$this->type === PostType::PAGE;
    }

    /**
     * @param string $languageCode
     * @param bool $fallback
     * @return PostTranslation|Translation
     */
    public function getTranslation($languageCode = null, $fallback = true)
    {
        return $this->getTranslationModel(PostTranslation::class, $languageCode, $fallback);
    }

    /**
     * @param string|bool $scheme
     * @param string|null $language
     * @return string
     */
    public function getUrl($scheme = false, string $language = null)
    {
        if ($this->type->value === PostType::PAGE) {
            return Url::toFeRoute(['/'. $this->getTranslation($language)->url_key, 'language' => $language], $scheme);
        }
        return Url::toFeRoute(['/' . $this->type . '/view', 'key' => $this->getTranslation($language)->url_key, 'language' => $language], $scheme);
    }

    /**
     * @param string $type
     * @param false $scheme
     * @param string|null $language
     * @return string
     */
    public function getCustomTypeUrl(string $type, $scheme = false, string $language = null): string
    {
        return Url::toFeRoute(['/' . $type . '/view', 'key' => $this->getTranslation($language)->url_key, 'language' => $language], $scheme);
    }

    /**
     * @param int $wordCount
     * @return string
     */
    public function getExcerpt($wordCount = null)
    {
        if ($wordCount === null) {
            $wordCount = c::param('excerptWords');
        }
        return StringHelper::truncateWords(strip_tags($this->getTranslation()->content), $wordCount);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getVendor()), ['id' => 'vendor_id']);
    }

    /**
     * @deprecated use getAttachmentImage() instead
     *
     * @return yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getImage()
    {
        return $this->hasOne(File::class, ['id' => 'file_id'])->viaTable(PostAttachment::tableName(), ['post_id' => 'id'], function ($query) {
            /**
             * @var yii\db\ActiveQuery $query
             */
            $query->onCondition(['order' => 1]);
        });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShop()
    {
        return $this->hasOne(Shop::class, ['id' => 'shop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachmentImage()
    {
        return $this->hasOne(PostAttachment::class, ['post_id' => 'id'])->onCondition(['order' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(PostAttachment::class, ['post_id' => 'id'])->orderBy(['order' => SORT_ASC]);
    }

    /**
     * @return yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getAttachmentFiles()
    {
        return $this->hasMany(File::class, ['id' => 'file_id'])->viaTable(PostAttachment::tableName(), ['post_id' => 'id']);
    }

    /**
     * @return yii\db\ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getCategories()
    {
        return $this->hasMany(PostCategory::class, ['id' => 'category_id'])->viaTable(PostCategoryPostRelation::tableName(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(PostTag::class, ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PostTranslation::class, ['post_id' => 'id']);
    }
}
