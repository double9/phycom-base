<?php

namespace Phycom\Base\Models;


use yii\base\Model;

/**
 * Class UserMessage
 * @package Phycom\Base\Models
 */
class UserMessage extends Model
{
    protected $user;

    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }


}
