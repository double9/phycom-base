<?php

namespace Phycom\Base\Models;

use Phycom\Base\Models\Attributes\FileType;

use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "message_attachment".
 *
 * @property integer $id
 * @property integer $message_id
 * @property FileType $mime_type
 * @property string $file_name
 * @property string $file_path
 * @property \DateTime $created_at
 *
 * @property Message $message
 */
class MessageAttachment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_attachment';
    }

    /**
     * @param int $messageId
     * @param string $filename
     * @param null $mimeType
     * @return MessageAttachment|object
     * @throws InvalidConfigException
     */
    public static function create(int $messageId, string $filename, $mimeType = null): MessageAttachment
    {
        if (!file_exists($filename)) {
            throw new InvalidArgumentException('File ' . $filename . ' was not found');
        }
        if (null === $mimeType) {
            $mimeType = FileHelper::getMimeType($filename);
            if (!$mimeType) {
                throw new InvalidArgumentException('Could not find message attachment file mime type');
            }
        }

        $rootPath = Yii::getAlias(Yii::$app->messages->attachmentFileRootPath);
        $filePath = Yii::getAlias($filename);

        // keep only relative paths in db
        if (substr($filePath, 0, strlen($rootPath)) === $rootPath) {
            $filePath = substr($filePath, strlen($rootPath));
        } else {
            throw new InvalidArgumentException('Message attachment path ' . $filename . ' is invalid');
        }

        return Yii::createObject([
            'class'      => static::class,
            'message_id' => $messageId,
            'file_path'  => $filePath,
            'mime_type'  => FileType::create($mimeType)
        ]);
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'mime_type' => FileType::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'mime_type', 'file_path'], 'required'],
            [['message_id'], 'integer'],
            [['file_name', 'file_path'], 'string'],
            [['created_at', 'mime_type'], 'safe'],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['message_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('phycom/base/main', 'ID'),
            'message_id' => Yii::t('phycom/base/main', 'Message ID'),
            'mime_type'  => Yii::t('phycom/base/main', 'Mime Type'),
            'file_name'  => Yii::t('phycom/base/main', 'File Name'),
            'file_path'  => Yii::t('phycom/base/main', 'File Path'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
        ];
    }

    public function getFullFilename(): string
    {
        return Yii::getAlias('@files/' . $this->file_path) ;
    }

    /**
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        unlink($this->getFullFilename());
        return parent::delete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::class, ['id' => 'message_id']);
    }
}
