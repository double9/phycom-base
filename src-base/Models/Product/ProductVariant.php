<?php

namespace Phycom\Base\Models\Product;

use Phycom\Base\Models\Attributes\ProductVariantStatus;
use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\User;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use yii;

/**
 * This is the model class for table "product_variant".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property integer $default_option_id
 * @property bool $user_select
 * @property ProductVariantStatus $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read Product $product
 * @property-read ProductVariantOption[] $options
 *
 * @property-read string $label
 * @property-read Variant $variant
 * @property-read VariantOption[] $allOptions
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property ProductVariantOption $defaultOption
 */
class ProductVariant extends ActiveRecord
{
    private $variant;

    /**
     * @param $variant
     * @param $distinctVariant
     * @param $distinctOption
     *
     * @return array|yii\db\ActiveRecord[]|static[]
     * @throws yii\base\InvalidConfigException
     */
	public static function findByDistinctOption($variant, $distinctVariant, $distinctOption)
    {
        $a = Yii::$app->modelFactory->getVariant()::findByName($variant);

        $ordering = [];
        foreach ($a->options as $index => $option) {
            $ordering[] = [
                'key' => $option->key,
                'ordering' => $index
            ];
        }

        return static::findBySql(<<<SQL
SELECT DISTINCT ON(a2.value) a2.*
FROM product_variant_option o
  INNER JOIN product_variant a ON (o.product_variant_id = a.id)
  INNER JOIN product_variant a2 ON (a2.product_id = a.product_id AND a2.name = :select_variant)
--   INNER JOIN json_to_recordset(:ordering) AS x(key text, ordering int) ON (a2.value = x.key)
WHERE a.name = :distinct_variant AND o.key = :distinct_option ORDER BY a2.value DESC;
SQL
            ,   [
                'select_variant'   => $variant,
                'distinct_variant' => $distinctVariant,
                'distinct_option'  => $distinctOption,
//                'ordering' => yii\helpers\Json::encode($ordering)
            ])->all();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_variant';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'dynamic-attribute' => [
                'attributes' => [
                    'status' => ProductVariantStatus::class
                ]
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name'], 'required'],
            [['product_id', 'default_option_id'], 'integer'],
	        [['user_select'], 'boolean'],
            [['created_at', 'updated_at', 'status'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['default_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductVariantOption::class, 'targetAttribute' => ['default_option_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('phycom/base/product/variant', 'ID'),
            'product_id'        => Yii::t('phycom/base/product/variant', 'Product ID'),
            'name'              => Yii::t('phycom/base/product/variant', 'Name'),
            'label'             => Yii::t('phycom/base/product/variant', 'Label'),
            'default_option_id' => Yii::t('phycom/base/product/variant', 'Default option'),
            'user_select'       => Yii::t('phycom/base/product/variant', 'User selectable'),
            'status'            => Yii::t('phycom/base/product/variant', 'Status'),
            'created_by'        => Yii::t('phycom/base/product/variant', 'Created By'),
            'updated_by'        => Yii::t('phycom/base/product/variant', 'Updated By'),
            'created_at'        => Yii::t('phycom/base/product/variant', 'Created At'),
            'updated_at'        => Yii::t('phycom/base/product/variant', 'Updated At'),
        ];
    }

    /**
     * @return int - number of rows affected
     * @throws yii\db\Exception
     */
    public function clearOptions()
    {
        if ($this->isNewRecord) {
            throw new yii\base\InvalidCallException(__METHOD__ . ' should not be called on new record');
        }
        return Yii::$app->db->createCommand()
            ->delete(ProductVariantOption::tableName(), ['product_variant_id' => $this->id])
            ->execute();
    }

    /**
     * @return VariantOption[]
     * @throws yii\base\InvalidConfigException
     */
    public function getAllOptions()
    {
        return Yii::$app->modelFactory->getVariantOption()::findAll($this->name);
    }

    /**
     * @param string $optionKey
     * @return ProductVariantOption|null
     */
    public function getOptionByKey(string $optionKey)
    {
        foreach ($this->options as $option) {
            if ($option->key === $optionKey) {
                return $option;
            }
        }
        return null;
    }

    /**
     * @return array|ProductAttachment[]
     * @throws yii\base\InvalidConfigException
     */
    public function getLinkedAttachments()
    {
        /**
         * @var ProductAttachment[] $attachments
         */
        $attachments = [];
        /**
         * @var AttachmentParam[] $attachmentParams
         */
        $attachmentParams = [];
        foreach (Yii::$app->modelFactory->getAttachmentParam()::findAll() as $attachmentParam) {
            if ($attachmentParam->enabledByVariant === $this->name) {
                $attachmentParams[] = $attachmentParam;
            }
        }
        foreach ($this->product->attachments as $productAttachment) {
            foreach ($attachmentParams as $param) {
                if ($productAttachment->getParamValue($param->name)) {
                    $attachments[] = $productAttachment;
                    break;
                }
            }
        }
        return $attachments;
    }

    /**
     * @return null|Variant
     * @throws yii\base\InvalidConfigException
     */
    public function getVariant()
    {
        return $this->variant ?: $this->variant = Yii::$app->modelFactory->getVariant()::findByName($this->name);
    }

    /**
     * @return string
     * @throws yii\base\InvalidConfigException
     */
    public function getLabel()
    {
    	return $this->getVariant() ? $this->variant->label : ($this->name ? Inflector::titleize($this->name) : null);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(ProductVariantOption::class, ['product_variant_id' => 'id'])->inverseOf('productVariant');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultOption()
    {
        return $this->hasOne(ProductVariantOption::class, ['id' => 'default_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'updated_by']);
    }
}
