<?php

namespace Phycom\Base\Models\Product;


use Phycom\Base\Models\CustomFieldOption;

use Yii;

/**
 * Class AttachmentParamOption
 *
 * @package Phycom\Base\Models\Product
 */
class AttachmentParamOption extends CustomFieldOption
{
    /**
     * @return mixed|string
     */
    public function getCustomFieldClassName()
    {
        return get_class(Yii::$app->modelFactory->getParam());
    }
}
