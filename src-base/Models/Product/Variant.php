<?php

namespace Phycom\Base\Models\Product;

use Phycom\Base\Models\CustomField;
use Phycom\Base\Helpers\Currency;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Variant
 *
 * @package Phycom\Base\Models\Product
 *
 * @property VariantOption[] $options
 */
class Variant extends CustomField
{
    /**
     * @var string|null
     */
    public ?string $sku = null;

    /**
     * @var number
     */
	public $price;

	/**
	 * @var bool - when true the variant can be configured for each product attachment file
	 */
	public bool $attachmentOption = false;

	/**
	 * @var bool - works only with the TYPE_OPTION variants. When set true a custom price can be defined for every option
	 */
	public bool $priceOption = false;

    /**
     * @var bool - When set true a custom price can be defined for one unit
     */
    public bool $unitPrice = false;

    /**
     * @var string|null
     */
	public ?string $orderLineLabel = null;

    /**
     * Determines if a separate order line should be created for this variant or not
     *
     * @return bool
     */
    public function isOrderLine(): bool
    {
        return $this->priceOption;
    }

    /**
     * @return static[]
     * @throws yii\base\InvalidConfigException
     */
	public static function findAttachmentVariants()
	{
		$matches = [];
		foreach (static::data() as $name => $value) {
			if (isset($value['attachmentOption'])) {
				$matches[] = static::createInstance($name, $value);
			}
		}
		return $matches;
	}

    /**
     * @param string $name
     * @param array $data
     * @return static|CustomField
     * @throws yii\base\InvalidConfigException
     */
	protected static function createInstance(string $name, array $data)
	{
//		if (isset($data['priceOption']) && (!isset($data['type']) || $data['type'] !== self::TYPE_OPTION)) {
//			unset($data['priceOption']);
//		}
		if (isset($data['price'])) {
			$data['price'] = Currency::toInteger($data['price']);
		}
		return parent::createInstance($name, $data);
	}

	/**
	 * @return array
	 */
	public static function getNames()
	{
		return static::getConstants('V_');
	}

    /**
     * @return \Phycom\Base\Models\CustomFieldOption|string
     */
    public function getOptionClassName()
    {
        return get_class(Yii::$app->modelFactory->getVariantOption());
    }

    /**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'label', 'type'], 'required'],
			['price', 'number', 'min' => 0],
			[['name', 'label', 'orderLineLabel', 'type'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return ArrayHelper::merge(parent::attributeLabels(), [
            'price' => Yii::t('phycom/base/product/variant', 'Price')
        ]);
	}
}
