<?php

namespace Phycom\Base\Models\Product;


use Phycom\Base\Models\CustomFieldOption;

use Yii;

/**
 * Class ParamOption
 *
 * @package Phycom\Base\Models\Product
 */
class ParamOption extends CustomFieldOption
{
    /**
     * @return mixed|string
     */
    public function getCustomFieldClassName()
    {
        return get_class(Yii::$app->modelFactory->getParam());
    }
}
