<?php

namespace Phycom\Base\Models\Product;

use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\Review;

use yii\db\Query;
use yii;

/**
 * This is the model class for table "product_review".
 *
 * @property integer $product_id
 * @property integer $review_id
 *
 * @property Product $product
 * @property Review $review
 */
class ProductReview extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_review';
    }


    /**
     * @param int $productId
     * @return array
     */
    public static function groups($productId)
    {
        $query = (new Query())
            ->select('r.score, count(r.id) as count')
            ->from(['r' => Review::tableName()]);

        $query->innerJoin(self::tableName(), ['product_id' => $productId]);
        $query->groupBy(['r.score']);
        $query->orderBy(['r.score' => SORT_DESC]);
        return $query->all();
    }

    public static function totalCount()
    {

    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'review_id'], 'required'],
            [['product_id', 'review_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Review::class, 'targetAttribute' => ['review_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('phycom/base/main', 'Product ID'),
            'review_id' => Yii::t('phycom/base/main', 'Review ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::class, ['id' => 'review_id']);
    }
}
