<?php

namespace Phycom\Base\Models\Product\Import;

/**
 * Interface ProductImportStrategyInterface
 *
 * @package Phycom\Base\Models\Product\Import
 */
interface ProductImportStrategyInterface
{
    /**
     * @return $this
     */
    public function prepare();

    /**
     * @param callable|null $onProgress
     * @return bool
     */
    public function import(callable $onProgress = null);
}
