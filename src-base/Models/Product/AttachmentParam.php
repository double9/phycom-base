<?php

namespace Phycom\Base\Models\Product;


use Phycom\Base\Models\CustomField;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class AttachmentParam
 *
 * @package Phycom\Base\Models\Product
 */
class AttachmentParam extends CustomField
{
    /**
     * @var string
     */
    public $hint;
    /**
     * @var string
     */
    public $enabledByVariant;
    /**
     * @return array
     */
    public static function getNames()
    {
        return static::getConstants('P_');
    }

    public function getOptionClassName()
    {
        return get_class(Yii::$app->modelFactory->getAttachmentParamOption());
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'hint'             => Yii::t('phycom/base/custom-field', 'Hint'),
            'enabledByVariant' => Yii::t('phycom/base/custom-field', 'Enabled by variant'),
        ]);
    }
}
