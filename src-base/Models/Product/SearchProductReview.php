<?php

namespace Phycom\Base\Models\Product;



use Phycom\Base\Models\SearchReview;

/**
 * Class SearchProductReview
 * @package Phycom\Base\Models\Product
 */
class SearchProductReview extends SearchReview
{
    public function init()
    {
        parent::init();
        $this->joinTableName = ProductReview::tableName();
        $this->joinKey = 'product_id';
    }
}
