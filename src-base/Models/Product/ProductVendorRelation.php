<?php

namespace Phycom\Base\Models\Product;

use Phycom\Base\Models\Vendor;

use yii;

/**
 * This is the model class for table "product_in_vendor".
 *
 * @property integer $product_id
 * @property integer $vendor_id
 * @property \DateTime $created_at
 *
 */
class ProductVendorRelation extends \Phycom\Base\Models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_in_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'product_id'], 'integer'],
            [['created_at'], 'safe'],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vendor::class, 'targetAttribute' => ['vendor_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::class, ['id' => 'vendor_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => 'category_id']);
	}

}
