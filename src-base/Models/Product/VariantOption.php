<?php

namespace Phycom\Base\Models\Product;

use Phycom\Base\Models\CustomFieldOption;
use Phycom\Base\Helpers\Currency;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class VariantOption
 *
 * @package \Phycom\Base\Models\Product
 *
 * @property Variant $customField
 */
class VariantOption extends CustomFieldOption
{
    /**
     * @var string
     */
    public $sku;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var mixed
     */
    public $meta;

    /**
     * @var int
     */
	public $price;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
	    $rules = parent::rules();
	    $rules[] = [['sku', 'icon'], 'string'];
	    $rules[] = ['price', 'number'];
	    $rules[] = ['meta', 'safe'];
	    $rules[] = ['price', 'required'];

		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
        return ArrayHelper::merge(parent::attributeLabels(), [
            'price' => Yii::t('phycom/base/product/option', 'Price')
        ]);
	}

    /**
     * @return mixed|string
     */
	public function getCustomFieldClassName()
	{
		return get_class(Yii::$app->modelFactory->getVariant());
	}

    /**
     * @param array $config
     * @return array
     */
	protected function loadConfig(array $config)
    {
        $config = parent::loadConfig($config);
        if (isset($config['price'])) {
            $config['price'] = Currency::toInteger($config['price']);
        }
        return $config;
    }
}
