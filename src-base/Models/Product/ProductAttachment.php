<?php

namespace Phycom\Base\Models\Product;


use Phycom\Base\Models\AttachmentUrl;

use yii;

/**
 * This is the model class for table "product_attachment".
 *
 * @property integer $product_id
 * @property mixed $meta
 * @property bool $is_visible
 * @property Product $product
 *
 * @property-read AttachmentUrl $url
 */
class ProductAttachment extends \Phycom\Base\Models\Attachment
{
    protected $targetAttributeName = 'product_id';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['is_visible'], 'boolean'];
        $rules[] = ['is_visible', 'default', 'value' => true];
        $rules[] = [[$this->targetAttributeName], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getProduct()), 'targetAttribute' => [$this->targetAttributeName => 'id']];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            $this->targetAttributeName => Yii::t('phycom/base/main', 'Product ID'),
            'is_visible' => Yii::t('phycom/base/main', 'Is Visible'),
        ]);
    }

    /**
     * @return AttachmentUrl|object
     * @throws yii\base\InvalidConfigException
     */
    public function getUrl()
    {
        return Yii::createObject([
            'class'    => AttachmentUrl::class,
            'bucket'   => $this->file ? $this->file->bucket : null,
            'filename' => $this->file ? $this->file->filename : null,
        ]);
    }

    /**
     * @param $attachmentParamName
     * @return mixed|null
     * @throws yii\base\InvalidConfigException
     */
    public function getParamValue($attachmentParamName)
    {
        if (!$attachmentParam = Yii::$app->modelFactory->getAttachmentParam()::findByName($attachmentParamName)) {
            throw new yii\base\InvalidArgumentException('Invalid attachment param name ' . $attachmentParamName);
        }
        if (is_array($this->meta) && array_key_exists($attachmentParamName, $this->meta)) {
            return $this->meta[$attachmentParamName];
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getProduct()), ['id' => $this->targetAttributeName]);
    }
}
