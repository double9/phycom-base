<?php

namespace Phycom\Base\Models;

use Phycom\Base\Helpers\c;
use Phycom\Base\Helpers\ThumbnailGenerator;
use Phycom\Base\Models\Attributes\FileType;
use Phycom\Base\Models\Attributes\FileStatus;

use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use Yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property FileType $mime_type
 * @property string $name
 * @property string $filename
 * @property string $bucket
 * @property FileStatus $status
 * @property integer $created_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 *
 * @property-read string $thumbFilename
 * @property User $createdBy
 * @property-read string $url
 * @property-read string $thumbUrl
 */
class File extends ActiveRecord
{
	const DEFAULT_THUMB_QUALITY = 80;

	const THUMB_SIZE_SMALL = 'small';
	const THUMB_SIZE_MEDIUM = 'medium';
	const THUMB_SIZE_LARGE = 'large';
	const THUMB_SIZE_DEFAULT = self::THUMB_SIZE_MEDIUM;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(parent::behaviors(), [
			'dynamic-attribute' => [
				'attributes' => [
					'status' => FileStatus::class,
					'mime_type' => FileType::class
				]
			]
		]);
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mime_type', 'filename', 'status', 'bucket'], 'required'],
            [['name', 'filename'], 'string'],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['bucket'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => get_class(Yii::$app->modelFactory->getUser()), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('phycom/base/main', 'ID'),
            'mime_type'  => Yii::t('phycom/base/main', 'Mime Type'),
            'name'       => Yii::t('phycom/base/main', 'Name'),
            'filename'   => Yii::t('phycom/base/main', 'Filename'),
            'bucket'     => Yii::t('phycom/base/main', 'Bucket'),
            'status'     => Yii::t('phycom/base/main', 'Status'),
            'created_by' => Yii::t('phycom/base/main', 'Created By'),
            'created_at' => Yii::t('phycom/base/main', 'Created At'),
            'updated_at' => Yii::t('phycom/base/main', 'Updated At'),
        ];
    }

    public function getThumbFilename($size = self::THUMB_SIZE_DEFAULT)
    {
    	return pathinfo($this->filename, PATHINFO_FILENAME) . '_' . $size . '.' . pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    public function getUrl()
    {
		return Yii::$app->fileStorage->getBucket($this->bucket)->getFileUrl($this->filename);
    }

    public function getThumbUrl($size = self::THUMB_SIZE_DEFAULT)
    {
	    if (!$this->mime_type->isImage) {
		    throw new InvalidCallException('Error creating thumbnail. File ' . $this->id . ' is not an image file');
	    }
	    return Yii::$app->fileStorage->getBucket($this->bucket)->getFileUrl($this->getThumbFilename($size));
    }

    /**
     * @throws InvalidConfigException
     */
    public function generateThumbs()
    {
    	if (!$this->mime_type->isImage) {
    		throw new InvalidCallException('Error creating thumbnail. File ' . $this->id . ' is not an image file');
	    }
        /**
         * @var ThumbnailGenerator|object $thumbnailGenerator
         */
        $thumbnailGenerator = Yii::createObject([
            'class'        => ThumbnailGenerator::class,
            'thumbQuality' => static::DEFAULT_THUMB_QUALITY
        ], [$this->bucket]);

        return $thumbnailGenerator->generate($this->filename);
    }

	/**
	 * @return bool
	 */
    public function delete()
    {
    	$this->status = FileStatus::DELETED;
    	return $this->save();
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(get_class(Yii::$app->modelFactory->getUser()), ['id' => 'created_by']);
    }

    public function __toString()
    {
        return $this->getUrl();
    }
}
