<?php

namespace Phycom\Base\Components;

use Phycom\Base\Helpers\Date;
use Phycom\Base\Helpers\Diacritics;
use yii\db\ActiveQuery as BaseActiveQuery;
use yii;

/**
 * Class ActiveQuery
 * @package Phycom\Base\Components
 */
class ActiveQuery extends BaseActiveQuery
{
	/**
	 * @param string $attribute
	 * @param string $from
	 * @param string $to
     * @param bool $dateTimeParsing
	 */
	public function filterDateRange($attribute, $from, $to, bool $dateTimeParsing = true)
	{
		$dateFormat = Yii::$app->formatter->dateFormat;

		if ($from && $to) {
			$fromDate = Date::create($from, $dateFormat)->dateTime->setTime(0, 0, 1);
			$toDate = Date::create($to, $dateFormat)->dateTime->setTime(23, 59, 59);
			$this->andFilterWhere([
				'between',
				$attribute,
				$dateTimeParsing ? Date::create($fromDate)->dbTimestamp : Date::create($fromDate)->getDbDate(),
                $dateTimeParsing ? Date::create($toDate)->dbTimestamp : Date::create($toDate)->getDbDate()
			]);
		} else if ($from && !$to) {
			$fromDate = Date::create($from, $dateFormat)->dateTime->setTime(0, 0, 1);
			$this->andFilterWhere([
				'>',
				$attribute,
                $dateTimeParsing ? Date::create($fromDate)->dbTimestamp : Date::create($fromDate)->getDbDate(),
			]);
		} else if (!$from && $to) {
			$toDate = Date::create($to, $dateFormat)->dateTime->setTime(23, 59, 59);
			$this->andFilterWhere([
				'<=',
				$attribute,
                $dateTimeParsing ? Date::create($toDate)->dbTimestamp : Date::create($toDate)->getDbDate()
			]);
		}
	}

	/**
	 * @param string $attribute
	 * @param string $searchValue
	 * @param bool $unAccent
	 */
	public function filterText($attribute, $searchValue, $unAccent = true)
	{
		if ($searchValue) {
			if ($unAccent) {
				$this->andWhere('(UNACCENT(' . $attribute . ') ILIKE :search_value)', ['search_value' => '%' . trim(Diacritics::remove($searchValue)) . '%']);
			} else {
				$this->andWhere('(' . $attribute . ' ILIKE :search_value)', ['search_value' => '%' . trim($searchValue) . '%']);
			}
		}
	}

	public function filterMultiAttribute(array $attributes, $searchValue, $unAccent = true)
	{
		if ($searchValue) {
			$a = 'COALESCE(' . implode(', (\' \')::text, ', $attributes) . ')';
			if ($unAccent) {
				$this->andWhere('(UNACCENT(' . $a . ') ILIKE :search_value)', ['search_value' => '%' . trim(Diacritics::remove($searchValue)) . '%']);
			} else {
				$this->andWhere('(' . $a . ' ILIKE :search_value)', ['search_value' => '%' . trim($searchValue) . '%']);
			}
		}
	}

	/**
	 * @param array $attributes
	 * @param string $searchValue
	 * @param bool $unAccent
	 * @param bool $extraCondition
	 */
	public function filterFullName(array $attributes, $searchValue, $unAccent = true, $extraCondition = false)
	{
		if ($searchValue) {
			$c1 = 'CONCAT(' . implode(', (\' \')::text, ', $attributes) . ')';
			if ($unAccent) {
				$this->andWhere('(UNACCENT(' . $c1 . ') ILIKE :name' . ($extraCondition ? ' ' . $extraCondition : '') . ')', [
					'name' => '%' . trim(Diacritics::remove($searchValue)) . '%'
				]);
			} else {
				$this->andWhere('(' . $c1 . ' ILIKE :name' . ($extraCondition ? ' ' . $extraCondition : '') . ')', [
					'name' => '%' . trim($searchValue) . '%'
				]);
			}
		}
	}
}
