<?php

namespace Phycom\Base\Components;


use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\PageSpace;

use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\helpers\Inflector;
use yii\base\Exception;
use yii\base\Component;
use Yii;

/**
 * Class PageSpaceCollection
 * @package Phycom\Base\Components
 */
class PageSpaceCollection extends Component
{
    /**
     * @var string
     */
    public string $postType = PostType::PAGE;

    /**
     * @var PageSpace[]
     */
    protected array $items = [];

    /**
     * @return PageSpace[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $pageSpace
     * @throws InvalidConfigException
     */
    public function setItems(array $pageSpace)
    {
        $items = [];
        foreach ($pageSpace as $identifier => $item) {

            if (is_array($item)) {
                if (!isset($item['class'])) {
                    $item['class'] = PageSpace::class;
                }
                if (isset($item['identifier'])) {
                    $identifier = $item['identifier'];
                }
            } elseif (is_string($item)) {
                $identifier = $item;
                $item = ['class' => PageSpace::class];
            }

            if (!isset($item['name'])) {
                $item['name'] = Inflector::titleize($identifier);
            }

            $item = Yii::createObject($item, [$identifier, $this->postType]);
            if (!$item instanceof PageSpace) {
                throw new InvalidArgumentException('Invalid PageSpace class');
            }
            $items[] = $item;
        }
        $this->items = $items;
    }

    /**
     * @param string $identifier
     * @param bool $onlyPublished
     * @return \Phycom\Base\Models\Post|\Phycom\Frontend\Models\Post\SearchPost|null
     * @throws Exception
     */
    public function getPage(string $identifier, bool $onlyPublished = false)
    {
        foreach ($this->items as $pageSpace) {
            if ($pageSpace->identifier === $identifier) {
                $page = $pageSpace->getPage();
                if (!$page || ($onlyPublished && !$page->status->is(PostStatus::PUBLISHED))) {
                    return null;
                };
                return $page;
            }
        }
        return null;
    }
}
