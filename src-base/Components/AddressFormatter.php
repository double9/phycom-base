<?php

namespace Phycom\Base\Components;


use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Country;

use yii\base\Component;
use Yii;

/**
 * Class AddressFormatter
 *
 * @package Phycom\Base\Components
 */
class AddressFormatter extends Component
{
    protected AddressField $address;

    public function __construct(AddressField $addressField, $config = [])
    {
        $this->address = $addressField;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function asL2() : array
    {
        $l2 = [];
        if ($this->address->city) {
            $l2[] = $this->address->city;
        } else if ($this->address->locality) {
            $l2[] = $this->address->locality;
            if ($this->address->province) {
                $l2[] = $this->address->province;
            }
        }
        if ($this->address->state) {
            $l2[] = $this->address->state;
        }
        if ($this->address->postcode) {
            $l2[] = $this->address->postcode;
        }
        if ($this->address->country !== (string)Yii::$app->country) {
            $country = Country::findOne(['code' => $this->address->country]);
            $l2[] = $country->name;
        }

        return [$this->getStreet(), implode(', ', $l2)];
    }

    /**
     * @return array
     */
    public function asL3() : array
    {
        $lines = [];

        $street = $this->getStreet();
        $l1 = $street ? [$street] : [];

        if ($this->address->district) {
            $l1[] = $this->address->district;
        }
        $lines[] = implode(', ', $l1);

        $l2 = [];
        if ($this->address->city) {
            $l2[] = $this->address->city;
        }
        if ($this->address->locality) {
            $l2[] = $this->address->locality;
        }
        if ($this->address->province) {
            $l2[] = $this->address->province;
        }
        if ($this->address->state) {
            $l2[] = $this->address->state;
        }
        $lines[] = implode(', ', $l2);

        $l3 = [];
        if ($this->address->postcode) {
            $l3[] = $this->address->postcode;
        }
        if ($this->address->country !== (string)Yii::$app->country) {
            $country = Country::findOne(['code' => $this->address->country]);
            $l3[] = $country->name;
        }
        $lines[] = implode(', ', $l3);
        return $lines;
    }

    /**
     * @return array
     */
    public function asArray() : array
    {
        return $this->address->exportArray(true);
    }

    /**
     * @return string|null
     */
    public function asLabel() : ?string
    {
        $street = $this->getStreet();
        $address = $street ? [$street] : [];

        if ($this->address->city) {
            $address[] = $this->address->city;
        }
        if ($this->address->locality) {
            $address[] = $this->address->locality;
        }
        if (!$this->address->city && $this->address->province) {
            $address[] = $this->address->province;
        }
        if ($this->address->state) {
            $address[] = $this->address->state;
        }
        if ($this->address->country !== (string)Yii::$app->country) {
            $country = Country::findOne(['code' => $this->address->country]);
            $address[] = $country->name;
        }
        $address[] = $this->address->postcode;

        return implode(', ', $address);
    }

    /**
     * @return string|null
     */
    protected function getStreet() : ?string
    {
        $street = $this->address->street;
        if ($this->address->house) {
            $street .=  ' ' . $this->address->house;
            if ($this->address->room) {
                $street .= '-' . $this->address->room;
            }
        }
        return $street;
    }
}
