<?php

namespace Phycom\Base\Components;


use Phycom\Base\Interfaces\PersistableConfigurationInterface;
use yii\base\Component;
use yii\helpers\Inflector;

/**
 * Class PhycomComponent
 *
 * @package Phycom\Base\Components
 */
abstract class PhycomComponent extends Component
{
    use PhycomComponentTrait;

    public bool $enabled = false;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function init()
    {
        parent::init();
        // load config when persisted
        if ($this instanceof PersistableConfigurationInterface) {
            $this->loadConfig();
        }
    }
}
