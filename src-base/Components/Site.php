<?php

namespace Phycom\Base\Components;

use Phycom\Base\Interfaces\PersistableConfigurationInterface;
use Phycom\Base\Interfaces\PhycomComponentInterface;

use yii\helpers\FileHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class Site
 *
 * @package Phycom\Base\Components
 */
class Site extends PhycomComponent implements PhycomComponentInterface, PersistableConfigurationInterface
{
    use PersistableComponentTrait;

    /**
     * @var string|null
     */
    public ?string $frontMessage = null;

    /**
     * @var bool
     */
    public bool $cookiePolicyNotification = true;

    /**
     * @var string
     */
    public string $logoPath = '@root/public/img/brand/logo.png';

    /**
     * @var string
     */
    public string $logoUrl = '/img/brand/logo.png';

    /**
     * @var array
     */
    public array $phones = [];

    /**
     * @var array
     */
    public array $emails = [];


    public function getLabel(): string
    {
        return Yii::t('phycom/base/component', 'Global');
    }

    /**
     * @return string
     */
    public function getFullLogoUrl(): string
    {
        return Yii::$app->urlManagerFrontend->createAbsoluteUrl($this->logoUrl);
    }

    /**
     * @return string
     */
    public function getFullLogoPath(): string
    {
        return Yii::getAlias($this->logoPath);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getLogoMimeType(): string
    {
        $fullPath = $this->getFullLogoPath();
        if ($mimeType = FileHelper::getMimeType($fullPath)) {
            $mimeType = mime_content_type($fullPath);
        }
        return $mimeType;
    }

    /**
     * @return array
     */
    public function getJsonSchema(): array
    {
        return [
            'title'       => Yii::t('phycom/base/component', 'Site config'),
            '$ref'        => '#/definitions/site',
            'definitions' => [
                'site' => [
                    'type'       => 'object',
                    'format'     => 'table',
                    'required'   => ['enabled','cookiePolicyNotification', 'logoPath', 'logoUrl'],
                    'properties' => [
                        'enabled' => [
                            'title' => Yii::t('phycom/base/component', 'Enabled'),
                            'type'    => 'boolean',
                            'options' => [
                                'hidden' => true,
                                'enum_titles' => [
                                    Yii::t('phycom/base', 'Yes'),
                                    Yii::t('phycom/base', 'No')
                                ]
                            ]
                        ],
                        'frontMessage' => [
                            'title' => Yii::t('phycom/base/component', 'Front-page Message'),
                            'type' => 'string',
                            'format' => 'textarea',
                            'description' => Yii::t('phycom/base/component', 'An alert message that is shown on site front-page'),
                        ],
                        'cookiePolicyNotification' => [
                            'title' => Yii::t('phycom/base/component', 'Cookie Policy Notification'),
                            'type'    => 'boolean',
                            'options' => [
                                'enum_titles' => [
                                    Yii::t('phycom/base/main', 'Yes'),
                                    Yii::t('phycom/base/main', 'No')
                                ]
                            ],
                            'description' => Yii::t('phycom/base/component', 'Enable or disable cookie policy popup'),
                        ],
                        'logoPath' => [
                            'title' => Yii::t('phycom/base/component', 'Logo Path'),
                            'type' => 'string',
                            'options' => [
                                'hidden' => true
                            ]
                        ],
                        'logoUrl' => [
                            'title' => Yii::t('phycom/base/component', 'Logo Url'),
                            'type' => 'string',
                            'options' => [
                                'hidden' => true
                            ]
                        ],
                        'phones' => [
                            'title' => Yii::t('phycom/base/component', 'Phone numbers'),
                            'type'   => 'array',
                            'uniqueItems' =>  true,
                            'format' => 'table',
                            'items'  => [
                                'title' => Yii::t('phycom/base/component', 'Phone number'),
                                'type' => 'string',
                                'format' => 'tel'
                            ],
                            'description' => Yii::t('phycom/base/component', 'List of phone numbers for the site'),
                        ],
                        'emails' => [
                            'title' => Yii::t('phycom/base/component', 'Emails'),
                            'type'   => 'array',
                            'uniqueItems' =>  true,
                            'format' => 'table',
                            'items'  => [
                                'title' => Yii::t('phycom/base/component', 'Email'),
                                'type' => 'string',
                                'format' => 'email'
                            ],
                            'description' => Yii::t('phycom/base/component', 'List of emails for the site'),
                        ]
                    ]
                ]
            ]
        ];
    }
}
