<?php

namespace Phycom\Base\Components;

use Phycom\Base\Models\Product\Product;

use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\UnknownPropertyException;
use yii\helpers\Inflector;
use Yii;

/**
 * Class ModelFactory
 *
 * @package Phycom\Base\Components
 *
 * @method \Phycom\Base\Models\User getUser(array $params = [])
 * @method \Phycom\Base\Models\UserTag getUserTag(array $params = [])
 * @method \Phycom\Base\Models\Vendor getVendor(array $params = [])
 * @method \Phycom\Base\Models\Shop getShop(array $params = [])
 * @method \Phycom\Base\Models\VendorUser getVendorUser(array $params = [])
 * @method \Phycom\Base\Models\Product\Product getProduct(array $params = [])
 * @method \Phycom\Base\Models\Product\Variant getVariant(array $params = [])
 * @method \Phycom\Base\Models\Product\VariantOption getVariantOption(array $params = [])
 * @method \Phycom\Base\Models\Product\Param getParam(array $params = [])
 * @method \Phycom\Base\Models\Product\ParamOption getParamOption(array $params = [])
 * @method \Phycom\Base\Models\Product\AttachmentParam getAttachmentParam(array $params = [])
 * @method \Phycom\Base\Models\Product\AttachmentParamOption getAttachmentParamOption(array $params = [])
 * @method \Phycom\Base\Models\Product\ProductPrice getProductPrice(array $params = [])
 * @method \Phycom\Base\Models\Product\ProductPriceVariation getProductPriceVariation(array $params = [])
 * @method \Phycom\Base\Models\Product\ProductTag getProductTag(array $params = [])
 * @method \Phycom\Base\Models\Product\ProductCategory getProductCategory(array $params = [])
 * @method \Phycom\Base\Models\Product\ProductStatistics getProductStatistics(array $params = [])
 * @method \Phycom\Base\Helpers\ProductPrice getProductPriceHelper(Product $product, $units = null, array $params = [])
 * @method \Phycom\Base\Models\Invoice getInvoice(array $params = [])
 * @method \Phycom\Base\Models\Payment getPayment(array $params = [])
 * @method \Phycom\Base\Models\Order getOrder(array $params = [])
 * @method \Phycom\Base\Models\OrderItem getOrderItem(array $params = [])
 * @method \Phycom\Base\Models\OrderLineCollection getOrderLineCollection(array $orderItems, array $params = [])
 * @method \Phycom\Base\Models\OrderLine getOrderLine(array $params = [])
 * @method \Phycom\Base\Models\Shipment getShipment(array $params = [])
 * @method \Phycom\Base\Models\ShipmentItem getShipmentItem(array $params = [])
 * @method \Phycom\Base\Models\Message getMessage(array $params = [])
 * @method \Phycom\Base\Models\MessageTemplate getMessageTemplate(array $params = [])
 * @method \Phycom\Base\Models\Setting getSetting(array $params = [])
 * @method \Phycom\Base\Models\UserMessage getUserMessage(array $params = [])
 * @method \Phycom\Base\Models\DiscountRule getDiscountRule(array $params = [])
 * @method \Phycom\Base\AccessControl\Data getRbacData(array $params = [])
 */
class ModelFactory extends Component
{
	const DEFAULT_NS = 'Common\Models\\';

    /**
     * @var array - array of namespaces to search for
     */
	public array $fallbackSequence = ['Phycom\\'];

    /**
     * @var array - can be extended in child implementation
     */
	protected array $definitions = [];

    /**
     * @var array - contains all model definitions
     */
	private array $repository = [];
    /**
     * @var array - base namespace conf
     */
    private array $commonDefinitions = [
        'getProduct'               => 'Common\Models\Product\Product',
        'getVariant'               => 'Common\Models\Product\Variant',
        'getVariantOption'         => 'Common\Models\Product\VariantOption',
        'getParam'                 => 'Common\Models\Product\Param',
        'getParamOption'           => 'Common\Models\Product\ParamOption',
        'getAttachmentParam'       => 'Common\Models\Product\AttachmentParam',
        'getAttachmentParamOption' => 'Common\Models\Product\AttachmentParamOption',
        'getProductPrice'          => 'Common\Models\Product\ProductPrice',
        'getProductPriceHelper'    => 'Common\Helpers\ProductPrice',
        'getProductPriceVariation' => 'Common\Models\Product\ProductPriceVariation',
        'getProductTag'            => 'Common\Models\Product\ProductTag',
        'getProductStatistics'     => 'Common\Models\Product\ProductStatistics',
        'getProductCategory'       => 'Common\Models\Product\ProductCategory',
        'getRbacData'              => 'Common\AccessControl\Data'
    ];

    public function init()
    {
        parent::init();
        $this->repository = ArrayHelper::merge($this->commonDefinitions, $this->definitions);
    }

    /**
     * @return array
     */
    public function getModels()
    {
        return $this->repository;
    }

    /**
     * @param array $definitions
     */
    public function setDefinitions(array $definitions)
    {
        $this->definitions = ArrayHelper::merge($this->definitions, $definitions);
    }

	/**
	 * Custom getter for accessing factory model class names like properties $modelFactory->{ModelClassName}
	 *
	 * @param string $name - model name
	 * @return mixed|string
     *
     * @throws UnknownPropertyException
	 */
	public function __get($name)
	{
		try {
			return parent::__get($name);
		} catch (UnknownPropertyException $e) {
			return $this->getClassName($name);
		}
	}

    /**
     * For accessing model instances as methods of model factory: $modelFactory->get{ModelClassName}()
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     *
     * @throws UnknownPropertyException;
     * @throws \yii\base\InvalidConfigException
     */
	public function __call($name, $arguments)
	{
		try {
			return $this->getModelInstance(substr($name, 3), $arguments);
		} catch (UnknownPropertyException $e) {
			try {
				return parent::__call($name, $arguments);
			} catch (\Exception $e2) {
				throw $e;
			}
		}
	}

    /**
     * @param string $name - class namespace
     * @param array $arguments
     * @return mixed
     *
     * @throws UnknownPropertyException
     * @throws \yii\base\InvalidConfigException
     */
	public function getModelInstance($name, array $arguments = [])
	{
		$className = $this->getClassName($name);
		return Yii::createObject($className, $arguments);
	}

	/**
	 * @param $className
	 * @return string - correct class name path
     *
	 * @throws UnknownPropertyException
	 */
	public function getClassName($className)
	{
        $className = $this->repository['get' . $className] ?? static::DEFAULT_NS . $className;

        if (class_exists($className)) {
            return $className;
        }

        foreach ($this->fallbackSequence as $namespace) {

            if (substr($namespace, 0, 5) === 'psr4:') {
                $namespace = substr($namespace, 5);
            }

            if (class_exists($namespace . $className)) {
                return $namespace . $className;
            }

            if (substr($className, 0, 6) === 'Common') {
                $baseClassName = 'Base' . substr($className, 6);
                if (class_exists($namespace . $baseClassName)) {
                    return $namespace . $baseClassName;
                }
            }


//            if (substr($namespace, 0, 5) === 'psr4:') {
//                $fullClassName = substr($namespace, 5) . $this->getPsr4ClassName($className);
//                if (class_exists($fullClassName)) {
//                    return $fullClassName;
//                }
//            } else if (class_exists($namespace .  $className)) {
//                return $namespace .  $className;
//            }
        }

        throw new UnknownPropertyException('Class ' . $className . ' not found.');
	}

    /**
     * @param string $className
     * @return string
     */
    protected function getPsr4ClassName(string $className)
    {
        $parts = explode('\\', $className);
        $name = array_pop($parts);
        $className = '';
        foreach ($parts as $part) {
            if (strlen($part)) {
                $className .= Inflector::camelize($part);
            }
            $className .= '\\';
        }
        return $className . $name;
    }
}
