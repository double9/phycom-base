<?php
namespace Phycom\Base\Components\Queue;

use yii\queue\db\Queue;
use yii\queue\LogBehavior;
use yii\queue\serializers\JsonSerializer;

/**
 * Class Db
 *
 * @package Phycom\Base\Components\Queue
 */
class Db extends Queue
{
    public $commandClass = Command::class;
    public $serializer = JsonSerializer::class;

    public function behaviors()
    {
        return ['log' => LogBehavior::class];
    }

    /**
     * bury's all jobs in tube
     */
    public function buryAll()
    {

    }
}
