<?php

namespace Phycom\Base\Components\Queue;

class Command extends \yii\queue\beanstalk\Command
{
	public $isolate = false;
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'info' => InfoAction::class,
            'bury-all' => BuryAllAction::class
		];
	}
}

