<?php

namespace Phycom\Base\Components;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 *
 * @package Phycom\Base\Components
 */
class Bootstrap implements BootstrapInterface
{
	/**
	 * @param \FrontendApplication|\BackendApplication|\ConsoleApplication $app
	 */
	public function bootstrap($app)
	{

    }
}
