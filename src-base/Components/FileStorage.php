<?php

namespace Phycom\Base\Components;

use yii2tech\filestorage\hub\Storage;

/**
 * Class FileStorage
 * @package Phycom\Base\Components
 */
class FileStorage extends Storage
{
	const BUCKET_TEMP = 'temp';
	const BUCKET_AVATARS = 'avatar';
	const BUCKET_PRODUCT_FILES = 'product';
    const BUCKET_CONTENT_FILES = 'content';
	const BUCKET_ORDER = 'order';
	const BUCKET_INVOICES = 'invoice';
    const BUCKET_POSTAGE_LABELS = 'postagelabel';
}
