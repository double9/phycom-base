<?php

namespace Phycom\Base\Components;


use Phycom\Base\Helpers\Currency;

/**
 * Class FileExportFormatter
 *
 * @package Phycom\Base\Components
 */
class FileExportFormatter extends Formatter
{
    public $nullDisplay = '';
    public $decimalSeparator = '.';
    public $thousandSeparator = '';
    public $currencyCode = null;
    public $numberFormatterOptions = [
        \NumberFormatter::MAX_FRACTION_DIGITS => 3,
        \NumberFormatter::MIN_FRACTION_DIGITS => 2,
        \NumberFormatter::GROUPING_USED       => 0,
    ];
    public $numberFormatterTextOptions = [
        \NumberFormatter::CURRENCY_CODE     => '',
        \NumberFormatter::ZERO_DIGIT_SYMBOL => '.'
    ];
    public $numberFormatterSymbols = [
        \NumberFormatter::CURRENCY_SYMBOL          => '',
        \NumberFormatter::PERCENT_SYMBOL           => '',
        \NumberFormatter::DECIMAL_SEPARATOR_SYMBOL => '.'
    ];

    public function asCurrencyDisplay($value, $currency = null, $options = [], $textOptions = [])
    {
        return parent::asDecimal($value, 2);
    }

    public function asCurrency($value, $currency = null, $options = [], $textOptions = [])
    {
        return parent::asDecimal(Currency::toDecimal($value), 2);
    }
}
