<?php

namespace Phycom\Base\Components;

use Phycom\Base\Helpers\Date;
use Phycom\Base\Models\Setting;

use yii\helpers\Json;
use Yii;

trait PersistableComponentTrait
{
    abstract public function getName(): string;

    public function serialize(): string
    {
        return Json::encode($this);
    }

    public function unserialize(string $data): void
    {
        $config = Json::decode($data);
        foreach ($config as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function persistConfig(): bool
    {
        $setting = Setting::set($this->getName(), $this->serialize());
        return !$setting->hasErrors();
    }

    public function loadConfig(): bool
    {
        if ($value = Setting::get($this->getName())) {
            $this->unserialize($value);
            return true;
        }
        return false;
    }

}