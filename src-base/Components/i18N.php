<?php

namespace Phycom\Base\Components;

use yii\helpers\ArrayHelper;

class i18N extends \yii\i18n\I18N
{
    public $termBegin;
    public $termClose;

    public function translate($category, $message, $params, $language)
    {
        $newParams = ArrayHelper::merge($params, ['term' => $this->termBegin, '/term' => $this->termClose]);
        return parent::translate($category, $message, $newParams, $language);
    }

    public function format($message, $params, $language)
    {
        $params = (array) $params;
        if ($params === []) {
            return $message;
        }
        $p = [];
        foreach ($params as $name => $value) {
            $p['{' . $name . '}'] = $value;
        }
        $fixedMessage = strtr($message, $p);
        return parent::format($fixedMessage, $params, $language);
    }

    /**
     * Translates string without formatting parameters.
     *
     * @param $category
     * @param $message
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function rawTranslate($category, $message)
    {
        $messageSource = $this->getMessageSource($category);
        $translation = $messageSource->translate($category, $message, \Yii::$app->language);

        return $translation ? $translation : $message;
    }
}
