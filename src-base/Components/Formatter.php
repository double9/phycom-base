<?php

namespace Phycom\Base\Components;

use Phycom\Base\Helpers\Diacritics;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Helpers\ProductPrice;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Attributes\PriceUnitMode;
use Phycom\Base\Models\Attributes\UnitType;
use Phycom\Base\Models\Country;
use Phycom\Base\Models\Phone;
use Phycom\Base\Helpers\PhoneHelper as PhoneHelper;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use NumberFormatter;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Formatter
 * @package Phycom\Base\Components
 *
 * @property string $jsDateFormat
 * @property string $jsDatetimeFormat
 * @property-read string $countryCode
 * @property-read Country $country
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * @var string
     */
    public string $addressFieldSeparator = ', ';

    /**
     * @var string
     */
    public string $addressTemplate = '{street}{locality}{city}{province}{postcode}{country}';

    /**
     * Configurable custom logic applied to address field item
     * Array key is address field item and value is callable. The response of callable is the value of the field.
     * @var array
     */
	public array $addressFields = [];

    public $jsDateFormat;
	public $jsDatetimeFormat;

    public string $systemLocaleTemplate = '{locale}.utf-8';

    public function getCountryCode()
	{
		return (explode('-', $this->locale))[1];
	}

	public function getCountry()
	{
		return Country::findOne($this->getCountryCode());
	}

    /**
     * @param $value
     * @param int $decimals
     * @param string|null $decimalSeparator
     * @param string|null $thousandSeparator
     *
     * @return string
     */
	public function asScore($value, $decimals = 1, $decimalSeparator = '.', $thousandSeparator = '')
    {
        $decimalSeparator = $decimalSeparator ?: $this->decimalSeparator;
        $thousandSeparator = $thousandSeparator ?: $this->thousandSeparator;
        return number_format($value, $decimals, $decimalSeparator, $thousandSeparator);
    }

	/**
	 * @param \DateInterval $di
     * @param string $lang
	 * @return string
	 */
	public function asHumanInterval(\DateInterval $di, $lang = null)
	{
	    $language = substr(($lang ?: Yii::$app->lang->current->code), 0, 2);
		CarbonInterval::setLocale($language);
		$ci = CarbonInterval::instance($di);
		return $ci->forHumans();
	}

    /**
     * @param \DateTime $time
     * @param string|null $lang
     *
     * @return string
     * @throws \Exception
     * @throws yii\base\InvalidConfigException
     */
	public function asHumanDate(\DateTime $time = null, $lang = null)
    {
        if (!$time) {
            return $this->nullDisplay;
        }
        $threshold = new \DateTime();
        $threshold->sub(new \DateInterval('P1D'));

        if ($time > $threshold) {
            $language = substr(($lang ?: Yii::$app->lang->current->code), 0, 2);
            Carbon::setLocale($language);
            return Carbon::instance($time)->diffForHumans();
        }
        return $this->asDate($time);
    }

    /**
     * @param $value
     * @param $unitType
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function asUnits($value, $unitType)
    {
        if (empty($value)) {
            return $this->asDecimal($value);
        }
        $formattedValue = $this->asDecimal($value);

        if (!$unitType instanceof UnitType) {
            $unitType = UnitType::create($unitType);
        }

        return $formattedValue . ' ' . $unitType->unitLabel;
    }

    /**
     * @param \DateTime $time
     * @param string|null $lang
     *
     * @return string
     * @throws \Exception
     * @throws yii\base\InvalidConfigException
     */
    public function asHumanDateTime(\DateTime $time = null, $lang = null)
    {
        if (!$time) {
            return $this->nullDisplay;
        }
        $threshold = new \DateTime();
        $threshold->sub(new \DateInterval('P7D'));

        if ($time > $threshold) {
            $language = substr(($lang ?: Yii::$app->lang->current->code), 0, 2);
            Carbon::setLocale($language);
            return Carbon::instance($time)->diffForHumans();
        }
        return $this->asDateTime($time);
    }

    /**
     * @param mixed $address
     * @param bool $link
     *
     * @return string
     * @throws \Yii\base\InvalidConfigException
     */
    public function asPlainAddress($address = null, bool $link = false)
    {
        if (!$address) {
            return $this->nullDisplay;
        }

        if ($address instanceof AddressField) {
            $f = $address;
        } elseif ($address instanceof Address) {
            $f = $address->export();
        } else {
            $f = Yii::createObject(AddressField::class, [$address]);
        }

        $street = $f->street;

        if ($f->house) {
            $street .= ' ' . $f->house;
            if ($f->room) {
                $street .= '-' . $f->room;
            }
        }

        $out = [$street];

        if ($f->city) {
            $out[] = $f->city;
        }
        if ($f->locality) {
            $out[] = $f->locality;
        }
        if (!$f->city && $f->province) {
            $out[] = $f->province;
        }
        if ($f->postcode) {
            $out[] = $f->postcode;
        }

        $q = $out;

        if ($f->country && is_string($f->country)) {
            $country = Country::findOne(['code' => strtoupper($f->country)]);
            $q[] = $country->name;

            if ($f->country !== (string)Yii::$app->country) {
                $out[] = $country->info->getNativeName();
            }
        }

        if (empty($out)) {
            return $this->nullDisplay;
        }

        $addressStr = implode($this->addressFieldSeparator, $out);
        $label = $addressStr;

        if ($link) {
            $mapLinkUrl = 'https://maps.google.com/?q=' . urlencode(implode(',', $q));
            return yii\helpers\Html::a($label, $mapLinkUrl, ['target' => '_blank']);
        }
        return $label;
    }

    /**
     * @param AddressField $f
     * @return string
     */
    protected function parseAddressField(AddressField $f): ?string
    {
        $fields = [];
        $i = 0;
        $token = '{}';
        do {
            $item = (0 === $i++)
                ? strtok($this->addressTemplate, $token)
                : strtok($token);
            if ($item) {
                $fields[] = $item;
            }
        } while ($item);

        $out = [];
        foreach ($fields as $field) {
            if (property_exists($f, $field)) {
                $value = array_key_exists($field, $this->addressFields)
                    ? $this->addressFields[$field]($f)
                    : $f->$field;

                if (!empty($value)) {
                    $out[] = $value;
                }
            }
        }

        if (empty($out)) {
            return null;
        }

        return implode($this->addressFieldSeparator, $out);
    }

    /**
     * @param AddressField $f
     * @return string
     */
    protected function getMapLink(AddressField $f): ?string
    {
        $street = $f->street;

        if ($f->house) {
            $street .= ' ' . $f->house;
            if ($f->room) {
                $street .= '-' . $f->room;
            }
        }

        $q = [];

        if ($street) {
            $q[] = $street;
        }
        if ($f->city) {
            $q[] = $f->city;
        }
        if ($f->locality) {
            $q[] = $f->locality;
        }
        if (!$f->city && $f->province) {
            $q[] = $f->province;
        }
        if ($f->postcode) {
            $q[] = $f->postcode;
        }
        if ($f->country && is_string($f->country)) {
            $country = Country::findOne(['code' => strtoupper($f->country)]);
            $q[] = $country->name;
        }

        if (empty($q)) {
            return null;
        }

        return 'https://maps.google.com/?q=' . urlencode(implode(',', $q));
    }


    /**
     * @param mixed $address
     * @param bool $link
     *
     * @return string
     * @throws \Yii\base\InvalidConfigException
     */
    public function asAddress($address = null, bool $link = false)
    {
        if (!$address) {
            return $this->nullDisplay;
        }

        if ($address instanceof AddressField) {
            $f = $address;
        } elseif ($address instanceof Address) {
            $f = $address->export();
        } else {
            $f = Yii::createObject(AddressField::class, [$address]);
        }

        $addressStr = $this->parseAddressField($f);

        if (!$addressStr) {
            return $this->nullDisplay;
        }

        $recipient = $f->name;
        if (!$recipient && $f->firstName && $f->lastName) {
            $recipient = $f->firstName . ' ' . $f->lastName;
        }
        if ($f->company) {
            if ($recipient) {
                $recipient .= ' (' . $f->company . ')';
            } else {
                $recipient = $f->company;
            }
        }

        $label = $recipient
            ? $recipient . ' - ' . $addressStr
            : $addressStr;

        if ($link && $url = $this->getMapLink($f)) {
            return yii\helpers\Html::a($label, $url, ['target' => '_blank']);
        }
        return $label;
    }

    /**
     * @param mixed $address
     * @return string
     * @throws Yii\base\InvalidConfigException
     */
    public function asMapUrl($address = null)
    {
        if (!$address) {
            return $this->nullDisplay;
        }

        if ($address instanceof AddressField) {
            $f = $address;
        } elseif ($address instanceof Address) {
            $f = $address->export();
        } else {
            $f = Yii::createObject(AddressField::class, [$address]);
        }

        return $this->getMapLink($f) ?: $this->nullDisplay;
    }

    /**
     * Formats area/location
     *
     * @param null $area
     * @return string
     * @throws Yii\base\InvalidConfigException
     */
    public function asArea($area = null)
    {
        if (!$area) {
            return $this->nullDisplay;
        }

        if ($area instanceof AddressField) {
            $f = $area;
        } elseif ($area instanceof Address) {
            $f = $area->export();
        } else {
            $f = Yii::createObject(AddressField::class, [$area]);
        }

        $out = [];

        if ($f->country) {
            $country = Country::findOne(['code' => $f->country]);
            if ($f->country === (string)Yii::$app->country) {
                $out[] = $country->info->getNativeName();
            } else {
                $out[] = $country->name;
            }

            if ($f->province) {
                if (isset($country->divisions[$f->province])) {
                    $out[] = $country->divisions[$f->province];
                } else {
                    $out[] = $f->province;
                }
            }

            if ($f->locality) {
                $out[] = $f->locality;
            }

            if ($f->city) {
                $out[] = $f->city;
            }

            if ($f->district) {
                $out[] = $f->district;
            }

            if ($f->street) {
                $out[] = $f->street;
            }
        }

        if (!$f->name && empty($out)) {
            return $this->nullDisplay;
        }
        $out = array_reverse($out);

        $addressStr = implode($this->addressFieldSeparator, $out);
        $label = $f->name ? $f->name . ' - ' . $addressStr : $addressStr;

        return $label;
    }

    /**
     * Formats area/location
     *
     * @param null $area
     * @return string
     * @throws Yii\base\InvalidConfigException
     */
    public function asShortArea($area = null)
    {
        if (!$area) {
            return $this->nullDisplay;
        }

        if ($area instanceof AddressField) {
            $f = $area;
        } elseif ($area instanceof Address) {
            $f = $area->export();
        } else {
            $f = Yii::createObject(AddressField::class, [$area]);
        }

        if ($f->name) {
            return $f->name;
        }
        if ($f->street) {
            return $f->street;
        }
        if ($f->district) {
            return $f->district;
        }
        if ($f->city) {
            return $f->city;
        }
        if ($f->locality) {
            return $f->locality;
        }
        if ($f->province) {
            return $f->province;
        }
        if ($f->country) {
            $country = Country::findOne(['code' => $f->country]);
            return $f->country === (string)Yii::$app->country ? $country->info->getNativeName() : $country->name;
        } else {
            return $this->nullDisplay;
        }
    }

    /**
     * Decorator for asAddress method
     *
     * @param mixed $address
     * @return string
     * @throws Yii\base\InvalidConfigException
     */
	public function asAddressLink($address = null)
    {
        return $this->asAddress($address, true);
    }

    /**
     * @param mixed $phone - Phone model or full phone number with country code
     * @param bool $link - if true creates a proper "a" tag
     * @param int $format
     * @return string
     */
	public function asPhone($phone, $link = false, $format = PhoneNumberFormat::INTERNATIONAL)
    {
        $nationalNumber = $phone instanceof Phone ? $phone->phone_nr : PhoneHelper::getNationalPhoneNumber($phone);
        $phoneCode = $phone instanceof Phone ? $phone->country_code : PhoneHelper::getPhoneCode($phone);

        $phone = new PhoneNumber();
        $phone->setNationalNumber($nationalNumber);
        $phone->setCountryCode($phoneCode);

        $formattedValue = PhoneNumberUtil::getInstance()->format($phone, $format);
        return $link ? yii\helpers\Html::a($formattedValue, 'tel:+' . $phoneCode . $nationalNumber) : $formattedValue;
    }

    /**
     * @param int $value
     * @return string
     */
    public function asDays($value)
    {
        return Yii::t('phycom/base/main', '{days, plural, =1{# day} other{# days}}', ['days' => (int)$value]);
    }

	/**
	 * Prints out pretty weekday name
	 *
	 * @param int $value - 1 to 7 where 1 is monday
	 * @param string $format
     * @param string $lang
	 * @return false|string
	 */
    public function asWeekday($value, $format = '%a', $lang = null)
    {
	    if ($value > 7 || $value < 0) {
		    throw new yii\base\InvalidArgumentException('Invalid weekday number ' . $value);
	    }

        $languageCode = $lang ?: Yii::$app->lang->current->code;

	    $locale = $this->getFullLocaleFromLanguageCode($languageCode);              // get the locale code from the current language code
	    $locale = str_replace('-', '_', $locale);                                   // sanitize locale name to use underscores
	    $locale = str_replace('{locale}', $locale, $this->systemLocaleTemplate);    // compose correct system locale name by the template

        $fmt = new \IntlDateFormatter(
            $locale,
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::NONE,
            null,
            null,
            "cccc"
        );
        $timestamp = strtotime('next Sunday +' . $value . ' days');
        return $fmt->format($timestamp);
    }

    /**
     * Formats raw input to a normal decimal value
     *
     * examples:
     *
     *      96,33 -> 96.33
     *      87,20 -> 87.2
     *  100 00.29 -> 10000.29
     *
     * @param number|string $value
     * @return float
     */
    public function asDecimalValue($value)
    {
        if (is_numeric($value)) {
            return (float) $value;
        } else if (is_string($value)) {
            return (float) str_replace('/\s/', '', str_replace(',', '.', $value));
        } else {
            throw new yii\base\InvalidArgumentException('Invalid decimal value given');
        }
    }

	/**
	 * Formats the currency as decimal value for editing - just the amount without the currency symbol making sure decimal symbol is a dot and n decimal places
	 *
	 * @param number $value
     * @param int $decimals
	 * @return string
	 */
    public function asCurrencyValue($value, $decimals = 2)
    {
        if ($value instanceof ProductPrice) {
            $value = $value->getPrice();
        }
    	return number_format(Currency::toDecimal($value), $decimals, '.', '');
    }

    /**
     * @inheritdoc
     */
    public function asCurrency($value, $currency = null, $options = [], $textOptions = [])
    {
        if ($value instanceof ProductPrice) {
            $value = $value->getPrice();
        }
        if (is_string($currency) && !strlen($currency)) {
            return parent::asDecimal(Currency::toDecimal($value), 2, $options, $textOptions);
        }
        return parent::asCurrency(Currency::toDecimal($value), $currency, $options, $textOptions);
    }

    /**
     * Similar to "asCurrency" method but does not convert the value to decimal
     *
     * @param mixed $value
     * @param string|null $currency
     * @param array $options
     * @param array $textOptions
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function asCurrencyDisplay($value, $currency = null, $options = [], $textOptions = [])
    {
        return parent::asCurrency($value, $currency, $options, $textOptions);
    }

    /**
     * @param mixed $value
     * @param string|null $currency
     * @param array $options
     * @param array $textOptions
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function asShortCurrency($value, $currency = null, $options = [], $textOptions = [])
    {
        if (is_string($currency) && !strlen($currency)) {
            return parent::asDecimal(Currency::toDecimal($value), 0, ArrayHelper::merge([
                NumberFormatter::MAX_FRACTION_DIGITS => 2,
                NumberFormatter::MIN_FRACTION_DIGITS => 0,
            ], $options), $textOptions);
        }
        return parent::asCurrency(Currency::toDecimal($value), $currency, ArrayHelper::merge([
            NumberFormatter::MAX_FRACTION_DIGITS => 2,
            NumberFormatter::MIN_FRACTION_DIGITS => 0,
            NumberFormatter::ZERO_DIGIT_SYMBOL => '.'

        ], $options), ArrayHelper::merge([
            NumberFormatter::CURRENCY_CODE => $this->currencyCode,
            NumberFormatter::ZERO_DIGIT_SYMBOL => '.'
        ], $textOptions));
    }

    /**
     * @param string $value
     * @return string
     */
    public function asMessageSubject($value)
    {
        if ($value && strlen($value)) {
            return $this->asText($value);
        } else {
            return $this->asText(null);
        }
    }

    /**
     * @param string|array $value
     * @param null $cut
     * @return string
     */
    public function asJson($value, $cut = null)
    {
        if ($value === null || $value === '[]' || $value === '{}') {
            return $this->asText(null);
        }
        if (is_array($value)) {
            $o = $value;
        } else {
            $o = json_decode($value, false, 512, JSON_UNESCAPED_UNICODE);
        }
	    return '<pre class="json">' . json_encode($o, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) . '</pre>';
    }

    /**
     * @param string $value
     * @return string
     */
    public function asTitleCase($value)
    {
        $value = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
        $value = preg_replace('/\s+/', ' ', $value);
        $value = preg_replace_callback("/(-.)/", function ($matches) {
            return strtoupper($matches[1]);
        }, $value);
        return $value;
    }

    public function asPrettyPrint($value)
    {
        return $value ? ucfirst(str_replace('_', ' ', (string)$value)) : $this->asText(null);
    }

    /**
     * @param $obj
     * @param null $cut
     * @return mixed
     */
    protected function htmlEscapeObj($obj, $cut = null)
    {
        foreach ($obj as $key => $value) {
            if (is_object($value)) {
                $obj->$key = $this->htmlEscapeObj($value, $cut);
            } elseif (is_string($value)) {
                $obj->$key = $cut ? $this->cutStr(htmlspecialchars($value), $cut) : htmlspecialchars($value);
            }
        }
        return $obj;
    }

    /**
     * @param string|null $value
     * @return string|string[]|null
     */
    public function asTrimContent(string $value = null)
    {
        if (null === $value) {
            return null;
        }
        $value = trim($value);
        // remove empty p tags example: <p></p>
        $value = preg_replace('/<p[^>]*><\\/p[^>]*>/', '', $value);
        // remove empty p tags that contain just whitespace
        $value = preg_replace('/<p>&nbsp;<\/p>/', '', $value);

        return $value;
    }

    /**
     * @param string $str
     * @param int $len - desired max length
     * @return string
     */
    protected function cutStr($str, $len = 100)
    {
        if (($trimmed = substr($str, 0, (int)$len)) !== $str) {
            return $trimmed . '...';
        }
        return $str;
    }

    /**
     * @param string $json
     * @return string
     */
    protected function prettyJson($json)
    {

        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = '  ';
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;

        for ($i = 0; $i <= $strLen; $i++) {

            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else if (($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos--;

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {

                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos++;
                }

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }
        return $result;
    }

    /**
     * Replace accented characters this method should,
     * be overridden in country specific Formatter class if needed
     *
     * @param string $value
     * @return string
     */
    public function asUnaccented($value)
    {
        return Diacritics::remove($value);
    }

    /**
     * Based on the boolean value returns a corresponding string
     *
     * @param $value
     * @return string
     */
    public function asYesNo($value): string
    {
        return $value ? Yii::t('phycom/base/main', 'Yes') : Yii::t('phycom/base/main', 'No');
    }

    /**
     * Converts string to bool
     * Examples: "true" -> 1, "false" -> 0
     *
     * @param $value
     * @return bool
     */
    public function asStrToBool(string $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Converts number to ordinal with suffix
     *
     * @param int $number
     * @param string $locale
     * @return string
     */
    public function asOrdinalSuffix(int $number, $locale = 'en_GB'): string
    {
        return (new NumberFormatter($locale, NumberFormatter::ORDINAL))->format($number);
    }

    /**
     * @param $languageCode
     * @return mixed|null
     */
    protected function getFullLocaleFromLanguageCode($languageCode)
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = [
            'af-ZA',
            'am-ET',
            'ar-AE',
            'ar-BH',
            'ar-DZ',
            'ar-EG',
            'ar-IQ',
            'ar-JO',
            'ar-KW',
            'ar-LB',
            'ar-LY',
            'ar-MA',
            'arn-CL',
            'ar-OM',
            'ar-QA',
            'ar-SA',
            'ar-SY',
            'ar-TN',
            'ar-YE',
            'as-IN',
            'az-Cyrl-AZ',
            'az-Latn-AZ',
            'ba-RU',
            'be-BY',
            'bg-BG',
            'bn-BD',
            'bn-IN',
            'bo-CN',
            'br-FR',
            'bs-Cyrl-BA',
            'bs-Latn-BA',
            'ca-ES',
            'co-FR',
            'cs-CZ',
            'cy-GB',
            'da-DK',
            'de-AT',
            'de-CH',
            'de-DE',
            'de-LI',
            'de-LU',
            'dsb-DE',
            'dv-MV',
            'el-GR',
            'en-029',
            'en-AU',
            'en-BZ',
            'en-CA',
            'en-GB',
            'en-IE',
            'en-IN',
            'en-JM',
            'en-MY',
            'en-NZ',
            'en-PH',
            'en-SG',
            'en-TT',
            'en-US',
            'en-ZA',
            'en-ZW',
            'es-AR',
            'es-BO',
            'es-CL',
            'es-CO',
            'es-CR',
            'es-DO',
            'es-EC',
            'es-ES',
            'es-GT',
            'es-HN',
            'es-MX',
            'es-NI',
            'es-PA',
            'es-PE',
            'es-PR',
            'es-PY',
            'es-SV',
            'es-US',
            'es-UY',
            'es-VE',
            'et-EE',
            'eu-ES',
            'fa-IR',
            'fi-FI',
            'fil-PH',
            'fo-FO',
            'fr-BE',
            'fr-CA',
            'fr-CH',
            'fr-FR',
            'fr-LU',
            'fr-MC',
            'fy-NL',
            'ga-IE',
            'gd-GB',
            'gl-ES',
            'gsw-FR',
            'gu-IN',
            'ha-Latn-NG',
            'he-IL',
            'hi-IN',
            'hr-BA',
            'hr-HR',
            'hsb-DE',
            'hu-HU',
            'hy-AM',
            'id-ID',
            'ig-NG',
            'ii-CN',
            'is-IS',
            'it-CH',
            'it-IT',
            'iu-Cans-CA',
            'iu-Latn-CA',
            'ja-JP',
            'ka-GE',
            'kk-KZ',
            'kl-GL',
            'km-KH',
            'kn-IN',
            'kok-IN',
            'ko-KR',
            'ky-KG',
            'lb-LU',
            'lo-LA',
            'lt-LT',
            'lv-LV',
            'mi-NZ',
            'mk-MK',
            'ml-IN',
            'mn-MN',
            'mn-Mong-CN',
            'moh-CA',
            'mr-IN',
            'ms-BN',
            'ms-MY',
            'mt-MT',
            'nb-NO',
            'ne-NP',
            'nl-BE',
            'nl-NL',
            'nn-NO',
            'nso-ZA',
            'oc-FR',
            'or-IN',
            'pa-IN',
            'pl-PL',
            'prs-AF',
            'ps-AF',
            'pt-BR',
            'pt-PT',
            'qut-GT',
            'quz-BO',
            'quz-EC',
            'quz-PE',
            'rm-CH',
            'ro-RO',
            'ru-RU',
            'rw-RW',
            'sah-RU',
            'sa-IN',
            'se-FI',
            'se-NO',
            'se-SE',
            'si-LK',
            'sk-SK',
            'sl-SI',
            'sma-NO',
            'sma-SE',
            'smj-NO',
            'smj-SE',
            'smn-FI',
            'sms-FI',
            'sq-AL',
            'sr-Cyrl-BA',
            'sr-Cyrl-CS',
            'sr-Cyrl-ME',
            'sr-Cyrl-RS',
            'sr-Latn-BA',
            'sr-Latn-CS',
            'sr-Latn-ME',
            'sr-Latn-RS',
            'sv-FI',
            'sv-SE',
            'sw-KE',
            'syr-SY',
            'ta-IN',
            'te-IN',
            'tg-Cyrl-TJ',
            'th-TH',
            'tk-TM',
            'tn-ZA',
            'tr-TR',
            'tt-RU',
            'tzm-Latn-DZ',
            'ug-CN',
            'uk-UA',
            'ur-PK',
            'uz-Cyrl-UZ',
            'uz-Latn-UZ',
            'vi-VN',
            'wo-SN',
            'xh-ZA',
            'yo-NG',
            'zh-CN',
            'zh-HK',
            'zh-MO',
            'zh-SG',
            'zh-TW',
            'zu-ZA',
        ];

        $languageCode = substr($languageCode, 0, 2);

        foreach ($locales as $locale) {
            if (strtolower(locale_get_primary_language($locale)) === $languageCode) {
                return $locale;
            }
        }
        return null;
    }

}
