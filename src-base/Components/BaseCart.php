<?php

namespace Phycom\Base\Components;

use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartItemInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Base\Interfaces\CartStorageInterface;
use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\Cart\Storage\DatabaseStorage;
use Phycom\Base\Models\Cart\Storage\SessionStorage;

use Yii;

/**
 * Class Cart provides basic cart functionality (adding, removing, clearing, listing items). You can extend this class and
 * override it in the application configuration to extend/customize the functionality
 *
 * modified version of yii2mod\cart package
 *
 * @package Phycom\Base\Components
 */
abstract class BaseCart extends PhycomComponent implements CartInterface
{
    const ID = 'cart'; // component id used

    const EVENT_AFTER_CLEAR = 'eventAfterClear';

    /**
     * Override this to provide custom (e.g. database) storage for cart data
     *
     * @var string|array|object CartStorageInterface
     */
    public $storageProvider = SessionStorage::class;

    /**
     * @var array cart items
     */
    protected array $items;

    /**
     * @var CartStorageInterface
     */
    private CartStorageInterface $storage;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        $this->clear(false);
        $this->setStorage(Yii::createObject($this->storageProvider));
        $this->items = $this->getStorage()->load($this);
    }


    /**
     * @return mixed
     */
    public function getStorage() : CartStorageInterface
    {
        return $this->storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage(CartStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Add an item to the cart
     *
     * @param CartItemInterface $element
     * @param bool $save
     * @return $this
     */
	public function add(CartItemInterface $element, bool $save = true) : CartInterface
	{
		if ($element instanceof ActiveRecord) {
			foreach ($element->modelEvents() as $event) {
				$element->off($event);
			}
			foreach ($element->relatedRecords as $recordName => $model) {
			    unset($element->$recordName);
            }
		}

        $this->addItem($element);
        $save && $this->getStorage()->save($this);

        return $this;
	}

	/**
	 * @param CartItemInterface $item
	 */
	protected function addItem(CartItemInterface $item)
	{
		$uniqueId = $item->getUniqueId();

		if (isset($this->items[$uniqueId])) {
			$currentItem = $this->items[$uniqueId];
			if ($currentItem instanceof CartItemProductInterface && $item instanceof CartItemProductInterface) {
				$currentItem->setQuantity($currentItem->getQuantity() + $item->getQuantity());
			}
		} else {
			$this->items[$uniqueId] = $item;
		}
	}

    public function remove($uniqueId, bool $save = true) : CartInterface
    {
        if (!isset($this->items[$uniqueId])) {
            throw new yii\base\InvalidArgumentException('Item not found');
        }

        unset($this->items[$uniqueId]);

        $save && $this->getStorage()->save($this);

        return $this;
    }

    /**
     * Assigns cart to logged in user
     *
     * @param string
     * @param string
     * @throws \yii\base\InvalidConfigException
     */
    public function reassign($sessionId, $userId)
    {
        if (get_class($this->getStorage()) === DatabaseStorage::class) {
            if (!empty($this->items)) {
                $storage = $this->getStorage();
                $storage->reassign($sessionId, $userId);
                static::init();
            }
        }
    }

    public function clear(bool $save = true) : CartInterface
    {
        if (true === $save) {
            $k = 1;
        }
        $this->items = [];
        $save && $this->getStorage()->save($this) && $this->trigger(self::EVENT_AFTER_CLEAR);
        return $this;
    }

	public function hasItem($uniqueId) : bool
    {
        return $this->getItem($uniqueId) ? true : false;
    }

    public function save()
    {
        $this->getStorage()->save($this);
    }

    public function getItem($uniqueId) : ?CartItemInterface
    {
        foreach ($this->getItems() as $item) {
            if ($item->getUniqueId() == $uniqueId) {
                return $item;
            }
        }
        return null;
    }

    public function getItems($itemType = null) : array
    {
        $items = $this->items;

        if (!is_null($itemType)) {
            $items = array_filter(
                $items,
                function ($item) use ($itemType) {
                    /* @var $item CartItemInterface */
                    return is_subclass_of($item, $itemType);
                }
            );
        }

        return $items;
    }

    public function getCount($itemType = null) : int
    {
        return count($this->getItems($itemType));
    }
}
