<?php

namespace Phycom\Base\Components;

use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;

use yii\db\ActiveQuery;
use Yii;
/**
 * Class Admin
 *
 * @package Phycom\Base\Components
 */
class Admin extends PhycomComponent implements PhycomComponentInterface
{
    /**
     * @return ActiveQuery
     */
    public function getMessageSubscribers() : ActiveQuery
    {
        return Yii::$app->modelFactory->getUser()::find()
            ->where(['type' => UserType::ADMIN])
            ->andWhere(['status' => UserStatus::ACTIVE])
            ->andWhere([
                'or',
                '(settings #>> \'{"receiveOrderNotifications"}\') :: text = \'true\'',
                '(settings #>> \'{"receiveErrorNotifications"}\') :: text = \'true\'',
                '(settings #>> \'{"receiveShipmentNotifications"}\') :: text = \'true\''
            ]);
    }
}
