<?php

namespace Phycom\Base\Components;

use Phycom\Base\Models\UserActivity;
use Phycom\Base\Models\Vendor;

use Yii;

/**
 * Class User
 *
 * @package Phycom\Base\Components
 *
 * @property \Phycom\Base\Models\User $identity
 * @property \Phycom\Base\Models\Vendor $vendor
 */
class User extends \yii\web\User
{
    /**
     * @var Vendor
     */
	private Vendor $vendor;

    /**
     * @param $value
     * @return bool
     */
    public function activity($value) : bool
    {
        return UserActivity::create($this->id, $value);
    }

    /**
     * @return Vendor|null
     */
    public function getVendor() : ?Vendor
    {
    	if ($this->identity) {
    		if (!isset($this->vendor)) {
    			$this->vendor = Vendor::findOne(['id' => 1]);
		    }
    		return $this->vendor;
	    }
	    return null;
    }

    /**
     * @param yii\web\IdentityInterface $identity
     */
    public function setLanguage(yii\web\IdentityInterface $identity)
    {
        if ($identity instanceof \Phycom\Base\Models\User && $identity->settings->language) {
            Yii::$app->language = $identity->settings->language;
        }
    }

    /**
     * This method is called after the user is successfully logged in.
     * The default implementation will trigger the [[EVENT_AFTER_LOGIN]] event.
     * If you override this method, make sure you call the parent implementation
     * so that the event is triggered.
     * @param yii\web\IdentityInterface $identity the user identity information
     * @param boolean $cookieBased whether the login is cookie-based
     * @param integer $duration number of seconds that the user can remain in logged-in status.
     * If 0, it means login till the user closes the browser or the session is manually destroyed.
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
	    /**
	     * @var \Phycom\Base\Models\User $identity
	     */
        Yii::$app->session->set('roles', Yii::$app->authManager->getRolesByUser($identity->id));

	    $this->setLanguage($identity);
        $this->activity('login');

        parent::afterLogin($identity, $cookieBased, $duration);
    }

    /**
     * @param yii\web\IdentityInterface $identity
     */
    protected function afterLogout($identity)
    {
	    /**
	     * @var \Phycom\Base\Models\User $identity
	     */
        UserActivity::create($identity->id, 'logout');
        parent::afterLogout($identity);
    }
}

