<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class ShopScheduleClosed
 *
 * @package Phycom\Base\Components\Commerce
 */
class ShopScheduleClosed extends CommerceComponent implements CommerceComponentInterface
{

}
