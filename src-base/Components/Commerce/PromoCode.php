<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class PromoCode
 *
 * @package Phycom\Base\Components\Commerce
 */
class PromoCode extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var int
     */
    public int $defaultCouponCodeLength = 8;

    /**
     * @var bool 
     */
    public bool $caseSensitive = false;
}
