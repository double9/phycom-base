<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class ShopContent
 *
 * @package Phycom\Base\Components\Commerce
 */
class ShopContent extends CommerceComponent implements CommerceComponentInterface
{

}
