<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class ShopScheduleOpen
 *
 * @package Phycom\Base\Components\Commerce
 */
class ShopScheduleOpen extends CommerceComponent implements CommerceComponentInterface
{

}
