<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class Variant
 *
 * @package Phycom\Base\Components\Commerce
 */
class Variant extends CommerceComponent implements CommerceComponentInterface
{
    public array $definitions = [];
}
