<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;
use Phycom\Base\Models\Attributes\ShopType;
use Phycom\Base\Models\Attributes\VendorType;

use Yii;

/**
 * Class Shop
 *
 * @package Phycom\Base\Components\Commerce
 */
class Shop extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var bool - specifies if multiple vendor mode is enable. If true then additional vendor list and form is shown on backend
     */
    public bool $multipleVendors = false;

    /**
     * @var array - available shop types used
     */
    public array $shopTypes = [
        ShopType::POINT_OF_SALE,
        ShopType::SHOWROOM
    ];

    /**
     * @var string|null
     */
    public ?string $defaultShopType = ShopType::POINT_OF_SALE;

    /**
     * @var array - available vendor types used
     */
    public array $vendorTypes = [
        VendorType::RESELLER
    ];

    /**
     * @var array
     */
    public array $vendorFields = [
        'name',
        'type',
        'legalName',
        'regNumber',
        'vatNumber',
        'email',
        'status'
    ];

    /**
     * @var string|null
     */
    public ?string $defaultVendorType = VendorType::RESELLER;

    /**
     * @var string
     */
    public string $shopI18nCategory = 'phycom/backend/shop';

    /**
     * @var string
     */
    public string $vendorI18nCategory = 'phycom/backend/shop';

    /**
     * @var array - shops list-view columns in back-office.
     */
    public array $backendShopListColumns = [];

    /**
     * @var array - vendor list-view columns in back-office
     */
    public array $backendVendorListColumns = [];

    /**
     * @param string $columnName
     * @return bool
     */
    public function shopListHasColumn(string $columnName): bool
    {
        return in_array($columnName, $this->backendShopListColumns);
    }

    /**
     * @param string $columnName
     * @return bool
     */
    public function vendorListHasColumn(string $columnName): bool
    {
        return in_array($columnName, $this->backendVendorListColumns);
    }

    /**
     * @param string $fieldName
     * @return bool
     */
    public function vendorHasField(string $fieldName): bool
    {
        return in_array($fieldName, $this->vendorFields);
    }
}
