<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class Shipment
 *
 * @package Phycom\Base\Components\Commerce
 */
class Shipment extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var bool - if this is true then shipment postage label is auto-generated
     */
    public bool $autoPostageLabel = false;
}
