<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;
use Phycom\Base\Models\Product\Import\ProductImportStrategyInterface;

/**
 * Class SProductImporter
 *
 * @package Phycom\Base\Components\Commerce
 */
class ProductImporter extends CommerceComponent implements CommerceComponentInterface
{
    public string $logCategory = 'product-import';
    /**
     * @param ProductImportStrategyInterface $strategy
     * @param callable|null $onProgress
     * @return bool
     */
    public function import(ProductImportStrategyInterface $strategy, callable $onProgress = null) : bool
    {
        return $strategy->prepare()->import($onProgress);
    }
}

