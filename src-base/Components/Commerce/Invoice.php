<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class invoice
 *
 * @package Phycom\Base\Components\Commerce
 */
class Invoice extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var int - number of days when invoice is due
     */
    public $defaultDueDateTerm = 3;
}
