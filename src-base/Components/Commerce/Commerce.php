<?php

namespace Phycom\Base\Components\Commerce;


use Phycom\Base\Components\PhycomComponentTrait;
use Phycom\Base\Interfaces\CommerceComponentInterface;
use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\Attributes\SaleState;
use Phycom\Base\Modules\Payment\Module as PaymentModule;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use yii\base\InvalidConfigException;
use yii\di\ServiceLocator;
use Yii;

/**
 * Class Commerce
 *
 * @package Phycom\Base\Components\Commerce
 *
 * @property-read PaymentModule|CommerceComponentInterface $payment
 * @property-read DeliveryModule|CommerceComponentInterface $delivery
 *
 * @property-read Order $order
 * @property-read Invoice $invoice
 * @property-read Shipment $shipment
 * @property-read PromoCode $promoCodes
 * @property-read ClientCard $clientCards
 * @property-read Variant $variants
 * @property-read Param $params
 * @property-read Shop $shop
 * @property-read ShopScheduleOpen $shopOpen
 * @property-read ShopScheduleClosed $shopClosed
 * @property-read ShopSupply $shopSupply
 * @property-read ShopContent $shopContent
 * @property-read ProductImporter $productImporter
 */
class Commerce extends ServiceLocator implements PhycomComponentInterface
{
    use PhycomComponentTrait;

    /**
     * @var bool
     */
    public bool $enabled = true;

    /**
     * @var array|\Closure|float|int
     */
    public array|\Closure|float|int $vatConfig = 0;

    /**
     * @var string
     */
    public string $saleState = SaleState::OPEN;

    /**
     * @var string
     */
    public ?string $closedMessage;

    /**
     * @var bool - when true every pricing row also has a SKU field so that SKU could be different for some unit(s)
     */
    public bool $usePricingSkuPerUnit = false;

    /**
     * @var bool - when true pricing rows can be set as non purchaseable and customer cannot buy these particular quantities
     */
    public bool $usePricingPurchaseState = true;

    /**
     * @var array
     */
    public array $unitLabels = [];

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @return PaymentModule|\yii\base\Module|CommerceComponentInterface|null
     */
    public function getPayment() : ?PaymentModule
    {
        return Yii::$app->getModule('payment');
    }

    /**
     * @return DeliveryModule|\yii\base\Module|CommerceComponentInterface|null
     */
    public function getDelivery() : ?DeliveryModule
    {
        return Yii::$app->getModule('delivery');
    }

    /**
     * @return float|int
     */
    public function getVatRate(int $vendorId = null): float|int
    {
        if (\is_array($this->vatConfig)) {
            if (!array_key_exists($vendorId, $this->vatConfig)) {
                throw new InvalidConfigException('Invalid VatRate config: vendor ' . $vendorId . ' key was not found');
            }
            return \is_callable($this->vatConfig[$vendorId])
                ? call_user_func($this->vatConfig[$vendorId], $vendorId)
                : $this->vatConfig[$vendorId];

        } else if (\is_callable($this->vatConfig)) {
            return call_user_func($this->vatConfig, $vendorId);
        }
        return $this->vatConfig;
    }
}
