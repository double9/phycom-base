<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class Param
 *
 * @package Phycom\Base\Components\Commerce
 */
class Param extends CommerceComponent implements CommerceComponentInterface
{
    public array $definitions = [];
}
