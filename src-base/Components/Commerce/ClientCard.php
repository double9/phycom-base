<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class ClientCard
 *
 * @package Phycom\Base\Components\Commerce
 */
class ClientCard extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var bool
     */
    public bool $useBirthdayDiscount = false;

    /**
     * @var int
     */
    public int $defaultCouponCodeLength = 8;
}
