<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Components\PhycomComponentTrait;

use yii\base\BaseObject;
use Yii;

/**
 * Class ObjectComponent
 *
 * @package Phycom\Base\Components\Commerce
 */
abstract class CommerceComponent extends BaseObject
{
    use PhycomComponentTrait;

    public bool $enabled = true;

    public function isEnabled() : bool
    {
        return Yii::$app->commerce->isEnabled() && $this->enabled;
    }
}
