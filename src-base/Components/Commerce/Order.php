<?php

namespace Phycom\Base\Components\Commerce;

use Phycom\Base\Components\FileStorage;
use Phycom\Base\Interfaces\CommerceComponentInterface;

/**
 * Class Order
 *
 * @package Phycom\Base\Components\Commerce
 */
class Order extends CommerceComponent implements CommerceComponentInterface
{
    /**
     * @var bool - if this is true then automatic sms message is sent to admin users who have been subscribed to order notifications
     */
    public bool $sendOrderNotificationSms = false;

    /**
     * @var array
     */
    public array $orderNotificationEmails = [];

    /**
     * @var string
     */
    public string $invoiceFilePath = '@files/' . FileStorage::BUCKET_INVOICES;
}
