<?php

namespace Phycom\Base\Components;





use Geocoder\Collection;
use Phycom\Base\Helpers\c;
use Phycom\Base\Interfaces\PhycomComponentInterface;
use Phycom\Base\Models\Attributes\AddressField;

use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Provider\Provider;
use Http\Client\Curl\Client;

use yii\base\InvalidConfigException;
use Yii;


/**
 * Class Geocoder
 *
 * @package Phycom\Base\Helpers
 */
class Geocoder extends PhycomComponent implements PhycomComponentInterface
{
    /**
     * @param AddressField $address
     * @return Collection
     * @throws InvalidConfigException
     * @throws \Geocoder\Exception\Exception
     */
    public function geocode(AddressField $address): Collection
    {
        $geocoder = new \Geocoder\StatefulGeocoder($this->createProvider());
        $addressStr = implode(', ', $address->exportArray());
        return $geocoder->geocode($addressStr);
    }

    /**
     * @param $lat
     * @param $lng
     * @return Collection
     * @throws InvalidConfigException
     * @throws \Geocoder\Exception\Exception
     */
    public function reverse($lat, $lng): Collection
    {
        $geocoder = new \Geocoder\StatefulGeocoder($this->createProvider());
        return $geocoder->reverse($lat, $lng);
    }

    /**
     * @return Provider|object
     * @throws InvalidConfigException
     */
    protected function createProvider(): Provider
    {
        return Yii::createObject(GoogleMaps::class, [$this->createAdapter(), null, c::param('googleMapsApiKey')]);
    }

    /**
     * @return Client|object
     * @throws InvalidConfigException
     */
    protected function createAdapter()
    {
        $options = [
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_SSL_VERIFYPEER => false,
        ];
        return Yii::createObject(Client::class, [null, null, $options]);
    }
}
