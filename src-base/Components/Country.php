<?php

namespace Phycom\Base\Components;

use yii\helpers\ArrayHelper;
use yii\base\Component;

/**
 * Class Country
 * @package Phycom\Base\Helpers
 *
 * @property string $defaultCountry
 * @property string $country
 * @property array $countries
 * @property array $preferredCountries
 * @property-read \Phycom\Base\Models\Country $model
 */
class Country extends Component
{
    /**
     * @var string
     */
	public $defaultCountry;
    /**
     * @var string
     */
	protected $country;
    /**
     * @var array
     */
	protected array $countries = [];
    /**
     * @var array
     */
	protected array $preferredCountries = [];
    /**
     * @var \Phycom\Base\Models\Country
     */
	private \Phycom\Base\Models\Country $model;

    /**
     * @return array
     */
	public function getCountries() : array
	{
		return $this->countries;
	}

    /**
     * @param string $countryCode
     */
	public function setCountry(string $countryCode)
    {
        $this->country = $countryCode;
    }
    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country ?: $this->defaultCountry;
    }

    /**
     * @return \Phycom\Base\Models\Country|null
     */
    public function getModel() : ?\Phycom\Base\Models\Country
    {
        if (!isset($this->model) || !$this->model || $this->model->code !== $this->getCountry()) {
            $this->model = \Phycom\Base\Models\Country::findOne(['code' => $this->getCountry()]);
        }
        return $this->model;
    }

    /**
     * @param array $value
     */
	public function setCountries(array $value)
	{
		if (!empty($value) && count($this->preferredCountries) > 0) {
			$this->countries = ArrayHelper::merge([$this->defaultCountry], $this->preferredCountries, $value);
		}
		$this->countries = $value;
	}

    /**
     * @return array
     */
	public function getPreferredCountries() : array
	{
		return $this->preferredCountries;
	}

    /**
     * @param array $value
     */
	public function setPreferredCountries(array $value)
	{
		if (!empty($value) && count($this->countries) > 0) {
			$this->countries = ArrayHelper::merge($this->countries, $value);
		}
		$this->preferredCountries = $value;
	}

	public function __toString() : string
    {
        return $this->getCountry();
    }
}
