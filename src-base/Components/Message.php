<?php

namespace Phycom\Base\Components;

use Phycom\Base\Interfaces\PhycomComponentInterface;

/**
 * Class Message
 *
 * @package Phycom\Base\Components
 */
class Message extends PhycomComponent implements PhycomComponentInterface
{
    public $attachmentFileRootPath = '@files';
}
