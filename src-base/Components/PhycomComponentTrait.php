<?php

namespace Phycom\Base\Components;


use yii\helpers\Inflector;

trait PhycomComponentTrait
{
    public function getName(): string
    {
        return Inflector::camel2id((new \ReflectionClass($this))->getShortName());
    }

    public function getLabel(): string
    {
        $className = (new \ReflectionClass($this))->getShortName();
        return $className ?: Inflector::titleize($className);
    }

    public function getJsonSchema(): array
    {
        return [];
    }
}