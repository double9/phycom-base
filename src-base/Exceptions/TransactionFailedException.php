<?php

namespace Phycom\Base\Exceptions;

/**
 * Class TransactionFailedException
 * @package Phycom\Base\Exceptions
 */
class TransactionFailedException extends \yii\db\Exception
{

}
