<?php

namespace Phycom\Base\Exceptions;

use yii\base\Exception;

/**
 * Class DataImporterException
 *
 * @package Phycom\Base\Exceptions
 */
class DataImporterException extends Exception
{

}
