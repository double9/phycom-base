<?php

namespace Phycom\Base\Exceptions;

use yii\base\Exception;

/**
 * Class TranslationMissingException
 * @package Phycom\Base\Exceptions
 */
class TranslationMissingException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Missing translation';
    }
}
