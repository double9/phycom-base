<?php

namespace Phycom\Base\Modules\Delivery\Controllers;

use Phycom\Base\Models\Shop;
use Phycom\Base\Modules\Delivery\Models\DispatchTimeCalculator;
use Phycom\Base\Modules\Delivery\Module;
use Phycom\Console\Controllers\BaseConsoleController;

use yii\helpers\Console;
use yii\console\ExitCode;
use yii;

/**
 * Class ConsoleController
 * @package Phycom\Base\Modules\Delivery\Controllers
 */
class ConsoleController extends BaseConsoleController
{
    /**
     * @return null|yii\base\Module|Module
     */
    public function getModule()
    {
        return Yii::$app->getModule('delivery');
    }

    /**
     * @param string $deliveryMethod
     * @param int $shopId
     * @param string $orderTime
     * @param int $processingTime
     * @return int
     */
    public function actionCalculateDeliveryDates($deliveryMethod, int $shopId, $orderTime = 'now', $processingTime = 0)
    {
        $deliveryMethod = $this->getModule()->getDeliveryMethod($deliveryMethod);
        $shop = Shop::findOne($shopId);


        $DispatchTimeCalculator = $deliveryMethod->dispatchTimeCalculator;
        /**
         * @var DispatchTimeCalculator $calculator
         */
        $calculator = new $DispatchTimeCalculator($deliveryMethod, $shop, $processingTime = 0);


        $times = $calculator->calculateDeliveryDates(new \DateTime($orderTime));
        $this->printLn(json_encode($times, JSON_PRETTY_PRINT), Console::FG_YELLOW);

        return ExitCode::OK;
    }
}
