<?php

namespace Phycom\Base\Modules\Delivery\Controllers;

use Phycom\Frontend\Components\Cart;

use Phycom\Base\Models\Traits\WebControllerTrait;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Helpers\f;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Modules\Delivery\DeliveryException;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\ShippingForm;
use Phycom\Base\Modules\Delivery\Module;

use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use Yii;

/**
 * Class CartActionsController
 * @package Phycom\Base\Modules\Delivery\Controllers
 *
 * @property-read Module $module
 */
class CheckoutController extends Controller
{
	use WebControllerTrait;
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => ['add-address', 'get-delivery-dates', 'get-delivery-times', 'get-price'],
						'allow' => true,
						'roles' => ['?','@']
					],
					['allow' => true, 'roles' => ['@']], // Only logged in users should be able to access
					['allow' => false]
				],
			],
			'verbs' => [
				'class' => VerbFilter::class,
                'actions' => [
                    'add-address'         => ['post'],
                    'set-delivery-method' => ['post'],
                    'get-delivery-dates'  => ['get'],
                    'get-delivery-times'  => ['get'],
                    'get-price'           => ['get']
                ]
			]
		];
	}

    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['ok'];
    }

    /**
     * @param string $deliveryMethod
     * @param string $areaCode
     * @return float
     *
     * @throws DeliveryException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
	public function actionGetPrice(string $deliveryMethod, string $areaCode)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        /**
         * @var DeliveryMethod|DeliveryMethodInterface $method
         */
        if (!$method = $this->module->getModule($deliveryMethod)) {
            throw new DeliveryException('Delivery method ' . $deliveryMethod . ' not found');
        }

        if (!$deliveryArea = DeliveryArea::findOne(['method' => $deliveryMethod, 'area_code' => $areaCode])) {
            throw new DeliveryException('Delivery area ' . $areaCode . ' not found');
        }

        $price = $method
            ->invokePriceStrategy([Yii::$app->cart->getItems(Cart::ITEM_PRODUCT)])
            ->calculatePrice($deliveryArea);

        return Currency::toDecimal($price);
    }

	/**
	 * If user is logged in we add the address to database and return the id.
	 * In case user is not authenticated we just validate the form and return the serialized address value instead of the id.
	 *
	 * @param string $id - delivery method id
	 * @return array
	 * @throws DeliveryException
     * @throws NotFoundHttpException|InvalidConfigException
     */
	public function actionAddAddress($id)
	{
	    $this->ajaxOnly(Response::FORMAT_JSON);
		/**
		 * @var DeliveryMethod|DeliveryMethodInterface $method
		 */
		$method = $this->module->getModule($id);
		$addressForm = $method->getAddressFormModel();

		if ($addressForm->load(Yii::$app->request->post())) {
			if ($address = $addressForm->save()) {

			    $value = $address->isNewRecord ? (string)$address->export() : $address->id;
			    $price = $method->invokePriceStrategy([Yii::$app->cart->getProductItems()])->calculatePrice($addressForm->deliveryArea);

			    if (!is_numeric($value)) {
                    Yii::$app->session->set('delivery_method', $id);
			        Yii::$app->session->set('delivery_address', $value);
                }

				FlashMsg::success(Yii::t('phycom/modules/delivery', 'Address added successfully'));

			    $createLabel = $this->module->createDeliveryAddressTitle;

                return $this->ajaxSuccess([
                    'value'        => $value,
                    'label'        => $createLabel($address, $price),
                    'dataLabel'    => $addressForm->deliveryArea->getLabel(),
                    'price'        => Currency::toDecimal($price),
                    'priceDisplay' => f::currency($price),
                    'code'         => $addressForm->deliveryArea->getCode(),
                    'areaCode'     => $addressForm->deliveryArea->getAreaCode()
                ]);
			}
			FlashMsg::error($addressForm->lastError);
			return $this->ajaxError();

		} else {
			throw new DeliveryException('Invalid call ' . $this->route);
		}
	}

    /**
     * @return mixed
     * @throws yii\web\NotFoundHttpException|\yii\base\InvalidConfigException
     */
	public function actionGetDeliveryDates()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);
        $formattedDates = [];

        /**
         * @var ShippingForm $form
         */
        $form = Yii::createObject(ShippingForm::class);

        if ($form->load(Yii::$app->request->get())) {

            $shipment = $form->method->createShipment(Yii::$app->cart->getProductItems(), $form->address, $form->areaCode);

            $calculator = $shipment->getDispatchTimeCalculator();
            $calculator->setDeliveryArea($form->getArea());
            $deliveryDates = $calculator->calculateDeliveryDates();

            $formattedDates = array_map(function (\DateTime $date) {
                return $date->format('Y-m-d');
            }, $deliveryDates);
        }
        return $formattedDates;
    }

    /**
     * @param string $deliveryMethod
     * @param string $areaCode
     * @param string $date
     *
     * @return mixed
     * @throws DeliveryException
     * @throws yii\web\NotFoundHttpException|\yii\base\InvalidConfigException
     */
    public function actionGetDeliveryTimes(string $deliveryMethod, string $areaCode, string $date)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$deliveryDate = new \DateTime($date)) {
            throw new DeliveryException('Invalid delivery date ' . $date);
        }
        if (!$deliveryArea = DeliveryArea::findOne(['method' => $deliveryMethod, 'area_code' => $areaCode])) {
            throw new DeliveryException('Delivery area not found');
        }

        /**
         * @var DeliveryMethodInterface|DeliveryMethod $method
         */
        $method = $this->module->getModule($deliveryMethod);

        $shipment = $method->createShipment(Yii::$app->cart->getProductItems(), null, $areaCode);
        $dispatchTimeCalculator = $shipment->getDispatchTimeCalculator();
        $dispatchTimeCalculator->setDeliveryArea($deliveryArea);

        return $dispatchTimeCalculator->calculateDeliveryTimes($deliveryDate);
    }
}
