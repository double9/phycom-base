<?php

namespace Phycom\Base\Modules\Delivery\Helpers;

use yii;

/**
 * Class Log
 * @package Phycom\Base\Modules\Delivery\Helpers
 */
class Log
{
    const DEFAULT_CATEGORY = 'delivery';

    public static function info($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::info($msg, $category);
    }

    public static function trace($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::trace($msg, $category);
    }

    public static function warning($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::warning($msg, $category);
    }

    public static function error($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::error($msg, $category);
    }
}
