var shippingBox = (function ($) {

    var instance;

    var ShippingBox = function(el, options) {

        this.$el = $(el);
        if (!this.$el.length) {
            throw 'Element ' + el + ' was not found';
        }
        this.el = this.$el[0];
        this.cart = typeof window.cart !== 'undefined' ? window.cart : null;
        this.baseUrl = this.$el.find('.shipping-box-container').data('base-url');

        this.opts = options;
        this.init();
    };

    ShippingBox.prototype.init = function () {
        let self = this;

        this.$el.on('dp.change', 'input[name="ShippingForm[deliveryDate]"]', function (e) {

            let deliveryMethod = $(this).closest('form').find('input[name="ShippingForm[methodId]"]').val(),
                options = self.opts[deliveryMethod],
                areaCode, url;

            if (options.customDeliveryTime) {

                areaCode = this.closest('form').querySelector('.delivery-address option:checked').dataset.areaCode;

                if (areaCode) {

                    url = self.baseUrl + '/get-delivery-times?deliveryMethod=' + deliveryMethod + '&areaCode=' + areaCode + '&date=' + e.date.format('YYYY-MM-DD');

                    // remove selected time
                    if (options.customDeliveryTime === 'options') {
                        let optionDropdown = self.getTimeOptionDropdown(deliveryMethod);
                        optionDropdown.value = '';
                        optionDropdown.selectedIndex = -1;
                    }

                    $.get(url).done(function (times) {

                        if (options.customDeliveryTime === 'options') {
                            self.handleDeliveryTimeOptions(deliveryMethod, times);
                        } else {
                            self.handleDeliveryTimeRange(deliveryMethod, times);
                        }

                    });
                }
            }

        });


        this.$el.on('change', '.delivery-address', function (e) {
            var value = $(this).val() || null,
                url = self.baseUrl + '/get-delivery-dates?' + $(this).closest('form').serialize(),
                $dateField = $(this).closest('form').find('input[name="ShippingForm[deliveryDate]"]'),
                datePicker,
                selectedDate;

            if (value) {
                self.updateCart($(this).find('option:selected'));


                if ($dateField.length) {
                    datePicker = $dateField.data('DateTimePicker');
                    selectedDate = $dateField.val();

                    $.get(url).done(function (dates) {

                        datePicker.enabledDates(dates);

                        if (-1 === $.inArray(selectedDate, dates)) {
                            if (dates.length) {
                                datePicker.date(dates[0]);
                            }
                        } else {
                            $dateField.trigger({
                                type: 'dp.change',
                                date: moment(selectedDate, moment.HTML5_FMT.DATE),
                                oldDate: null
                            });
                        }

                    });
                }
            }
        });


        $('.nav', this.el).on('shown.bs.tab', function (e) {
            var $deliveryAddress = self.getSelectedAddress();
            $deliveryAddress.trigger('change');
            self.checkOpenAddressForm($deliveryAddress);
        });

        // if ($('.cart-table tr[data-type="delivery"]').length > 0) {
        //
        //
        // } else {
        //
        //
        // }

        var $deliveryAddress = this.getSelectedAddress();

        if ($deliveryAddress.length && !$deliveryAddress.val()) {
            let selected = $deliveryAddress.find('option[selected]').val();
            if (selected) {
                $deliveryAddress.val(selected);
            }
        };

        if ($deliveryAddress.length && $deliveryAddress.val()) {
            $deliveryAddress.trigger('change');
        }
    };

    ShippingBox.prototype.handleDeliveryTimeRange = function (deliveryMethod, times) {

        let timePicker = $('#' + deliveryMethod + '-time').data('DateTimePicker'),
            hStart = parseInt(times.from.split(':')[0]),
            hEnd = parseInt(times.to.split(':')[0]),
            hours = [];

        for (let i = hStart; i <= hEnd; i++) {
            hours.push(i);
        }
        timePicker.enabledHours(hours);

        if (hStart > 0) {
            timePicker.disabledHours([0]);
        }

        let intervals = [];
        let from = times.from;

        if (moment(e.date.format('YYYY-MM-DD') + 'T' + from, moment.HTML5_FMT.DATETIME_LOCAL).isBefore(moment())) {
            from = moment().add(15, 'm').format(moment.HTML5_FMT.TIME);
        }

        if (from !== '00:00') {
            intervals.push([
                moment('00:00', moment.HTML5_FMT.TIME),
                moment(from, moment.HTML5_FMT.TIME)
            ]);
        }

        if (parseInt(times.from.replace(':','')) < parseInt(times.to.replace(':','')) && times.to !== '00:00') {
            intervals.push([
                moment(times.to, moment.HTML5_FMT.TIME),
                moment('23:59', moment.HTML5_FMT.TIME)
            ]);
        }

        if (intervals.length) {
            timePicker.disabledTimeIntervals(intervals);
        }

        if (timePicker.date().isBefore(moment(from, moment.HTML5_FMT.TIME))) {
            timePicker.date(moment(from, moment.HTML5_FMT.TIME));
        }
    };

    ShippingBox.prototype.getTimeOptionDropdown = function (deliveryMethod) {
        return document.getElementById(deliveryMethod).querySelector('#shippingform-deliverytimeoption');
    };

    ShippingBox.prototype.handleDeliveryTimeOptions = function (deliveryMethod, times) {

        let shippingTime = this.getTimeOptionDropdown(deliveryMethod);

        if (shippingTime) {

            let selectedTime = shippingTime.dataset.selected;

            shippingTime.options.length = 0;

            if (!times.length) {
                shippingTime[0] = new Option('-', '0');
            }

            let n = 0;
            for (let key in times) {
                let optionValue = key;
                let optionLabel = times[key];
                let optionIsSelected = optionValue === selectedTime;

                shippingTime[n] = new Option(optionLabel, optionValue, false, optionIsSelected);

                n++;
            }
        }
    };


    ShippingBox.prototype.checkOpenAddressForm = function ($deliveryAddress) {
        if ($deliveryAddress.find('option').length === 0 && !$deliveryAddress.val()) {
            var $addressBtn = $deliveryAddress.closest('.shipping-box').find('.tab-content .tab-pane.active .add-address');
            if ($addressBtn.length) {
                $addressBtn.trigger('click');
            }
        }
    };


    ShippingBox.prototype.getSelectedAddress = function () {
        return this.$el.find('.delivery-address:visible');
    };


    ShippingBox.prototype.destroy = function () {
        this.$el.off('change');
    };

    ShippingBox.prototype.updateCart = function ($deliveryAddress) {

        if (!this.cart || !this.cart.productCount()) {
            return;
        }

        var self = this,
            multiService = $deliveryAddress.data('multi-service') || false,
            data = {}, labels = {};

        if (multiService) {
            return;
        }

        data['id'] = $deliveryAddress.attr('data-code');
        data['code'] = $deliveryAddress.attr('data-code');
        data['price'] = $deliveryAddress.attr('data-price');
        data['type'] = 'delivery';
        data['ref'] = $deliveryAddress.closest('form').find('input[name="ShippingForm[methodId]"]').val();


        // let deliveryMethod = $deliveryAddress.closest('form').find('input[name="ShippingForm[methodId]"]').val(),
        //     areaCode = $deliveryAddress.attr('data-area-code'),
        //     priceUpdateUrl = this.baseUrl + '/get-price?deliveryMethod=' + deliveryMethod + '&areaCode=' + areaCode;
        //
        // data['price'] = function() {
        //     const csrfToken = document.querySelector('meta[name="csrf-token"]').content;
        //     return fetch(priceUpdateUrl, {
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'X-Requested-With': 'XMLHttpRequest',
        //             'X-CSRF-Token': csrfToken,
        //         },
        //     }).then(response => response.json());
        // };

        labels['price'] = $deliveryAddress.attr('data-price-display');
        labels['label'] = $deliveryAddress.attr('data-label');

        this.cart.removeByType('delivery');
        this.cart.addOrUpdateJs(data, labels);
    };

    ShippingBox.prototype.getActiveForm = function () {
        return $('#delivery-method').find('.tab-pane.active > form');
    };


    ShippingBox.prototype.exportSelectedValues = function (methodId) {

        let selected = {
            method: methodId
        };

        let shippingForm = this.el.querySelector('form[data-method="' + methodId + '"]');
        if (shippingForm) {

            let deliveryArea = shippingForm.querySelector('select[name="ShippingForm[areaCode]"]');
            if (deliveryArea) {
                selected.area = deliveryArea.options[deliveryArea.selectedIndex].value
            }

            let deliveryDate = shippingForm.querySelector('input[name="ShippingForm[deliveryDate]"]');
            if (deliveryDate && deliveryDate.value.length) {
                selected.date = deliveryDate.value;
            }

            let deliveryTime = shippingForm.querySelector('select[name="ShippingForm[deliveryTimeOption]"]');
            if (deliveryTime) {
                selected.time = deliveryTime.options[deliveryTime.selectedIndex].value
            }
        }

        return selected;
    };

    return {
        getPrototype: function () {
            return ShippingBox.prototype;
        },
        init: function (el, config) {
            instance = new ShippingBox(el, config);
        },
        getInstance: function () {
            return instance;
        },
        getForm: function () {
            return instance.getActiveForm();
        }
    };
})(jQuery);
