<?php

namespace Phycom\Base\Modules\Delivery\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * ShippingBox widget asset bundle.
 */
class ShippingBoxAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Base/Modules/Delivery/Assets/shipping-box';
	public $js = [
		'main.js'
	];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
	public $depends = [
		JqueryAsset::class
	];
}
