<?php

namespace Phycom\Base\Modules\Delivery\Assets;

use yii\web\AssetBundle;

/**
 * ShippingBox widget asset bundle.
 */
class ShippingBoxStyles extends AssetBundle
{
	public $sourcePath = '@Phycom/Base/Modules/Delivery/Assets/shipping-box';
	public $css = [
	    'main.css'
    ];
	public $publishOptions = ['except' => ['*.less']];
}
