<?php

namespace Phycom\Base\Modules\Delivery\Widgets;


use Phycom\Base\Helpers\Json;
use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartItemDeliveryInterface;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Order;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Models\Address;
use Phycom\Base\Modules\Delivery\Assets\ShippingBoxAsset;
use Phycom\Base\Modules\Delivery\Assets\ShippingBoxStyles;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;
use Phycom\Base\Modules\Delivery\Models\ShippingForm;
use Phycom\Base\Modules\Delivery\Module;

use Phycom\Frontend\Components\Cart;
use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use Phycom\Base\Helpers\f;
use Phycom\Base\Helpers\PhoneHelper;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\Shipment;

use rmrevin\yii\fontawesome\FAS;

use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\di\Instance;
use yii;

/**
 * Class DeliveryWidget
 * @package Phycom\Base\Modules\Delivery\Widgets
 */
abstract class DeliveryWidget extends Widget
{
    /**
     * @var string|null
     */
    public ?string $selectedMethod = null;

    /**
     * @var Address|int|null
     */
    public $selectedAddress = null;

    /**
     * @var string|null
     */
    public ?string $selectedArea = null;

    /**
     * @var string|null
     */
    public ?string $selectedDate = null;

    /**
     * @var string|null
     */
    public ?string $selectedTime = null;

    /**
     * @var Shipment
     */
    public $shipment;


    public bool $loadStyles = true;


    public bool $inlineAddressForm = false;
    /**
     * @var string|null - a message shown when no delivery methods are available
     */
    public string $msgNotAvailable;

    public array $options = ['class' => 'shipping-box'];
    public array $tabContentOptions = ['class' => 'col-md-12'];
    public array $tabItemOptions = ['role' => 'tabpanel', 'class' => 'tab-pane'];
    public array $navOptions = ['class' => 'nav nav-pills nav-stacked', 'role' => 'tablist'];
    public array $navItemOptions = [];
    public array $navItemLinkOptions = ['data-toggle' => 'pill'];

    public array $clientOptions = [];

    /**
     * @var CartInterface|mixed
     */
    public $cart = 'cart';

    /**
     * @var DeliveryMethodInterface[]|DeliveryMethod[]
     */
    public array $methods = [];

    /**
     * @var ActiveForm
     */
    protected $form;

    protected $model;

    protected bool $hideMethodsNav = false;

    /**
     * @var Module
     */
    protected $deliveryModule;

    /**
     * Initializes the widget.
     * This renders the form open tag.
     *
     * @throws yii\base\InvalidConfigException
     */
    public function init()
    {
        /**
         * @var Module $module
         */
        $this->deliveryModule = Yii::$app->getModule('delivery');
        $this->cart = Instance::ensure($this->cart, CartInterface::class);

        if (!isset($this->msgNotAvailable)) {
            $this->msgNotAvailable = Yii::t('phycom/modules/delivery', 'No delivery methods available for this order');
        }
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
        $this->loadItems();
        if (!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity->addresses)) {
            $this->inlineAddressForm = false;
        }

        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order): DeliveryWidget
    {
        $this->selectedMethod = $order->shipment->method;
        $this->shipment = $order->shipment;

        return $this;
    }

    /**
     * @return CartItemDeliveryInterface[]
     */
    protected function getCartDeliveryItems() : array
    {
        return $this->cart->getItems(CartItemDeliveryInterface::class);
    }


    public function loadItems()
    {
        $this->methods = $this->deliveryModule->methods;
    }


    public function __toString()
    {
        return '';
    }


    protected function registerAssets()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        ShippingBoxAsset::register($view);

        if ($this->loadStyles) {
            ShippingBoxStyles::register($view);
        }

        $options = [];
        foreach ($this->methods as $deliveryMethod) {
            $options[$deliveryMethod->getId()] = [
                'customDeliveryDate'       => $deliveryMethod->customDeliveryDate,
                'customDeliveryTime'       => $deliveryMethod->customDeliveryTime,
                'customDeliveryTimeOption' => $deliveryMethod->customDeliveryTime === 'option' && !empty($deliveryMethod->deliveryTimeOptions)
            ];
        }
        $options = Json::encode(ArrayHelper::merge($options, $this->clientOptions));
        $view->registerJs("shippingBox.init('#$id', $options);");
    }


    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return ShippingForm|object
     * @throws InvalidConfigException
     */
    protected function createShippingForm(DeliveryMethodInterface $deliveryMethod): ShippingForm
    {
        $config = [
            'class'        => ShippingForm::class,
            'methodId'     => $deliveryMethod->getId(),
            'areaCode'     => $this->selectedArea,
            'deliveryDate' => $this->selectedDate
        ];

        /**
         * @var ShippingForm|object $model
         */
        $model = Yii::createObject($config);

        if ($this->shipment && $this->shipment->method === $deliveryMethod->getId()) {
            $model->loadShipmentData($this->shipment);
        }

        if ($this->selectedTime) {
            $model->populateDeliveryTime($this->selectedTime);
        }

        if (!$deliveryMethod->multiService) {
            $model->service = $deliveryMethod->defaultService;
        }

        return $model;
    }


    protected function findActiveMethod()
    {
        if ($this->shipment) {
            $this->selectedMethod = $this->shipment->method;
        } elseif ($this->selectedAddress && !$this->selectedMethod) {
            foreach ($this->methods as $deliveryMethod) {
                if ($deliveryMethod->hasDeliveryAddress()) {
                    $this->selectedMethod = $deliveryMethod->getId();
                    break;
                }
            }
        }
        if (!$this->selectedMethod && !empty($this->methods)) {
            $this->selectedMethod = $this->methods[0]->getId();
        }
    }

    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @return string
     */
    protected function renderInlineAddressForm(DeliveryMethodInterface $deliveryMethod)
    {
        $model = $deliveryMethod->getAddressFormModel();
        if ($this->selectedAddress) {
            $model->name = $this->selectedAddress->name;
            $model->firstName = $this->selectedAddress->firstName;
            $model->lastName = $this->selectedAddress->lastName;
            $model->email = $this->selectedAddress->email;
            $model->phone = PhoneHelper::getNationalPhoneNumber($this->selectedAddress->phone);
            $model->phoneNumber = $this->selectedAddress->phone;
            if ($model->hasProperty('country')) {
                $model->country = $this->selectedAddress->country;
            }
            $model->province = $this->selectedAddress->province;
            $model->city = $this->selectedAddress->city;
            $model->street = $this->selectedAddress->street;
            $model->postcode = $this->selectedAddress->postcode;

            if (!$this->selectedAddress->area) {
                $this->selectedAddress->findArea($deliveryMethod);
            }
            $model->area = $this->selectedAddress->area;
        }

        $html = Html::beginTag('div', ['class' => 'row clearfix', 'style' => 'margin-bottom: 20px;']);
        $html .= Html::tag('div', $deliveryMethod->renderAddressForm(['model' => $model]), ['class' => 'col-md-12']);
        $html .= Html::endTag('div');
        return $html;
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param callable|null $category
     * @return \Phycom\Frontend\Widgets\Bootstrap4\ActiveField
     * @throws yii\base\InvalidConfigException
     */
    protected function renderDeliveryAreaDropdown(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, callable $category = null)
    {
        $options = [];

        // fetch only active delivery areas
        $areas = array_filter($deliveryMethod->getAreas(), fn($area) => $area->status->is(DeliveryAreaStatus::ACTIVE));

        $createDeliveryAreaLabel = is_callable($this->deliveryModule->createDeliveryAreaTitle)
            ? $this->deliveryModule->createDeliveryAreaTitle
            : function (DeliveryArea $area, $price) { return $area->name; };

        $areaList = [];
        $mappedAreas = ArrayHelper::map($areas, 'area_code', function ($item) {return $item;}, $category);
        foreach ($mappedAreas as $key => $value) {
            if (is_array($value)) {
                $areaList[$key] = [];
                foreach ($value as $code => $deliveryArea) {
                    $price = $deliveryMethod->invokePriceStrategy([$this->cart->getItems(Cart::ITEM_PRODUCT)])->calculatePrice($deliveryArea);
                    $areaList[$key][$code] = $createDeliveryAreaLabel($deliveryArea, $price);
                }
            } else {
                $price = $deliveryMethod->invokePriceStrategy([$this->cart->getItems(Cart::ITEM_PRODUCT)])->calculatePrice($value);
                $areaList[$key] = $createDeliveryAreaLabel($value, $price);
            }
        }

        foreach ($areas as $area) {
            $price = $deliveryMethod->invokePriceStrategy([$this->cart->getItems(Cart::ITEM_PRODUCT)])->calculatePrice($area);
            $options[$area->area_code] = [
                'data-code'          => $area->getCode(),
                'data-area-code'     => $area->getAreaCode(),
                'data-price'         => Currency::toDecimal($price),
                'data-label'         => $area->getLabel(),
                'data-price-display' => f::currency($price),
                'data-method'        => $area->carrier
            ];
        }

        if ($this->selectedAddress && $deliveryMethod->getId() === $this->selectedMethod && $this->selectedAddress->findArea($deliveryMethod)) {
            $model->areaCode = $this->selectedAddress->getArea()->area_code;
        }

        $dropdownOptions = [
            'options' => $options,
            'class' => 'delivery-address form-control',
        ];

        if (!$deliveryMethod->selectFirstAreaByDefault) {
            $dropdownOptions['prompt'] = $deliveryMethod->defaultPromptText;
        }

        return $this->form->field($model, 'areaCode')->dropDownList(
            $areaList,
            $dropdownOptions
        )->label(false);
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $addressList
     * @return \Phycom\Frontend\Widgets\ActiveField
     * @throws \Yii\base\InvalidConfigException
     */
    protected function renderDeliveryAddressDropdown(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array &$addressList)
    {
        $shipmentUrl = $deliveryMethod->shipmentAction ? Url::toRoute($deliveryMethod->shipmentAction) : '';
        $options = [];


        $items = [];
        foreach ($model->userAddresses as $address) {
            if ($address->findArea($deliveryMethod)) {
                $price = $deliveryMethod->invokePriceStrategy([$this->cart->getItems(Cart::ITEM_PRODUCT)])->calculatePrice($address->area);
                $items[$address->id] = [$address, $price];
                $options[$address->id] = [
                    'data-address'       => $address->export()->toJson(),
                    'data-code'          => $address->area->getCode(),
                    'data-area-code'     => $address->area->area_code,
                    'data-price'         => Currency::toDecimal($price),
                    'data-label'         => $address->area->getLabel(),
                    'data-price-display' => f::currency($price),
                    'data-method'        => $address->area->carrier,
                    'data-multi-service' => $deliveryMethod->multiService ? 1 : 0
                ];
            }
        }

        if ($this->selectedAddress && $deliveryMethod->hasDeliveryAddress() && $deliveryMethod->getId() === $this->selectedMethod) {
            $address = $this->selectedAddress;
            if ($address->findArea($deliveryMethod)) {
                $value = (string)$address->export();
                $price = $deliveryMethod->invokePriceStrategy([$this->cart->getItems(Cart::ITEM_PRODUCT)])->calculatePrice($address->area);
                $items[$value] = [$address, $price];
                $options[$value] = [
                    'data-address'       => $address->export()->toJson(),
                    'data-code'          => $address->area->getCode(),
                    'data-area-code'     => $address->area->area_code,
                    'data-price'         => Currency::toDecimal($price),
                    'data-label'         => $address->area->getLabel(),
                    'data-price-display' => f::currency($price),
                    'data-method'        => $address->area->carrier,
                    'data-multi-service' => $deliveryMethod->multiService ? 1 : 0
                ];
                $model->addressId = $value;
            }
        }

        $createDeliveryAddressLabel = is_callable($this->deliveryModule->createDeliveryAddressTitle)
            ? $this->deliveryModule->createDeliveryAddressTitle
            : function (Address $address, $price) { return Yii::$app->formatter->asAddress($address); };

        $items = array_map(function (array $item) use ($createDeliveryAddressLabel) {
            return $createDeliveryAddressLabel(...$item);
        }, $items);

        return $this->form->field($model, 'addressId')->dropDownList(
            $items,
            [
                'options'  => $options,
                'class'    => 'delivery-address form-control' . (empty($addressList) ? ' hide' : ''),
                'data-url' => $shipmentUrl,
                'prompt'   => (!empty($addressList) ? Yii::t('phycom/modules/delivery', 'Select delivery address') : null)
            ]
        )->label(false);
    }


    /**
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return string
     */
    protected function renderAddressButton(DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        if ($deliveryMethod->hasDeliveryAddress()) {

            $defaultOptions = [
                'class'       => 'btn btn-flat btn-primary add-address',
                'data-toggle' => 'modal',
                'data-target' => '#delivery-address-modal_' . $deliveryMethod->getId(),
                'style'       => 'margin-bottom: 10px;'
            ];

            if (isset($options['class'])) {
                Html::addCssClass($defaultOptions, $options['class']);
                unset($options['class']);
            }

            $btn = Html::button(
                FAS::i(FAS::_MAP_MARKER_ALT, ['tag' => 'span']) . '&nbsp;&nbsp;' . Yii::t('phycom/modules/delivery', 'Add new delivery address'),
                ArrayHelper::merge($defaultOptions, $options)
            );

            return $btn;
        }
        return '';
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return \Phycom\Frontend\Widgets\ActiveField
     */
    protected function renderDeliveryDatePicker(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        return $this->form->field($model, 'deliveryDate')->datePicker(ArrayHelper::merge([
            'inline' => true,
            'clientOptions' => [
                'minDate'     => (new \DateTime('today'))->format('Y-m-d'),
                'defaultDate' => (new \DateTime('today'))->format('Y-m-d')
            ]
        ], $options));
    }

    /**
     * @param ShippingForm $model
     * @param DeliveryMethodInterface|DeliveryMethod $deliveryMethod
     * @param array $options
     * @return \Phycom\Frontend\Widgets\ActiveField
     * @throws \Exception
     */
    protected function renderDeliveryTimePicker(ShippingForm $model, DeliveryMethodInterface $deliveryMethod, array $options = [])
    {
        /**
         * @var DeliveryMethod $deliveryMethod
         */
        if ($deliveryMethod->customDeliveryTime === 'options') {

            $options = [];
            if ($this->selectedTime) {
                $options['data-selected'] = $this->selectedTime;
            }

            return $this->form->field($model, 'deliveryTimeOption')->dropDownList([], $options);
        }

        if (!empty($deliveryMethod->deliveryTimeOptions)) {

            $items = ArrayHelper::map($deliveryMethod->deliveryTimeOptions, 'key', 'label');

            $options = [];
            if ($this->selectedTime) {
                $options['data-selected'] = $this->selectedTime;
            }

            return $this->form->field($model, 'deliveryTimeOption')->dropDownList($items, $options);

        } else {

            $dateTime = $model->deliveryDate ? new \DateTime($model->deliveryDate . ($model->deliveryTime ? ' ' . $model->deliveryTime . ':00' : ' 00:00:00')) : null;
            $date = $dateTime ? $dateTime->format('Y-m-d H:i:s') : null;

            $roundToQuarterHour = function ($timeString) {
                $minutes = date('i', strtotime($timeString));
                return $minutes + ($minutes % 15);
            };
            $defaultDate = new \DateTime();
            $minutes = $roundToQuarterHour($defaultDate->format('Y-m-d H:i:s'));
            $defaultDate->setTime((int)$defaultDate->format('H'), $minutes);
            $defaultDate->add(new \DateInterval('PT15M'));

            return $this->form->field($model, 'deliveryTime')->timePicker(ArrayHelper::merge([
                'inline' => true,
                'clientOptions' => [
                    'stepping'    => 15,
                    'date'        => $date,
                    'defaultDate' => $defaultDate->format('Y-m-d H:i:s'),
                    'useCurrent'  => false
                ]
            ], $options));
        }
    }

}
