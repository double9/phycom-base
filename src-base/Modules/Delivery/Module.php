<?php
namespace Phycom\Base\Modules\Delivery;

use Phycom\Base\Interfaces\CommerceComponentInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Models\Address;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\Settings;

use Phycom\Base\Interfaces\ModuleSettingsInterface;
use Phycom\Base\Models\Traits\ModuleSettingsTrait;

use Phycom\Base\Modules\BaseModule;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Module
 *
 * @package Phycom\Base\Modules\Delivery
 * @property DeliveryMethodInterface[] $methods - enabled carrier modules
 * @property DeliveryMethodInterface[] $allMethods - all carrier modules
 * @property-read array $shippingBoxConfig
 */
class Module extends BaseModule implements ModuleSettingsInterface, CommerceComponentInterface
{
    use ModuleSettingsTrait;

	const METHOD_SELF_PICKUP = 'self-pickup';
	const METHOD_COURIER = 'courier';


	const NON_DELIVERY_ABANDON = 'ABANDON';
	const NON_DELIVERY_RETURN = 'RETURN';

    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Base\Modules\Delivery\Controllers';
    /**
     * @var bool
     */
	public bool $enabled = true;
    /**
     * @var string
     */
	public $insuranceContent = 'merchandise';
    /**
     * @var string
     */
	public $customsDescription;
    /**
     * Name of the person who created the customs declaration and is responsible for the validity of all information provided.
     * @var string
     */
	public $customsSigner;
    /**
     * Indicates how the carrier should proceed in case the shipment can't be delivered.
     * @var string
     */
	public $nonDeliveryOption = self::NON_DELIVERY_RETURN;
    /**
     * Explanation of the type of goods of the shipment.
     * @var string
     */
	public $contentsExplanation;
    /**
     * @var string - ISO 3166-1-alpha-2 code (ISO 2 country code) - Country of origin of the items shipped
     */
	public $originCountry;

    /**
     * @var callable
     */
	public $createDeliveryAreaTitle;

    /**
     * @var callable
     */
	public $createDeliveryAddressTitle;

    /**
     * @var string[]
     */
	public array $nonDeliveryDays = [];

    /**
     * @var int
     */
	public int $firstPickupDayOffset = 0;

    /**
     * @var int
     */
	public int $preOrderMaxDays = 30;

    /**
     * @throws InvalidConfigException
     */
	public function init()
	{
        $this->initSettings();
		parent::init();
		$subModules = [
			self::METHOD_SELF_PICKUP => [
				'class' => Methods\SelfPickup\Module::class,
                'useSettings' => true,
                'enabled' => false,
			],
			self::METHOD_COURIER     => [
				'class' => Methods\Courier\Module::class,
                'useSettings' => true,
                'enabled' => false,
			]
		];

		foreach ($this->modules as $id => $moduleParams) {
            if (is_string($moduleParams)) {
                $moduleParams = ['class' => $moduleParams];
            }
            $params = isset($subModules[$id]) ? ArrayHelper::merge($subModules[$id], $moduleParams) : $moduleParams;
            $this->setModule($id, $params);
        }

		if (!$this->createDeliveryAddressTitle) {
		    $this->createDeliveryAddressTitle = function (Address $deliveryAddress, int $price) {
		        return Yii::$app->formatter->asAddress($deliveryAddress);
            };
        }

		if (!$this->createDeliveryAreaTitle) {
		    $this->createDeliveryAreaTitle = function (DeliveryArea $deliveryArea, int $price) {
		        return $deliveryArea->name;
            };
        }
	}

	public function isEnabled(): bool
    {
        return Yii::$app->commerce->isEnabled() && $this->enabled;
    }

    public function getName(): string
    {
        return 'delivery';
    }

    public function getLabel(): string
    {
        return Yii::t('phycom/modules/delivery', 'Delivery');
    }

    public function getJsonSchema(): array
    {
        return [];
    }

    public function getAllSettings(): array
    {
        $modules = [$this];
        foreach ($this->modules as $id => $moduleParams) {
            $modules[] = $this->getModule($id);
        }
        return $modules;
    }

    public function getBackendSettingsView(): ?string
    {
        return 'delivery-methods';
    }

    /**
     * @return array|callable|string
     */
    public function getSettingsForm()
    {
        return Settings::class;
    }

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getMethods(): array
	{
		$enabled = [];

		foreach ($this->modules as $id => $moduleParams) {
			$module = $this->getModule($id);
			/**
			 * @var DeliveryMethodInterface $module
			 */
			if ($module->getIsEnabled()) {
				$enabled[] = $module;
			}
		}
		return $enabled;
	}

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getAllMethods(): array
	{
		$couriers = [];
		foreach ($this->modules as $id => $moduleParams) {
			$couriers[] = $this->getModule($id);
		}
		return $couriers;
	}

    /**
     * @param string $id
     * @return null|BaseModule|DeliveryMethod|DeliveryMethodInterface
     */
	public function getDeliveryMethod($id): ?DeliveryMethodInterface
    {
        return $this->getModule($id);
    }

    /**
     * @param array $config
     * @return array
     * @throws InvalidConfigException
     */
    public function getShippingBoxConfig(array $config = []): array
    {
//        if (Yii::$app->session->get('order')) {
//            $order = Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order'));
//            if ($order && $order->shipment) {
//                $config['shipment'] = $order->shipment;
//            }
//        }

        $deliveryMethodId = Yii::$app->session->get('delivery_method');
        $deliveryAddress = Yii::$app->session->get('delivery_address');
        $deliveryAreaCode = Yii::$app->session->get('delivery_area');
        $deliveryDate = Yii::$app->session->get('delivery_date');
        $deliveryTime = Yii::$app->session->get('delivery_time');

        if ($deliveryMethodId) {
            $config['selectedMethod'] = $deliveryMethodId;
        }

        if ($deliveryAddress && $deliveryMethodId === static::METHOD_COURIER) {
            $config['selectedAddress'] = Address::create($deliveryAddress);
        }

        if ($deliveryAreaCode && $deliveryMethodId === static::METHOD_SELF_PICKUP) {
            $config['selectedArea'] = (string) $deliveryAreaCode;
        }

        if ($deliveryDate) {

            if (
                ($dateTime = new \DateTime($deliveryDate))
                && $dateTime->format('Ymd') >= (new \DateTime('today'))->format('Ymd')
            ) {
                $config['selectedDate'] = (string)$deliveryDate;
            } else {
                Yii::$app->session->remove('delivery_date');
                Yii::$app->session->remove('delivery_time');
                $deliveryTime = null;
            }
        }

        if ($deliveryTime) {
            $config['selectedTime'] = (string) $deliveryTime;
        }

        return $config;
    }
}
