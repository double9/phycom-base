<?php

namespace Phycom\Base\Modules\Delivery\Interfaces;

use Phycom\Base\Modules\Delivery\Models\Address;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryType;
use Phycom\Base\Modules\Delivery\Models\AddressForm;
use Phycom\Base\Modules\Delivery\Models\PostageLabel;
use Phycom\Base\Modules\Delivery\Models\Shipment;

/**
 * Interface CourierInterface
 * @package Phycom\Base\Modules\Delivery\Interfaces
 */
interface DeliveryMethodInterface
{
	/**
	 * @return bool
	 */
	public function getIsEnabled();
	/**
	 * Unique delivery provider id
	 * @return string
	 */
	public function getId();

    /**
     * code shown at cart line
     *
     * @param $defaultCode
     * @return mixed
     */
    public function getCode($defaultCode);

	/**
	 * Delivery provider display name
	 * @return string
	 */
	public function getName();

	/**
	 * Delivery provider short name
	 * @return string
	 */
	public function getLabel();

	/**
	 * @param DeliveryArea $area
	 * @return mixed
	 */
	public function getPrice(DeliveryArea $area);

	/**
	 * Returns list of delivery areas supported by the courier
     * @param string|array|null $countries
	 * @return DeliveryArea[]
	 */
	public function getAreas($countries = null);

	/**
	 * @return DeliveryType
	 */
	public function getType();

    /**
     * @return bool
     */
	public function hasDeliveryAddress();

    /**
     * @return AddressForm
     */
    public function getAddressFormModel();

    /**
     * @param array $params
     * @return string
     */
	public function renderAddressForm(array $params = []);

    /**
     * @return Shipment - string Shipment model class name
     */
    public function getShipmentClassName();

    /**
     * @return bool
     */
    public function hasPostageLabel(): bool;

    /**
     * @param array $params
     * @return LabelStrategyInterface
     */
    public function invokeLabelStrategy(array $params = []): LabelStrategyInterface;


    /**
     * @param array $items
     * @param Address $address
     * @param string $areaCode
     * @param mixed $deliveryDate
     * @param mixed $deliveryTime
     *
     * @return Shipment
     */
    public function createShipment(array $items, Address $address = null, string $areaCode = null, $deliveryDate = null, $deliveryTime = null);

    /**
     * Returns human readable carrier service name
     * @param $service - carrier service token
     * @return string
     */
    public function getServiceLabel($service);
}
