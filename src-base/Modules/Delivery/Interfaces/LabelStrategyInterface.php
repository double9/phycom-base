<?php

namespace Phycom\Base\Modules\Delivery\Interfaces;

use Phycom\Base\Models\Shipment;
use Phycom\Base\Modules\Delivery\Models\Label\PostageLabel;

/**
 * Interface LabelStrategyInterface
 *
 * @package Phycom\Base\Modules\Delivery\Interfaces
 */
interface LabelStrategyInterface
{
    public function createLabel(Shipment $shipment): ?PostageLabel;
}
