<?php

namespace Phycom\Base\Modules\Delivery\Interfaces;

use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

/**
 * Interface PriceStrategyInterface
 *
 * @package Phycom\Base\Modules\Delivery\Interfaces
 */
interface PriceStrategyInterface
{
    public function calculatePrice(DeliveryArea $area): int;
}
