<?php

namespace Phycom\Base\Modules\Delivery;

use Phycom\Base\Modules\Delivery\Helpers\Log;
use yii\base\Exception;

/**
 * Class DeliveryException
 * @package Phycom\Base\Modules\Delivery
 */
class DeliveryException extends Exception
{
	public function __construct($message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		Log::error($this->getMessage());
	}
}
