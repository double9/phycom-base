<?php

namespace Phycom\Base\Modules\Delivery\Methods\Courier;

use Phycom\Base\Helpers\Currency;
use Phycom\Base\Assets\Md\MD;
use Phycom\Base\Modules\Delivery\Methods\Courier\Carriers\Carrier;
use Phycom\Base\Modules\Delivery\Methods\Courier\Interfaces\CarrierInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\DeliveryException;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\Courier\Models\ServiceLevel;
use Phycom\Base\Modules\Delivery\Methods\Courier\Models\Shipment;
use Phycom\Base\Modules\Delivery\Models\AddressForm;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryType;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii;

/**
 * Class Module
 * @package Phycom\Base\Modules\Delivery\Methods\Courier
 */
class Module extends DeliveryMethod implements DeliveryMethodInterface
{
    public $defaultService = 'default';

	private $type;

	public function init()
	{
        $carrier = $this->getCarrier();

        /**
         * Overload some params from carrier
         */
        if (!empty($carrier->deliveryTimeOptions)) {
            $this->deliveryTimeOptions = $carrier->deliveryTimeOptions;
        }
		parent::init();
		$this->type = DeliveryType::create(DeliveryType::DELIVERY);
	}

    public function getIsMultiCarrier()
    {
        return true;
    }

    public function getIsEnabled()
    {
        return $this->enabled && !empty($this->getAreas());
    }

	public function getName()
	{
        return Yii::t('phycom/modules/delivery', 'Courier');
	}

	public function getLabel()
	{
        return MD::icon(MD::_LOCAL_TAXI, ['style' =>'top: 4px;'])->size(MD::SIZE_2X) . '&nbsp;&nbsp;' . '<span style="position: relative; top: -4px;">' . $this->getName() . '</span>';
	}

    /**
     * @return CarrierInterface[]|Carrier[]
     */
    public function getCarriers()
    {
        $enabled = [];
        foreach ($this->modules as $id => $moduleParams) {
            $module = $this->getModule($id);
            /**
             * @var Carrier $module
             */
            if ($module->enabled) {
                $enabled[] = $module;
            }
        }
        ArrayHelper::multisort($enabled, 'preference');
        return $enabled;
    }

    /**
     * @return null|yii\base\Module|Carrier|CarrierInterface
     */
    public function getCarrier()
    {
        return $this->getCarriers()[0] ?? null;
    }

    /**
     * @param null $countries
     * @return array|DeliveryArea[]|yii\db\ActiveRecord[]
     */
	public function getAreas($countries = null)
	{
	    $carriers = $this->getCarriers();
	    if (empty($carriers)) {
	        return [];
        }

        $ordering = [];
	    foreach ($carriers as $carrier) {
            $ordering[] = ['key' => $carrier->id, 'ordering' => $carrier->preference];
        }

        $query = DeliveryArea::find()
            ->select('DISTINCT ON (a.area_code) a.*')
            ->alias('a')
            ->where(['a.method' => $this->id])
            ->andWhere(['a.carrier' => ArrayHelper::getColumn($carriers, 'id')])
            ->innerJoin('json_to_recordset(:ordering) AS x(key text, ordering int)', 'a.carrier = x.key', ['ordering' => Json::encode($ordering)])
            ->orderBy(['a.area_code' => SORT_DESC, 'x.ordering' => SORT_ASC]);

		if ($countries) {
		    $query->andWhere(['(area #>> \'{country}\') :: VARCHAR' => $countries]);
        }
        return $query->all();
	}

    public function getShipmentClassName()
    {
        return Shipment::class;
    }

	public function getPrice(DeliveryArea $area)
	{
		return Currency::toInteger($area->price);
	}

	public function getType()
	{
		return $this->type;
	}

    public function hasDeliveryAddress()
    {
        return true;
    }

    /**
     * @return object|AddressForm
     * @throws yii\base\InvalidConfigException
     */
    public function getAddressFormModel()
    {
        /**
         * @var AddressForm|object $model
         */
        $model = Yii::createObject(AddressForm::class, [$this]);
        return $model;
    }

    public function renderAddressForm(array $params = [])
    {
        if (isset($params['model']) && !$params['model'] instanceof AddressForm) {
            throw new DeliveryException('Invalid delivery address model ' . get_class($params['model']));
        } else {
            $params['model'] = $this->getAddressFormModel();
        }
        $addressFormViewPath = $this->addressFormViewPath ?: 'address-form';
        return Yii::$app->view->render($addressFormViewPath, $params, $this);
    }

    public function getServiceLabel($service)
    {
        if ($service = ServiceLevel::exists($service)) {
            return $service->label;
        }
        return parent::getServiceLabel($service);
    }
}
