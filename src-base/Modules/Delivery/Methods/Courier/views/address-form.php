<?php
/**
 * @var $this yii\web\View
 * @var $model \Phycom\Base\Modules\Delivery\Models\AddressForm
 */

use Phycom\Base\Modules\Delivery\Methods\Courier\Assets\AddressFormAsset;

use yii\helpers\Html;
use yii\helpers\Json;

$targetId = '#shippingform-addressid';
$formId = '#delivery-address-form_' . $model->deliveryMethod->getId();
$modalId = '#delivery-address-modal_' . $model->deliveryMethod->getId();

$jsonParams = Json::encode([
    'formId' => $formId,
    'modalId' => $modalId,
    'targetId' => $targetId
]);

AddressFormAsset::register($this);
$this->registerJs(
    "var courierAddressFormParams = ". $jsonParams .";",
    yii\web\View::POS_HEAD,
    'delivery-address-form-script'
);

$submitBtn = $submitBtn ?? true;

/**
 * @var \Phycom\Frontend\Widgets\ActiveForm $ActiveForm
 */
$ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');
$form = $ActiveForm::begin([
    'id' => substr($formId, 1),
    'action' => ['/delivery/checkout/add-address', 'id' => $model->deliveryMethod->getId()]
]);

?>

    <div class="address-form-messages"></div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'street')->textInput(); ?>
            <?= $form->field($model, 'postcode')->textInput(); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'area')->dropDownList($model->areas); ?>
        </div>
    </div>

    <?php if ($submitBtn): ?>
    <div class="row">
        <div class="col-md-12">
            <?= Html::button(Yii::t('phycom/modules/delivery', 'Submit'), ['class' => 'btn btn-flat btn-primary submit']); ?>
        </div>
    </div>
    <?php endif; ?>

<?php $ActiveForm::end(); ?>
