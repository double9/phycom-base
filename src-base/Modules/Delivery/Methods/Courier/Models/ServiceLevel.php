<?php

namespace Phycom\Base\Modules\Delivery\Methods\Courier\Models;


use Phycom\Base\Modules\Delivery\Models\Token;
use yii;

/**
 * Class ServiceLevel
 * @package Phycom\Base\Modules\Delivery\Methods\Courier\Models
 */
class ServiceLevel extends Token
{
    protected function getTokens()
    {
        return [
            'default' => Yii::t('phycom/modules/delivery', 'Default courier service'),
        ];
    }
}
