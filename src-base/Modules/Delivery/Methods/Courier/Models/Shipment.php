<?php

namespace Phycom\Base\Modules\Delivery\Methods\Courier\Models;

use Phycom\Base\Modules\Delivery\Methods\Courier\Module;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryRate;

use yii\base\InvalidConfigException;
use Yii;

/**
 * Class Shipment
 * @package Phycom\Base\Modules\Delivery\Methods\Courier\Models
 *
 * @property Module $method
 */
class Shipment extends \Phycom\Base\Modules\Delivery\Models\Shipment
{
    private $rates = [];

    public function build($deliveryService = null)
    {
        $this->selectRateByService($deliveryService);
        return $this;
    }

    public function getRates()
    {
        if (empty($this->rates)) {
            $deliveryAreas = $this->toAddress->findAreas($this->method);
            if (empty($deliveryAreas)) {
                $this->rates = [$this->getDefaultRate()];
            } else {
                $this->populateRates($deliveryAreas);
            }
        }
        return $this->rates;
    }

    /**
     * @param DeliveryArea[]|array $ratesData
     * @throws InvalidConfigException
     */
    protected function populateRates(array $ratesData)
    {
        $rates = [];
        foreach ($ratesData as $area) {

            // use only predefined service levels when serviceLevels param. is set
            if (is_array($this->method->serviceLevels) && !in_array($area->service, $this->method->serviceLevels)) {
                continue;
            }

            $service = ServiceLevel::exists($area->service);

            $config = [
                'class'         => DeliveryRate::class,
                'referenceId'   => (string)$area->id,
                'referenceCode' => $area->code,
                'carrier'       => $area->carrier,
                'service'       => $service ? $service->token : null,
                'serviceLabel'  => $service ? $service->label : null,
                'attributes'    => [DeliveryRate::A_BEST_VALUE],
                'amount'        => $this->method->invokePriceStrategy([$this->contents])->calculatePrice($area),
                'zone'          => $area->area_code,
                'messages'      => [],
                'createdAt'     => new \DateTime()
            ];

            $rates[] = Yii::createObject($config, [$this]);
        }
        $this->rates = $rates;
    }
}
