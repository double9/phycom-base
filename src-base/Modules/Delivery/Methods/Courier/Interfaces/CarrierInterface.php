<?php

namespace Phycom\Base\Modules\Delivery\Methods\Courier\Interfaces;


use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

/**
 * Interface CarrierInterface
 * @package Phycom\Base\Modules\Delivery\Methods\Courier\Interfaces
 */
interface CarrierInterface
{
    /**
     * @return string
     */
    public function getLabel();
}
