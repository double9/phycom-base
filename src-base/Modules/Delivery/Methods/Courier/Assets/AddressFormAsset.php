<?php

namespace Phycom\Base\Modules\Delivery\Methods\Courier\Assets;

use Phycom\Frontend\Assets\ButtonHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class AddressFormAsset
 * @package Phycom\Base\Modules\Delivery\Methods\Courier\Assets
 */
class AddressFormAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Base/Modules/Delivery/Methods/Courier/Assets/address-form';
//    public $css = [
//        'main.css'
//    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        ButtonHelperAsset::class
    ];
}
