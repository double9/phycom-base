<?php

namespace Phycom\Base\Modules\Delivery\Methods\SelfPickup;

use Phycom\Base\Assets\Md\MD;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\SelfPickup\Models\ServiceLevel;
use Phycom\Base\Modules\Delivery\Methods\SelfPickup\Models\Shipment;
use Phycom\Base\Modules\Delivery\Models\AddressForm;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;
use Phycom\Base\Modules\Delivery\Models\DeliveryType;

use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Module
 * @package Phycom\Base\Modules\Delivery\Methods\SelfPickup
 */
class Module extends DeliveryMethod implements DeliveryMethodInterface
{
    /**
     * @var string
     */
    public $defaultService = 'default';

    /**
     * @var bool
     */
    public $postageLabel = false;

    /**
     * @var bool - if this is false delivery widget show prompt text instead of selecting first area
     */
    public bool $selectFirstAreaByDefault = true;

    /**
     * @var string|null
     */
    public ?string $defaultPromptText = null;

    /**
     * @var DeliveryType
     */
	private $type;

	public function init()
	{
		parent::init();
		$this->type = DeliveryType::create(DeliveryType::DROP_OFF);

		if (null === $this->defaultPromptText) {
		    $this->defaultPromptText = Yii::t('phycom/modules/delivery', 'Select pickup location');
        }
	}

	public function getName()
	{
        return Yii::t('phycom/modules/delivery', 'Self pickup');
	}

	public function getLabel()
	{
        return MD::icon(MD::_DIRECTIONS_WALK, ['style' =>'top: 4px;'])->size(MD::SIZE_2X) . '&nbsp;&nbsp;' . '<span style="position: relative; top: -4px;">' . $this->getName() . '</span>';
	}

	public function getType()
	{
		return $this->type;
	}

	public function getAreas($countries = null)
	{
	    $deliveryAreas = DeliveryArea::find()->where(['method' => $this->id])->andWhere(['not', ['status' => [DeliveryAreaStatus::DELETED]]])->all();
		ArrayHelper::multisort($deliveryAreas, function ($item) {
            /**
             * @var DeliveryArea $item
             */
            $shop = $item->getShop();
            return $shop ? $shop->order : INF;
        });

		return $deliveryAreas;
	}

    public function getShipmentClassName()
    {
        return Shipment::class;
    }

	public function getPrice(DeliveryArea $area)
	{
		return Currency::toInteger($area->price);
	}

    public function hasDeliveryAddress()
    {
        return false;
    }

    /**
     * @return object|AddressForm
     * @throws yii\base\InvalidConfigException
     */
    public function getAddressFormModel()
    {
        return Yii::createObject(AddressForm::class, [$this]);
    }

    public function renderAddressForm(array $params = [])
    {
        return '';
    }

    public function getServiceLabel($service)
    {
        if ($service = ServiceLevel::exists($service)) {
            return $service->label;
        }
        return parent::getServiceLabel($service);
    }
}
