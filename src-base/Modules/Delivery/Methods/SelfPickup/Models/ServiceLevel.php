<?php

namespace Phycom\Base\Modules\Delivery\Methods\SelfPickup\Models;


use Phycom\Base\Modules\Delivery\Models\Token;
use yii;

/**
 * Class ServiceLevel
 * @package Phycom\Base\Modules\Delivery\Methods\SelfPickup\Models
 */
class ServiceLevel extends Token
{
    protected function getTokens()
    {
        return [
            'default' => Yii::t('phycom/modules/delivery', 'Self pickup'),
        ];
    }
}
