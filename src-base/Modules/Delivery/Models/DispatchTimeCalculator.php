<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Models\Shop;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;

use yii\base\BaseObject;
use yii;

/**
 * Class DispatchDateCalculator
 * @package Phycom\Base\Modules\Delivery\Models
 */
abstract class DispatchTimeCalculator extends BaseObject
{
    /**
     * @var \DateTimeInterface
     */
    public \DateTimeInterface $orderConfirmedAt;

    /**
     * @var Shop|null
     */
    protected $shop;

    /**
     * @var DeliveryMethod
     */
    protected $deliveryMethod;

    /**
     * @var DeliveryArea
     */
    protected $deliveryArea;

    /**
     * @var int
     */
    protected $orderProcessingTime;


    /**
     * DispatchTimeCalculator constructor.
     *
     * @param DeliveryMethod $deliveryMethod
     * @param Shop|null $shop
     * @param int $orderProcessingTime
     * @param array $config
     */
    public function __construct(DeliveryMethod $deliveryMethod, Shop $shop = null, int $orderProcessingTime = 0, array $config = [])
    {
        $this->deliveryMethod = $deliveryMethod;
        $this->shop = $shop ?: Yii::$app->vendor->shop;
        $this->orderProcessingTime = $orderProcessingTime;

        if (!isset($this->orderConfirmedAt)) {
            $this->orderConfirmedAt = new \DateTime();
        }

        parent::__construct($config);
    }

    public function setDeliveryArea(DeliveryArea $deliveryArea)
    {
        $this->deliveryArea = $deliveryArea;
    }

    /**
     * @return \DateTimeInterface|null - First available time when order is ready for dispatch
     */
    abstract public function calculateDispatchTime(): ?\DateTimeInterface;

    /**
     * @return \DateTimeInterface|null - An estimated (quickest) time of arrival considering the order is shipped as soon as it is ready to dispatch
     */
    abstract public function calculateEta(): ?\DateTimeInterface;

    /**
     * @param \DateInterval|null $interval
     * @return \DateTimeInterface[] - Estimated available delivery/arrival dates within selected time-frame. If no dates are found then ETA is returned even if it is outside of the time-frame
     */
    abstract public function calculateDeliveryDates(\DateInterval $interval = null): array;

    /**
     * Calculates possible delivery times for a specific delivery date
     *
     * @param \DateTimeInterface $deliveryDate
     * @return array
     */
    abstract public function calculateDeliveryTimes(\DateTimeInterface $deliveryDate): array;
}
