<?php

namespace Phycom\Base\Modules\Delivery\Models;


use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\BaseObject;
use Yii;

/**
 * Class DeliveryTimeOption
 * @package Phycom\Base\Modules\Delivery\Models
 *
 * @property-read string $key
 * @property-read string $label
 */
class DeliveryTimeOption extends BaseObject
{
    const DELIMITER = '-';

    public ?string $from;

    public ?string $to;

    /**
     * @param string $key
     * @return static|object
     * @throws InvalidConfigException
     */
    public static function create(string $key)
    {
        $times = explode(self::DELIMITER, $key);

        if (count($times) !== 2) {
            throw new InvalidArgumentException('Invalid ' . __CLASS__ . ' definition ' . $key);
        }

        return Yii::createObject([
            'class' => static::class,
            'from'  => $times[0],
            'to'    => $times[1]
        ]);
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->from . self::DELIMITER . $this->to;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return Yii::t('phycom/modules/delivery', '{from} to {to}', ['from' => $this->from, 'to' => $this->to]);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getKey();
    }
}
