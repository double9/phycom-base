<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Models\Attributes\EnumAttribute;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class DeliveryAreaStatus
 * @package Phycom\Base\Modules\Delivery\Models
 */
class DeliveryAreaStatus extends EnumAttribute
{
	const ACTIVE = 'active';
	const DISABLED = 'disabled';
	const DELETED = 'deleted';

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            self::ACTIVE   => Yii::t('phycom/modules/delivery', 'Active'),
            self::DISABLED => Yii::t('phycom/modules/delivery', 'Disabled')
        ]);
    }
}
