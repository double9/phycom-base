<?php

namespace Phycom\Base\Modules\Delivery\Models\Pricing;


use Phycom\Base\Helpers\Currency;
use Phycom\Base\Modules\Delivery\Interfaces\PriceStrategyInterface;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

/**
 * This strategy allows free of charge shipping when order is over foc_threshold_amount
 *
 * Class FocPriceStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Pricing
 */
class FocPriceStrategy extends PriceStrategy implements PriceStrategyInterface
{
    /**
     * @param DeliveryArea $area
     * @return int
     */
    public function calculatePrice(DeliveryArea $area): int
    {
        if ($area->foc_threshold_amount) {
            $totalPurchaseAmount = $this->calculateTotalPurchaseAmount();
            if ($totalPurchaseAmount >= $area->foc_threshold_amount) {
                return 0;
            }
        }
        return $area->price;
    }

    /**
     * Calculates total purchase amount
     * @return int
     */
    protected function calculateTotalPurchaseAmount() : int
    {
        $amount = 0;
        foreach ($this->orderItems as $item) {
            if ($item->product) {
                $amount += $item->getTotalPrice();
            }
        }
        return $amount;
    }
}
