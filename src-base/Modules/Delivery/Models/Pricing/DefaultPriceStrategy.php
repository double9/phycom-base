<?php

namespace Phycom\Base\Modules\Delivery\Models\Pricing;


use Phycom\Base\Modules\Delivery\Interfaces\PriceStrategyInterface;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;

/**
 * Class DefaultPriceStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Pricing
 */
class DefaultPriceStrategy extends PriceStrategy implements PriceStrategyInterface
{
    public function calculatePrice(DeliveryArea $area): int
    {
        return $area->price;
    }
}
