<?php

namespace Phycom\Base\Modules\Delivery\Models\Pricing;

use Phycom\Base\Interfaces\CartItemProductInterface;

use yii\base\BaseObject;

/**
 * Class PriceStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Pricing
 */
abstract class PriceStrategy extends BaseObject
{
    /**
     * @var CartItemProductInterface[]|array
     */
    protected $orderItems = [];

    /**
     * PriceStrategy constructor.
     *
     * @param CartItemProductInterface[] $orderItems
     * @param array $config
     */
    public function __construct(array $orderItems, array $config = [])
    {
        $this->orderItems = $orderItems;
        parent::__construct($config);
    }
}
