<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Models\ModuleSettingsForm;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;

use yii;

/**
 * Class Settings
 * @package Phycom\Base\Modules\Delivery\Models
 *
 * @property-read DeliveryMethod|DeliveryMethodInterface $module
 */
class DeliveryMethodSettings extends ModuleSettingsForm
{
    public $enabled;

    public function rules()
    {
        return [
            ['enabled', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled' => Yii::t('phycom/modules/delivery', 'Enabled')
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled' => Yii::t('phycom/modules/delivery', 'Is delivery method enabled')
        ];
    }
}
