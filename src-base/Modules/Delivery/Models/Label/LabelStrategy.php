<?php

namespace Phycom\Base\Modules\Delivery\Models\Label;

use yii\base\BaseObject;

/**
 * Class LabelStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Label
 */
abstract class LabelStrategy extends BaseObject
{
    /**
     * @var array
     */
    protected $labelParams = [];

    public function __construct(array $params = [], $config = [])
    {
        $this->labelParams = $params;
        parent::__construct($config);
    }
}
