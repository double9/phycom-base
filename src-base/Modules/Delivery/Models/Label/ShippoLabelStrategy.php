<?php

namespace Phycom\Base\Modules\Delivery\Models\Label;

use Phycom\Base\Models\Shipment;
use Phycom\Base\Modules\Delivery\Interfaces\LabelStrategyInterface;

/**
 * Class ShippoLabelStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Label
 */
class ShippoLabelStrategy extends LabelStrategy implements LabelStrategyInterface
{

    public function createLabel(Shipment $shipment): ?PostageLabel
    {
        // TODO: move to shippo module and implement properly
        $label = new PostageLabel();

        $shipment->transaction_id = $label->referenceId;
        $shipment->tracking_number = $label->trackingNumber;
        $shipment->tracking_url = $label->trackingUrl;
        $shipment->tracking_status = $label->trackingStatus;
        $shipment->eta = $label->eta;
        $shipment->original_eta = $label->eta;
    }
}
