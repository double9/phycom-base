<?php

namespace Phycom\Base\Modules\Delivery\Models\Label;

use Phycom\Base\Models\Shipment;
use Phycom\Base\Modules\Delivery\Interfaces\LabelStrategyInterface;

/**
 * Class PdfLabelStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Label
 */
class NoLabelStrategy extends LabelStrategy implements LabelStrategyInterface
{

    public function createLabel(Shipment $shipment): ?PostageLabel
    {
        return null;
    }
}
