<?php

namespace Phycom\Base\Modules\Delivery\Models\Label;


use Phycom\Base\Components\FileStorage;
use Phycom\Base\Models\Shipment;
use Phycom\Base\Modules\Delivery\Interfaces\LabelStrategyInterface;

use Mpdf\Mpdf;

use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class PdfLabelStrategy
 *
 * @package Phycom\Base\Modules\Delivery\Models\Label
 */
class PdfLabelStrategy extends LabelStrategy implements LabelStrategyInterface
{
    public string $template = 'postage_label';

    public string $fileStorageBucket = FileStorage::BUCKET_POSTAGE_LABELS;

    public array $pdfRendererConfig = [];

    /**
     * @param Shipment $shipment
     * @return PostageLabel|object
     * @throws Exception
     * @throws InvalidConfigException
     * @throws \Mpdf\MpdfException
     */
    public function createLabel(Shipment $shipment): ?PostageLabel
    {
        $pdfFile = $this->createPdfFile($shipment);
        $bucket = Yii::$app->fileStorage->getBucket($this->fileStorageBucket);
        $createdAt = new \DateTime('now');

        $invoice = $shipment->order->invoice;

        $config = [
            'class'                => PostageLabel::class,
            'status'               => PostageLabel::STATUS_SUCCESS,
            'labelUrl'             => $bucket->getFileUrl($pdfFile),
            'referenceId'          => $shipment->order->number,
            'commercialInvoiceUrl' => $invoice->file ? Yii::$app->urlManagerFrontend->createAbsoluteUrl($invoice->getFileUrl()) : null,
            'createdAt'            => $createdAt->format('Y-m-dTH:i:s'),
            'updatedAt'            => $createdAt->format('Y-m-dTH:i:s'),
            'trackingNumber'       => $shipment->tracking_number,
            'originalEta'          => $shipment->delivery_date->format('Y-m-d') . ' ' . $shipment->delivery_time
        ];

        return Yii::createObject($config);
    }


    public function populateTemplateParams(Shipment $shipment): array
    {
        return [];
    }


    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function createBase64Logo(): string
    {
        $data = file_get_contents(Yii::$app->site->getFullLogoPath());
        $type = Yii::$app->site->getLogoMimeType();

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    /**
     * @param Shipment $shipment
     * @return string
     * @throws Exception
     * @throws InvalidConfigException
     * @throws \Mpdf\MpdfException
     */
    protected function createPdfFile(Shipment $shipment): string
    {
        $filename = md5($shipment->id) . '.pdf';

        if (!$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($this->template)) {
            throw new InvalidConfigException('Postage label template ' . $this->template . ' was not found');
        }

        $params = [
            'shipment' => $shipment,
            'logo'     => $this->createBase64Logo(),
            'params'   => ArrayHelper::merge($this->labelParams, $this->populateTemplateParams($shipment))
        ];

        $html = $template->render($params);

        $pdf = $this->createPdf($html);

        $bucket = Yii::$app->fileStorage->getBucket($this->fileStorageBucket);
        $bucket->saveFileContent($filename, $pdf);

        if (!$bucket->fileExists($filename)) {
            throw new Exception('Error saving postage label pdf ' . $filename);
        }

        return $filename;
    }



    /**
     * @param string $html
     * @return string
     * @throws \Mpdf\MpdfException|InvalidConfigException
     */
    protected function createPdf(string $html): string
    {
        $config = $this->pdfRendererConfig;

        if (!isset($config['tempDir'])) {
            $tempDir = Yii::getAlias('@files/mpdf');

            if (!is_dir($tempDir)) {
                mkdir($tempDir);
            }
            $config['tempDir'] = $tempDir;
        }

        /**
         * @var Mpdf|object $mPdf
         */
        $mPdf = Yii::createObject(Mpdf::class, [$config]);
        $mPdf->WriteHTML($html);

        return $mPdf->Output('', 'S');
    }
}
