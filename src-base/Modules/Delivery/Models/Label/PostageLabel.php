<?php

namespace Phycom\Base\Modules\Delivery\Models\Label;

use Phycom\Base\Modules\Delivery\Models\ShipmentMessage;

use Phycom\Base\Helpers\Date;

use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * Class PostageLabel
 * @package Phycom\Base\Modules\Delivery\Models\Label
 *
 * @property-read ShipmentMessage[] $messages
 */
class PostageLabel extends BaseObject
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public $status;
    public $transactionId;
    public $referenceId;
    public $labelUrl;
    public $commercialInvoiceUrl;
    public $metadata;
    public $createdAt;
    public $updatedAt;

    public $trackingNumber;
    public $trackingStatus;
    public $trackingUrl;
    public $eta;
    public $originalEta;

    protected $messages = [];

    public function addMessage($json)
    {
        if (is_string($json) && !empty($json)) {
            $this->messages[] = ShipmentMessage::create($json);
        }
    }

    /**
     * @return ShipmentMessage[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function export()
    {
        return [
            'url'                  => $this->labelUrl,
            'commercialInvoiceUrl' => $this->commercialInvoiceUrl,
            'metadata'             => $this->metadata,
            'createdAt'            => Date::create($this->createdAt)->dbTimestamp,
            'updatedAt'            => Date::create($this->updatedAt)->dbTimestamp
        ];
    }

    public function exportJson()
    {
        return Json::encode($this->export());
    }
}
