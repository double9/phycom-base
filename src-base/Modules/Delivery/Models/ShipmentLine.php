<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Interfaces\CartItemDeliveryInterface;

use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * Class ShipmentLine
 * @package Phycom\Base\Modules\Delivery\Models
 */
class ShipmentLine extends BaseObject implements CartItemDeliveryInterface
{
    const DEFAULT_CODE = 'SHIPPING';

    public $id;

    public $shipmentId;
    public $rateId;

    public $code = self::DEFAULT_CODE;
    public $label;
    public $price;

    public $method;
    public $carrier;
    public $service;

    public $dispatchAt;
    public $deliveryDays;
    public $deliveryDate;
    public $deliveryTime;
    public $arrivalTime;
    public $durationTerms;

    public $areaCode;
    public $fromAddress;
    public $toAddress;
    public $returnAddress;


    public function getUniqueId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getCarrier()
    {
        return $this->carrier;
    }

    public function getService()
    {
        return $this->service;
    }

    public function getDeliveryDays()
    {
        return $this->deliveryDays;
    }

    public function getArrivalTime()
    {
        return $this->arrivalTime;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    public function getDispatchAt()
    {
        return $this->dispatchAt;
    }

    public function getDurationTerms()
    {
        return $this->durationTerms;
    }

    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    public function getRateId()
    {
        return $this->rateId;
    }

    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    public function getToAddress()
    {
        $attributes = Json::decode($this->toAddress);
        if (array_key_exists('id', $attributes)) {
            unset($attributes['id']);
        }
        return Json::encode($attributes);
    }

    public function getToAddressId()
    {
        $attributes = Json::decode($this->toAddress);
        return $attributes['id'] ?? null;
    }

    public function getReturnAddress()
    {
        return $this->returnAddress;
    }

    public function getAreaCode()
    {
        return $this->areaCode;
    }
}
