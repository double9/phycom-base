<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Models\ShopOpen;

/**
 * Class DefaultDispatchTimeCalculator
 *
 * @package Phycom\Base\Modules\Delivery\Models
 */
class DefaultDispatchTimeCalculator extends DispatchTimeCalculator
{
    /**
     * @return \DateTimeInterface|null - First available time when order is ready for dispatch
     */
    public function calculateDispatchTime(): ?\DateTimeInterface
    {
        return null;
    }

    /**
     * @return \DateTimeInterface|null - An estimated (quickest) time of arrival considering the order is shipped as soon as it is ready to dispatch
     */
    public function calculateEta(): ?\DateTimeInterface
    {
        return null;
    }

    /**
     * @param \DateInterval|null $interval
     * @return \DateTimeInterface[] - Estimated available delivery/arrival dates within selected time-frame. If no dates are found then ETA is returned even if it is outside of the time-frame
     */
    public function calculateDeliveryDates(\DateInterval $interval = null): array
    {
        return [];
    }

    /**
     * Calculates possible delivery times for a specific delivery date
     *
     * @param \DateTimeInterface $deliveryDate
     * @return array
     * @throws \Exception
     */
    public function calculateDeliveryTimes(\DateTimeInterface $deliveryDate): array
    {
        if (!$shop = $this->deliveryArea->getShop()) {
            return ['from' => '00:00', 'to' => '00:00'];
        }

        $dayOfWeek = (int) $deliveryDate->format('N');

        if (!$shopOpen = ShopOpen::findOne(['shop_id' => $shop->id, 'day_of_week' => $dayOfWeek])) {
            return ['from' => '00:00', 'to' => '00:00'];
        }

        $from = clone $shopOpen->opened_at;
        $to = clone $shopOpen->closed_at;

        $method = $this->deliveryMethod;
        if ($method->lastDeliveryBeforeClosingTime) {
            $to->sub(new \DateInterval('PT' . $method->lastDeliveryBeforeClosingTime . 'M'));
            // set 15 min. as minimum open duration which cannot be reduced
            if ($to < $from) {
                $to = clone $from;
                $to->add(new \DateInterval('PT15M'));
            }
        }

        return ['from' => $from->format('H:i'), 'to' => $to->format('H:i')];
    }
}
