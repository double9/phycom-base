<?php

namespace Phycom\Base\Modules\Delivery\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\base\Model;
use Yii;

/**
 * Class ShippingForm
 * @package Phycom\Base\Modules\Delivery\Models
 *
 * @property-read DeliveryModule $module
 *
 * @property-read DeliveryMethodInterface[] $methods
 * @property-read DeliveryMethodInterface|DeliveryMethod $method
 *
 * @property-read Address $address
 * @property-read DeliveryArea $area
 *
 * @property-read Address[] $userAddresses
 * @property-read array $addressList
 */
class ShippingForm extends Model
{
	use ModelTrait;

	public $methodId;
	public $service;
	public $areaCode;
	public $addressId;
	public $insurance;
    public $deliveryDate;
    public $deliveryTime;
    public $deliveryTimeOption;

    /**
     * @var Address[]|null
     */
	private ?array $userAddresses = null;

	public function rules()
	{
		return [
			[['addressId', 'insurance'], 'safe'],
			[['service', 'methodId', 'areaCode', 'deliveryDate', 'deliveryTime', 'deliveryTimeOption'], 'string'],
			['areaCode', 'required', 'when' => function ($model) {
				return $model->addressId === null;
			}],
			['addressId', 'required', 'when' => function ($model) {
				return $model->areaCode === null;
			}]
		];
	}

	public function attributeLabels()
	{
        $labels = [
            'areaCode'           => Yii::t('phycom/modules/delivery', 'Shipping area'),
            'addressId'          => Yii::t('phycom/modules/delivery', 'Delivery address'),
            'insurance'          => Yii::t('phycom/modules/delivery', 'Insurance'),
            'deliveryDate'       => Yii::t('phycom/modules/delivery', 'Delivery date'),
            'deliveryTime'       => Yii::t('phycom/modules/delivery', 'Delivery time'),
            'deliveryTimeOption' => Yii::t('phycom/modules/delivery', 'Delivery time'),
        ];
        if ($this->methodId === DeliveryModule::METHOD_SELF_PICKUP) {
            $labels['addressId'] = Yii::t('phycom/modules/delivery', 'Pickup address');
            $labels['deliveryDate'] = Yii::t('phycom/modules/delivery', 'Pickup date');
            $labels['deliveryTime'] = Yii::t('phycom/modules/delivery', 'Pickup time');
            $labels['deliveryTimeOption'] = Yii::t('phycom/modules/delivery', 'Pickup time');
        }
		return $labels;
	}

    /**
     * @return array
     */
	public function getUserAddresses(): array
	{
	    if (Yii::$app->user->isGuest) {
            $this->userAddresses = [];
        } else if ($this->userAddresses === null) {
            /**
             * @var Address[] $addresses
             */
			$addresses = Address::find()
				->where(['user_id' => Yii::$app->user->id])
				->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]])
				->all();

                $filtered = [];
                foreach ($addresses as $address) {
                    if ($address->findArea($this->method)) {
                        $filtered[] = $address;
                    }
                };

			$this->userAddresses = $filtered;
		}
		return $this->userAddresses;
	}

    /**
     * @return array
     */
	public function getAddressList(): array
	{
		$addresses = [];
		foreach ($this->getUserAddresses() as $address) {
			$addresses[$address->id] = $address->label;
		}
		return $addresses;
	}

    /**
     * @return bool
     */
	public function hasAddress(): bool
    {
        return $this->addressId || $this->areaCode;
    }

    /**
     * @return null|Address
     * @throws InvalidConfigException
     */
	public function getAddress()
	{
		if (!$this->addressId) {
            $deliveryArea = $this->getArea();
		    $addressFields = $deliveryArea->area;
		    if ($deliveryArea && !isset($addressFields['name'])) {
                $addressFields['name'] = $deliveryArea->name;
            }
			return Address::create($addressFields);
		}
		if (is_numeric($this->addressId)) {
			return Address::findOne($this->addressId);
		}
		return Address::create($this->addressId);
	}

    /**
     * @return DeliveryArea|null
     * @throws InvalidConfigException
     */
	public function getArea()
	{
		if ($this->areaCode) {
			foreach ($this->method->getAreas() as $area) {
				if ($area->area_code === $this->areaCode) {
					return $area;
				}
			}
		} else if ($this->addressId) {
			$address = $this->getAddress();
			return $address->findArea($this->method);
		}
		return null;
	}

    /**
     * @return null|DeliveryMethod|DeliveryMethodInterface
     */
	public function getMethod()
	{
	    return $this->methodId ? $this->module->getModule($this->methodId) : null;
	}

	/**
	 * @return DeliveryMethod[]|DeliveryMethodInterface[]
	 */
	public function getMethods()
	{
		return $this->module->methods;
	}

	/**
	 * @return DeliveryModule|Module
	 */
	public function getModule()
	{
		return Yii::$app->getModule('delivery');
	}

    /**
     * @param array $items
     * @return Shipment
     * @throws InvalidConfigException
     */
    public function createShipment(array $items): Shipment
    {
        return $this->createShipmentInternal($items)->build($this->service);
    }

    /**
     * @param \Phycom\Base\Models\Shipment $shipment
     */
    public function loadShipmentData(\Phycom\Base\Models\Shipment $shipment)
    {
        if (!$this->service && $shipment->carrier_service) {
            $this->service = $shipment->carrier_service;
        }
        if (!$this->deliveryDate && $shipment->delivery_date) {
            $this->deliveryDate = $shipment->delivery_date->format('Y-m-d');
        }
        if ($shipment->delivery_time) {
            $this->populateDeliveryTime($shipment->delivery_time);
        }
    }

    /**
     * @param string $deliveryTime
     */
    public function populateDeliveryTime(string $deliveryTime)
    {
        $attribute = preg_match('/^\d{2}\:\d{2}\-\d{2}\:\d{2}/', $deliveryTime) ? 'deliveryTimeOption' : 'deliveryTime';
        $this->$attribute = $deliveryTime;
    }

    /**
     * @param array $productItems
     * @return bool
     * @throws InvalidConfigException
     */
    public function validateDeliveryDate(array $productItems)
    {
        $shipment = $this->createShipmentInternal($productItems);
        $calculator = $shipment->getDispatchTimeCalculator();
        $calculator->setDeliveryArea($this->getArea());
        $deliveryDates = $calculator->calculateDeliveryDates();

        $formattedDates = array_map(function (\DateTime $date) {
            return $date->format('Y-m-d');
        }, $deliveryDates);

        $isValid = !empty($formattedDates) && in_array($this->deliveryDate, $formattedDates);

        if (!$isValid) {
            $this->addError('deliveryDate', Yii::t('phycom/modules/delivery', 'Invalid delivery date'));
        }

        return $isValid;
    }

    /**
     * @param array $productItems
     * @return Shipment
     * @throws InvalidConfigException
     */
    protected function createShipmentInternal(array $productItems): Shipment
    {
        $deliveryDate = !empty($this->deliveryDate) ? $this->deliveryDate : null;

        if (!empty($this->deliveryTimeOption)) {
            $deliveryTime = DeliveryTimeOption::create($this->deliveryTimeOption);
        } elseif (!empty($this->deliveryTime)) {
            $deliveryTime = $this->deliveryTime;
        } else {
            $deliveryTime = null;
        }

        return $this->method->createShipment($productItems, $this->address, $this->areaCode, $deliveryDate, $deliveryTime);
    }
}
