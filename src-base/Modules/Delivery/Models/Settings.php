<?php

namespace Phycom\Base\Modules\Delivery\Models;


use Phycom\Base\Models\ModuleSettingsForm;
use Phycom\Base\Modules\Delivery\Module;

use Yii;

/**
 * Class Settings
 * @package Phycom\Base\Modules\Delivery\Models
 *
 * @property-read Module $module
 */
class Settings extends ModuleSettingsForm
{
    public $insuranceContent;
    public $nonDeliveryOption;
    public $customsSigner;
    public $customsDescription;
    public $contentsExplanation;
    public $originCountry;
    public $nonDeliveryDays;


    protected array $attributeTypes = [
        'nonDeliveryDays' => self::TYPE_ARRAY
    ];

    public function rules()
    {
        return [
            [['insuranceContent','customsDescription', 'customsSigner','contentsExplanation'], 'string'],
            ['originCountry', 'string', 'min' => 2, 'max' => 2],
            ['nonDeliveryOption', 'in', 'range' => [Module::NON_DELIVERY_ABANDON, Module::NON_DELIVERY_RETURN]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'insuranceContent'    => Yii::t('phycom/modules/delivery', 'Insured content'),
            'nonDeliveryOption'   => Yii::t('phycom/modules/delivery', 'Non delivery option'),
            'customsSigner'       => Yii::t('phycom/modules/delivery', 'Customs signer'),
            'customsDescription'  => Yii::t('phycom/modules/delivery', 'Item customs description'),
            'contentsExplanation' => Yii::t('phycom/modules/delivery', 'Contents explanation'),
            'originCountry'       => Yii::t('phycom/modules/delivery', 'Origin country'),
            'nonDeliveryDays'     => Yii::t('phycom/modules/delivery', 'Non delivery days'),
        ];
    }

    public function attributeHints()
    {
        return [
            'insuranceContent'    => Yii::t('phycom/modules/delivery', 'Content type insured'),
            'customsDescription'  => Yii::t('phycom/modules/delivery', 'Fallback description of parcel item for customs declaration'),
            'customsSigner'       => Yii::t('phycom/modules/delivery', 'Name of the person who created the customs declaration and is responsible for the validity of all information provided'),
            'nonDeliveryOption'   => Yii::t('phycom/modules/delivery', 'Indicates how the carrier should proceed in case the shipment can\'t be delivered'),
            'contentsExplanation' => Yii::t('phycom/modules/delivery', 'Explanation of the type of goods of the shipment for customs'),
            'originCountry'       => Yii::t('phycom/modules/delivery', 'Country of origin of the items shipped (ISO 2 country code)'),
        ];
    }
}
