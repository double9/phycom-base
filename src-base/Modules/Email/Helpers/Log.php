<?php

namespace Phycom\Base\Modules\Email\Helpers;

use yii;

/**
 * Class Log
 * @package Phycom\Base\Modules\Email\Helpers
 */
class Log
{
    const DEFAULT_CATEGORY = 'email';

    public static function info($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::info($msg, $category);
    }

    public static function trace($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::debug($msg, $category);
    }

    public static function warning($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::warning($msg, $category);
    }

    public static function error($msg, $category = self::DEFAULT_CATEGORY)
    {
        Yii::error($msg, $category);
    }
}
