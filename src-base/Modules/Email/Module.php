<?php
namespace Phycom\Base\Modules\Email;

use Phycom\Base\Models\Attributes\MessagePriority;
use Phycom\Base\Models\Attributes\MessageType;
use Phycom\Base\Models\Message;
use Phycom\Base\Modules\Email\Helpers\Log;

use Phycom\Base\Modules\BaseModule;

use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package Phycom\Base\Modules\Email
 */
class Module extends BaseModule
{
    const PROVIDER_MAILER = 'mailer';

    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Base\Modules\Email\Controllers';


    public $bcc;
    public $domain;
    public $testMode = false;
    public $maxFileSize = 83886080;

    /**
     * This maps the priorities and email services (sub-module keys). So we can define that n priority emails are sent using m service.
     * Keep in mind the priority should be unique otherwise the array key is overwritten.
     * Example:
     * ~~~
     *  [
     *      MessagePriority::PRIORITY_1 => 'mailer',
     *      MessagePriority::PRIORITY_2 => 'sendinblue',
     *  ]
     * ~~~
     * @var array
     */
    public array $priorityMap = [];
    /**
     * @var string - a default email provider (this is one of the sub-module id's in email module)
     */
    public ?string $defaultProvider = self::PROVIDER_MAILER;

    public function init()
    {
        parent::init();
        $subModules = [
            self::PROVIDER_MAILER => [
                'class' => Modules\Mailer\Module::class,
            ]
        ];
        foreach ($this->getModules() as $id => $moduleParams) {
            if (is_string($moduleParams)) {
                $moduleParams = ['class' => $moduleParams];
            }
            if (isset($subModules[$id])) {
                $moduleParams = ArrayHelper::merge($moduleParams, $subModules[$id]);
            }
            $this->setModule($id, $moduleParams);
        }
    }

    /**
     * @param Message $message
     * @param string|bool $customDomain
     * @param string|null $providerId - forces a specific provider
     * @return mixed
     * @throws EmailException
     */
    public function send(Message $message, $customDomain = false, $providerId = null)
    {
        $this->validateMessage($message);
        $module = $providerId ? $this->getModule($providerId) : $this->getModuleByPriority($message->priority);

        if ($customDomain && property_exists($module, 'domain')) {
            $module->domain = $customDomain;
        }

        $response = $module->send($message);
        Log::trace('Email message ' . $message->id . ' was sent');
        return $response;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $content
     * @param array $attachments
     * @param string $from
     * @param string $providerId
     *
     * @return bool|mixed|\stdClass
     * @throws EmailException
     */
    public function sendEmail($to, $subject, $content = '', $attachments = [], $from = null, $providerId = null)
    {
        /**
         * @var \Phycom\Base\Modules\Email\Modules\mailgun\Module|\Phycom\Base\Modules\Email\Modules\sendinblue\Module $module
         */
        $module = $this->getModule($providerId ?: $this->defaultProvider);
        return $module->sendEmail($to, $subject, $content, $attachments, $from);
    }

	/**
	 * @param MessagePriority $priority
	 * @return yii\base\Module $module
	 */
    protected function getModuleByPriority($priority)
    {
        if (YII_ENV === 'prod' && $priority && array_key_exists($priority->export(), $this->priorityMap)) {
            return $this->getModule($this->priorityMap[$priority->export()]);
        } else {
            return $this->getModule($this->defaultProvider);
        }
    }


    protected function validateMessage(Message $message)
    {
        if ($message->isNewRecord) {
            throw new EmailException('Message must be saved before it can be sent.');
        }
        if (!$message->type->isEmail) {
            throw new EmailException('Invalid message type ' . $message->type . '. Only type "' . MessageType::EMAIL . '" is allowed');
        }
        if (!$message->validate()) {
            Log::error($message->errors);
            throw new EmailException('Invalid message ' . $message->id);
        }
    }
}
