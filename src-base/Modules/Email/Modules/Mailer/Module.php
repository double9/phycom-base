<?php
namespace Phycom\Base\Modules\Email\Modules\Mailer;


use Phycom\Base\Models\Message;
use Phycom\Base\Modules\Email\EmailException;
use Phycom\Base\Modules\Email\Helpers\EmailTrait;
use Phycom\Base\Modules\Email\Helpers\Log;

use yii\mail\MailerInterface;
use yii\di\Instance;
use yii\base\Module as BaseModule;
use yii;

/**
 * Class Module
 * @package Phycom\Base\Modules\Email\Modules\Mailer
 *
 * @property-read \Phycom\Base\Modules\Email\Module $module
 * @property-read bool $isTestMode
 */
class Module extends BaseModule
{
    use EmailTrait;
    /**
     * @var MailerInterface|array|string
     */
    public $mailer;


    public function init()
    {
        parent::init();

        if (is_array($this->mailer) && $this->getIsTestMode()) {
            $this->mailer['useFileTransport'] = true;
        }

        $this->mailer = Instance::ensure($this->mailer, MailerInterface::class);
    }

    /**
     * @return bool
     */
	public function getIsTestMode()
	{
		return $this->module->testMode;
	}

    /**
     * @param Message $message
     * @return string - message id
     * @throws EmailException
     */
    public function send(Message $message)
    {
        $this->validateMessage($message);

        $email = $this->mailer->compose();
        $email->setFrom([$this->getSenderEmail($message) => $this->getSenderName($message)]);
        $email->setTo([$this->getRecipientEmail($message) => $this->getRecipientName($message)]);
        $email->setSubject($message->subject);
        $email->setHtmlBody(strlen($message->content) ? $message->content : '&nbsp;');

        Log::trace('Email composed: ' . $email->toString());

        foreach ($message->getAttachmentFiles() as $filename) {
            $email->attach($filename);
        }

        if (!$email->send()) {
            throw new EmailException('Error sending email');
        }
        return md5($email->toString());
    }

    /**
     * @param $to
     * @param $subject
     * @param string $content
     * @param array $attachments
     * @param null $from
     * @return string
     * @throws EmailException
     */
    public function sendEmail($to, $subject, $content = '', $attachments = [], $from = null)
    {
        $email = $this->mailer->compose();

        if (!$from) {
            $from = [(string)Yii::$app->systemUser->email  => Yii::$app->systemUser->fullName];
        }

        $email->setFrom($from);
        $email->setTo($to);
        $email->setSubject($subject);
        $email->setHtmlBody(strlen($content) ? $content : '&nbsp;');

        Log::trace('Email composed: ' . $email->toString());

        foreach ($attachments as $filename) {
            $email->attach($filename);
        }

        if (!$email->send()) {
            throw new EmailException('Error sending email');
        }
        return md5($email->toString());
    }

}
