<?php

namespace Phycom\Base\Modules;


use Phycom\Base\Interfaces\ModuleInterface;
use Phycom\Base\Interfaces\ModuleSettingsInterface;
use Phycom\Base\Interfaces\PhycomComponentInterface;

use yii\base\Module;
use yii\helpers\Inflector;

/**
 * Class BaseModule
 *
 * @package Phycom\Base\Modules
 */
abstract class BaseModule extends Module implements ModuleInterface
{
    /**
     * @return string
     */
    public function getLabel(): string
    {
        return Inflector::titleize($this->id);
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return null;
    }

    /**
     * @return ModuleSettingsInterface[]|Module[]
     */
    public function getAllSettings() : array
    {
        return [];
    }

    /**
     * @return string|null
     */
    public function getBackendSettingsView() : ?string
    {
        return null;
    }

    /**
     * @return PhycomComponentInterface|null
     */
    public function getComponent() : ?PhycomComponentInterface
    {
        return null;
    }
}
