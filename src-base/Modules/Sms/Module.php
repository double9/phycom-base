<?php

namespace Phycom\Base\Modules\Sms;

use Phycom\Base\Models\Attributes\MessageType;
use Phycom\Base\Models\Message;
use Phycom\Base\Models\Phone;

use Phycom\Base\Modules\BaseModule;

use Yii;

/**
 * Class Module
 * @package Phycom\Base\Modules\Sms
 */
class Module extends BaseModule
{
    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Base\Modules\Sms\Controllers';


    public bool $enabled = false;

    public bool $testMode = false;
    /**
     * @var string - a default sms provider (this is one of the sub-module id's in sms module)
     */
    public ?string $defaultProvider = null;

    /**
     * @param Message $message
     * @param string|null $providerId - forces a specific provider
     * @return mixed
     * @throws SmsException
     */
    public function sendMessage(Message $message, $providerId = null)
    {
        $this->validateMessage($message);
        return $this->send($message->content, $message->userTo->phone, $providerId);
    }

    /**
     * @param string $message
     * @param Phone $phone
     * @param string|null $providerId - forces a specific provider
     * @return mixed - provider message id, if available or boolean indicating if message was sent or not
     * @throws SmsException
     */
    public function send($message, Phone $phone, $providerId = null)
    {
        if (!$this->enabled) {
            Yii::warning(ucfirst($this->id) . ' module is disabled', __METHOD__);
            return false;
        }
        /**
         * @var SmsProviderInterface $provider
         */
        $provider = $providerId ? $this->getModule($providerId) : $this->getModule($this->defaultProvider);
        $id = $response = $provider->sendSms($message, $phone);

        Yii::debug('Sms message ' . $id . ' was sent', __METHOD__);
        return $response;
    }

    /**
     * @param Message $message
     * @throws SmsException
     */
    protected function validateMessage(Message $message)
    {
        if ($message->isNewRecord) {
            throw new SmsException('Message must be saved before it can be sent.');
        }
        if (!$message->type->isSms) {
            throw new SmsException('Invalid message type ' . $message->type . '. Only "' . MessageType::SMS . '" type is allowed');
        }
        if (!$message->validate()) {
            Yii::error($message->errors, __METHOD__);
            throw new SmsException('Invalid message ' . $message->id);
        }
    }
}
