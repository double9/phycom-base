<?php

namespace Phycom\Base\Modules\Sms;

use yii\base\Exception;

/**
 * SmsException is the base class for exceptions that are related to sms module
 */
class SmsException extends Exception
{

}
