<?php

namespace Phycom\Base\Modules\Sms;

use Phycom\Base\Models\Phone;

/**
 * Interface SmsProviderInterface
 *
 * @package Phycom\Base\Modules\Sms
 */
interface SmsProviderInterface
{
    /**
     * @param string $message
     * @param Phone $to
     * @param mixed|null $from
     *
     * @return bool|mixed
     * @throws SmsException
     */
    public function sendSms(string $message, Phone $to, $from = null);
}
