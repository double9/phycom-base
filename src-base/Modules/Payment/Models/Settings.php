<?php

namespace Phycom\Base\Modules\Payment\Models;


use Phycom\Base\Models\ModuleSettingsForm;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Methods\PaymentMethod;

use Yii;

/**
 * Class Settings
 * @package Phycom\Base\Modules\Payment\Models
 *
 * @property-read PaymentMethod|PaymentMethodInterface $module
 */
class Settings extends ModuleSettingsForm
{
    public $enabled;
    public $testMode;

    public function rules()
    {
        return [
            ['enabled', 'boolean'],
            ['testMode', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'enabled' => Yii::t('phycom/modules/payment', 'Enabled'),
            'testMode' => Yii::t('phycom/modules/payment', 'Test Mode'),
        ];
    }

    public function attributeHints()
    {
        return [
            'enabled' => Yii::t('phycom/modules/payment', 'Is payment method enabled'),
            'testMode' => Yii::t('phycom/modules/payment', 'Execute payments on test environment'),
        ];
    }
}
