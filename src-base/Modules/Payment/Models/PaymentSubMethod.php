<?php

namespace Phycom\Base\Modules\Payment\Models;

use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Interfaces\PaymentSubMethodInterface;

use yii\base\BaseObject;

/**
 * Class PaymentMedium
 *
 * @package Phycom\Base\Modules\Payment\Models
 */
class PaymentSubMethod extends BaseObject implements PaymentSubMethodInterface
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $label;

    /**
     * @var string
     */
    public string $logo;

    /**
     * @var PaymentMethodInterface
     */
    protected PaymentMethodInterface $method;

    /**
     * PaymentMedium constructor.
     *
     * @param PaymentMethodInterface $method
     * @param array $config
     */
    public function __construct(PaymentMethodInterface $method, $config = [])
    {
        $this->method = $method;
        parent::__construct($config);
    }

    public function getMethod(): PaymentMethodInterface
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }
}
