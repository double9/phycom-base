<?php

namespace Phycom\Base\Modules\Payment\Models;


use Phycom\Base\Modules\Payment\Exceptions\PaymentException;
use Phycom\Base\Modules\Payment\Interfaces\PaymentSubMethodInterface;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Module as PaymentModule;

use yii\base\Module;
use yii\base\Model;
use Yii;

/**
 * Class PaymentMethodSelectionForm
 *
 * @package Phycom\Base\Modules\Payment\Models
 */
class PaymentMethodSelectionForm extends Model
{
    const DELIMITER = '__';

    /**
     * @var string
     */
    public $method;


    public function rules()
    {
        return [
            ['method', 'required'],
            ['method', 'string', 'max' => 255]
        ];
    }


    public function attributeLabels()
    {
        return [
            'method' => Yii::t('phycom/modules/payment', 'Payment Method')
        ];
    }

    /**
     * @return PaymentMethodInterface|null
     */
    public function getMethod(): ?PaymentMethodInterface
    {
        if ($methodId = $this->getSelectedMethodId()) {
            foreach ($this->getModule()->getMethods() as $paymentMethod) {
                if ($paymentMethod->getId() === $methodId) {
                    return $paymentMethod;
                }
            }
        }
        return null;
    }

    /**
     * @return PaymentSubMethodInterface|null
     * @throws PaymentException
     */
    public function getSubMethod(): ?PaymentSubMethodInterface
    {
        if ($subMethodName = $this->getSelectedSubMethodName()) {
            if (!$paymentMethod = $this->getMethod()) {
                throw new PaymentException('Invalid payment method identifier ' . $this->method . '. Payment method was not found');
            }
            foreach ($paymentMethod->getSubMethods() as $paymentSubMethod) {
                if ($paymentSubMethod->getName() === $subMethodName) {
                    return $paymentSubMethod;
                }
            }
        }
        return null;
    }

    /**
     * @return string|null
     */
    protected function getSelectedSubMethodName(): ?string
    {
        $parts = explode(self::DELIMITER, $this->method);
        return $parts[1] ?? null;
    }

    /**
     * @return string|null
     */
    protected function getSelectedMethodId(): ?string
    {
        $parts = explode(self::DELIMITER, $this->method);
        return $parts[0] ?? null;
    }

    /**
     * @return PaymentModule|Module|null
     */
    protected function getModule(): ?PaymentModule
    {
        return Yii::$app->getModule('payment');
    }
}
