<?php

namespace Phycom\Base\Modules\Payment\Interfaces;

use Phycom\Base\Models\Payment;

/**
 * Interface PaymentMethodInterface
 * @package Phycom\Base\Modules\Payment\Interfaces
 */
interface PaymentMethodInterface
{
	/**
	 * @return bool
	 */
	public function isEnabled() : bool;
	/**
	 * Unique payment provider id
	 * @return string
	 */
	public function getId() : string;

	/**
	 * Payment provider display name
	 * @return string
	 */
	public function getName() : string;

	/**
	 * Payment provider short name
	 * @return string
	 */
	public function getLabel() : string;

    /**
     * @return string|null
     */
    public function getLogo() : string;

	/**
	 * @return string
	 */
	public function getPaymentInfo() : ?string;

	/**
	 * @param array $params
	 * @return string
	 */
	public function getPaymentButtons(array $params = []) : string;

    /**
     * @param array $params
     * @return string
     */
    public function renderPaymentForm(array $params = []): string;

    /**
     * @return PaymentSubMethodInterface[]
     */
	public function getSubMethods(): array;

	/**
	 * @param Payment $payment
	 * @param string|null $methodName
	 * @return TransactionInterface
	 */
	public function createTransaction(Payment $payment, string $methodName = null) : TransactionInterface;
}
