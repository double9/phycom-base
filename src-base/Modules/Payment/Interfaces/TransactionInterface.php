<?php

namespace Phycom\Base\Modules\Payment\Interfaces;

/**
 * Interface TransactionInterface
 * @package Phycom\Base\Modules\Payment\Interfaces
 */
interface TransactionInterface
{
	/**
	 * @return string
	 */
	public function getId();
	/**
	 * @return number
	 */
	public function getAmount();
	/**
	 * @return string - json encoded raw transaction data
	 */
	public function getData();
	/**
	 * @return string
	 */
	public function getPaymentLink();
    /**
     * @param array $params - additional params to include
     * @return mixed - data returned by transaction/begin controller action
     */
	public function getResponseData(array $params = []);
}
