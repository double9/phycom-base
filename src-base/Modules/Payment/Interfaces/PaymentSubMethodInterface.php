<?php

namespace Phycom\Base\Modules\Payment\Interfaces;

/**
 * Interface PaymentSubMethodInterface
 * @package Phycom\Base\Modules\Payment\Interfaces
 */
interface PaymentSubMethodInterface
{
    /**
     * Unique payment option name
     * @return string
     */
    public function getName() : string;

    /**
     * Payment option display name
     * @return string
     */
    public function getLabel() : string;

    /**
     * @return string|null
     */
    public function getLogo() : string;

    /**
     * @return PaymentMethodInterface
     */
    public function getMethod(): PaymentMethodInterface;

}
