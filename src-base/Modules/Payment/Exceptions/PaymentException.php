<?php

namespace Phycom\Base\Modules\Payment\Exceptions;

use yii\base\Exception;

/**
 * Class BaseException
 * @package Phycom\Base\Modules\Payment\Exceptions
 */
class PaymentException extends Exception
{

}
