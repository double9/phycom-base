<?php

namespace Phycom\Base\Modules\Payment\Exceptions;


use yii\web\BadRequestHttpException;


/**
 * Class BadRequestException
 * @package Phycom\Base\Modules\Payment\Exceptions
 */
class BadRequestException extends BadRequestHttpException
{

}
