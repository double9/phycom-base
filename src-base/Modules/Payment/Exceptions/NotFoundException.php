<?php

namespace Phycom\Base\Modules\Payment\Exceptions;

/**
 * Class NotFoundException
 * @package Phycom\Base\Modules\Payment\Exceptions
 */
class NotFoundException extends PaymentException
{

}
