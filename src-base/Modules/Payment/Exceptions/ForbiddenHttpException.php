<?php

namespace Phycom\Base\Modules\Payment\Exceptions;


/**
 * Class ForbiddenHttpException
 * @package Phycom\Base\Modules\Payment\Exceptions
 */
class ForbiddenHttpException extends \yii\web\ForbiddenHttpException
{

}
