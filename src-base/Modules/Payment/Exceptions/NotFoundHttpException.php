<?php

namespace Phycom\Base\Modules\Payment\Exceptions;


/**
 * Class NotFoundHttpException
 * @package Phycom\Base\Modules\Payment\Exceptions
 */
class NotFoundHttpException extends \yii\web\NotFoundHttpException
{

}
