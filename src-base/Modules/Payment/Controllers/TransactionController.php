<?php

namespace Phycom\Base\Modules\Payment\Controllers;


use Phycom\Base\Modules\Payment\Exceptions\PaymentException;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Helpers\Log;

use Phycom\Base\Models\Traits\WebControllerTrait;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Invoice;

use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii;

/**
 * Class TransactionController
 * @package Phycom\Base\Modules\Payment\Controllers
 */
class TransactionController extends \yii\web\Controller
{
	use WebControllerTrait;

	public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['begin'],
                        'allow' => true,
                        'roles' => ['?','@']
                    ],
                    ['allow' => true, 'roles' => ['@']], // Only logged in users should be able to access
                    ['allow' => false]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'begin' => ['post']
                ]
            ]
        ];
    }


    /**
     * @param string $method
     * @return mixed
     *
     * @throws NotFoundHttpException
     * @throws PaymentException
     * @throws \Throwable
     * @throws \yii\db\Exception
     */
	public function actionBegin($method)
	{
		$this->ajaxOnly(Response::FORMAT_JSON);

        $methodParts = explode('__', $method);
        $paymentMethodId = $methodParts[0];
        $paymentSubMethod = $methodParts[1] ?? null;

        $order = $this->findOrder(Yii::$app->session->get("order"));
        $initialOrderStatus = $order->status;

        if ($paymentMethodId === \Phycom\Base\Modules\Payment\Methods\Cash\Module::ID) {
            $order->status = OrderStatus::PENDING;
        } else {
            $order->status = OrderStatus::PENDING_PAYMENT;
        }
        $order->update();

        /**
         * @var Invoice $invoice
         */
        if (!$invoice = $order->getHasValidInvoice(true)) {
            $invoice = $order->createInvoice();
        }

        $t = Yii::$app->db->beginTransaction();
        try {

            $payment = $invoice->createPayment($paymentMethodId);
            $transaction = $payment->createTransaction($paymentSubMethod);
            $response = $transaction->getResponseData(['order' => $order->number]);

            $t->commit();
            return $response;

        } catch (\Exception $e) {
            $t->rollBack();
            $order->updateStatus($initialOrderStatus);
            throw new PaymentException($e->getMessage(), $e->getCode(), $e);
        }
	}

	protected function findOrder($id)
	{
		if (!$id || !$order = Yii::$app->modelFactory->getOrder()::findOne($id)) {
		    Log::warning('Order ' . $id . ' was not found');
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $order;
	}

	/**
	 * @param $id
	 * @return null|\yii\base\Module|PaymentMethodInterface
	 */
	public function getPaymentMethod($id)
	{
		return Yii::$app->getModule('payment')->getModule($id);
	}
}
