<?php

namespace Phycom\Base\Modules\Payment\Methods\Invoice\Assets;

use yii\web\AssetBundle;

/**
 * Invoice brand asset bundle.
 */
class InvoiceBrandAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Base/Modules/Payment/Methods/Invoice/Assets/brand';
}
