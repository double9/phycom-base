<?php

namespace Phycom\Base\Modules\Payment\Methods;

use Phycom\Base\Modules\Payment\Interfaces\PaymentSubMethodInterface;
use Phycom\Base\Modules\Payment\Module as PaymentModule;
use Phycom\Base\Modules\Payment\Models\Settings;

use Phycom\Base\Interfaces\ModuleSettingsInterface;
use Phycom\Base\Models\Traits\ModuleSettingsTrait;

use yii\base\Module as BaseModule;
use yii\helpers\Inflector;
use Yii;

/**
 * Class PaymentMethod
 *
 * @package Phycom\Base\Modules\Payment\Methods
 */
abstract class PaymentMethod extends BaseModule implements ModuleSettingsInterface
{
    const ID = 'default';

    use ModuleSettingsTrait;

    /**
     * @var bool
     */
    public bool $enabled = true;

    /**
     * @var bool
     */
    public bool $testMode = false;

    /**
     * @var string|null
     */
    public ?string $paymentInfo = null;

    /**
     * @return PaymentSubMethodInterface[]
     */
    public function getSubMethods(): array
    {
        return [];
    }

    /**
     * @return PaymentModule|yii\base\Module|null
     */
    public function getBaseModule() : ?PaymentModule
    {
        return Yii::$app->getModule('payment');
    }

    public function getName() : string
    {
        return $this->id;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getLabel() : string
    {
        return Inflector::humanize($this->id);
    }

    public function getPaymentInfo() : ?string
    {
        return $this->paymentInfo ?: null;
    }

    public function isEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @param array $params
     * @return string
     */
    public function renderPaymentForm(array $params = []): string
    {
        return '';
    }

    /**
     * @return array|callable|string
     */
    public function getSettingsForm()
    {
        return Settings::class;
    }

}
