<?php

/**
 * @var \Phycom\Base\Modules\Payment\Methods\Cash\Module $module
 */

use Phycom\Base\Modules\Payment\Methods\Cash\Assets\CashAsset;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

CashAsset::register($this);

?>

<div class="payment-button-item">
    <?php
    $form = ActiveForm::begin([
        'id' => 'cash-form',
        'action' => Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/payment-return', 'status' => 'success']),
        'options' => ['class' => 'cash-form']
    ]);
    ?>
    <?= Html::submitButton(
        Html::img($module->getLogo(), ['style' => 'height: 46px;']),
        [
            'id' => 'cash-payment-button',
            'class' => 'cash-payment-button payment-button btn btn-flat btn-default',
        ]
    ); ?>
    <?php ActiveForm::end(); ?>
</div>
