<?php

namespace Phycom\Base\Modules\Payment\Methods\Cash\Assets;

use yii\web\AssetBundle;

/**
 * Cash payment brand asset bundle.
 */
class CashBrandAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Base/Modules/Payment/Methods/Cash/Assets/brand';
}
