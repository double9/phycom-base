jQuery(function($) {

    const PAYMENT_METHOD = 'cash';
    const PAYMENT_EXECUTE_CASH = PAYMENT_EXECUTE + '.' + PAYMENT_METHOD;

    PubSub.subscribe(PAYMENT_EXECUTE_CASH, function (msg, data) {
        var $paymentForm = $("#cash-form");
        if ($paymentForm.length) {
            console.log("Writing data: ", data, " to ", $paymentForm);
            $paymentForm.submit();
        }
    });

    $("#cash-payment-button:not(.disabled)").click(function(e) {
        e.preventDefault();
        window.submitPayment(PAYMENT_METHOD, this).done( data => PubSub.publish(PAYMENT_EXECUTE_CASH, data) );
    });
});
