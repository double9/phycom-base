<?php

namespace Phycom\Base\Modules\Payment\Methods\Cash\Assets;

use Phycom\Base\Modules\Payment\Assets\PaymentAsset;

use yii\web\AssetBundle;

/**
 * Cash payment method asset bundle.
 */
class CashAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Base/Modules/Payment/Methods/Cash/Assets/src';
    public $css = [];
    public $js = [
        'cash.js'
    ];
    public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        PaymentAsset::class
    ];
}
