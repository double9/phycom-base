<?php

namespace Phycom\Base\Modules\Payment\Methods\Cash;

use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\Payment;
use Phycom\Base\Modules\Payment\Exceptions\PaymentException;
use Phycom\Base\Modules\Payment\Helpers\Reference;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Interfaces\TransactionInterface;
use Phycom\Base\Modules\Payment\Models\Transaction;
use Phycom\Base\Modules\Payment\Methods\PaymentMethod;
use Phycom\Base\Modules\Payment\Methods\Cash\Assets\CashBrandAsset;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package Phycom\Base\Modules\Payment\Methods\Cash
 *
 * @property string $id
 * @property bool $isEnabled
 * @property string $name
 * @property string $label
 */
class Module extends PaymentMethod implements PaymentMethodInterface
{
    const ID = 'cash';

	public ?string $paymentInfo = null;

    /**
     * @return string
     */
	public function getLabel() : string
	{
		return Yii::t('phycom/modules/payment', 'Cash on delivery');
	}

    /**
     * @return string
     */
	public function getLogo() : string
	{
        $bundle = CashBrandAsset::register(Yii::$app->view);
        return $bundle->baseUrl . '/cash.svg';
	}

    /**
     * @param array $params
     * @return string
     */
	public function getPaymentButtons(array $params = []) : string
	{
		return Yii::$app->view->renderFile(dirname(__FILE__) . '/views/button.php', ['module' => $this]);;
	}

    /**
     * @param array $params
     * @return string
     */
    public function renderPaymentForm(array $params = []): string
    {
        return Yii::$app->view->renderFile(dirname(__FILE__) . '/views/payment-form.php', ArrayHelper::merge(['module' => $this], $params));
    }

    /**
     * @return string|null
     */
	public function getPaymentInfo() : ?string
	{
		if ($this->paymentInfo === false) {
			return null;
		}
		if (strlen($this->paymentInfo)) {
			return $this->paymentInfo;
		}
		return $this->label;
	}

    /**
     * @param Payment $payment
     * @param string|null $methodName
     * @return TransactionInterface|Transaction|object
     * @throws \yii\base\InvalidConfigException
     */
	public function createTransaction(Payment $payment, string $methodName = null) : TransactionInterface
	{
        return Yii::createObject(Transaction::class, [[
            'amount'    => Currency::toDecimal($payment->amount),
            'reference' => Reference::create($payment)
        ]]);
	}
}
