<?php

namespace Phycom\Base\Modules\Payment\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Main backend application asset bundle.
 */
class PaymentAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Base/Modules/Payment/Assets/src';
    public $css = [
    	'payment.css'
    ];
    public $js = [
        'events.js',
	    'payment.js'
    ];
	public $publishOptions = ['except' => ['*.less']];
    public $depends = [
        JqueryAsset::class,
        PubSubAsset::class
    ];
}
