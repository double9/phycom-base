<?php

namespace Phycom\Base\Modules\Payment\Assets;


use yii\web\AssetBundle;

/**
 * Class PubSubAsset
 *
 * @package Phycom\Base\Modules\Payment\Assets
 */
class PubSubAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/pubsub-js/src';

    public $js = [
        'pubsub.js'
    ];

}
