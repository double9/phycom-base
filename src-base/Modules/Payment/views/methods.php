<?php

/**
 * @var \yii\web\View $this
 */

use Phycom\Base\Modules\Payment\Assets\PaymentAsset;

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\web\View;



$submitOrder = $submitOrder ?? false;
$message = $message ?? false;
$icon = $icon ?? false;
$title = $title ?? Yii::t('phycom/modules/payment','Select payment method');
$titleTag = $titleTag ?? 'h3';


if ($submitOrder) {
    $this->registerJs(';window.submitOrderWithPayment = true;', View::POS_HEAD);
}

PaymentAsset::register($this);

/**
 * @var \Phycom\Base\Modules\Payment\Module $module
 */
$module = Yii::$app->getModule("payment");
$methods = $module->getMethods();
?>

<div id="payment-methods" class="row" data-transaction-route="<?= Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/payment/transaction']) ?>">
	<div class="col-md-12">
        <?= Html::tag($titleTag, ($icon ?: '') . $title, ['class' => 'checkout-section-title']) ?>
		<hr style="margin: 0px 0px 10px 0px;">

        <div class="row">
            <div class="col-md-12">
				<?php
				if ($message) {
                    echo Alert::widget([
                        'body'    => $message,
                        'options' => ['class' => 'alert-info']
                    ]);
				}
				?>
            </div>
        </div>

        <div class="payment-methods clearfix">
            <?php foreach ($methods as $method): ?>
                <div class="payment-method <?= $method->getId(); ?>">
                    <?php
                    if ($method->getPaymentInfo()) {
                        echo Html::tag('h5', $method->getPaymentInfo(), ['class' => 'payment-info']);
                    }
                    ?>
                    <div class="clearfix">
                        <?= $method->getPaymentButtons(); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

	</div>
</div>
