<?php
namespace Phycom\Base\Modules\Payment;

use Phycom\Base\Interfaces\CommerceComponentInterface;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Methods\PaymentMethod;

use Phycom\Base\Modules\BaseModule;

use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Inflector;

/**
 * Class Module
 * @package Phycom\Base\Modules\Payment
 * @property PaymentMethodInterface[] $methods - enabled payment methods
 */
class Module extends BaseModule implements CommerceComponentInterface
{
    public bool $enabled = true;

    /**
     * @var string
     */
    public $controllerNamespace = 'Phycom\Base\Modules\Payment\Controllers';


	public $defaultMethod;
	public $keyPath = '@files/payment-keys';
	public $currency = 'EUR';
	public $country = 'ee';
	public $companyName;

	public function init()
	{
		parent::init();
		$subModules = [
            Methods\Invoice\Module::ID => [
				'class' => Methods\Invoice\Module::class,
			],
            Methods\Cash\Module::ID => [
				'class' => Methods\Cash\Module::class,
			]
		];

        foreach ($this->getModules() as $id => $params) {
            if (is_string($params)) {
                $params = ['class' => $params];
            }
            $params['useSettings'] = true;
            $params['enabled'] = false;

            if (isset($subModules[$id])) {
                $params = ArrayHelper::merge($subModules[$id], $params);
            }
            $this->setModule($id, $params);
        }
	}

	public function isEnabled(): bool
    {
        return Yii::$app->commerce->isEnabled() && $this->enabled;
    }

    public function getName(): string
    {
        return 'payment';
    }

    public function getLabel() : string
    {
        return Yii::t('phycom/modules/payment', 'Payment methods');
    }

    public function getJsonSchema(): array
    {
        return [];
    }

    public function getAllSettings(): array
    {
        $modules = [];
        foreach ($this->modules as $id => $moduleParams) {
            $modules[] = $this->getModule($id);
        }
        return $modules;
    }

    public function getBackendSettingsView(): ?string
    {
        return 'payment-methods';
    }

    /**
     * @param bool $onlyEnabled
     * @return PaymentMethod[]|PaymentMethodInterface[]
     */
	public function getMethods(bool $onlyEnabled = true)
	{
		$methods = [];

		foreach ($this->getModules() as $id => $module) {
		    if (is_array($module)) {
		        $module = $this->getModule($id);
            }
			/**
			 * @var PaymentMethodInterface $module
			 */
			if (!$onlyEnabled || ($onlyEnabled && $module->isEnabled())) {
				$methods[] = $module;
			}
		}
		return $methods;
	}
}
