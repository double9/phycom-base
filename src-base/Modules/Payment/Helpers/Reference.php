<?php

namespace Phycom\Base\Modules\Payment\Helpers;

use Phycom\Base\Modules\Payment\Exceptions\NotFoundException;
use Phycom\Base\Models\Payment;

use yii\base\InvalidConfigException;
use yii\base\BaseObject;
use Yii;

/**
 * This is used to create identifier that is passed along with payment transaction.
 * When payment response is returned from gateway we can identify corresponding Payment
 *
 *
 * Class Reference
 * @package Phycom\Base\Modules\Payment\Helpers
 */
class Reference extends BaseObject
{
    /**
     * @param Payment $payment
     * @return string
     * @throws InvalidConfigException
     */
    public static function create(Payment $payment): string
    {
        /**
         * @var static|object $reference
         */
        $reference = Yii::createObject(static::class);
        return $reference->createIdentifier($payment);
    }

    /**
     * @param string $key
     * @return Payment
     * @throws NotFoundException|InvalidConfigException
     */
    public static function parse($key): Payment
    {
        /**
         * @var static|object $reference
         */
        $reference = Yii::createObject(static::class);
        return $reference->findPayment($key);
    }

    /**
     * @param Payment $payment
     * @return string
     */
    public function createIdentifier(Payment $payment): string
    {
        return $payment->id . '-' . $payment->invoice->number;
    }

    /**
     * @param string $identifier
     * @return Payment
     * @throws NotFoundException
     */
    public function findPayment(string $identifier): Payment
    {
        [$paymentId] = explode('-', $identifier);
        $payment = Yii::$app->modelFactory->getPayment()::findOne($paymentId);
        if (!$payment) {
            throw new NotFoundException('Payment ' . $paymentId . ' not found');
        }
        return $payment;
    }
}
