<?php

namespace Phycom\Base\Events;

use yii\base\Event;

/**
 * Class StatusUpdateEvent
 * @package Phycom\Base\Events
 */
class StatusUpdateEvent extends Event
{
	/**
	 * @var string The status value that had changed and were saved.
	 */
	public $prevStatus;
}
