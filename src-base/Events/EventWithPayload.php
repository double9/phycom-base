<?php

namespace Phycom\Base\Events;

use yii\base\Event;

/**
 * Class EventWithPayload
 *
 * @package Phycom\Base\Events
 */
class EventWithPayload extends Event
{
    /**
     * @var mixed
     */
    public $payload;
}
