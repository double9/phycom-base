<?php

namespace Phycom\Base\Events;

use yii\base\Event;
use yii\base\Model;

/**
 * Class ModelUpdateEvent
 * @package Phycom\Base\Events
 */
class ModelUpdateEvent extends Event
{
    /**
     * @var mixed
     */
    public $key;
    /**
     * @var Model
     */
    public $model;
    /**
     * @var array
     */
    public $attributes;
}
