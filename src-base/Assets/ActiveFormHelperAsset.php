<?php

namespace Phycom\Base\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\widgets\ActiveFormAsset;

/**
 * Class ActiveFormHelperAsset
 * @package Phycom\Base\Assets
 */
class ActiveFormHelperAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Base/Assets/active-form-helper';
	public $js = [
	    'form.js'
	];
	public $depends = [
	    ActiveFormAsset::class,
        JqueryAsset::class
	];
}
