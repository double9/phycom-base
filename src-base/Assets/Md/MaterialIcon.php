<?php

namespace Phycom\Base\Assets\Md;


use yii\base\InvalidArgumentException;
use yii\helpers\Html;


/**
 * Class Icon
 * @package Phycom\Base\Assets\Md
 */
class MaterialIcon
{
    /** @var array Html options */
    private $options = [];

    /** @var string Html tag */
    private $tag = 'i';


    /**
     * @param string $icon
     * @param array $options
     */
    public function __construct($icon, $options = [])
    {
        if (isset($options['tag'])) {
            if (is_string($options['tag'])) {
                $this->tag = $options['tag'];
            }
            unset($options['tag']);
        }

        Html::addCssClass($options, MD::$cssPrefix . ' ' . MD::$cssPrefix . '-' . $icon);
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @return MaterialIcon
     */
    public function inverse()
    {
        return $this->addCssClass(MD::$cssPrefix . '-inverse');
    }

    /**
     * @return MaterialIcon
     */
    public function spin()
    {
        return $this->addCssClass(MD::$cssPrefix . '-spin');
    }

    /**
     * @return MaterialIcon
     */
    public function fixedWidth()
    {
        return $this->addCssClass(MD::$cssPrefix . '-fw');
    }

    /**
     * @return MaterialIcon
     */
    public function border()
    {
        return $this->addCssClass(MD::$cssPrefix . '-border');
    }

    /**
     * @return MaterialIcon
     */
    public function pullLeft()
    {
        return $this->addCssClass('pull-left');
    }

    /**
     * @return MaterialIcon
     */
    public function pullRight()
    {
        return $this->addCssClass('pull-right');
    }

    /**
     * @param string $value
     * @return MaterialIcon
     * @throws InvalidArgumentException
     */
    public function size($value)
    {
        return $this->addCssClass(
            MD::$cssPrefix . '-' . $value,
            in_array((string)$value, [MD::SIZE_LARGE, MD::SIZE_2X, MD::SIZE_3X, MD::SIZE_4X, MD::SIZE_5X], true),
            sprintf(
                '%s - invalid value. Use one of the constants: %s.',
                'MD::size()',
                'MD::SIZE_LARGE, MD::SIZE_2X, MD::SIZE_3X, MD::SIZE_4X, MD::SIZE_5X'
            )
        );
    }

    /**
     * @param string $value
     * @return MaterialIcon
     * @throws InvalidArgumentException
     */
    public function rotate($value)
    {
        return $this->addCssClass(
            MD::$cssPrefix . '-rotate-' . $value,
            in_array((string)$value, [MD::ROTATE_90, MD::ROTATE_180, MD::ROTATE_270], true),
            sprintf(
                '%s - invalid value. Use one of the constants: %s.',
                'MD::rotate()',
                'MD::ROTATE_90, MD::ROTATE_180, MD::ROTATE_270'
            )
        );
    }

    /**
     * @param string $value
     * @return MaterialIcon
     * @throws InvalidArgumentException
     */
    public function flip($value)
    {
        return $this->addCssClass(
            MD::$cssPrefix . '-flip-' . $value,
            in_array((string)$value, [MD::FLIP_HORIZONTAL, MD::FLIP_VERTICAL], true),
            sprintf(
                '%s - invalid value. Use one of the constants: %s.',
                'MD::flip()',
                'MD::FLIP_HORIZONTAL, MD::FLIP_VERTICAL'
            )
        );
    }

    /**
     * @param string $class
     * @param bool $condition
     * @param string|bool $throw
     * @return MaterialIcon
     * @throws InvalidArgumentException
     */
    public function addCssClass($class, $condition = true, $throw = false)
    {
        if ($condition === false) {
            if (!empty($throw)) {
                $message = !is_string($throw)
                    ? 'Condition is false'
                    : $throw;

                throw new InvalidArgumentException($message);
            }
        } else {
            Html::addCssClass($this->options, $class);
        }

        return $this;
    }

    /**
     * @param string $tag
     * @return MaterialIcon
     * @throws InvalidArgumentException
     */
    public function tag($tag)
    {
        if (is_string($tag)) {
            $this->tag = $tag;
        } else {
            throw new InvalidArgumentException(
                sprintf('Param tag expect type string, %s given ', gettype($tag))
            );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        return Html::tag($this->tag, null, $this->options);
    }
}
