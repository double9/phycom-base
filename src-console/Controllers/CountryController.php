<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Models\Country;

use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CountryController
 * @package Phycom\Console\Controllers
 */
class CountryController extends BaseConsoleController
{
    public function actionListByRegion($region = 'Europe')
    {
        $countries = Country::loadByCondition('geo.region', $region);
        $codes = ArrayHelper::map($countries, 'code', 'name');

        $this->printLn(Json::encode($codes, JSON_PRETTY_PRINT));
        return ExitCode::OK;
    }
}
