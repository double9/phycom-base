<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Models\Product\Import\SpreadsheetImportStrategy;

use Phycom\Base\Models\Product\Product;
use yii\web\NotFoundHttpException;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\helpers\Json;
use yii;


/**
 * Class ProductController
 * @package Phycom\Console\Controllers
 */
class ProductController extends BaseConsoleController
{
    public $defaultAction = 'print';

    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        if (!$this->isComponentEnabled(Yii::$app->commerce)) {
            throw new yii\base\InvalidCallException('Commerce component is not enabled');
        }
        return parent::beforeAction($action);
    }

    /**
     * @param int $id
     * @return int
     */
	public function actionPrint(int $id)
	{
	    $product = Yii::$app->modelFactory->getProduct()::findOne(['id' => $id]);

	    $this->printLn(Json::encode($product->attributes, JSON_PRETTY_PRINT));
        $this->printLn(Json::encode($product->params, JSON_PRETTY_PRINT));

        return ExitCode::OK;
	}

    /**
     * @param string $filename
     * @return int
     * @throws yii\base\InvalidConfigException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
	public function actionImportSpreadsheet(string $filename)
    {
        if (!$this->isComponentEnabled(Yii::$app->commerce->productImporter)) {
            throw new yii\base\InvalidCallException('Product importer is not enabled');
        }
        /**
         * @var SpreadsheetImportStrategy|object $importStrategy
         */
        $importStrategy = Yii::createObject(SpreadsheetImportStrategy::class, [$filename]);

        Yii::$app->commerce->productImporter->import($importStrategy, function (int $i, int $total, Product $product = null) {
            (0 === $i)
                ? Console::startProgress(0, $total)
                : Console::updateProgress($i, $total, str_pad('product ' . $product->sku, 20));
        });
        Console::endProgress();

        return ExitCode::OK;
    }
}
