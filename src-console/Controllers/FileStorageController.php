<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Models\Attributes\FileStatus;
use Phycom\Base\Models\Attributes\FileType;
use Phycom\Base\Models\File;

use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Class FileStorageController
 * @package Phycom\Console\Controllers
 */
class FileStorageController extends BaseConsoleController
{
	/**
	 * Regenerates all image thumbnail files
	 * @return int
	 */
	public function actionRegenerateThumbs()
	{
		$query = File::find()->where(['not', ['status' => FileStatus::DELETED]])->andWhere(['mime_type' => FileType::getImageTypes()]);
		$n = 0;
		$this->printLn('Regenerating ' . $query->count() . ' images...', Console::FG_YELLOW);
		foreach ($query->batch() as $files) {
			/**
			 * @var File[] $files
			 */
			foreach ($files as $file) {
				$file->generateThumbs();
				$this->printLn('Image ' . $file->id . ' - ' . $file->filename . ' done', Console::FG_GREEN);
				$n++;
			}
		}
		$this->printLn('All done. ' . $n . ' images processed', Console::FG_GREEN);
		return ExitCode::OK;
	}
}
