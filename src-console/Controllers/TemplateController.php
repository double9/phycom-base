<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Models\Attributes\MessageType;
use Phycom\Base\Models\Message;

use yii\console\ExitCode;
use yii;

class TemplateController extends BaseConsoleController
{
	public function actionTest()
	{
		$tpl = Yii::$app->modelFactory->getMessageTemplate()::getTemplate('user_invitation');
		echo $tpl->render([
			'appname' => 'Appname',
			'firstname' => 'John',
			'link' => Yii::$app->urlManagerBackend->createAbsoluteUrl(['/site/register', 't' => 'abcdefg'])
		]);
		return ExitCode::OK;
	}


	public function actionSendMessage($templateName, $userId)
	{
		if (!$template = Yii::$app->modelFactory->getMessageTemplate()::getTemplate($templateName)) {
			throw new yii\base\InvalidArgumentException('Template ' . $templateName . ' not found');
		}
		$message = Message::createByTemplate($template, $template->generateDummyParameters());
		$message->user_to = $userId;
		$message->type = MessageType::EMAIL;
		$message->priority = Message::PRIORITY_HIGH;

		if (!$message->save()) {
			$this->printLn('Error creating order '.$this->id.' email message: ' . json_encode($message->errors));
			return ExitCode::USAGE;
		}

		$id = $message->queue();
		$this->printLn('Job ' . $id . ' queued');
		return ExitCode::OK;
	}
}
