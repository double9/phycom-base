<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Helpers\Currency;
use Phycom\Base\Helpers\Date;
use Phycom\Base\Models\Attributes\VendorType;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\LanguageStatus;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\Language;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\Vendor;
use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;

use yii\console\ExitCode;
use yii\helpers\Inflector;
use yii\helpers\Console;
use yii;

/**
 * Class AppController
 * @package Phycom\Console\Controllers
 */
class AppController extends BaseConsoleController
{
	use ModelTrait;

	public $defaultAction = 'info';

	public $systemEmail;

	public $languages = [];

	public $vendorName;
	public $vendorLegalName;
	public $vendorRegNo;
	public $vendorEmail;
	public $vendorPhone;
	public $vendorPhoneCode;
	public $vendorAddress;

	public $shops = [];
	public $deliveryAreas = [];

    /**
     * Prints out the application info
     *
     * @return int
     * @throws \Exception
     */
	public function actionInfo()
	{
		$sysUser = Yii::$app->modelFactory->getUser()::findOne(['username' => $this->getSystemUsername()]);
		$info = [
			'name' => Yii::$app->name,
			'system-user' => $sysUser ? $sysUser->attributes : null,
			'timeZone' => Yii::$app->timeZone
		];
		echo json_encode($info, JSON_PRETTY_PRINT) . PHP_EOL;

		$date = new \DateTime();
//		echo json_encode($date, JSON_PRETTY_PRINT) . PHP_EOL;

		$date2 = Date::create($date);
		echo json_encode($date2, JSON_PRETTY_PRINT) . PHP_EOL;

		$date3 = Date::create($date)->dbTimestamp;
		echo json_encode($date3, JSON_PRETTY_PRINT) . PHP_EOL;

		return ExitCode::OK;
	}

    /**
     * Initializes the application by populating the db with required or default data.
     *
     * @param bool $force
     * @return int
     */
	public function actionInit(bool $force = false)
	{
	    $settingName = 'isInitialized';
        if (!Yii::$app->getSetting($settingName) || $force) {
            $this->initDb();
            \Phycom\Base\Models\Setting::set($settingName, '1');
        }
		return ExitCode::OK;
	}

	protected function initDb()
	{
		$user = Yii::$app->modelFactory->getUser()::findOne(['username' => $this->getSystemUsername()]);
		if (!$user) {
			$user = Yii::$app->modelFactory->getUser();
			$user->username = $this->getSystemUsername();
			$user->first_name = Yii::$app->name;
			$user->last_name = '';
			$user->status = UserStatus::ACTIVE;
			$user->type = UserType::ADMIN;
			$user->generateAuthKey();

			if (!$user->save()) {
				$this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_RED);
			} else {
				$this->printLn(json_encode($user->attributes, JSON_PRETTY_PRINT), Console::FG_GREEN);
			}
		}
		if (!$user->email) {
			// create user email
			$email = new Email();
			$email->email = $this->systemEmail;
			$email->user_id = $user->id;
			$email->status = ContactAttributeStatus::VERIFIED;

			if (!$email->save()) {
				$this->printLn(json_encode($email->errors, JSON_PRETTY_PRINT), Console::FG_RED);
			} else {
				$this->printLn($email->email . ' created', Console::FG_GREEN);
			}
		}

		$this->setupVendorShop();
		$this->setupLanguages();
		$this->setupDeliveryAreas();
	}

	protected function setupVendorShop()
	{
		/**
		 * @var $vendor Vendor
		 */
		if ($vendor = Yii::$app->modelFactory->getVendor()::findOne(['1'])) {
			$vendor->name = $this->vendorName;
			$vendor->reg_number = $this->vendorRegNo;
            $vendor->type = VendorType::SITE_OWNER;
			$vendor->legal_name = $this->vendorLegalName;
		} else {
			$vendor = Yii::$app->modelFactory->getVendor();
			$vendor->name = $this->vendorName;
			$vendor->legal_name = $this->vendorLegalName;
			$vendor->reg_number = $this->vendorRegNo;
			$vendor->type = VendorType::SITE_OWNER;
			$vendor->status = new VendorStatus(VendorStatus::ACTIVE);
		}
		$this->checkSaveModel($vendor);

		$phoneFound = false;
		foreach ($vendor->phones as $phone) {
			if ($this->vendorPhoneCode . $this->vendorPhone === $phone->msisdn) {
				$phoneFound = true;
				break;
			}
		}
		if (!$phoneFound) {
			$phone = new Phone();
			$phone->vendor_id = $vendor->id;
			$phone->phone_nr = $this->vendorPhone;
			$phone->country_code = $this->vendorPhoneCode;
			$phone->status = ContactAttributeStatus::VERIFIED;
			$this->checkSaveModel($phone);
		}

		$emailFound = false;
		foreach ($vendor->emails as $email) {
			if ($email->email === $this->vendorEmail) {
				$emailFound = true;
				break;
			}
		}
		if (!$emailFound) {
			$vendorEmail = new Email();
			$vendorEmail->vendor_id = $vendor->id;
			$vendorEmail->email = $this->vendorEmail;
			$vendorEmail->status = ContactAttributeStatus::VERIFIED;
			$this->checkSaveModel($vendorEmail);
		}

		if (isset($this->vendorAddress) && !empty($this->vendorAddress)) {
			$addressFound = false;
			foreach ($vendor->addresses as $address) {
				if ($address->street === $this->vendorAddress['street']) {

					$address->postcode = $this->vendorAddress['postcode'];
					$address->province = $this->vendorAddress['province'];
					$address->district = $this->vendorAddress['district'] ?? null;
					$address->city = $this->vendorAddress['city'] ?? null;
					$address->locality = $this->vendorAddress['locality'] ?? null;
					$address->country = $this->vendorAddress['country'];
					$this->checkSaveModel($address);

					$addressFound = true;
					break;
				}
			}
			if (!$addressFound) {
				$address = new Address();
				$address->vendor_id = $vendor->id;
				$address->status = ContactAttributeStatus::VERIFIED;
				$address->type = AddressType::MAIN;
				$address->country = $this->vendorAddress['country'];
				$address->province = $this->vendorAddress['province'];
				$address->city = $this->vendorAddress['city'] ?? null;
				$address->locality = $this->vendorAddress['locality'] ?? null;
				$address->district = $this->vendorAddress['district'] ?? null;
				$address->street = $this->vendorAddress['street'];
				$address->postcode = $this->vendorAddress['postcode'];

				$this->checkSaveModel($address);
			}
		}

		foreach ($this->shops as $shopData) {
			$shopFound = false;
			$shop = null;
			foreach ($vendor->shops as $shop) {
				if ($shop->name === $shopData['name']) {
					$shopFound = true;
					break;
				}
			}
			if (!$shopFound) {
				$shop = new Shop();
				$shop->name = $shopData['name'];
				$shop->status = $shopData['status'] ?? ShopStatus::OPEN;
				$shop->vendor_id = $vendor->id;
				$this->checkSaveModel($shop);
			}
			if (isset($shopData['phone'])) {
				$phoneFound = false;
				foreach ($shop->phones as $phone) {
					if ($phone->msisdn === $shopData['phoneCode'] . $shopData['phone']) {
						$phoneFound = true;
						break;
					}
				}
				if (!$phoneFound) {
					$shopPhone = new Phone();
					$shopPhone->shop_id = $shop->id;
					$shopPhone->phone_nr = $shopData['phone'];
					$shopPhone->country_code = $shopData['phoneCode'];
					$shopPhone->status = ContactAttributeStatus::VERIFIED;
					$this->checkSaveModel($shopPhone);
				}
			}
			if (isset($shopData['email'])) {
				$emailFound = false;
				foreach ($shop->emails as $email) {
					if ($email->email === $shopData['email']) {
						$emailFound = true;
						break;
					}
				}
				if (!$emailFound) {
					$shopEmail = new Email();
					$shopEmail->shop_id = $shop->id;
					$shopEmail->email = $shopData['email'];
					$shopEmail->status = ContactAttributeStatus::VERIFIED;
					$this->checkSaveModel($shopEmail);
				}
			}
			if (isset($shopData['address'])) {
				$addressFound = false;
				foreach ($shop->addresses as $address) {
					if ($address->street === $shopData['address']['street']) {
						$addressFound = true;
						break;
					}
				}
				if (!$addressFound) {
					$shopAddress = Address::create(json_encode($shopData['address']));
					$shopAddress->shop_id = $shop->id;
					$shopAddress->status = ContactAttributeStatus::VERIFIED;
					$shopAddress->type = AddressType::MAIN;
					$this->checkSaveModel($shopAddress);
				}
			}
		}
	}

	protected function setupLanguages()
	{
		$languageCodes = yii\helpers\ArrayHelper::merge([substr(Yii::$app->language, 0, 2)], $this->languages);
		/**
		 * @var Language[] $languages
		 */
		$languages = Language::find()
			->where(['code' => $languageCodes])
			->orWhere(['status' => LanguageStatus::VISIBLE])
			->all();

		foreach ($languages as $language) {
			if (in_array($language->code, $this->languages)) {
				$language->status = LanguageStatus::VISIBLE;
			} else {
				$language->status = LanguageStatus::HIDDEN;
			}
			$language->save();
		}
	}

	protected function setupDeliveryAreas()
	{
		foreach ($this->deliveryAreas as $areaData) {

			$a = new DeliveryArea();
			$a->code = $areaData['code'];
			$a->price = Currency::toInteger($areaData['price']);
			$a->delivery_time = $areaData['time'] ?? null;
			$a->carrier = $areaData['courier'];
			$a->service = $areaData['service'] ?? null;
			$a->status = $areaData['status'] ?? DeliveryAreaStatus::ACTIVE;

			unset($areaData['code']);
			unset($areaData['price']);
			unset($areaData['time']);
			unset($areaData['courier']);
			unset($areaData['service']);
			unset($areaData['status']);

			$a->area = json_encode($areaData);

			$a->attributes;

			$prevModel = DeliveryArea::findOne(['code' => $a->code, 'carrier' => $a->carrier]);
			if ($prevModel) {
				$prevModel->attributes = $a->attributes;
				$prevModel->update();
			} else {
				$a->save();
			}
		}
	}


	protected function getSystemUsername()
	{
		return Inflector::slug(Yii::$app->name) . '_system';
	}


	protected function checkSaveModel(yii\db\ActiveRecord $model)
	{
	    $isNewRecord = $model->isNewRecord;
		if ($model->save()) {
			$this->printLn(get_class($model) . ($isNewRecord ? ' saved' : ' updated'), Console::FG_GREEN);
			$attributes = [];
			foreach ($model->attributes as $key => $value) {
				if (is_object($value)) {
					if (method_exists($value, '__toString')) {
						$attributes[$key] = (string) $value;
					} else if ($value instanceof \DateTime) {
						$attributes[$key] = $value->format(\DateTime::RFC3339_EXTENDED);
					} else {
						$attributes[$key] = $value;
					}
				} else {
					$attributes[$key] = $value;
				}
			}
			$this->printLn(json_encode($attributes, JSON_PRETTY_PRINT), Console::FG_GREY);
			return true;
		} else {
			$this->printLn(json_encode($model->errors, JSON_PRETTY_PRINT), Console::FG_RED);
			return false;
		}
	}
}
