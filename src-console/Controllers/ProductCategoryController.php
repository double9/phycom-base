<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Models\Attributes\CategoryStatus;
use Phycom\Base\Models\Product\ProductCategory;

use yii\console\ExitCode;


/**
 * Class ProductCategoryController
 * @package Phycom\Console\Controllers
 */
class ProductCategoryController extends BaseConsoleController
{
    /**
     * Checks that all categories with parent deleted will be deleted also
     */
	public function actionPurge()
	{
        /**
         * @var ProductCategory[] $categories
         */
        $categories = ProductCategory::find()->where(['status' => CategoryStatus::DELETED])->all();
        foreach ($categories as $category) {
            $category->deleteRecursive();
        }
        return ExitCode::OK;
	}
}
