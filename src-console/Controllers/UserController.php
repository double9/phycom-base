<?php

namespace Phycom\Console\Controllers;

use Phycom\Base\Jobs\ReferenceNumberJob;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserTokenType;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\User;
use Phycom\Base\Models\UserToken;

use yii\base\InvalidArgumentException;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\db\Query;
use yii;

/**
 * Class UserController
 *
 * @package Phycom\Console\Controllers
 */
class UserController extends BaseConsoleController
{
	const DEFAULT_PASSWORD = 'parool';

    /**
     * Resets password for an user
     *
     * @param int $userId
     * @param string $password
     * @return int
     */
	public function actionChangePassword(int $userId, string $password)
    {
        if (!$user = Yii::$app->modelFactory->getUser()::findOne(['id' => $userId])) {
            throw new yii\base\InvalidArgumentException('User ' . $userId . ' was not found');
        }

        $user->password = $password;

        if (!$user->save()) {
            $this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
            return ExitCode::USAGE;
        }

        return ExitCode::OK;
    }

    /**
     * Activates pending user
     *
     * @param int $userId
     * @return int
     * @throws \Throwable
     * @throws yii\db\Exception
     */
    public function actionActivate(int $userId)
    {
        if (!$user = Yii::$app->modelFactory->getUser()::findOne(['id' => $userId])) {
            throw new yii\base\InvalidArgumentException('User ' . $userId . ' was not found');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {

            if ($user->status !== UserStatus::ACTIVE) {
                $user->status = UserStatus::ACTIVE;

                if (!$user->save()) {
                    $this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
                    $transaction->rollBack();
                    return ExitCode::USAGE;
                }
            }

            if ($user->email->status !== ContactAttributeStatus::VERIFIED && !$user->email->setVerified()) {
                $this->printLn(json_encode($user->email->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
                $transaction->rollBack();
                return ExitCode::USAGE;
            }

            $transaction->commit();

        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return ExitCode::OK;
    }

    /**
     * Creates a new admin user
     *
     * @param string $username
     * @param string $password
     * @param integer $vendorId
     * @return int
     * @throws \Exception
     */
	public function actionCreateAdminUser(string $username, $password = self::DEFAULT_PASSWORD, $vendorId = 1)
	{
		$user = Yii::$app->modelFactory->getUser();

		$user->username = $username;
		$user->first_name = ucfirst($username);
		$user->last_name = ucfirst(strrev($username));
		$user->password = $password;
		$user->type = UserType::ADMIN;
		$user->status = UserStatus::ACTIVE;
		$user->generateAuthKey();

		if (!$user->save()) {
			$this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
			return ExitCode::USAGE;
		}

		Yii::$app->authManager->assign(Yii::$app->authManager->getRole('admin'), $user->id);

		$vendor = Yii::$app->modelFactory->getVendor()::findOne($vendorId);
		if ($vendor) {
			$vendor->addUser($user);
		}

		$this->printLn('User ' . $user->id . ' successfully created', Console::FG_GREEN);
		$this->printLn(json_encode($user->attributes, JSON_PRETTY_PRINT), Console::FG_PURPLE);
		return ExitCode::OK;
	}

	/**
	 * Creates a new test user
	 *
	 * @param $username
	 * @param string $password
	 * @return int
	 */
	public function actionCreateTestUser(string $username, $password = self::DEFAULT_PASSWORD)
	{
		$user = Yii::$app->modelFactory->getUser();

		$user->username = $username;
		$user->first_name = ucfirst($username);
		$user->last_name = ucfirst(strrev($username));
		$user->password = $password;
		$user->type = UserType::CLIENT;
		$user->status = UserStatus::ACTIVE;
		$user->generateAuthKey();

		if (!$user->save()) {
			$this->printLn(json_encode($user->errors, JSON_PRETTY_PRINT), Console::FG_YELLOW);
			return ExitCode::USAGE;
		}
		$this->printLn('User ' . $user->id . ' successfully created', Console::FG_GREEN);
		$this->printLn(json_encode($user->attributes, JSON_PRETTY_PRINT), Console::FG_PURPLE);
		return ExitCode::OK;
	}

	/**
	 * Generates access token
	 * @param int $userId
	 * @return int
	 */
	public function actionGenerateAccessToken(int $userId)
	{
		$token = new UserToken();
		$token->user_id = $userId;
		$token->type = UserTokenType::ACCESS_TOKEN;
		$token->generate()->save();

		$this->printLn($token->token, Console::FG_GREEN);
		return ExitCode::OK;
	}

    /**
     * Sets admin role for admin user(s)
     *
     * @param int $userId
     * @param string $roleName
     *
     * possible roles are:
     *  - super_admin
     *  - admin
     *  - shop_admin
     *  - product_admin
     *  - contributor
     *
     * @return int
     * @throws \Exception
     */
	public function actionAssignAdminUserPermissions(int $userId, string $roleName = 'admin')
	{
		$query = Yii::$app->modelFactory->getUser()::find()->where(['type' => UserType::ADMIN]);
		$query->andWhere(['id' => $userId]);

        if (!$role = Yii::$app->authManager->getRole($roleName)) {
            throw new InvalidArgumentException('Role ' . $roleName . ' not found');
        }
		foreach ($query->batch() as $users) {
			/**
			 * @var User[] $users
			 */
			foreach ($users as $user) {
			    // remove existing role(s)
                Yii::$app->db->createCommand()->delete(Yii::$app->authManager->assignmentTable, ['user_id' => $user->id])->execute();
				// assign new role
                Yii::$app->authManager->assign($role, $user->id);

				$this->printLn('User ' . $user->id . ' permissions successfully regenerated', Console::FG_PURPLE);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}

	/**
	 * @return int
	 */
	public function actionAssignAdminUserVendor()
	{
		$query = Yii::$app->modelFactory->getUser()::find()->where(['type' => UserType::ADMIN]);
		$vendor = Yii::$app->modelFactory->getVendor()::findOne(1);
		foreach ($query->batch() as $users) {
			/**
			 * @var User[] $users
			 */
			foreach ($users as $user) {
				if ($vendor) {
					$vendor->addUser($user);
				}
				$this->printLn('User ' . $user->id . ' vendor ' . $vendor->id, Console::FG_PURPLE);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}

	public function actionGenRefNumbers()
	{
		$query = Yii::$app->modelFactory->getUser()::find();
		$query->where('reference_number IS NULL');

		foreach ($query->batch() as $users) {
			foreach ($users as $user) {
				/**
				 * @var User $user
				 */
				Yii::$app->queue1->push(new ReferenceNumberJob(['id' => $user->id]));
				$this->printLn($user->id . ' queued', Console::FG_GREY);
			}
		}
		$this->printLn('Done', Console::FG_GREEN);
		return ExitCode::OK;
	}
}
