<?php

namespace Phycom\Console\Controllers;


use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;

use yii\helpers\Console;
use yii\console\ExitCode;

use Yii;

/**
 * Class AddressController
 *
 * @package Phycom\Console\Controllers
 */
class AddressController extends BaseConsoleController
{

    /**
     * @return int
     * @throws \Geocoder\Exception\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGeocodeAllShops(): int
    {
        if (!Yii::$app->geocoder->isEnabled()) {
            $this->printLn('Geocoder is not enabled', Console::FG_YELLOW);
            return ExitCode::UNAVAILABLE;
        }

        $query = Address::find()->where('shop_id is not null')->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
        $total = $query->count();

        if ($this->confirm($total . ' items found continue?')) {
            Console::startProgress($i=0, $total);
            foreach ($query->batch() as $addresses) {
                /**
                 * @var Address[] $addresses
                 */
                foreach ($addresses as $address) {
                    $address->geocode(true);
                    Console::updateProgress(++$i, $total);
                }
            }
            Console::endProgress();
        }

        return ExitCode::OK;
    }
}
