<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M201022185617DeliveryAreaName extends Migration
{
    const TABLE = '{{%delivery_area}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'name', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'name');
    }

}
