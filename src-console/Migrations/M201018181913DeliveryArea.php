<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M201018181913DeliveryArea
 *
 * @package Phycom\Console\Migrations
 */
class M201018181913DeliveryArea extends Migration
{
    const TABLE = '{{%delivery_area}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'foc_threshold_amount', $this->decimal(13, 4));
        $this->addColumn(self::TABLE, 'settings', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'foc_threshold_amount');
        $this->dropColumn(self::TABLE, 'settings');
    }
}
