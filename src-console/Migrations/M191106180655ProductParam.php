<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M191106180655ProductParam extends Migration
{
    const TABLE = '{{%product_param}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'product_variant_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'value' => 'JSONB NOT NULL',
            'is_public' => $this->boolean()->defaultValue(false),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
            'updated_at' => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey('fk_product_param_created_by', self::TABLE, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_product_param_updated_by', self::TABLE, 'updated_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_product_param_product_id', self::TABLE, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');

        $this->db->createCommand('CREATE UNIQUE INDEX idx_product_param_name_3 ON ' . self::TABLE . ' ("product_id", "product_variant_id", "name") WHERE "product_variant_id" IS NOT NULL;')->execute();
        $this->db->createCommand('CREATE UNIQUE INDEX idx_product_param_name_2 ON ' . self::TABLE . ' ("product_id", "name") WHERE "product_variant_id" IS NULL;')->execute();
    }

    public function safeDown()
    {
        $this->dropIndex('idx_product_param_name_2', self::TABLE);
        $this->dropIndex('idx_product_param_name_3', self::TABLE);

        $this->dropForeignKey('fk_product_param_created_by', self::TABLE);
        $this->dropForeignKey('fk_product_param_updated_by', self::TABLE);
        $this->dropForeignKey('fk_product_param_product_id', self::TABLE);
        $this->dropTable(self::TABLE);
    }

}
