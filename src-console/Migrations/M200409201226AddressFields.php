<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M200409201226AddressFields
 *
 * @package Phycom\Console\Migrations
 */
class M200409201226AddressFields extends Migration
{
    const TABLE_NAME = '{{%address}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'house', $this->string());
        $this->addColumn(self::TABLE_NAME, 'room', $this->string());

        $this->createIndex('idx_address_province', self::TABLE_NAME, 'province');
        $this->createIndex('idx_address_city', self::TABLE_NAME, 'city');
        $this->createIndex('idx_address_locality', self::TABLE_NAME, 'locality');
        $this->createIndex('idx_address_house', self::TABLE_NAME, 'house');
        $this->createIndex('idx_address_room', self::TABLE_NAME, 'room');
        $this->createIndex('idx_address_postcode', self::TABLE_NAME, 'postcode');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_address_province', self::TABLE_NAME);
        $this->dropIndex('idx_address_city', self::TABLE_NAME);
        $this->dropIndex('idx_address_locality', self::TABLE_NAME);
        $this->dropIndex('idx_address_house', self::TABLE_NAME);
        $this->dropIndex('idx_address_room', self::TABLE_NAME);
        $this->dropIndex('idx_address_postcode', self::TABLE_NAME);

        $this->dropColumn(self::TABLE_NAME, 'house');
        $this->dropColumn(self::TABLE_NAME, 'room');
    }

}
