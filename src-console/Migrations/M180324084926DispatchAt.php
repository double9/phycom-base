<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180324084926DispatchAt extends Migration
{
    const TABLE_NAME = '{{%shipment}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'dispatch_at', 'timestamptz');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'dispatch_at');
    }

}
