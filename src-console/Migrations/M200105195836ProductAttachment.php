<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M200105195836ProductAttachment extends Migration
{
    const TABLE = '{{%product_attachment}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE, 'is_primary', 'is_visible');
    }

    public function safeDown()
    {
        $this->renameColumn(self::TABLE, 'is_visible', 'is_primary');
    }

}
