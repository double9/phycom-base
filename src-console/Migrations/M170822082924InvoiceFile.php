<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170822082924InvoiceFile extends Migration
{
	const TABLE_NAME = '{{%invoice}}';

    public function safeUp()
    {
	    $this->alterColumn(self::TABLE_NAME, 'file', 'DROP NOT NULL'); //for drop not null
	    $this->alterColumn(self::TABLE_NAME, 'file', 'SET DEFAULT NULL'); //for set default null value
    }

    public function safeDown()
    {

    }
}
