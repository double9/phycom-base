<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170408212036Phone extends Migration
{

    public function safeUp()
    {
	    $table = '{{%phone}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer(),
		    'vendor_id' => $this->integer(),
		    'shop_id' => $this->integer(),
		    'country_code' => $this->string(10)->notNull(),
		    'phone_nr' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_phone_user', $table, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_phone_vendor', $table, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_phone_shop', $table, 'shop_id', 'shop', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_phone_user', '{{%phone}}');
	    $this->dropForeignKey('fk_phone_vendor', '{{%phone}}');
	    $this->dropForeignKey('fk_phone_shop', '{{%phone}}');
	    $this->dropTable('{{%phone}}');
    }
}
