<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180330123403DeliveryAreaCarrier extends Migration
{

    const TABLE_NAME = '{{%delivery_area}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'carrier', 'DROP NOT NULL'); //for drop not null

    }

    public function safeDown()
    {
        echo "M180330_123403_delivery_area_carrier cannot be reverted.\n";
        return false;
    }

}
