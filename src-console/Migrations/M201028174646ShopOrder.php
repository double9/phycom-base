<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M201028174646ShopOrder extends Migration
{
    const TABLE_NAME = '{{%shop}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'order', $this->smallInteger());
        $this->update(self::TABLE_NAME, ['order' => 1], '"order" is null');
        $this->alterColumn(self::TABLE_NAME, 'order', $this->smallInteger()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'order');
    }
}
