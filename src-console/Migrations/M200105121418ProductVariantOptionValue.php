<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M200105121418ProductVariantOptionValue extends Migration
{
    const TABLE = '{{%product_variant_option}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'default_value', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'default_value');
    }

}
