<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;
use yii\db\Schema;
use Phycom\Base\Models\AddressMeta;

class M171015022718RefactorDeliveryModule extends Migration
{

    public function safeUp()
    {
        $this->renameColumn('delivery_area', 'courier', 'carrier');
        $this->addColumn('delivery_area', 'method', Schema::TYPE_STRING);

        $reader = $this->db->createCommand('SELECT * FROM "delivery_area" WHERE status != \'deleted\'')->query();
        while ($row = $reader->read()) {
            $this->update('delivery_area', ['method' => $row['carrier']], 'id = :id', ['id' => $row['id']]);
        }

        $this->renameColumn('shipment', 'courier_name', 'method');
        $this->renameColumn('shipment', 'courier_code', 'reference_id');
        $this->renameColumn('shipment', 'courier_service', 'carrier_service');
        $this->renameColumn('shipment', 'courier_area', 'carrier_area');
        $this->renameColumn('shipment', 'courier_delivery_time', 'carrier_delivery_time');
        $this->renameColumn('shipment', 'destination_address', 'to_address');

        $this->addColumn('shipment', 'from_address', 'jsonb');
        $this->addColumn('shipment', 'return_address', 'jsonb');
        $this->addColumn('shipment', 'carrier_name', Schema::TYPE_STRING);
        $this->addColumn('shipment', 'carrier_delivery_days', Schema::TYPE_INTEGER);
        $this->addColumn('shipment', 'postage_label', 'jsonb');

        $reader = $this->db->createCommand('SELECT * FROM shipment')->query();
        while ($row = $reader->read()) {
            $address = json_decode($row['to_address'], true);
            if (isset($row['recipient_name'])) {
                $address['name'] = $row['recipient_name'];
            }
            if (isset($row['recipient_phone'])) {
                $address['phone'] = $row['recipient_phone'];
            }
            $this->update('shipment', ['to_address' => json_encode($address)], 'id = :id', ['id' => $row['id']]);
        }

        $this->dropColumn('shipment', 'recipient_name');
        $this->dropColumn('shipment', 'recipient_phone');

        $reader = $this->db->createCommand('SELECT * FROM "order" WHERE status != \'deleted\'')->query();
        while ($row = $reader->read()) {
            $shipments = $this->db->createCommand('SELECT * FROM shipment WHERE order_id = :order_id', ['order_id' => $row['id']])->queryAll();
            if (empty($shipments)) {
                $time = \Phycom\Base\Helpers\Date::create('now')->dbTimestamp;
                $this->insert('shipment', [
                    'order_id' => $row['id'],
                    'method' => $row['delivery_service'],
                    'carrier_area' => $row['delivery_area'],
                    'to_address' => $row['delivery_address'],
                    'status' => 'assembled',
                    'type' => 'delivery',
                    'comment' => 'migrated',
                    'created_at' => $time,
                    'updated_at' => $time
                ]);
            }
        }

        $this->dropColumn('order', 'delivery_service');
        $this->dropColumn('order', 'delivery_area');
        $this->dropColumn('order', 'delivery_address');
        $this->dropColumn('order', 'delivery_time');
    }

    public function safeDown()
    {
        $this->addColumn('shipment', 'recipient_name', Schema::TYPE_STRING);
        $this->addColumn('shipment', 'recipient_phone', Schema::TYPE_STRING);

        $this->addColumn('order', 'delivery_service', Schema::TYPE_STRING);
        $this->addColumn('order', 'delivery_area', Schema::TYPE_STRING);
        $this->addColumn('order', 'delivery_address', 'jsonb');
        $this->addColumn('order', 'delivery_time', 'timestamptz');

        $reader = $this->db->createCommand('SELECT * FROM shipment')->query();
        while ($row = $reader->read()) {
            $address = json_decode($row['to_address'], true);
            $name = $address['name'] ?? '';
            $phone = $address['phone'] ?? '';

            $this->update('shipment', [
                'recipient_name' => $name,
                'recipient_phone' => $phone
            ], 'id = :id', ['id' => $row['id']]);
        }

        $this->dropColumn('shipment', 'from_address');
        $this->dropColumn('shipment', 'return_address');
        $this->dropColumn('shipment', 'carrier_delivery_days');
        $this->dropColumn('shipment', 'carrier_name');
        $this->dropColumn('shipment', 'postage_label');

        $this->renameColumn('shipment', 'method', 'courier_name');
        $this->renameColumn('shipment', 'carrier_service', 'courier_service');
        $this->renameColumn('shipment', 'reference_id', 'courier_code');
        $this->renameColumn('shipment', 'carrier_area', 'courier_area');
        $this->renameColumn('shipment', 'carrier_delivery_time', 'courier_delivery_time');
        $this->renameColumn('shipment', 'to_address', 'destination_address');

        $reader = $this->db->createCommand('SELECT * FROM "delivery_area" WHERE status != \'deleted\'')->query();
        while ($row = $reader->read()) {
            $this->update('delivery_area', ['carrier' => $row['method']], 'id = :id', ['id' => $row['id']]);
        }

        $this->dropColumn('delivery_area', 'method');
        $this->renameColumn('delivery_area', 'carrier', 'courier');
    }
}
