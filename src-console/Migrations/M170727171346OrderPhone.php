<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;
use yii\db\Schema;

class M170727171346OrderPhone extends Migration
{
	const TABLE_NAME = '{{%order}}';

	public function safeUp()
	{
		$this->addColumn(self::TABLE_NAME, 'phone_number', Schema::TYPE_STRING);
		$this->addColumn(self::TABLE_NAME, 'phone_code', Schema::TYPE_STRING);
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE_NAME, 'phone_number');
		$this->dropColumn(self::TABLE_NAME, 'phone_code');
	}

}
