<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M181019022338OrderLanguage extends Migration
{
    const TBL = '{{%order}}';

    public function safeUp()
    {
        $this->addColumn(self::TBL, 'language_code', $this->char(2));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TBL, 'language_code');
    }
}
