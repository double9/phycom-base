<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M171125141259AddressState extends Migration
{
    const TABLE_NAME = '{{%address}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'state', \yii\db\Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'state');
    }

}
