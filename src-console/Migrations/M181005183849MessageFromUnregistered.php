<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M181005183849MessageFromUnregistered extends Migration
{
    const TBL = '{{%message}}';

    public function safeUp()
    {
        $this->addColumn(self::TBL, 'from', $this->string());
        $this->addColumn(self::TBL, 'from_name', $this->string());
        $this->alterColumn(self::TBL, 'user_from', 'DROP NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TBL, 'from');
        $this->dropColumn(self::TBL, 'from_name');
    }

}
