<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M191118183522ProductPriceVariation extends Migration
{
    const TABLE = '{{%product_price_variation}}';

    public function safeUp()
    {
        $this->renameTable('{{%product_price_attribute}}', self::TABLE);

        $this->renameColumn(self::TABLE, 'attribute', 'variant_name');
        $this->renameColumn(self::TABLE, 'option', 'option_key');

        $this->cmd('ALTER TABLE ' . self::TABLE . ' RENAME CONSTRAINT fk_product_price_attribute_product_price TO fk_product_price_variation_product_price');
        $this->dropIndex('idx_product_price_attribute', self::TABLE);

        $this->cmd('CREATE UNIQUE INDEX idx_product_price_variation_3 ON ' . self::TABLE . ' ("product_price_id", "variant_name", "option_key") WHERE "option_key" IS NOT NULL;');
        $this->cmd('CREATE UNIQUE INDEX idx_product_price_variation_2 ON ' . self::TABLE . ' ("product_price_id", "variant_name") WHERE "option_key" IS NULL;');

        $this->renameColumn('{{%product_price}}', 'discount_price', 'discount_amount');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%product_price}}', 'discount_amount', 'discount_price');

        $this->dropIndex('idx_product_price_variation_2', self::TABLE);
        $this->dropIndex('idx_product_price_variation_3', self::TABLE);

        $this->createIndex('idx_product_price_attribute', self::TABLE, ['product_price_id', 'variant_name', 'option_key'], true);
        $this->cmd('ALTER TABLE ' . self::TABLE . ' RENAME CONSTRAINT fk_product_price_variation_product_price TO fk_product_price_attribute_product_price');

        $this->renameColumn(self::TABLE, 'variant_name', 'attribute');
        $this->renameColumn(self::TABLE, 'option_key', 'option');

        $this->renameTable(self::TABLE, '{{%product_price_attribute}}');
    }

}
