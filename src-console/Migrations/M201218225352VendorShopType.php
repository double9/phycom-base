<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;


class M201218225352VendorShopType extends Migration
{
    const TABLE_SHOP = '{{%shop}}';
    const TABLE_VENDOR = '{{%vendor}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_SHOP, 'type', $this->string());
        $this->addColumn(self::TABLE_VENDOR, 'type', $this->string());
        $this->addColumn(self::TABLE_VENDOR, 'main', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_VENDOR, 'main');
        $this->dropColumn(self::TABLE_SHOP, 'type');
        $this->dropColumn(self::TABLE_VENDOR, 'type');
    }
}
