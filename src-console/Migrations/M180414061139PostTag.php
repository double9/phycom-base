<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180414061139PostTag extends Migration
{

    const TABLE_NAME = '{{%post_tag}}';
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey('fk_post_tag_created_by', self::TABLE_NAME, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_post_tag_post_id', self::TABLE_NAME, 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_post_tag_post_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_post_tag_created_by', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

}
