<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170701090242ProductProperties extends Migration
{
	const TABLE_NAME = '{{%product}}';

    public function safeUp()
    {
    	$this->renameColumn(self::TABLE_NAME, 'data', 'properties');
    }

    public function safeDown()
    {
	    $this->renameColumn(self::TABLE_NAME, 'properties', 'data');
    }

}
