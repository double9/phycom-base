<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M171216184128PostKey extends Migration
{
    const TABLE_NAME = '{{%post}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'url_key', 'key');
        $this->createIndex('idx_post_key', self::TABLE_NAME, 'key', true);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_post_key', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'key', 'url_key');
    }

}
