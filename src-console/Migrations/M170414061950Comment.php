<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170414061950Comment extends Migration
{

    public function safeUp()
    {
	    $table = '{{%comment}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_id' => $this->integer(),
		    'shop_id' => $this->integer(),
		    'post_id' => $this->integer(),
		    'content' => $this->text()->notNull(),
		    'status' => $this->string()->notNull(),
		    'author_name' => $this->string()->notNull(),
		    'author_email' => $this->string(),
		    'author_ip' => $this->string()->notNull(),
		    'author_agent' => $this->text(),
		    'created_by' => $this->integer(),
		    'parent_id' => $this->integer(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_comment_parent', $table, 'parent_id', $table, 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_comment_user', $table, 'created_by', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_comment_product', $table, 'product_id', 'product', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_comment_shop', $table, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_comment_post', $table, 'post_id', 'post', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_comment_parent', '{{%comment}}');
	    $this->dropForeignKey('fk_comment_user', '{{%comment}}');
	    $this->dropForeignKey('fk_comment_product', '{{%comment}}');
	    $this->dropForeignKey('fk_comment_shop', '{{%comment}}');
	    $this->dropForeignKey('fk_comment_post', '{{%comment}}');
	    $this->dropTable('{{%comment}}');
    }

}
