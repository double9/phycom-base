<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M201104191035OrderItemUnits
 *
 * @package Phycom\Console\Migrations
 */
class M201104191035OrderItemUnits extends Migration
{
    const TABLE = '{{%order_item}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'num_units', $this->decimal(6, 2));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'num_units');
    }
}
