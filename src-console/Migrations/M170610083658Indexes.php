<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170610083658Indexes extends Migration
{

    public function safeUp()
    {
	    $this->createIndex('idx_user_created_at', '{{%user}}', 'created_at');
	    $this->createIndex('idx_email_updated_at', '{{%email}}', 'updated_at');
    }

    public function safeDown()
    {
	    $this->dropIndex('idx_user_created_at', '{{%user}}');
	    $this->dropIndex('idx_email_updated_at', '{{%email}}');
    }

}
