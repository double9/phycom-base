<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M201124222649OrderItemPriceData
 *
 * @package Phycom\Console\Migrations
 */
class M201124222649OrderItemPriceData extends Migration
{
    const TABLE = '{{%order_item}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'price_data', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'price_data');
    }
}
