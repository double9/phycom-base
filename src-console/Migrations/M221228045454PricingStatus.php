<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M221228045454PricingStatus extends Migration
{

    public function safeUp()
    {
        $this->addColumn('product_price', 'can_purchase', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('product_price', 'can_purchase');
    }

}
