<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M210121210822InvoiceChecksum extends Migration
{

    public function safeUp()
    {
        $this->addColumn('invoice', 'checksum', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'checksum');
    }

}
