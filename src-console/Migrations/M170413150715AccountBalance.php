<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170413150715AccountBalance extends Migration
{
    public function safeUp()
    {
	    $table = '{{%account_balance}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'user_id' => $this->integer()->notNull(),
		    'payment_id' => $this->integer(),
		    'amount' => $this->decimal(13, 4)->notNull()->defaultValue(0),
		    'created_at' => 'TIMESTAMPTZ NOT NULL'
	    ], null);

	    $this->addForeignKey('fk_account_balance_user', $table, 'user_id', 'user', 'id', 'SET NULL', 'CASCADE');
	    $this->addForeignKey('fk_account_balance_payment', $table, 'payment_id', 'payment', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_account_balance_user', '{{%account_balance}}');
	    $this->dropForeignKey('fk_account_balance_payment', '{{%account_balance}}');
	    $this->dropTable('{{%account_balance}}');
    }
}
