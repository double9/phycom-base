<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170413144744Payment extends Migration
{

    public function safeUp()
    {
	    $table = '{{%payment}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'invoice_id' => $this->integer()->notNull(),
		    'payment_method' => $this->string()->notNull(),
		    'reference_number' => $this->string(),
		    'remitter' => $this->string(),
		    'amount' => $this->decimal(13, 4)->notNull(),
		    'status' => $this->string()->notNull(),
		    'explanation' => $this->text(),
		    'transaction_time' => 'TIMESTAMPTZ',
		    'transaction_data' => 'jsonb',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_payment_invoice', $table, 'invoice_id', 'invoice', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_payment_invoice', '{{%payment}}');
	    $this->dropTable('{{%payment}}');
    }

}
