<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180523180528PostTranslationMeta extends Migration
{
    const TABLE_NAME = '{{%post_translation}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'meta', 'JSONB');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta');
    }

}
