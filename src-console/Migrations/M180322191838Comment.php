<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180322191838Comment extends Migration
{
    const TABLE_COMMENT = '{{%comment}}';
    const TABLE_PRODUCT_COMMENT = '{{%product_comment}}';
    const TABLE_POST_COMMENT = '{{%post_comment}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_PRODUCT_COMMENT, [
            'product_id' => $this->integer()->notNull(),
            'comment_id' => $this->integer()->notNull()
        ], null);
        $this->addPrimaryKey('pk_product_comment', self::TABLE_PRODUCT_COMMENT, ['product_id','comment_id']);
        $this->addForeignKey('fk_product_comment_product', self::TABLE_PRODUCT_COMMENT, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_comment_comment', self::TABLE_PRODUCT_COMMENT, 'comment_id', self::TABLE_COMMENT, 'id', 'CASCADE', 'CASCADE');

        $reader = $this->db->createCommand('SELECT * FROM ' . self::TABLE_COMMENT . ' c WHERE c.product_id IS NOT NULL')->query();
        while ($row = $reader->read()) {
            $this->insert(self::TABLE_PRODUCT_COMMENT, ['product_id' => $row['product_id'], 'comment_id' => $row['id']]);
        }

        $this->dropColumn(self::TABLE_COMMENT, 'product_id');



        $this->createTable(self::TABLE_POST_COMMENT, [
            'post_id' => $this->integer()->notNull(),
            'comment_id' => $this->integer()->notNull()
        ], null);
        $this->addPrimaryKey('pk_post_comment', self::TABLE_POST_COMMENT, ['post_id','comment_id']);
        $this->addForeignKey('fk_post_comment_post', self::TABLE_POST_COMMENT, 'post_id', 'post', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_post_comment_comment', self::TABLE_POST_COMMENT, 'comment_id', self::TABLE_COMMENT, 'id', 'CASCADE', 'CASCADE');

        $reader = $this->db->createCommand('SELECT * FROM ' . self::TABLE_COMMENT . ' c WHERE c.post_id IS NOT NULL')->query();
        while ($row = $reader->read()) {
            $this->insert(self::TABLE_POST_COMMENT, ['post_id' => $row['post_id'], 'comment_id' => $row['id']]);
        }

        $this->dropColumn(self::TABLE_COMMENT, 'post_id');
        $this->dropColumn(self::TABLE_COMMENT, 'shop_id');

        $this->addColumn(self::TABLE_COMMENT, 'approved_by', \yii\db\Schema::TYPE_INTEGER);
        $this->addForeignKey('fk_comment_approved_by', self::TABLE_COMMENT, 'approved_by', 'user', 'id', 'SET NULL', 'CASCADE');

    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_COMMENT, 'product_id', \yii\db\Schema::TYPE_INTEGER);
        $this->addColumn(self::TABLE_COMMENT, 'shop_id', \yii\db\Schema::TYPE_INTEGER);
        $this->addColumn(self::TABLE_COMMENT, 'post_id', \yii\db\Schema::TYPE_INTEGER);

        $this->addForeignKey('fk_comment_product', self::TABLE_COMMENT, 'product_id', 'product', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_comment_shop', self::TABLE_COMMENT, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_comment_post', self::TABLE_COMMENT, 'post_id', 'post', 'id', 'SET NULL', 'CASCADE');



        $reader = $this->db->createCommand('SELECT * FROM ' . self::TABLE_PRODUCT_COMMENT)->query();
        while ($row = $reader->read()) {
            $this->update(self::TABLE_COMMENT, ['product_id' => $row['product_id']], 'id = :comment_id', ['comment_id' => $row['comment_id']]);
        }
        $this->dropTable(self::TABLE_PRODUCT_COMMENT . ' CASCADE');



        $reader = $this->db->createCommand('SELECT * FROM ' . self::TABLE_POST_COMMENT)->query();
        while ($row = $reader->read()) {
            $this->update(self::TABLE_COMMENT, ['post_id' => $row['post_id']], 'id = :comment_id', ['comment_id' => $row['comment_id']]);
        }
        $this->dropTable(self::TABLE_POST_COMMENT . ' CASCADE');

        $this->dropForeignKey('fk_comment_approved_by', self::TABLE_COMMENT);
        $this->dropColumn(self::TABLE_COMMENT, 'approved_by');
    }

}
