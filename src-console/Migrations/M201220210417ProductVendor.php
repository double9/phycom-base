<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M201220210417ProductVendor
 *
 * @package Phycom\Console\Migrations
 */
class M201220210417ProductVendor extends Migration
{
    const TABLE_NAME = '{{%product_in_vendor}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'product_id' => $this->integer()->notNull(),
            'vendor_id'  => $this->integer()->notNull(),
            'created_at' => 'TIMESTAMPTZ NOT NULL',
        ]);
        $this->addPrimaryKey('pk_product_in_vendor', self::TABLE_NAME, ['product_id', 'vendor_id']);
        $this->addForeignKey('fk_product_in_vendor_vendor', self::TABLE_NAME, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_product_in_vendor_product', self::TABLE_NAME, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');

        $this->alterColumn('vendor', 'legal_name', 'DROP NOT NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_in_vendor_product', self::TABLE_NAME);
        $this->dropForeignKey('fk_product_in_vendor_vendor', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

}
