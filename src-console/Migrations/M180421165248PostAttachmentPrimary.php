<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180421165248PostAttachmentPrimary extends Migration
{
    const TABLE_NAME = '{{%post_attachment}}';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_primary');
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, 'is_primary', $this->boolean()->defaultValue(false));
    }

}
