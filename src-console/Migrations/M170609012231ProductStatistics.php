<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170609012231ProductStatistics extends Migration
{

    public function safeUp()
    {
	    $table = '{{%product_statistics}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'product_id' => $this->integer()->notNull(),
		    'num_transactions' => $this->integer()->notNull()->defaultValue(0),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey('fk_product_statistics_product', $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_product_statistics_product', '{{%product_statistics}}');
	    $this->dropTable('{{%product_statistics}}');
    }

}
