<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;
use yii\db\pgsql\Schema;

class M180420155243UserMeta extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'meta', Schema::TYPE_JSONB);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta');
    }

}
