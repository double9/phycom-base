<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170408221431Currency extends Migration
{

    public function safeUp()
    {
	    $table = '{{%currency}}';
	    $this->createTable($table, [
		    'code' => $this->char(3),
		    'name' => $this->string()->notNull(),
		    'symbol' => $this->string()
	    ], null);
	    $this->addPrimaryKey('pk_currency_code', $table, ['code']);
	    $this->populateData();
    }

    public function safeDown()
    {
	    $this->dropTable('{{%currency}}');
    }

    public function populateData()
    {
	    $this->batchCmd('
            INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AFN\', \'Afghani\', \'؋\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ALL\', \'Lek\', \'Lek\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'DZD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AOA\', \'Kwanza\', \'Kz\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ARS\', \'Peso\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AMD\', \'Dram\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AWG\', \'Guilder\', \'ƒ\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AZN\', \'Manat\', \'ман\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BSD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BHD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BDT\', \'Taka\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BBD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BYR\', \'Ruble\', \'p.\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BZD\', \'Dollar\', \'BZ$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BMD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BTN\', \'Ngultrum\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BOB\', \'Boliviano\', \'$b\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BAM\', \'Marka\', \'KM\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BWP\', \'Pula\', \'P\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BRL\', \'Real\', \'R$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BND\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BGN\', \'Lev\', \'лв\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'BIF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KHR\', \'Riels\', \'៛\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CAD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CVE\', \'Escudo\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KYD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CLP\', \'Peso\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CNY\', \'Yuan Renminbi\', \'¥\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'COP\', \'Peso\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KMF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CRC\', \'Colon\', \'₡\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'HRK\', \'Kuna\', \'kn\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CUP\', \'Peso\', \'₱\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CYP\', \'Pound\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CZK\', \'Koruna\', \'Kč\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'DJF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'DOP\', \'Peso\', \'RD$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'EGP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SVC\', \'Colone\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ERN\', \'Nakfa\', \'Nfk\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'EEK\', \'Kroon\', \'kr\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ETB\', \'Birr\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'FKP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'FJD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GMD\', \'Dalasi\', \'D\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GEL\', \'Lari\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GHC\', \'Cedi\', \'¢\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GIP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'DKK\', \'Krone\', \'kr\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GTQ\', \'Quetzal\', \'Q\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GNF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GYD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'HTG\', \'Gourde\', \'G\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'HNL\', \'Lempira\', \'L\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'HKD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'HUF\', \'Forint\', \'Ft\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ISK\', \'Krona\', \'kr\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'INR\', \'Rupee\', \'₹\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'IDR\', \'Rupiah\', \'Rp\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'IRR\', \'Rial\', \'﷼\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'IQD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'JMD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'JPY\', \'Yen\', \'¥\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'JOD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KZT\', \'Tenge\', \'лв\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KES\', \'Shilling\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KWD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KGS\', \'Som\', \'лв\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LAK\', \'Kip\', \'₭\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LVL\', \'Lat\', \'Ls\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LBP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LSL\', \'Loti\', \'L\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LRD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LYD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LTL\', \'Litas\', \'Lt\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MOP\', \'Pataca\', \'MOP\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MKD\', \'Denar\', \'ден\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MGA\', \'Ariary\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MWK\', \'Kwacha\', \'MK\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MYR\', \'Ringgit\', \'RM\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MVR\', \'Rufiyaa\', \'Rf\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MTL\', \'Lira\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MRO\', \'Ouguiya\', \'UM\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MUR\', \'Rupee\', \'₨\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MXN\', \'Peso\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MDL\', \'Leu\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MNT\', \'Tugrik\', \'₮\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MZN\', \'Meticail\', \'MT\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MMK\', \'Kyat\', \'K\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NAD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NPR\', \'Rupee\', \'₨\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ANG\', \'Guilder\', \'ƒ\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NIO\', \'Cordoba\', \'C$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NGN\', \'Naira\', \'₦\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'KPW\', \'Won\', \'₩\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'OMR\', \'Rial\', \'﷼\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PKR\', \'Rupee\', \'₨\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ILS\', \'Shekel\', \'₪\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PAB\', \'Balboa\', \'B/.\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PGK\', \'Kina\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PYG\', \'Guarani\', \'Gs\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PEN\', \'Sol\', \'S/.\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PHP\', \'Peso\', \'Php\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'PLN\', \'Zloty\', \'zł\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'QAR\', \'Rial\', \'﷼\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'XAF\', \'Franc\', \'FCF\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'RON\', \'Leu\', \'lei\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'RUB\', \'Ruble\', \'руб\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'RWF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SHP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'XCD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'WST\', \'Tala\', \'WS$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'STD\', \'Dobra\', \'Db\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SAR\', \'Rial\', \'﷼\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'RSD\', \'Dinar\', \'Дин\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SCR\', \'Rupee\', \'₨\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SLL\', \'Leone\', \'Le\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SGD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SKK\', \'Koruna\', \'Sk\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SBD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SOS\', \'Shilling\', \'S\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ZAR\', \'Rand\', \'R\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'LKR\', \'Rupee\', \'₨\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SDD\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SRD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NOK\', \'Krone\', \'kr\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SZL\', \'Lilangeni\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SEK\', \'Krona\', \'kr\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'CHF\', \'Franc\', \'CHF\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'SYP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TWD\', \'Dollar\', \'NT$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TJS\', \'Somoni\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TZS\', \'Shilling\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'THB\', \'Baht\', \'฿\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'XOF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'NZD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TOP\', \'Pa\'\'anga\', \'T$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TTD\', \'Dollar\', \'TT$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TND\', \'Dinar\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TRY\', \'Lira\', \'YTL\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'TMM\', \'Manat\', \'m\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AUD\', \'Dollar\', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'UGX\', \'Shilling\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'UAH\', \'Hryvnia\', \'₴\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'AED\', \'Dirham\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'GBP\', \'Pound\', \'£\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'USD\', \'Dollar \', \'$\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'UYU\', \'Peso\', \'$U\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'UZS\', \'Som\', \'лв\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'VUV\', \'Vatu\', \'Vt\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'EUR\', \'Euro\', \'€\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'VEF\', \'Bolivar\', \'Bs\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'VND\', \'Dong\', \'₫\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'XPF\', \'Franc\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'MAD\', \'Dirham\', NULL);
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'YER\', \'Rial\', \'﷼\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ZMK\', \'Kwacha\', \'ZK\');
INSERT INTO "currency" ("code", "name", "symbol") VALUES(\'ZWD\', \'Dollar\', \'Z$\');
        ');
    }
}
