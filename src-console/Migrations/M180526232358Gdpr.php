<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M180526232358Gdpr extends Migration
{
    const TBL_CONTRACT = 'partner_contract';
    const TBL_CONSENT = 'user_consent';

    public function safeUp()
    {
        $this->createTable(self::TBL_CONTRACT, [
            'id'                  => $this->primaryKey(),
            'created_by'          => $this->integer(),
            'company_name'        => $this->string()->notNull(),
            'address'             => 'JSONB',
            'phone'               => $this->string(),
            'contact_email'       => $this->string(),
            'category'            => $this->string(),
            'contract_start'      => 'TIMESTAMPTZ NOT NULL',
            'contract_end'        => 'TIMESTAMPTZ',
            'consent_required_on' => $this->string()->notNull(),
            'max_consent_days'    => $this->integer()->notNull(),
            'mandatory'           => $this->boolean()->notNull(),
            'created_at'          => 'TIMESTAMPTZ NOT NULL',
            'updated_at'          => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey('fk_partner_contract_created_by', self::TBL_CONTRACT, 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');


        $this->createTable(self::TBL_CONSENT, [
            'id'                  => $this->primaryKey(),
            'user_id'             => $this->integer()->notNull(),
            'partner_contract_id' => $this->integer()->notNull(),
            'consent'             => $this->boolean()->notNull(),
            'valid_for'           => $this->integer()->notNull(),
            'time'                => 'TIMESTAMPTZ NOT NULL',
            'created_at'          => 'TIMESTAMPTZ NOT NULL',
        ]);

        $this->addForeignKey('fk_user_consent_user', self::TBL_CONSENT, 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_consent_partner_contract', self::TBL_CONSENT, 'partner_contract_id', self::TBL_CONTRACT, 'id', 'SET NULL', 'CASCADE');
        $this->createIndex('idx_user_consent_time', self::TBL_CONSENT, 'time');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_consent_user', self::TBL_CONSENT);
        $this->dropForeignKey('fk_user_consent_partner_contract', self::TBL_CONSENT);
        $this->dropTable(self::TBL_CONSENT);

        $this->dropForeignKey('fk_partner_contract_created_by', self::TBL_CONTRACT);
        $this->dropTable(self::TBL_CONTRACT);
    }

}
