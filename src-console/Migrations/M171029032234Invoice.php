<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;
use yii\db\Schema;

class M171029032234Invoice extends Migration
{
    const TABLE_NAME = '{{%invoice}}';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, 'recipient', 'customer');
        $this->renameColumn(self::TABLE_NAME, 'recipient_type', 'customer_type');
        $this->addColumn(self::TABLE_NAME, 'due_date', 'timestamptz');
        $this->addColumn(self::TABLE_NAME, 'notes', Schema::TYPE_TEXT);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'due_date');
        $this->dropColumn(self::TABLE_NAME, 'notes');
        $this->renameColumn(self::TABLE_NAME, 'customer', 'recipient');
        $this->renameColumn(self::TABLE_NAME, 'customer_type', 'recipient_type');
    }
}
