<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

/**
 * Class M201102230146ProductVariantDefaults
 *
 * @package Phycom\Console\Migrations
 */
class M201102230146ProductVariantDefaults extends Migration
{

    const TABLE = '{{%product_variant}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE, 'user_select', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE, 'user_select', $this->boolean()->defaultValue(false));
    }

}
