<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170408163930Vendor extends Migration
{

    public function safeUp()
    {
	    $table = '{{%vendor}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'name' => $this->string()->notNull()->unique(),
		    'legal_name' => $this->string()->notNull(),
		    'reg_number' => $this->string(),
		    'status' => $this->string()->notNull(),
		    'options' => 'jsonb',
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);
    }

    public function safeDown()
    {
	    $this->dropTable('{{%vendor}}');
    }

}
