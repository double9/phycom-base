<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;
use yii\db\Schema;

class M171103233453PrimaryProductAttachment extends Migration
{
    const TABLE_NAME = '{{%product_attachment}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'is_primary', Schema::TYPE_BOOLEAN . ' DEFAULT false');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_primary');
    }
}
