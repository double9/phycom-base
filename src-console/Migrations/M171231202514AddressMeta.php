<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M171231202514AddressMeta extends Migration
{
    const TABLE_NAME = '{{%address_meta}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'first_name', \yii\db\Schema::TYPE_STRING);
        $this->addColumn(self::TABLE_NAME, 'last_name', \yii\db\Schema::TYPE_STRING);

        $this->moveColumns(self::TABLE_NAME, [
            'company' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'lat' => $this->decimal(10, 6),
            'lng' => $this->decimal(10, 6),
            'description' => $this->text(),
            'created_at' => 'TIMESTAMPTZ',
            'updated_at' => 'TIMESTAMPTZ'
        ]);

        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' ALTER COLUMN "created_at" SET NOT NULL;');
        $this->cmd('ALTER TABLE ' . self::TABLE_NAME . ' ALTER COLUMN "updated_at" SET NOT NULL;');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'first_name');
        $this->dropColumn(self::TABLE_NAME, 'last_name');
    }
}
