<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M201021160423ProductParamDefaultValues extends Migration
{
    const TABLE = '{{%product_param}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE, 'is_public', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE, 'is_public', $this->boolean()->defaultValue(false));
    }

}
