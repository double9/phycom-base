<?php

namespace Phycom\Console\Migrations;

use Phycom\Console\Models\Migration;

class M170414061947Post extends Migration
{

    public function safeUp()
    {
	    $table = '{{%post}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'created_by' => $this->integer()->notNull(),
		    'vendor_id' => $this->integer()->notNull(),
		    'shop_id' => $this->integer(),
		    'url_key' => $this->string()->unique()->notNull(),
		    'type' => $this->string()->notNull(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ], null);

	    $this->addForeignKey('fk_post_user', $table, 'created_by', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_post_vendor', $table, 'vendor_id', 'vendor', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('fk_post_shop', $table, 'shop_id', 'shop', 'id', 'SET NULL', 'CASCADE');

	    $table = '{{%post_translation}}';
	    $this->createTable($table, [
		    'id' => $this->primaryKey(),
		    'post_id' => $this->integer()->notNull(),
		    'language' => $this->char(2)->notNull(),
		    'url_key' => $this->string()->unique()->notNull(),
		    'title' => $this->text(),
		    'content' => $this->text(),
		    'meta_title' => $this->text(),
		    'meta_keywords' => $this->text(),
		    'meta_description' => $this->text(),
		    'status' => $this->string()->notNull(),
		    'created_at' => 'TIMESTAMPTZ NOT NULL',
		    'updated_at' => 'TIMESTAMPTZ NOT NULL',
	    ]);

	    $this->addForeignKey('fk_post_translation_language', $table, 'language', 'language', 'code', 'NO ACTION', 'CASCADE');
	    $this->addForeignKey('fk_post_translation_post', $table, 'post_id', 'post', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
	    $this->dropForeignKey('fk_post_translation_language', '{{%post_translation}}');
	    $this->dropForeignKey('fk_post_translation_post', '{{%post_translation}}');
	    $this->dropTable('{{%post_translation}}');

	    $this->dropForeignKey('fk_post_user', '{{%post}}');
	    $this->dropForeignKey('fk_post_vendor', '{{%post}}');
	    $this->dropForeignKey('fk_post_shop', '{{%post}}');
	    $this->dropTable('{{%post}}');
    }

}
