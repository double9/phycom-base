<?php

namespace Phycom\Console\Models;

use yii\db\ColumnSchemaBuilder;
use yii\db\Migration as BaseMigration;
use Yii;

/**
 * Base class for migrations.
 *
 * Allows to run migrations on multiple connections.
 *
 * Use "safeUp" and "safeDown" methods to specify migration data
 *
 * Class BaseMigration
 * @package Phycom\Console\Migrations
 */
class Migration extends BaseMigration
{

    /**
     * Default database connections used by migration.
     * Override this if needed.
     * @var array
     */
    protected $connections = ['db'];

    /**
     * @param $connection - yii\db\Connection
     * @param $callback - function to run inside transaction
     * @return bool
     * @throws yii\db\Exception
     */
    protected function run($connection, $callback)
    {
        $this->db = $connection;
        $this->db->open();
        $transaction = $this->db->beginTransaction();

        try {
            if (call_user_func($callback) === false) {
                $transaction->rollBack();

                return false;
            }
            $transaction->commit();

        } catch (\Exception $e) {
            echo "Exception: " . $e->getMessage() . ' (' . $e->getFile() . ':' . $e->getLine() . ")\n";
            echo $e->getTraceAsString() . "\n";
            $transaction->rollBack();

            return false;
        }
        return true;
    }

    /**
     * migrate up using a db connection
     *
     * @throws Yii\db\Exception
     */
    private function setup($con)
    {
        return $this->run(Yii::$app->$con, [$this, 'safeUp']);
    }

    /**
     * migrate down using a db connection
     *
     * @throws Yii\db\Exception
     */
    private function tearDown($con)
    {
        return $this->run(Yii::$app->$con, [$this, 'safeDown']);
    }

    /**
     * executes single sql statement
     *
     * @param string $sql
     * @param array $params
     * @param string $msg
     * @return int
     * @throws yii\db\Exception
     */
    protected function cmd( $sql, array $params = [], $msg = '')
    {
        $command = $this->db->createCommand($sql, $params);
    	echo '    > ' . ($msg ?: substr($command->sql, 0, 200) . ' ...') . PHP_EOL;
        return $command->execute();
    }

    /**
     * execute multiple sql statements
     *
     * @param string $sql
     * @throws yii\db\Exception
     */
    protected function batchCmd( $sql )
    {
        $commands = array_map('trim', explode("\n", $sql));
        foreach($commands as $cmd){

            if (strlen($cmd)) {
                $this->cmd( $cmd );
            }
        }
    }

    /**
     * migrate up across all connections
     *
     * @return bool|null
     * @throws Yii\db\Exception
     */
    public function up()
    {
        foreach ($this->connections as $connection) {
            if (!$this->setup($connection)) {
                return false;
            }
        }
        return null;
    }

    /**
     * migrate down across all connections
     *
     * @return bool|null
     * @throws Yii\db\Exception
     */
    public function down()
    {
        foreach ($this->connections as $connection) {
            if (!$this->tearDown($connection)) {
                return false;
            }
        }
        return null;
    }

    /**
     * Recreate enum in safe way for transactional migration
     *
     * @param string $name - enum name
     * @param array $values - enum values
     * @return int
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function safeEnumCreation($name, array $values)
    {
        $this->db->getTransaction()->begin();
        try{
            $this->dropEnum($name);
            $response = $this->enum($name,$values);
            $this->db->getTransaction()->commit();
        } catch(\Exception $e){
            $this->db->getTransaction()->rollBack();
            throw $e;
        }
        return $response;
    }

    /**
     * Remove enum
     *
     * @param string $type - enum name
     * @param bool $force - (optional) IF EXISTS property for SQL no error if not present
     * @return int
     * @throws yii\db\Exception
     */
    public function dropEnum($type, $force = true)
    {
        return $this->cmd(sprintf("DROP TYPE %s %s", $force? 'IF EXISTS': '',$type),[], "drop ENUM $type");
    }

    /**
     * Create postgres enum
     *
     * @param string $type - enum name
     * @param array $values - values
     * @return void
     * @throws yii\db\Exception
     */
    public function createEnum($type, array $values)
    {
        $this->cmd(sprintf("CREATE TYPE %s AS ENUM (%s)", $type, "'".implode("','",$values)."'"),[],"create ENUM $type");
    }

    /**
     * @param $type
     * @param array $values
     * @return ColumnSchemaBuilder
     * @throws yii\base\NotSupportedException
     * @throws yii\db\Exception
     */
    public function enum($type, array $values)
    {
    	$this->createEnum($type, $values);
	    return $this->getDb()->getSchema()->createColumnSchemaBuilder($type);
    }

    /**
     * @param string $tableName
     * @param array $columns
     */
    protected function moveColumns($tableName, array $columns)
    {
        foreach ($columns as $name => $type) {
            $this->addColumn($tableName, $name . '_n', $type);
        }
        foreach ($columns as $name => $type) {
            $this->update($tableName, [
                $name . '_n' => new \yii\db\Expression($name)
            ]);
        }
        foreach ($columns as $name => $type) {
            $this->dropColumn($tableName, $name);
        }
        foreach ($columns as $name => $type) {
            $this->renameColumn($tableName, $name . '_n', $name);
        }
    }
}
