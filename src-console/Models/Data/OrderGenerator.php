<?php

namespace Phycom\Console\Models\Data;


use Phycom\Base\Models\Attributes\CustomerType;
use Phycom\Base\Models\Attributes\InvoiceStatus;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Attributes\PaymentStatus;
use Phycom\Base\Models\Attributes\ShipmentStatus;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Models\InvoiceItem;
use Phycom\Base\Models\Order;
use Phycom\Base\Models\OrderItem;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Shipment;
use Phycom\Base\Models\User;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;

use Faker\Provider\Lorem;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class OrderGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class OrderGenerator extends BaseGenerator
{
    /**
     * @param int $numOrders
     * @param callable $onProgress
     * @throws \Throwable
     */
    public function generateOrders(int $numOrders, callable $onProgress)
    {
        $onProgress && $onProgress(null, 0, $numOrders);
        $shop = Yii::$app->modelFactory->getShop()::findOne(['status' => ShopStatus::OPEN]);
        $numProducts = Yii::$app->modelFactory->getProduct()::find()->count();
        for ($n = 0; $n < $numOrders; $n++) {

            $transaction = Yii::$app->db->beginTransaction();
            try {
                /**
                 * @var User $user
                 */
                $user = $this->getRandomUser();

                $order = Yii::$app->modelFactory->getOrder();
                $order->number = $this->generateRandomNumberFixedLength(10);
                $order->user_id = $user->id;
                if ($shop) {
                    $order->shop_id = $shop->id;
                }
                $order->comment = Lorem::text(100);
                $order->status = $this->pickRandomValue(OrderStatus::all(), [OrderStatus::DELETED]);

                if (in_array($order->status, [
                    OrderStatus::COMPLETE,
                    OrderStatus::SHIPPED,
                    OrderStatus::PROCESSING,
                    OrderStatus::PARTIALLY_SHIPPED
                ])) {
                    $paidDate = new \DateTime();
                    $paidDate->add(new \DateInterval('PT'  . rand(1,120) . 'S'));
                    $order->paid_at = $paidDate;
                }

                $order->off($order::EVENT_AFTER_STATUS_UPDATE);

                if (!$order->save()) {
                    throw new Exception('Error saving order: ' . Json::encode($order->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                $orderLines = rand(1, min($numProducts, 5));
                $products = Yii::$app->modelFactory->getProduct()::find()->orderBy('random()')->limit($orderLines)->all();
                /**
                 * @var Product[] $products
                 */
                foreach ($products as $product) {
                    /**
                     * @var OrderItem $line
                     */
                    $line = Yii::createObject(OrderItem::class);
                    $line->order_id = $order->id;
                    $line->product_id = $product->id;
                    $line->quantity = rand(1,3);
                    $line->price = $product->price ? $product->price->getPrice() : 999;
                    $line->code = $product->sku;
                    $line->calculateTotal();

                    if (!$line->save()) {
                        throw new Exception('Error saving order line: ' . Json::encode($line->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                    }
                }
                /**
                 * @var OrderItem $line
                 */
                $line = Yii::createObject(OrderItem::class);
                $line->order_id = $order->id;
                $line->quantity = 1;
                $line->price = 500;
                $line->code = 'DELIVERY';
                $line->meta = Json::encode(['title' => 'Delivery']);
                $line->calculateTotal();

                if (!$line->save()) {
                    throw new Exception('Error saving order line: ' . Json::encode($line->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }


                $invoice = Yii::$app->modelFactory->getInvoice();
                $invoice->order_id = $order->id;
                $invoice->customer = $order->getFullName();
                if ($order->paid_at) {
                    $invoice->status = InvoiceStatus::PAID;
                } else {
                    $invoice->status = InvoiceStatus::ISSUED;
                }

                if ($n % 5 == 0) {
                    $invoice->customer_type = CustomerType::COMPANY;
                    $invoice->reg_no = 'EE12345';
                    $invoice->address = $this->generateAddress()->exportArray();
                } else {
                    $invoice->customer_type = CustomerType::INDIVIDUAL;
                }

                $invoice->due_date = clone $order->created_at;
                $invoice->due_date->add(new \DateInterval('P7D'));

                if ($n % 8 == 0) {
                    $invoice->notes = $this->faker->sentence(rand(2,4));
                }

                $invoice->generateNumber();

                if (!$invoice->save()) {
                    throw new Exception('Error saving invoice: ' . Json::encode($invoice->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                foreach ($order->orderItems as $orderItem) {
                    /**
                     * @var InvoiceItem $invoiceItem
                     */
                    $invoiceItem = Yii::createObject(InvoiceItem::class);
                    $invoiceItem->invoice_id = $invoice->id;
                    $invoiceItem->order_item_id = $orderItem->id;
                    $invoiceItem->quantity = $orderItem->quantity;
                    if (!$invoiceItem->save()) {
                        throw new Exception('Error saving invoice item: ' . Json::encode($invoiceItem->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                    }
                }

                $invoice->createFile();

                /**
                 * @var PaymentMethodInterface[] $paymentMethods
                 */
                $paymentMethods = Yii::$app->getModule('payment')->getMethods(false);

                $payment = Yii::$app->modelFactory->getPayment();
                $payment->amount = $order->getTotal();
                $payment->transaction_id = $this->faker->uuid;
                $payment->remitter = $user->fullName;
                $payment->explanation = 'For Order ' . $order->id;
                $payment->payment_method = $this->pickRandomValue($paymentMethods)->getId();
                $payment->invoice_id = $invoice->id;


                if ($order->paid_at) {
                    $payment->transaction_time = clone $order->paid_at;
                    $payment->status = PaymentStatus::COMPLETED;
                } else if ($n % 5 == 0) {
                    $payment->status = PaymentStatus::CANCELED;
                } else if ($n % 6 == 0) {
                    $payment->status = PaymentStatus::FAILED;
                } else if ($n % 12 == 0) {
                    $payment->status = PaymentStatus::EXPIRED;
                } else if ($n % 13 == 0) {
                    $payment = null;
                } else {
                    $payment->status = PaymentStatus::PENDING;
                }

                if ($payment && !$payment->save()) {
                    throw new Exception('Error saving payment: ' . Json::encode($payment->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                $onProgress && $onProgress($order, $n + 1, $numOrders);
                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * TODO
     * @param Order $order
     * @param array $params
     * @return Shipment
     * @throws Exception
     */
    public function createShipment(Order $order, array $params = []) : Shipment
    {
        $shipment = Yii::$app->modelFactory->getShipment(ArrayHelper::merge([
            'status' => ShipmentStatus::PENDING,
        ], $params));

        $shipment->order_id = $order->id;

        if (!$shipment->save()) {
            throw new Exception('Error saving shipment: ' . Json::encode($shipment->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }
        return $shipment;
    }

}
