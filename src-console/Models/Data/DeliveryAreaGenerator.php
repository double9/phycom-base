<?php

namespace Phycom\Console\Models\Data;


use Phycom\Base\Modules\Delivery\Models\DeliveryArea;
use Phycom\Base\Modules\Delivery\Models\DeliveryAreaStatus;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class DeliveryAreaGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class DeliveryAreaGenerator extends BaseGenerator
{
    /**
     * @param int $numItems
     * @param callable $onProgress
     * @throws \Throwable
     */
    public function generateDeliveryAreas(int $numItems, callable $onProgress)
    {
        $onProgress && $onProgress(null, 0, $numItems);
        for ($n = 0; $n < $numItems; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $params = [];

                if ($n % 4 == 0) {
                    $params['price'] = 0;
                }

                $deliveryArea = $this->createDeliveryArea($params);

                $onProgress && $onProgress($deliveryArea, $n + 1, $numItems);
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param array $params
     * @return DeliveryArea
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createDeliveryArea(array $params = []) : DeliveryArea
    {
        $addressField = $this->generateAddress();
        $addressField->name = $this->faker->word();

        /**
         * @var DeliveryArea $area
         */
        $area = Yii::createObject(DeliveryArea::class, [ArrayHelper::merge([
            'status'  => DeliveryAreaStatus::ACTIVE,
            'price'   => rand(0, 2000),
            'service' => 'default',
            'area'    => $addressField->toArray(),
            'method'  => $this->pickRandomValue([DeliveryModule::METHOD_SELF_PICKUP, DeliveryModule::METHOD_COURIER])
        ], $params)]);

        if ($area->method === DeliveryModule::METHOD_COURIER) {
            $area->carrier = $this->pickRandomValue(['DHL','dpd','omniva','itella']);
            $area->service = $this->pickRandomValue(['next-day delivery','priority','standard','low-priority']);
        }

        if (!$area->area_code) {
            $area->area_code = $area->generateAreaCode();
        }
        if (!$area->code) {
            $area->code = $area->generateCode();
        }

        if (!$area->save()) {
            throw new Exception('Error saving delivery area: ' . Json::encode($area->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        return $area;
    }
}
