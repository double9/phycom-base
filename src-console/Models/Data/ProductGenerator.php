<?php

namespace Phycom\Console\Models\Data;


use Phycom\Base\Models\Attributes\CategoryStatus;
use Phycom\Base\Models\Attributes\CommentStatus;
use Phycom\Base\Models\Attributes\PriceUnitMode;
use Phycom\Base\Models\Attributes\ProductStatus;
use Phycom\Base\Models\Attributes\ReviewStatus;
use Phycom\Base\Models\Attributes\TranslationStatus;
use Phycom\Base\Models\Attributes\UnitType;
use Phycom\Base\Models\Comment;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\ProductAttachment;
use Phycom\Base\Models\Product\ProductCategory;
use Phycom\Base\Models\Product\ProductCategoryProductRelation;
use Phycom\Base\Models\Product\ProductTag;
use Phycom\Base\Models\Review;
use Phycom\Base\Models\Translation\ProductCategoryTranslation;
use Phycom\Base\Models\Translation\ProductTranslation;

use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class ProductGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class ProductGenerator extends BaseGenerator
{
    /**
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateProductCategories(callable $onProgress = null)
    {
        $count = 0;
        foreach ($this->productCategories as $parent => $subCategories) {

            $count++;
            $count += count($subCategories);
        }
        $onProgress && $onProgress(null, 0, $count);
        $i = 0;
        foreach ($this->productCategories as $parentCategoryTitle => $subCategories) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$category = $this->findCategory($parentCategoryTitle)) {
                    $category = $this->createCategory(null, ['title' => $parentCategoryTitle], CategoryStatus::VISIBLE);
                }
                $onProgress && $onProgress($category, ++$i, $count);

                foreach ($subCategories as $key => $subCategoryTitle) {
                    $status = ($key % 2 == 0) ? CategoryStatus::VISIBLE : null;
                    if (!$subCategory = $this->findCategory($subCategoryTitle)) {
                        $subCategory = $this->createCategory($category->id, ['title' => $subCategoryTitle], $status);
                    }
                    $onProgress && $onProgress($subCategory, ++$i, $count);
                }
                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param null $parentCategoryId
     * @param array $params
     * @param null $status
     * @return ProductCategory
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createCategory($parentCategoryId = null, array $params = [], $status = null)
    {
        /**
         * @var ProductCategory $category
         */
        $category = Yii::createObject(ProductCategory::class);
        $category->status = $status ?: $this->pickRandomValue(CategoryStatus::all(), [CategoryStatus::DELETED]);
        $category->parent_id = $parentCategoryId;

        if (!$category->save()) {
            throw new Exception('Error saving product category: ' . Json::encode($category->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        $mergedParams = ArrayHelper::merge([
            'language'         => Yii::$app->lang->default->code,
            'title'            => $this->faker->sentence(rand(3, 4)),
            'description'      => $this->faker->text(rand(200, 500)),
            'meta_description' => $this->faker->text(50),
            'status'           => TranslationStatus::PUBLISHED
        ], $params);

        /**
         * @var ProductCategoryTranslation $categoryTranslation
         */
        $categoryTranslation = Yii::createObject(ProductCategoryTranslation::class, [$mergedParams]);
        $categoryTranslation->language = Yii::$app->lang->default->code;
        $categoryTranslation->product_category_id = $category->id;
        $categoryTranslation->meta_title = $categoryTranslation->title;
        $categoryTranslation->generateUrlKey();

        if (!$categoryTranslation->save()) {
            throw new Exception('Error saving product translation: ' . Json::encode($categoryTranslation->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        return $category;
    }

    /**
     * @param int $numCategories
     * @param int|null $parentCategoryId
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateRandomProductCategories(int $numCategories, int $parentCategoryId = null, callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, $numCategories);

        for ($n = 0; $n < $numCategories; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                /**
                 * @var ProductCategory $category
                 */
                $category = Yii::createObject(ProductCategory::class);
                $category->status = $this->pickRandomValue(CategoryStatus::all(), [CategoryStatus::DELETED]);
                $category->parent_id = $parentCategoryId;

                if (!$category->save()) {
                    throw new Exception('Error saving product category: ' . Json::encode($category->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }
                /**
                 * @var ProductCategoryTranslation $categoryTranslation
                 */
                $categoryTranslation = Yii::createObject(ProductCategoryTranslation::class);
                $categoryTranslation->language = Yii::$app->lang->default->code;
                $categoryTranslation->product_category_id = $category->id;
                $categoryTranslation->title = $this->faker->sentence(rand(3,4));
                $categoryTranslation->description = $this->faker->text(200);
                $categoryTranslation->meta_title = $categoryTranslation->title;
                $categoryTranslation->meta_description = $this->faker->text(50);
                $categoryTranslation->status = TranslationStatus::PUBLISHED;
                $categoryTranslation->generateUrlKey();

                if (!$categoryTranslation->save()) {
                    throw new Exception('Error saving product translation: ' . Json::encode($categoryTranslation->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                $onProgress && $onProgress($category, $n + 1, $numCategories);
                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param int $numProducts
     * @param bool $generateImages
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateProducts(int $numProducts, bool $generateImages = true, callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, $numProducts);
        $categories = ArrayHelper::getColumn(ProductCategory::find()->all(), 'id');

        for ($n = 0; $n < $numProducts; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $product = Yii::$app->modelFactory->getProduct();
                $product->sku = $this->faker->ean8;
                $product->status = $this->pickRandomValue(ProductStatus::all(), [ProductStatus::DELETED]);
                $product->discount = $this->faker->boolean;
                $product->price_unit = UnitType::PIECE;
                $product->price_unit_mode = PriceUnitMode::STRICT;
                $product->vendor_id = Yii::$app->vendor->id;


                if (!$product->save()) {
                    throw new Exception('Error saving product: ' . Json::encode($product->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }
                /**
                 * @var ProductTranslation $productTranslation
                 */
                $productTranslation = Yii::createObject(ProductTranslation::class);
                $productTranslation->language = Yii::$app->lang->default->code;
                $productTranslation->product_id = $product->id;
                $productTranslation->title = $this->faker->sentence(rand(2,3));
                $productTranslation->outline = $this->faker->sentence();
                $productTranslation->description = $this->faker->text(rand(500,1000));
                $productTranslation->meta_title = $productTranslation->title;
                $productTranslation->meta_description = $this->faker->text(50);
                $productTranslation->status = TranslationStatus::PUBLISHED;
                $productTranslation->generateUrlKey();

                if (!$productTranslation->save()) {
                    throw new Exception('Error saving product translation: ' . Json::encode($productTranslation->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                if (!empty($categories)) {
                    $categoryId = $categories[array_rand($categories)];
                    /**
                     * @var ProductCategoryProductRelation $productInCategory
                     */
                    $productInCategory = Yii::createObject(ProductCategoryProductRelation::class);
                    $productInCategory->product_id = $product->id;
                    $productInCategory->category_id = $categoryId;
                    $productInCategory->created_by = 1;

                    if (!$productInCategory->save()) {
                        throw new Exception('Error saving product category relation: ' . Json::encode($productInCategory->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                    }
                }

                if ($n % 4 == 0) { // assign discounted tag for every 4th item
                    $this->createProductTag($product, ProductTag::TAG_DISCOUNTED);
                }
                if ($n % 5 == 0) { // assign random tag for every 5th item
                    $this->createProductTag($product, $this->pickRandomValue(ProductTag::all())->value);
                }
                if ($n % 8 == 0) { // assign featured tag for every 8th item
                    $this->createProductTag($product, ProductTag::TAG_FEATURED);
                }

                $price = Yii::$app->modelFactory->getProductPrice();
                $price->product_id = $product->id;
                $price->price = $this->faker->numberBetween(500, 9999);
                $price->num_units = 1;
                $price->unit_type = UnitType::PIECE;

                if ($n % 4 == 0) {
                    $price->price = $this->faker->numberBetween(1000, 20000);
                    $price->discount_amount = $this->faker->numberBetween(200, 1000);
                } else if ($n % 3 == 0) {
                    $price->price = $this->faker->numberBetween(388, 4000);
                } else if ($n % 11 == 0) {
                    $price->price = $this->faker->numberBetween(9999, 99999);
                }

                if (!$price->save()) {
                    throw new Exception('Error saving product price: ' . Json::encode($price->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                $generateImages && $this->generateAttachmentImages($product, rand(1, 4));

                if ($n % 2 == 0) {
                    // generate comments
                    $values = [1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,5,6,6,7,8,9,10,11,12,13,14];
                    $count = $values[array_rand($values)];

                    for ($i = 0; $i<$count; $i++) {
                        /**
                         * @var Comment $comment
                         */
                        $comment = Yii::createObject(Comment::class);
                        $comment->content = $this->faker->realText(300);
                        $comment->author_name = $this->faker->name();
                        $comment->author_email = $this->faker->email;
                        $comment->author_ip = $this->faker->ipv4;
                        $comment->author_agent = $this->faker->userAgent;
                        $comment->status = CommentStatus::APPROVED;
                        if ($i % 5) {
                            $comment->status = $this->pickRandomValue(CommentStatus::all(), [CommentStatus::DELETED]);
                        }
                        if (!$comment->save()) {
                            throw new Exception('Error saving product comment: ' . Json::encode($comment->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                        }
                    }

                }

                if ($n % 3 == 0) {
                    // generate reviews
                    $values = [1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,5,6,6,7,8,9,10,11,12,13,14];
                    $count = $values[array_rand($values)];

                    for ($i = 0; $i<$count; $i++) {
                        /**
                         * @var Review $review
                         */
                        $review = Yii::createObject(Review::class);
                        $review->score = rand(1, 5);
                        $review->title = $this->faker->realText(50);
                        $review->description = $this->faker->realText(500);
                        $review->status = ReviewStatus::APPROVED;
                        $review->approved_by = 1;
                        $review->verified_purchase = $this->faker->boolean();
                        $review->created_by = $this->getRandomUser()->id;

                        if ($i % 5) {
                            $review->status = $this->pickRandomValue(ReviewStatus::all(), [ReviewStatus::DELETED]);
                        }
                        if ((string) $review->status !== ReviewStatus::APPROVED) {
                            $review->approved_by = null;
                        }
                        if (!$review->save()) {
                            throw new Exception('Error saving product review: ' . Json::encode($review->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                        }
                    }
                }

                $onProgress && $onProgress($product, $n + 1, $numProducts);
                $transaction->commit();


            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param Product $product
     * @param int $numImages
     * @param bool $useFaker
     * @return ProductAttachment[]
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function generateAttachmentImages(Product $product, int $numImages = 1, bool $useFaker = false) : array
    {
        $productAttachments = [];
        for ($i = 0; $i<$numImages; $i++) {
            $file = $this->createAttachmentFile($useFaker);
            /**
             * @var ProductAttachment $productAttachment
             */
            $productAttachment = Yii::createObject(ProductAttachment::class);
            $productAttachment->file_id = $file->id;
            $productAttachment->product_id = $product->id;

            if (!$productAttachment->save()) {
                throw new Exception('Error saving product attachment: ' . Json::encode($productAttachment->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
            }
            $productAttachments[] = $productAttachment;
        }
        unset($product->attachments);
        return $productAttachments;
    }

    /**
     * @param Product $product
     * @param string $tagValue
     * @return ProductTag
     * @throws Exception
     */
    protected function createProductTag(Product $product, string $tagValue) : ProductTag
    {
        $tag = Yii::$app->modelFactory->getProductTag();
        $tag->product_id = $product->id;
        $tag->value = $tagValue;
        $tag->created_by = 1;

        if (!$tag->save()) {
            throw new Exception('Error saving product tag: ' . Json::encode($tag->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        return $tag;
    }

    /**
     * @param string $title
     * @return ProductCategory|\yii\db\ActiveRecord|null
     */
    protected function findCategory(string $title)
    {
        return ProductCategory::find()->alias('c')->innerJoin(['t' => ProductCategoryTranslation::tableName()], [
            'and',
            't.product_category_id = c.id',
            ['t.title' => $title]
        ])->one();
    }
}
