<?php

namespace Phycom\Console\Models\Data;


use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Shop;
use Phycom\Base\Models\Attributes\ShopStatus;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class ShopGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class ShopGenerator extends BaseGenerator
{
    /**
     * @param int $numItems
     * @param bool $generateImages
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateShops(int $numItems, bool $generateImages = true, callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, $numItems);
        for ($n = 0; $n < $numItems; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $shop = $this->createShop();

                $post = $this->createPost(['type' => PostType::SHOP, 'shop_id' => $shop->id]);
                $this->generatePostAttachmentImages($post, rand(1, 5));

                $onProgress && $onProgress($shop, $n + 1, $numItems);
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param array $params
     * @return Shop
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createShop(array $params = []) : Shop
    {
        $shop = Yii::$app->modelFactory->getShop(ArrayHelper::merge([
            'name'      => $this->pickRandomCity() . ' esindus ' . $this->generateRandomNumberFixedLength(3),
            'status'    => $this->pickRandomValue(ShopStatus::all(), [ShopStatus::DELETED]),
            'vendor_id' => Yii::$app->vendor->id
        ], $params));



        if (!$shop->save()) {
            throw new Exception('Error saving shop: ' . Json::encode($shop->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        /**
         * @var Address $address
         */
        $address = Yii::createObject(Address::class);
        $address->shop_id = $shop->id;
        $address->attributes = $this->generateAddress()->toArray();
        $address->type = AddressType::MAIN;
        $address->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);

        if (!$address->save()) {
            throw new Exception('Error saving shop address: ' . Json::encode($address->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        /**
         * @var Email $email
         */
        $email = Yii::createObject(Email::class);
        $email->shop_id = $shop->id;
        $email->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);
        $email->email = $this->faker->email;

        if (!$email->save()) {
            throw new Exception('Error saving shop email: ' . Json::encode($email->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        /**
         * @var Phone $phone
         */
        $phone = Yii::createObject(Phone::class);
        $phone->shop_id = $shop->id;
        $phone->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);
        $phone->country_code = '372';
        $phone->phone_nr = '5' . $this->generateRandomNumberFixedLength(7);

        if (!$phone->save()) {
            throw new Exception('Error saving shop phone: ' . Json::encode($phone->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        return $shop;
    }

}
