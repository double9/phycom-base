<?php

namespace Phycom\Console\Models\Data;



use Phycom\Base\Components\FileStorage;
use Phycom\Base\Models\Attributes\FileStatus;
use Phycom\Base\Models\Attributes\FileType;
use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\TranslationStatus;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\File;
use Phycom\Base\Models\Post;
use Phycom\Base\Models\PostAttachment;
use Phycom\Base\Models\Translation\PostTranslation;
use Phycom\Base\Models\User;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;

use yii\base\BaseObject;
use yii\base\Exception;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class BaseGenerator
 *
 * @package Phycom\Console\Models\Data
 */
abstract class BaseGenerator extends BaseObject
{
    use MockDataTrait;

    protected FakerGenerator $faker;

    public function init()
    {
        parent::init();
        if (YII_ENV_PROD) {
            throw new InvalidCallException('Fake data generator cannot be used in production');
        }
        $this->faker = FakerFactory::create();
    }

    /**
     * @return static|object
     * @throws \yii\base\InvalidConfigException
     */
    public static function create()
    {
        return Yii::createObject(static::class, func_get_args());
    }

    /**
     * @return User|yii\db\ActiveRecord|null
     */
    protected function getRandomUser()
    {
        /**
         * @var User $user
         */
        return Yii::$app->modelFactory->getUser()::find()
            ->where(['type' => UserType::CLIENT])
            ->orderBy('random()')
            ->one();

    }

    /**
     * @param array $allValues
     * @param array $excludeValues
     * @param array $weights
     * @return mixed
     */
    protected function pickRandomValue(array $allValues, array $excludeValues = [], array $weights = [])
    {
        $values = array_diff($allValues, $excludeValues);
        return $values[array_rand($values)];
    }

    /**
     * @param array $postParams
     * @param array $postTranslationParams
     * @return Post
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createPost(array $postParams = [], array $postTranslationParams = [])
    {
        /**
         * @var Post $post
         */
        $post = Yii::createObject(Post::class, [ArrayHelper::merge([
            'status'     => $this->pickRandomValue(PostStatus::all(), [PostStatus::DELETED]),
            'vendor_id'  => Yii::$app->vendor->id,
            'created_by' => 1
        ], $postParams)]);

        if (!$post->identifier) {
            $post->identifier = Yii::$app->security->generateRandomString(32);
        }

        if (!$post->save()) {
            throw new Exception('Error saving post: ' . Json::encode($post->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }
        /**
         * @var PostTranslation $postTranslation
         */
        $postTranslation = Yii::createObject(PostTranslation::class, [ArrayHelper::merge([
            'language'         => Yii::$app->lang->default->code,
            'title'            => $this->faker->sentence(rand(2, 3)),
            'outline'          => $this->faker->sentence(),
            'content'          => $this->faker->text(rand(500, 1000)),
            'status'           => TranslationStatus::PUBLISHED,
            'meta_description' => $this->faker->text(50)
        ], $postTranslationParams)]);

        if (!$postTranslation->meta_title) {
            $postTranslation->meta_title = $postTranslation->title;
        }
        $postTranslation->post_id = $post->id;
        $postTranslation->generateUrlKey();

        if (!$postTranslation->save()) {
            throw new Exception('Error saving post translation: ' . Json::encode($postTranslation->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }
        $postTranslation->populateRelation('post', $post);
        return $post;
    }

    /**
     * @param Post $post
     * @param int $numImages
     * @param bool $useFaker
     * @return PostAttachment[]
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function generatePostAttachmentImages(Post $post, int $numImages = 1, bool $useFaker = false) : array
    {
        $postAttachments = [];
        for ($i = 0; $i<$numImages; $i++) {
            $file = $this->createAttachmentFile($useFaker);
            /**
             * @var PostAttachment $postAttachment
             */
            $postAttachment = Yii::createObject(PostAttachment::class);
            $postAttachment->file_id = $file->id;
            $postAttachment->post_id = $post->id;

            if (!$postAttachment->save()) {
                throw new Exception('Error saving post attachment: ' . Json::encode($postAttachment->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
            }
            $postAttachments[] = $postAttachment;
        }
        unset($post->attachments);
        return $postAttachments;
    }

    /**
     * @param bool $useFaker
     * @param string|null $imageCategory
     * @return File
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createAttachmentFile(bool $useFaker = false, string $imageCategory = null)
    {
        $imagePath = $this->pickRandomImage($useFaker ? $this->faker : null, $imageCategory);
        /**
         * @var File $file
         */
        $file = Yii::createObject(File::class);
        $file->mime_type = Yii::createObject(FileType::class, [FileType::JPG]);
        $file->name = Yii::$app->security->generateRandomString(32);
        $file->filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.jpg';
        $file->bucket = FileStorage::BUCKET_CONTENT_FILES;
        $file->status = FileStatus::PROCESSING;

        if (!$file->save()) {
            throw new Exception('Error saving product attachment file: ' . Json::encode($file->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        $file->filename = $file->id . '_' . $file->filename;
        $file->status = FileStatus::VISIBLE;

        if (!$file->save()) {
            throw new Exception('Error saving product attachment file: ' . Json::encode($file->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        $bucket = Yii::$app->fileStorage->getBucket($file->bucket);
        $bucket->copyFileIn($imagePath, $file->filename);

        try {
            $file->generateThumbs();
        } catch (\Exception $e) {
            Yii::error('Error creating thumbnails: ' . $e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine(), __METHOD__);
            Yii::error($e->getTraceAsString(), __METHOD__);
        }

        return $file;
    }
}
