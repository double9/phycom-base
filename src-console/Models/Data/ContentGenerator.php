<?php

namespace Phycom\Console\Models\Data;


use Phycom\Base\Models\Attributes\CategoryStatus;
use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Attributes\TranslationStatus;
use Phycom\Base\Models\User;
use Phycom\Base\Models\Post;
use Phycom\Base\Models\PostCategory;
use Phycom\Base\Models\PostCategoryPostRelation;
use Phycom\Base\Models\PostTag;
use Phycom\Base\Models\Translation\PostCategoryTranslation;


use yii\base\InvalidConfigException;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use Yii;

/**
 * Class ContentGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class ContentGenerator extends BaseGenerator
{
    protected array $authors = [];

    /**
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    public function init()
    {
        parent::init();
        $this->authors = $this->createContentAuthors();
    }

    /**
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateDefaultWebPages(callable $onProgress = null)
    {
        $pageSpaceItems = Yii::$app->pages->getItems();
        $count = count($pageSpaceItems);
        $onProgress && $onProgress(null, 0, $count);

        for ($n = 0; $n < $count; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $pageSpace = $pageSpaceItems[$n];

                if (!$page = Post::find()->where(['type' => PostType::PAGE, 'identifier' => $pageSpace->identifier])->one()) {
                    $page = $this->createPost([
                        'identifier' => $pageSpace->identifier,
                        'type'       => PostType::PAGE,
                        'status'     => $this->pickRandomValue(PostStatus::all(), [PostStatus::DELETED])
                    ], ['title' => $pageSpace->getLabel()]);
                }
                $onProgress && $onProgress($page, $n + 1, $count);

                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateLandingPages(callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, 3);
        $transaction = Yii::$app->db->beginTransaction();
        try {

            if (!$landingPage1 = Post::find()->where(['type' => PostType::LAND, 'identifier' => 'landing_1'])->one()) {
                $landingPage1 = $this->createPost([
                    'identifier' => 'landing_1',
                    'type'       => PostType::LAND,
                    'status'     => PostStatus::PUBLISHED
                ]);
                $this->generatePostAttachmentImages($landingPage1, 2);
            }
            $onProgress && $onProgress($landingPage1, 1, 3);
            if (!$landingPage2 = Post::find()->where(['type' => PostType::LAND, 'identifier' => 'landing_2'])->one()) {
                $landingPage2 = $this->createPost([
                    'identifier' => 'landing_2',
                    'type'       => PostType::LAND,
                    'status'     => PostStatus::HIDDEN
                ]);
                $this->generatePostAttachmentImages($landingPage2, 3);
            }
            $onProgress && $onProgress($landingPage2, 2, 3);
            if (!$landingPage3 = Post::find()->where(['type' => PostType::LAND, 'identifier' => 'landing_3'])->one()) {
                $landingPage3 = $this->createPost([
                    'identifier' => 'landing_3',
                    'type'       => PostType::LAND,
                    'status'     => PostStatus::DRAFT
                ]);
            }
            $onProgress && $onProgress($landingPage3, 3, 3);
            $transaction->commit();

        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generatePostCategories(callable $onProgress = null)
    {
        $count = 0;
        foreach ($this->postCategories as $parent => $subCategories) {
            $count++;
            $count += count($subCategories);
        }
        $onProgress && $onProgress(null, 0, $count);
        $i = 0;
        foreach ($this->postCategories as $parentCategoryTitle => $subCategories) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (!$category = $this->findPostCategory($parentCategoryTitle)) {
                    $category = $this->createPostCategory(null, ['title' => $parentCategoryTitle], CategoryStatus::VISIBLE);
                }
                $onProgress && $onProgress($category, ++$i, $count);

                foreach ($subCategories as $key => $subCategoryTitle) {
                    $status = ($key % 2 == 0) ? CategoryStatus::VISIBLE : null;
                    if (!$subCategory = $this->findPostCategory($subCategoryTitle)) {
                        $subCategory = $this->createPostCategory($category->id, ['title' => $subCategoryTitle], $status);
                    }
                    $onProgress && $onProgress($subCategory, ++$i, $count);
                }
                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param null $parentCategoryId
     * @param array $params
     * @param null $status
     * @return PostCategory
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function createPostCategory($parentCategoryId = null, array $params = [], $status = null)
    {
        /**
         * @var PostCategory $category
         */
        $category = Yii::createObject(PostCategory::class);
        $category->status = $status ?: $this->pickRandomValue(CategoryStatus::all(), [CategoryStatus::DELETED]);
        $category->parent_id = $parentCategoryId;

        if (!$category->save()) {
            throw new Exception('Error saving post category: ' . Json::encode($category->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        $mergedParams = ArrayHelper::merge([
            'language'         => Yii::$app->lang->default->code,
            'title'            => $this->faker->sentence(rand(3, 4)),
            'description'      => $this->faker->text(rand(200, 500)),
            'meta_description' => $this->faker->text(50),
            'status'           => TranslationStatus::PUBLISHED
        ], $params);

        /**
         * @var PostCategoryTranslation $categoryTranslation
         */
        $categoryTranslation = Yii::createObject(PostCategoryTranslation::class, [$mergedParams]);
        $categoryTranslation->language = Yii::$app->lang->default->code;
        $categoryTranslation->post_category_id = $category->id;
        $categoryTranslation->meta_title = $categoryTranslation->title;
        $categoryTranslation->generateUrlKey();

        if (!$categoryTranslation->save()) {
            throw new Exception('Error saving post category translation: ' . Json::encode($categoryTranslation->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }

        return $category;
    }

    /**
     * @param int $numPosts
     * @param callable|null $onProgress
     * @throws \Throwable
     */
    public function generateBlogPosts(int $numPosts, callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, $numPosts);
        $categories = ArrayHelper::getColumn(PostCategory::find()->all(), 'id');

        for ($n = 0; $n < $numPosts; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $post = $this->createPost(['type' => PostType::POST]);

                if (!empty($categories)) {
                    $categoryId = $categories[array_rand($categories)];
                    /**
                     * @var PostCategoryPostRelation $postInCategory
                     */
                    $postInCategory = Yii::createObject(PostCategoryPostRelation::class);
                    $postInCategory->post_id = $post->id;
                    $postInCategory->category_id = $categoryId;
                    $postInCategory->created_by = 1;

                    if (!$postInCategory->save()) {
                        throw new Exception('Error saving post category relation: ' . Json::encode($postInCategory->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                    }
                }

                if ($n % 4 == 0) { // assign tags for every 4th item
                    $this->createTag($post, $this->faker->word());
                }
                if ($n % 5 == 0) { // assign tags for every 5th item
                    $this->createTag($post, $this->faker->word());
                }
                if ($n % 8 == 0) { // assign tags for every 8th item
                    $this->createTag($post, $this->faker->word() . ' ' . strtolower($this->faker->word()));
                }

                $this->generatePostAttachmentImages($post, rand(1, 3));

                $onProgress && $onProgress(null, $n + 1, $numPosts);
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @param array $postParams
     * @param array $postTranslationParams
     * @return Post
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createPost(array $postParams = [], array $postTranslationParams = [])
    {
        if (isset($postParams['created_by'])) {
            $postParams['created_by'] = $this->pickRandomValue($this->authors)->id;
        }
        return parent::createPost($postParams, $postTranslationParams);
    }


    /**
     * @param Post $post
     * @param string $tagValue
     * @return PostTag
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function createTag(Post $post, string $tagValue) : PostTag
    {
        /**
         * @var PostTag $tag
         */
        $tag = Yii::createObject(PostTag::class);
        $tag->post_id = $post->id;
        $tag->value = $tagValue;
        $tag->created_by = 1;

        if (!$tag->save()) {
            throw new Exception('Error saving post tag: ' . Json::encode($tag->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }
        return $tag;
    }

    /**
     * @param string $title
     * @return PostCategory|\yii\db\ActiveRecord|null
     */
    protected function findPostCategory(string $title) : ?PostCategory
    {
        return PostCategory::find()->alias('c')->innerJoin(['t' => PostCategoryTranslation::tableName()], [
            'and',
            't.post_category_id = c.id',
            ['t.title' => $title]
        ])->one();
    }

    /**
     * @return User[]
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    protected function createContentAuthors() : array
    {
        $users = [];
        /**
         * @var UserGenerator $userGenerator
         */
        $userGenerator = Yii::createObject(UserGenerator::class);
        for ($i = 0; $i < 3; $i++) {
            $users[] = $userGenerator->generateBackendUser();
        }
        return $users;
    }
}
