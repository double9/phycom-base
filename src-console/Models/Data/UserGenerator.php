<?php

namespace Phycom\Console\Models\Data;

use Faker\Factory;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\AddressType;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\SubscriptionStatus;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Subscription;
use Phycom\Base\Models\User;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use Yii;

/**
 * Class UserGenerator
 *
 * @package Phycom\Console\Models\Data
 */
class UserGenerator extends BaseGenerator
{
    public string $defaultPassword = 'parool';

    /**
     * Generates users for testing
     *
     * @param int $numUsers - number of users to generate
     * @param callable $onProgress
     * @throws \Throwable
     */
    public function generateUsers(int $numUsers, callable $onProgress = null)
    {
        $onProgress && $onProgress(null, 0, $numUsers);

        for ($n = 0; $n < $numUsers; $n++) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $fName = $this->generateFirstName();
                $lName = $this->generateLastName();
                $user = $this->createUser([
                    'first_name' => $fName,
                    'last_name'  => $lName
                ]);

                /**
                 * @var Email $email
                 */
                $email = Yii::createObject(Email::class);
                $email->user_id = $user->id;
                $email->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);
                $email->email = Inflector::slug(strtolower($fName) . '.' . strtolower($lName) . '_' . $this->generateRandomNumberFixedLength(2)) . '@gmail.com';

                if (!$email->save()) {
                    throw new Exception('Error saving user email: ' . Json::encode($email->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                /**
                 * @var Phone $phone
                 */
                $phone = Yii::createObject(Phone::class);
                $phone->user_id = $user->id;
                $phone->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);
                $phone->country_code = '372';
                $phone->phone_nr = '5' . $this->generateRandomNumberFixedLength(7);

                if (!$phone->save()) {
                    throw new Exception('Error saving user phone: ' . Json::encode($phone->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                /**
                 * @var Address $address
                 */
                $address = Yii::createObject(Address::class);
                $address->attributes = $this->generateAddress()->attributes;
                $address->user_id = $user->id;
                $address->type = AddressType::MAIN;
                $address->status = $this->pickRandomValue(ContactAttributeStatus::all(), [ContactAttributeStatus::DELETED]);

                if (!$address->save()) {
                    throw new Exception('Error saving user address: ' . Json::encode($address->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                }

                if ($n % 5 == 0) {
                    /**
                     * @var Subscription $subscription
                     */
                    $subscription = Yii::createObject(Subscription::class);
                    $subscription->first_name = $user->first_name;
                    $subscription->last_name = $user->last_name;
                    $subscription->email = $email->email;
                    $subscription->status = $this->pickRandomValue(array_merge(
                        [
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE,
                            SubscriptionStatus::ACTIVE
                        ],
                        SubscriptionStatus::all()
                    ));
                    if (!$subscription->save()) {
                        throw new Exception('Error saving subscription: ' . Json::encode($subscription->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
                    }
                }

                $onProgress && $onProgress($user, $n + 1, $numUsers);

                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    /**
     * @return User
     * @throws \Throwable
     */
    public function generateBackendUser()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = $this->createUser([
                'first_name' => $this->generateFirstName(),
                'last_name'  => $this->generateLastName(),
                'type'       => UserType::ADMIN,
                'username'   => 'admin_' . $this->faker->ean13
            ]);
            $transaction->commit();
            return $user;

        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array $params
     * @return User
     * @throws Exception
     */
    protected function createUser(array $params)
    {
        $user = Yii::$app->modelFactory->getUser(ArrayHelper::merge([
            'password' => $this->defaultPassword,
            'type'     => UserType::CLIENT,
            'status'   => UserStatus::ACTIVE
        ], $params));

        $user->generateAuthKey();

        if (!$user->save()) {
            throw new Exception('Error saving user: ' . Json::encode($user->errors, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
        }
        return $user;
    }
}
